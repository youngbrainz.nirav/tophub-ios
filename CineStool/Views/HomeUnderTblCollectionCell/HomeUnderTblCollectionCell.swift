//
//  HomeUnderTblCollectionCell.swift
//  CineStool
//
//  Created by YB on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class HomeUnderTblCollectionCell: UICollectionViewCell, ReusableView, NibLoadableView {
    
    @IBOutlet weak var imgMoviePoster: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
