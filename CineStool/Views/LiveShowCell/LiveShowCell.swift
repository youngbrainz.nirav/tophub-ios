//
//  LiveShowCell.swift
//  CineStool
//
//  Created by hiren  mistry on 15/12/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit

class LiveShowCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vw_main: UIView!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var btnJoinEvent: UIButton!
    
    @IBOutlet weak var lblEntranceFee: UILabel!
    @IBOutlet weak var lblEndtime: UILabel!
    @IBOutlet weak var lblAgenda: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnJoinEvent.layer.cornerRadius = 19.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
