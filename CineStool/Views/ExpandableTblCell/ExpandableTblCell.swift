//
//  ExpandableTblCell.swift
//  CineStool
//
//  Created by YB on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class ExpandableTblCell: UITableViewCell, ReusableView, NibLoadableView {

    @IBOutlet weak var btnExpandCell: UIButton!
    @IBOutlet weak var tblExpanded: UITableView!
    @IBOutlet weak var lblheaderTitle: UILabel!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var imgDropDown: UIImageView!
    
    var viewcontroller = UIViewController()
    var blockBtnExpandAction : ((String)->())!
    var blockSubMenuAction : ((SubCategories)->())!
    var title: String!
    var arrSubMenu : [SubCategories] = [] {
        didSet{
            tblExpanded.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tblExpanded.tableFooterView = UIView()
        tblExpanded.register(ExpandedTblCell.self)
    }
    
    @IBAction func btnExpandCellAction(_ sender: UIButton) {
        blockBtnExpandAction((sender.titleLabel?.text)!)
    }
}

extension ExpandableTblCell : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSubMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ExpandedTblCell = tableView.dequeueReusableCellTb(for: indexPath)
        cell.lblSubTitle.text = "\(arrSubMenu[indexPath.row].name)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        title = "\(arrSubMenu[indexPath.row].name)"
        blockSubMenuAction(arrSubMenu[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
