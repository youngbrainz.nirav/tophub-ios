//
//  HomeTblCell.swift
//  CineStool
//
//  Created by YB on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import SDWebImage

class HomeTblCell: UITableViewCell, ReusableView, NibLoadableView {

    @IBOutlet weak var collectionHomeUnderTbl: UICollectionView!
    @IBOutlet weak var HeightcollectionHomeUnderTbl: NSLayoutConstraint!
    @IBOutlet weak var lblCategory: UILabel!
    
    var arrDashboardData: [Movies] = []
    
    var indexPath = IndexPath()
    var blockDidTapped: ((Int)->())!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.HeightcollectionHomeUnderTbl.constant = 20
        self.collectionHomeUnderTbl.register(HomeUnderTblCollectionCell.self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension HomeTblCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return arrDashboardData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: HomeUnderTblCollectionCell = collectionView.dequeueReusableCell(for: indexPath)
        var imgURL = ""
        imgURL = arrDashboardData[indexPath.item].poster_image
        cell.imgMoviePoster.sd_setImage(with: URL(string: imgURL), placeholderImage:nil)
        cell.lblName.text =  arrDashboardData[indexPath.item].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionHomeUnderTbl.frame.size.width - 20) / 3
        let height = width + 70//collectionHomeUnderTbl.frame.size.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        blockDidTapped(indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
