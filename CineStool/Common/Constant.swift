//
//  Constant.swift
//  SMA
//
//  Created by YB on 01/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import Foundation
import UIKit

//MARK:- Email Validation String
let Email_RegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

var appDel = UIApplication.shared.delegate as! AppDelegate

//let baseURL = "https://cinestool.com/api/"
//let baseURL = "http://18.188.175.234/api/"
//let baseURL = "http://3.22.95.237/api/"
//let baseURL = "http://3.15.24.56/api/"

let baseURL = "https://tophubonline.com/api/"

let loginRegister = baseURL + "loginRegister"
let updateProfile = baseURL + "updateProfile"
let getProfile = baseURL + "getProfile"
let getMovieCategory = baseURL + "getMovieCategory"
let getPages = baseURL + "getPages"
let changeUserStatus = baseURL + "changeUserStatus"
let getMovieList = baseURL + "getMovieList"
let getMovieDetail = baseURL + "getMovieDetail"
let giveMovieRating = baseURL + "giveMovieRating"
let getDashboardData = baseURL + "getDashboardData"
let getRecentData = baseURL + "getRecentMoviesList"
let addMovieToRecent = baseURL + "addMovieToRecent"



let getAdvertisement = baseURL + "getAdvertisement"
let makePayment = baseURL + "makePayment"
let getMyCollection = baseURL + "getMyCollection"
let logOut = baseURL + "logOut"
let addUserCard = baseURL + "addUserCard"
let getUserCard = baseURL + "getUserCard"
let getPaymentURL = baseURL + "getPaymentURL"
let addMovieViewCount = baseURL + "addMovieViewCount"
let getNotificationList = baseURL + "getNotificationList"
let deleteNotification = baseURL + "deleteNotification"
let makeSubscribeChannel = baseURL + "makeSubscribeChannel"
let getCustomerInstruction = baseURL + "getCustomerInstruction"
let makeDefaultUserCard = baseURL + "makeDefaultUserCard"
let getSubscriptionList = baseURL + "getSubscriptionList"
let deleteUserCard = baseURL + "deleteUserCard"
let SubscribedUserPayment = baseURL + "SubscribedUserPayment"
let SubscribedUserPaymentList = baseURL + "SubscribedUserPaymentList"

let forgotMobileNo = baseURL + "forgotMobileNo"
let changeMobileNo = baseURL + "changeMobileNo"

let changeSubscribedPlan = baseURL + "changeSubscribedPlan"
let generateOTP = baseURL + "generateOTP"
let sendEmailOTP = baseURL + "sendEmailOTP"

//NEW CHANGES API
let loginRegisterV2 = baseURL + "loginRegisterV2"
let getMovieCategoryV2 = baseURL + "getMovieCategoryV2"
let forgotPassword = baseURL + "forgotPassword"
let changePassword = baseURL + "changePassword"

let addToMyCollection = baseURL + "addToMyCollection"

let addeditEventData = baseURL + "createEvent"
let updateEventData = baseURL + "updateEvent"

let fetchPayemntData = baseURL + "fetchPayemntData"
let makeGoLivePayment = baseURL + "makeGoLivePayment"
let endGoLiveEvent = baseURL + "endGoLiveEvent"
let getGoLiveEventList = baseURL + "getGoLiveEventList"
let cancellGoLiveEvent = baseURL + "cancellGoLiveEvent"
let cancellEvent = baseURL + "cancelEvent"

let GetLiveShowList = baseURL + "eventList"
let getRunningEvent = baseURL + "getRunningEvent"


let addEventMessages = baseURL + "addEventMessages"
//required : [loginuser_id, session_token, user_type, event_id, event_id, comment]

let getEventMessagesList = baseURL + "getEventMessagesList"
//required : [loginuser_id, session_token, user_type, event_id]

let checkPromoCode = baseURL + "checkPromoCode"
let startGoLiveEvent = baseURL + "startGoLiveEvent"
let getEventDetails = baseURL + "getEventDetails"

let AgoraToken = baseURL + "AgoraToken"
let DeactivateAccount = baseURL + "deactivateAccount"
let savePartyInfo = baseURL + "savePartyInfo"
let changeMoviePartyStatus = baseURL + "changeMoviePartyStatus"
