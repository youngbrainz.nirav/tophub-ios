//
//  Extension.swift
//  SMA
//
//  Created by MacBook Air 002 on 27/06/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class Extension: NSObject {
    
}
extension Date {

  func isEqualTo(_ date: Date) -> Bool {
    return self == date
  }
  
  func isGreaterThan(_ date: Date) -> Bool {
     return self > date
  }
  
  func isSmallerThan(_ date: Date) -> Bool {
     return self < date
  }
}

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth2: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor2: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}


//MARK:- Activity Indicator
extension UIViewController: NVActivityIndicatorViewable{
    func startAnimatingLoader(message: String)  {
        let size = CGSize(width: 40, height: 40)
        
        startAnimating(size, message: message, type: NVActivityIndicatorType.squareSpin, fadeInAnimation: nil)
    }
    
    func stopAnimatingLoader() {
        stopAnimating()
    }
}
extension UIDevice {
    var isSimulator: Bool {
#if targetEnvironment(simulator)
            return true
        #else
            return false
        #endif
    }
}

//MARK:- Locale
extension Locale {
    static let currency: [String: (code: String?, symbol: String?)] = Locale.isoRegionCodes.reduce(into: [:]) {
        let locale = Locale(identifier: Locale.identifier(fromComponents: [NSLocale.Key.countryCode.rawValue: $1]))
        $0[$1] = (locale.currencyCode, locale.currencySymbol)
    }
    
    static let currencies = Dictionary(uniqueKeysWithValues: Locale.isoRegionCodes.map {
        region -> (String, (code: String, symbol: String, locale: Locale)) in
        let locale = Locale(identifier: Locale.identifier(fromComponents: [NSLocale.Key.countryCode.rawValue: region]))
        return (region, (locale.currencyCode ?? "", locale.currencySymbol ?? "", locale))
    })
}

//MARK:- Dictionary
extension Dictionary {
    func nullKeyRemoval() -> Dictionary {
        var dict = self
        
        let keysToRemove = Array(dict.keys).filter { dict[$0] is NSNull }
        for key in keysToRemove {
            dict.removeValue(forKey: key)
        }
        
        return dict
    }
}

//MARK:- String
extension String {
    func height(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        return label.frame.height
    }
    func convertToDictionary() -> [String: Any]? {
        if let data = data(using: .utf8) {
            return try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        }
        return nil
    }
}

//MARK:- Date
extension Date{
    
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }

    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }

    func toString(fromFormat: String, toFormat: String, timeZone: String = "", isAr:Bool = false) -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = fromFormat
        let myString = formatter.string(from: self) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = toFormat
        // again convert your date to string
        
//        if isAr == true{
//            if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
//                formatter.locale = Locale(identifier: "ar")
//            }else{
//                formatter.locale = Locale(identifier: "en")
//            }
//        }else{
//
//        }
        
        formatter.locale = Locale(identifier: "en")
        
        if timeZone != ""{
            formatter.timeZone = TimeZone (abbreviation: timeZone)
        }
        return formatter.string(from: yourDate!)
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }

    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }

    func getStartDate(toFormat: String) -> String{
        let calendar = NSCalendar.current
        let dateAtMidnight = calendar.startOfDay(for: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: dateAtMidnight)
    }
    
    func getEndDate(toFormat: String) -> String{
        var components = DateComponents()
        components.day = 1
        components.second = -1
        let calendar = NSCalendar.current
        let dateAtMidnight = calendar.startOfDay(for: self)
        let dateAtEnd = calendar.date(byAdding: components, to: dateAtMidnight)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: dateAtEnd!)
    }
    
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))year"   }
        if months(from: date)  > 0 { return "\(months(from: date))Month"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))Day"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))Hour"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))Minute" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))Second" }
        return ""
    }
}

struct dateFormatter {
    static let yyyy_MM_dd_HH_mm_ss_ZZZ = "yyyy-MM-dd HH:mm:ss ZZZ" //2009-10-09 08:39:20 +0000
    static let yyyy = "yyyy" //2009
    static let EEE_MMMM_dd = "EEE, MMMM dd" // Fri, January 17
    static let MMM_dd_yyyy = "MMM dd,yyyy" // Jan 17,2020
    static let MMM_dd_yyyy_hh_mm_a = "MMM dd,yyyy - hh:mm a" // Jan 17,2020 - 10:00AM
    static let yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss" //2020-01-21 00:00:00
    static let yyyy_MM_dd_HH_MM_ss = "yyyy-MM-dd HH:MM:ss" //2020-01-21 00:00:00
    static let yyyy_MM_dd = "yyyy-MM-dd" //2020-01-21
    static let yyyy_Coma_MM_dd = "yyyy, MMM dd" //2020-01-21
    static let yyyy_MM_ddWithSlash = "yyyy/MM/dd" //2020/01/21
    static let yyyy_MM_dd_T_HH_mm_ssXXX = "yyyy-MM-dd'T'HH:mm:ssXXX"//1999-03-22T05:06:07+01:00
    static let yyyy_MM_dd_T_HH_mm_ss_SSSXXX = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX" //1999-03-22T05:06:07.000+01:00
    static let yyyy_MM_dd_T_HH_mm_ss = "yyyy-MM-dd'T'HH:mm:ss"
    static let MM_dd_yyyy_hh_mm_ss_a = "MM/dd/yyyy hh:mm:ss a" //01/21/2020 09:20:22 AM
    static let MM_dd_yyyy_With_Slash = "MM/dd/yyyy" //01/21/2020
    static let MM = "MM" //01
    static let dd_MMM_yyyy = "dd MMM yyyy" //31 jan 2020
    static let dd_MMM_yyyy_hh_mm_a = "dd MMM yyyy hh:mm a" //31 jan 2020 06:40 PM
    static let dd_MM_yyyy_hh_mm = "dd-MM-yyyy hh:mm" //31 jan 2020
    
    static let MMM_Coma_dd_yyyy_hh_mm_a = "MMM, dd yyyy hh:mm a"
    static let MMM_NoComa_dd_yyyy_hh_mm_a = "MMM dd yyyy hh:mm a"
    
    static let dd_MM_yyyy = "dd-MM-yyyy"
}
extension UIColor {
    static let themeColor = UIColor.init(named: "appColorRed") ?? UIColor.red
}
