//
//  PrivacyPolicyVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 11/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class PrivacyPolicyVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var txtviewData: UITextView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    var commonFile: CommonFileType = .none
    var pageType = String()
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if commonFile == .PrivacyPolicy{
            self.lblHeaderTitle.text = "PRIVACY POLICY"
            pageType = "5"
        }else if commonFile == .TermsAndCondition{
            self.lblHeaderTitle.text = "TERMS & CONDITION"
            pageType = "4"
        }else{
            self.lblHeaderTitle.text = "About Us"
            pageType = "1"
        }
        getContentAPI()
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnBackTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PrivacyPolicyVC
{
    func getContentAPI()
    {
        let params = ["page_type":"\(self.pageType)"]
        WebService.Request.patch(url: getPages, type: .post, parameter: params, callSilently: false, header: nil) { (response, error) in
            if error == nil {
                print(response!)
                if response!["status"] as? Bool == true
                {
                    let dataresponse = response!["data"] as? [String:Any]
                    if dataresponse != nil
                    {
                        let htmlText = dataresponse?["description"] as! String
                        //self.txtviewData.attributedText = htmlText.htmlAttributed(family: "Cabin Medium", size: 100.0)
                        self.txtviewData.attributedText = htmlText.htmlAttributed(family: "Poppins-SemiBold", size: 15.0)
                        self.txtviewData.textColor = .white
                    }
                    else
                    {
                        
                    }
                } else
                {
                    
                }
            }
        }
    }
}
