//
//  GoLiveVC.swift
//  CineStool
//
//  Created by Dharam YB on 19/05/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import UITextView_Placeholder   
import UIKit
import AgoraRtcKit
import Alamofire
import IQKeyboardManagerSwift
import FirebaseDynamicLinks

enum isLiveFrom {
    case broadcast
    case audience
    case none
}

class GoLiveVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var broadcastersView: AGEVideoContainer!
    @IBOutlet weak var placeholderView: UIImageView!
    
    @IBOutlet weak var ShareButton: UIButton!
    @IBOutlet weak var videoMuteButton: UIButton!
    @IBOutlet weak var cameraChangeButton: UIButton!
    @IBOutlet weak var beautyEffectButton: UIButton!
    @IBOutlet weak var audioMuteButton: UIButton!
    @IBOutlet weak var CloseGoLiveButton: UIButton!
    
    @IBOutlet weak var tblChat:UITableView!
    @IBOutlet weak var btnSend:UIButton!
    @IBOutlet weak var viewBottom:UIView!
    @IBOutlet weak var txtMessage:UITextView!
    @IBOutlet weak var txtviewHeight:NSLayoutConstraint!
    @IBOutlet weak var viewBottomHeight:NSLayoutConstraint!
    @IBOutlet weak var viewBottomBottom:NSLayoutConstraint!
    
    @IBOutlet weak var viewProfile:UIView!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblEventName: UILabel!
    
    @IBOutlet weak var lblTotCount: UILabel!
    @IBOutlet weak var imgEye: UIImageView!
    
    @IBOutlet weak var viewRight: UIView!
    
    var isFrom:isLiveFrom = .none
    var arrEventData: EventData?
    var arrMessageData: [MessageModel] = []

    var videoProfile: CGSize!
    var metadata: Data?
    let MAX_META_LENGTH = 1024

    var timerForMessage = Timer()
    
    let domain: String = "https://tophub.page.link"
    var arrUserCount = [Any]()
    var tempUserCount = 0
    var isKeyboardOpen = false
    var msgCount = Int()
    var dynamicToekn = "00683ba20505503430a9b583c5a159766b3IAACuoqe2J/LwpZ+eTcSR1ZMU4HApeZrekLviQTGvFMSc55wcHYAAAAAEACsrxsmfMV5YAEAAQB8xXlg"
    var is_end_event = "0"
    
    private lazy var agoraKit: AgoraRtcEngineKit = {
        let engine = AgoraRtcEngineKit.sharedEngine(withAppId: KeyCenter.AppId, delegate: self)
        engine.setLogFilter(AgoraLogFilter.info.rawValue)
        engine.setLogFile(FileCenter.logFilePath())
        return engine
    }()

    private var settings = Settings()

    private var isSwitchCamera = false {
        didSet {
            agoraKit.switchCamera()
        }
    }
    
    private var isMutedVideo = false {
        didSet {
            // mute local video
            placeholderView.isHidden = !isMutedVideo
            agoraKit.muteLocalVideoStream(isMutedVideo)
            videoMuteButton.isSelected = isMutedVideo
        }
    }
    
    private var isMutedAudio = false {
        didSet {
            // mute local audio
            agoraKit.muteLocalAudioStream(isMutedAudio)
            audioMuteButton.isSelected = isMutedAudio
        }
    }
    private var isBeautyOn = false {
        didSet {
            // improve local render view
            agoraKit.setBeautyEffectOptions(isBeautyOn,
                                            options: isBeautyOn ? beautyOptions : nil)
            beautyEffectButton.isSelected = isBeautyOn
        }
    }
    
    private let beautyOptions: AgoraBeautyOptions = {
        let options = AgoraBeautyOptions()
        options.lighteningContrastLevel = .normal
        options.lighteningLevel = 0.7
        options.smoothnessLevel = 0.5
        options.rednessLevel = 0.1
        return options
    }()

    private var videoSessions = [VideoSession]() {
        didSet {
            placeholderView.isHidden = (videoSessions.count == 0 ? false : true)
            // update render view layout
            updateBroadcastersView()
        }
    }
    private let maxVideoSession = 4

    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblChat.register(CellMessage.self)
        
        self.btnSend.clipsToBounds = true
        self.btnSend.layer.cornerRadius = self.btnSend.frame.width / 2
        self.btnSend.layer.shadowColor = UIColor.lightGray.cgColor
        self.btnSend.layer.shadowOpacity = 0.5
        self.btnSend.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
        let img = #imageLiteral(resourceName: "eye").withRenderingMode(.alwaysTemplate)
        self.imgEye.image = img
        self.imgEye.tintColor = .white
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            view.endEditing(true)
        }
        sender.cancelsTouchesInView = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.isFrom == .broadcast{
            settings.role = .broadcaster
            appDel.eventLiveId = appDel.arrUserData?._id ?? ""
            let event = removeSpecialCharsFromString(text: appDel.arrUserData?.event_name ?? "")
            self.lblEventName.text = appDel.arrUserData?.event_name
            settings.roomName = event.replacingOccurrences(of: " ", with: "")
            let url = URL(string: appDel.arrUserData?.profile_image ?? "")
            self.imgUserProfile.sd_setImage(with: url, placeholderImage: nil)
        }else{
//            settings.role = .audience
            settings.role = .broadcaster
            appDel.eventLiveId = self.arrEventData?._id ?? ""
            let event = removeSpecialCharsFromString(text: self.arrEventData?.event_name ?? "")
            self.lblEventName.text = self.arrEventData?.event_name
            settings.roomName = event.replacingOccurrences(of: " ", with: "")
            let url = URL(string: self.arrEventData?.profile_image ?? "")
            self.imgUserProfile.sd_setImage(with: url, placeholderImage: nil)
        }
        appDel.isMessageListAPICall = true
        self.timerForMessage = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(APICallGetMessage), userInfo: nil, repeats: true)
        self.apiCallRecieveMessage()
        updateButtonsVisiablity()
        loadAgoraKit()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        appDel.isMessageListAPICall = false
    }
    
    @objc func APICallGetMessage() {
        if appDel.isMessageListAPICall{
            DispatchQueue.main.async {
                self.apiCallRecieveMessage()
            }
        }else{
            self.timerForMessage.invalidate()
        }
        
        if is_end_event == "1"{
            findtopViewController()?.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    //MARK:- Button Tapped Events
    @IBAction func doSharePressed(_ sender: UIButton) {
        //https://cinestool.com/api
        let shareLink:String = "\(baseURL)?event_id=\(appDel.eventLiveId)"
        guard let newSharelink = URL(string: shareLink) else { return }
        let components = DynamicLinkComponents.init(link: newSharelink, domainURIPrefix: self.domain)
        
        let iOSParams = DynamicLinkIOSParameters(bundleID: Params.bundleID.rawValue)
        iOSParams.minimumAppVersion = Params.minimumAppVersion.rawValue
        iOSParams.appStoreID = Params.appStoreID.rawValue
        components?.iOSParameters = iOSParams
        
        let androidParams = DynamicLinkAndroidParameters(packageName: Params.packageName.rawValue)
        androidParams.minimumVersion = Int(Params.minimumVersion.rawValue)!
        components?.androidParameters = androidParams
        
        let options = DynamicLinkComponentsOptions()
        //options.pathLength = .short
        options.pathLength = .unguessable
        components?.options = options

        guard let longDynamicLink = components?.url else { return }
        print("The long URL is: \(longDynamicLink)")
        
        //        self.longLink = components?.url
        //        print("Dharam",longLink as Any)
        
        components?.shorten { (shortURL, warnings, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            let shortLink = shortURL
            print(shortLink!)
            self.presentShareActivity(shortLink!)
        }
        //self.presentShareActivity(longDynamicLink)
    }
    
    private func presentShareActivity(_ url: URL?) {
        let promotText = self.lblEventName.text ?? ""
        let activityController = UIActivityViewController(activityItems: [promotText, url], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        activityController.popoverPresentationController?.sourceRect = self.view.frame
        self.present(activityController, animated: true, completion: nil)
    }
    
    @IBAction func doMuteVideoPressed(_ sender: UIButton) {
        isMutedVideo.toggle()
    }
    
    @IBAction func doSwitchCameraPressed(_ sender: UIButton) {
        isSwitchCamera.toggle()
    }

    @IBAction func doBeautyPressed(_ sender: UIButton) {
        isBeautyOn.toggle()
    }

    @IBAction func doMuteAudioPressed(_ sender: UIButton) {
        isMutedAudio.toggle()
    }

    @IBAction func doLeavePressed(_ sender: UIButton) {
        leaveChannel()
    }
    
    @IBAction func btnsend(_ sender:UIButton){
        self.view.endEditing(true)
        if self.txtMessage.text == "" {
            self.txtMessage.placeholder = "Reply..."
        }else{
            self.apiCallSendMessage()
            self.txtMessage.placeholder = "Reply..."
            self.txtMessage.text = ""
        }
    }
}

extension GoLiveVC:UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //let numLines = (textView.contentSize.height / textView.font!.lineHeight)
        //print("lines->",numLines)
        guard text.rangeOfCharacter(from: CharacterSet.newlines) == nil else {
            textView.resignFirstResponder() // uncomment this to close the keyboard when return key is pressed
            return false
        }
        
//        if self.txtMessage.contentSize.height < 100{
//            self.txtviewHeight.constant = self.txtMessage.contentSize.height
//            self.viewBottomHeight.constant = self.txtviewHeight.constant + 10
//        }
        return true
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.isKeyboardOpen = true
        
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame: NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        
        let newHeight: CGFloat
        if #available(iOS 11.0, *) {
            newHeight = keyboardRectangle.height - view.safeAreaInsets.bottom
        } else {
            newHeight = keyboardRectangle.height
        }
        
        var contentInsets:UIEdgeInsets
        contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: newHeight, right: 0.0);
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.viewBottomBottom.constant = newHeight// - (self.txtviewHeight.constant + 10)
        }, completion: { finished in
        })
        self.tblChat.contentInset = contentInsets
        
        if self.arrMessageData.count > 0{
            self.tblChat.scrollToRow(at: IndexPath (item: self.arrMessageData.count - 1, section: 0), at: .top, animated: true)
        }else{
            //self.tblChat.scrollToRow(at: IndexPath (item: 0, section: 0), at: .top, animated: true)
        }
        
        self.tblChat.scrollIndicatorInsets = self.tblChat.contentInset
        self.view.layoutIfNeeded()
    }

    @objc func keyboardWillHide(notification: NSNotification){
        self.isKeyboardOpen = false
        
        var contentInsets:UIEdgeInsets
        contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0, right: 0.0);
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.viewBottomBottom.constant = 0
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
                self.viewBottomBottom.constant = 0
                self.txtviewHeight.constant = 40
                self.viewBottomHeight.constant = 60
                if self.txtMessage.text == "" {
                    self.txtMessage.placeholder = "Reply..."
                }
            }, completion: { finished in
            })
        }, completion: { finished in
        })
        self.tblChat.contentInset = contentInsets
        if self.arrMessageData.count > 0{
            self.tblChat.scrollToRow(at: IndexPath (item: self.arrMessageData.count - 1, section: 0), at: .top, animated: true)
        }else{
            //self.tblChat.scrollToRow(at: IndexPath (item: 0, section: 0), at: .top, animated: true)
        }
        
        self.tblChat.scrollIndicatorInsets = self.tblChat.contentInset
    }
}

extension GoLiveVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMessageData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellMessage = tableView.dequeueReusableCellTb(for: indexPath)
        cell.lblUname.text = self.arrMessageData[indexPath.row].user_name
        cell.lblMessage.text = self.arrMessageData[indexPath.row].comment
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension GoLiveVC{
    func updateButtonsVisiablity() {
        if settings.role == .audience{
            self.viewRight.isHidden = true
            self.ShareButton.isHidden = false
        }else{
            self.viewRight.isHidden = false
            self.ShareButton.isHidden = true
        }
    }
    
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890")//!#$%&()+-:;<=.>?@[]^_{}|~,")
        return String(text.filter {okayChars.contains($0) })
    }
      
    func loadAgoraKit() {
//        guard let channelId = settings.roomName else {
//            return
//        }
        let channelId = "Demo"
       // demoChannel1
        setIdleTimerActive(false)
        agoraKit.delegate = self
        // Step 2, set live broadcasting mode
        // for details: https://docs.agora.io/cn/Video/API%20Reference/oc/Classes/AgoraRtcEngineKit.html#//api/name/setChannelProfile:
        agoraKit.setChannelProfile(.liveBroadcasting)
        // Step 3, Warning: only enable dual stream mode if there will be more than one broadcaster in the channel
        agoraKit.enableDualStreamMode(true)
        // Step 4, enable the video module

        agoraKit.setClientRole(settings.role)

        // register video tags delegate
        agoraKit.setMediaMetadataDataSource(self, with: .video)
        agoraKit.setMediaMetadataDelegate(self, with: .video)
        
        agoraKit.setRemoteDefaultVideoStreamType(.low)

        agoraKit.enableVideo()

        // set video configuration
        agoraKit.setVideoEncoderConfiguration(
            AgoraVideoEncoderConfiguration(
                size: settings.dimension,
                frameRate: settings.frameRate,
                bitrate: AgoraVideoBitrateStandard,
                orientationMode: .adaptative
            )
        )

        // if current role is broadcaster, add local render view and start preview
        if settings.role == .broadcaster {
            agoraKit.startPreview()
            addLocalSession()
        }
        
//        let appID  = "aaa27fc4c94747fd8002eec917da6709"
//        let appCertificate   = "4a0e1d1f2f6843eaa1d3b2246d58b92a"
        
//        let appID = "222c46e6d25c4da39c1ea07ba0f8df9a";
//        let appCertificate = "45f253d7c1fc4d69bf0cf972a8076cd7";
        
        let channelName = settings.roomName ?? ""
//        let ticks1 = Date().ticks
//        let uid = ticks1
        
        let tempOther = appDel.loggedInUserData?.phone_number.dropFirst() ?? ""
        let tempOther1 = UInt(tempOther)
    
//        var role = String()
//        if self.settings.role == .broadcaster {
//            role = "1"
//        }else{
//            role = "2"
//        }
//        print(role)
        
        self.agoraKit.joinChannel(byToken: self.dynamicToekn, channelId: channelId, info: nil, uid: UInt(Int(tempOther1 ?? 1234))) { (eventName, uID, otherData) in
                                
                                print("eventName",eventName, "uID",uID, "otherData",otherData)
                                
                                self.tempUserCount += 1
                                self.setIdleTimerActive(false)
                                self.agoraKit.setEnableSpeakerphone(true)
                            }
      //  self.apiCallGetToken(channelName: channelName, uid: "\(tempOther1 ?? 1234)", channelId: channelId, uid_Int: Int(tempOther1 ?? 1234))
    }

    func updateBroadcastersView() {
        // video views layout
        if videoSessions.count == maxVideoSession {
            broadcastersView.reload(level: 0, animated: true)
        } else {
            var rank: Int
            var row: Int

            if videoSessions.count == 0 {
                return
            } else if videoSessions.count == 1 {
                rank = 1
                row = 1
            } else if videoSessions.count == 2 {
                rank = 1
                row = 2
            } else {
                rank = 2
                row = Int(ceil(Double(videoSessions.count) / Double(rank)))
            }

            let itemWidth = CGFloat(1.0) / CGFloat(rank)
            let itemHeight = CGFloat(1.0) / CGFloat(row)
            let itemSize = CGSize(width: itemWidth, height: itemHeight)
            let layout = AGEVideoLayout(level: 0)
                        .itemSize(.scale(itemSize))

            broadcastersView
                .listCount { [unowned self] (_) -> Int in
                    return self.videoSessions.count
                }.listItem { [unowned self] (index) -> UIView in
                    return self.videoSessions[index.item].hostingView
                }
            broadcastersView.setLayouts([layout], animated: true)
        }
    }

    func setIdleTimerActive(_ active: Bool) {
        UIApplication.shared.isIdleTimerDisabled = !active
    }

    func getSession(of uid: UInt) -> VideoSession? {
        for session in videoSessions {
            if session.uid == uid {
                return session
            }
        }
        return nil
    }

    func videoSession(of uid: UInt) -> VideoSession {
        if let fetchedSession = getSession(of: uid) {
            return fetchedSession
        } else {
            let newSession = VideoSession(uid: uid)
            videoSessions.append(newSession)
            return newSession
        }
    }

    func addLocalSession() {
        let localSession = VideoSession.localSession()
        localSession.updateInfo(fps: settings.frameRate.rawValue)
        videoSessions.append(localSession)
        agoraKit.setupLocalVideo(localSession.canvas)
    }

    func leaveChannel() {

        self.tempUserCount -= 1
        
        // Step 1, release local AgoraRtcVideoCanvas instance
        agoraKit.setupLocalVideo(nil)
        // Step 2, leave channel and end group chat
        agoraKit.leaveChannel(nil)
        // Step 3, if current role is broadcaster,  stop preview after leave channel
        if settings.role == .broadcaster {
            self.apiCallEndEvent()
            agoraKit.stopPreview()
        }
        setIdleTimerActive(true)
        findtopViewController()?.navigationController?.popToRootViewController(animated: true)
//        for controller in self.navigationController!.viewControllers as Array {
//            if controller.isKind(of: HomeVC.self) {
//                self.navigationController!.popToViewController(controller, animated: true)
//                break
//            }
//        }
    }
}

// MARK: - AgoraRtcEngineDelegate
extension GoLiveVC: AgoraRtcEngineDelegate {
    func rtcEngine(_ engine: AgoraRtcEngineKit, didAudioMuted muted: Bool, byUid uid: UInt) {
        print("Audio")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didVideoMuted muted: Bool, byUid uid: UInt) {
        print("Video")
        if muted{
            placeholderView.isHidden = false
        }else{
            placeholderView.isHidden = true
        }
    }
    
    // first local video frame
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstLocalVideoFrameWith size: CGSize, elapsed: Int) {
        if let selfSession = videoSessions.first {
            selfSession.updateInfo(resolution: size)
        }
    }

    // local stats
    func rtcEngine(_ engine: AgoraRtcEngineKit, reportRtcStats stats: AgoraChannelStats) {
        if let selfSession = videoSessions.first {
            selfSession.updateChannelStats(stats)
        }
    }

    // first remote video frame
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid: UInt, size: CGSize, elapsed: Int) {
        guard videoSessions.count <= maxVideoSession else {
            return
        }

        let userSession = videoSession(of: uid)
        userSession.updateInfo(resolution: size)
        agoraKit.setupRemoteVideo(userSession.canvas)
    }

    // user offline
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        var indexToDelete: Int?
        for (index, session) in videoSessions.enumerated() where session.uid == uid {
            indexToDelete = index
            break
        }

        if let indexToDelete = indexToDelete {
            let deletedSession = videoSessions.remove(at: indexToDelete)
            deletedSession.hostingView.removeFromSuperview()

            // release canvas's view
            deletedSession.canvas.view = nil
        }
    }

    // remote video stats
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteVideoStats stats: AgoraRtcRemoteVideoStats) {
        if let session = getSession(of: stats.uid) {
            session.updateVideoStats(stats)
        }
    }

    // remote audio stats
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteAudioStats stats: AgoraRtcRemoteAudioStats) {
        if let session = getSession(of: stats.uid) {
            session.updateAudioStats(stats)
        }
    }

    // warning code
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurWarning warningCode: AgoraWarningCode) {
        print("warning code: \(warningCode.description)")
    }

    // error code
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        print("warning code: \(errorCode.description)")
    }
}

extension GoLiveVC: AgoraMediaMetadataDataSource, AgoraMediaMetadataDelegate {
    func metadataMaxSize() -> Int {
        // the data to send should not exceed this size
        return MAX_META_LENGTH
    }

    func readyToSendMetadata(atTimestamp timestamp: TimeInterval) -> Data? {
        guard let metadata = self.metadata else {return nil}

        // clear self.metadata to nil after any success send to avoid redundancy
        self.metadata = nil

        if(metadata.count > MAX_META_LENGTH) {
            //if data exceeding limit, return nil to not send anything
            print("invalid metadata: length exceeds \(MAX_META_LENGTH)")
            return nil
        }

        print("metadata sent")
        self.metadata = nil
        return metadata
    }

    func receiveMetadata(_ data: Data, fromUser uid: Int, atTimestamp timestamp: TimeInterval) {
        self.alertOk(title: "Metadata received", message: String(data: data, encoding: .utf8) ?? "")
    }
}

extension Date{
    var ticks: UInt64 {
           return UInt64((NSDate().timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

//MARK:- API Calling
extension GoLiveVC{
    func apiCallEndEvent() {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let result = formatter.string(from: date)
        var param = [String:Any]()
        param = ["event_end_date":result,"event_id":appDel.eventLiveId]
        WebService.Request.patch(url: endGoLiveEvent, type: .post, parameter: param, callSilently : true) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String : Any] {

                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    
    func apiCallSendMessage() {
        var param = [String:Any]()
        param = ["event_id":appDel.eventLiveId,"comment":self.txtMessage.text ?? ""]
        WebService.Request.patch(url: addEventMessages, type: .post, parameter: param, callSilently : true) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    
    func apiCallRecieveMessage() {
        var param = [String:Any]()
        param = ["event_id":appDel.eventLiveId,"count_type":"1","join_count":"\(self.tempUserCount)"]
        print("Param=>",param)
        WebService.Request.patch(url: getEventMessagesList, type: .post, parameter: param, callSilently : true) { (response, error) in
            print("Messages===>",response as Any)
            print("Messages===>",error as Any)
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.is_end_event = response!["is_end_event"] as? String ?? "1"
                    if let data = response!["data"] as? [[String: Any]] {
                        self.arrMessageData = data.map{ MessageModel(JSON: $0)! }
//                        print(self.arrMessageData.count)
//                        print(self.arrMessageData[0].comment)
//                        if self.settings.role == .broadcaster{
//                            if response!["total_counts"] as? String == "0"{
//                                self.lblTotCount.text = response!["total_counts"] as? String
//                            }else{
//                                let count = Int(response!["total_counts"] as? String ?? "0")
//                                self.lblTotCount.text = "\((count ?? 1) - 1)"
//                            }
//                        }else{
                            self.lblTotCount.text = response!["total_counts"] as? String
//                        }
                        
                        if self.arrMessageData.count > self.msgCount{
                            self.tblChat.reloadData()
                            self.msgCount = self.arrMessageData.count
                        }
                        
                        var contentInsets:UIEdgeInsets
                        contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0, right: 0.0);
                         
                        if self.isKeyboardOpen == true{
                            
                        }else{
                            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
                                self.viewBottomBottom.constant = 0
                                self.txtviewHeight.constant = 40
                                self.viewBottomHeight.constant = 60
                                self.txtMessage.placeholder = "Reply..."
                                self.txtMessage.text = ""
                            }, completion: { finished in
                            })
                        }
                        
                        self.tblChat.contentInset = contentInsets
                        self.tblChat.scrollToRow(at: IndexPath (item: self.arrMessageData.count - 1, section: 0), at: .top, animated: true)
                        self.tblChat.scrollIndicatorInsets = self.tblChat.contentInset
                    }
                } else {
                    //self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                //self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    
    func apiCallGetToken(channelName:String,uid:String,channelId:String,uid_Int:Int){
        var param = [String:Any]()
        param = ["channelName":channelName,"uid":uid]
        WebService.Request.patch(url: AgoraToken, type: .post, parameter: param, callSilently : true) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.dynamicToekn = response!["token"] as? String ?? ""
                    self.agoraKit.joinChannel(byToken: self.dynamicToekn, channelId: channelId, info: nil, uid: UInt(uid_Int)) { (eventName, uID, otherData) in
                        
                        print("eventName",eventName, "uID",uID, "otherData",otherData)
                        
//                        var count = Int()
//                        self.arrUserCount.append(uID)
//                        if UserDefaults.standard.value(forKey: channelName) != nil{
//                            count = UserDefaults.standard.value(forKey: channelName) as! Int + self.arrUserCount.count
//                            UserDefaults.standard.set(count, forKey: channelName)
//                            self.tempUserCount = UserDefaults.standard.value(forKey: channelName) as! Int
//                            // + self.arrUserCount.count
//                        }else{
//                            count = self.arrUserCount.count
//                            UserDefaults.standard.set(count, forKey: channelName)
//                            self.tempUserCount = count
//                        }
                        self.tempUserCount += 1
                        self.setIdleTimerActive(false)
                        self.agoraKit.setEnableSpeakerphone(true)
                    }
                }else{
                        
                }
            }else{
                    
            }
            WebService.Loader.hide()
        }
    }
}
