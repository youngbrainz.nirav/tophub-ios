//
//  CellMessage.swift
//  CineStool
//
//  Created by Dharam YB on 27/05/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit

class CellMessage: UITableViewCell, ReusableView, NibLoadableView {

    //MARK:- Variable & Outlets
    @IBOutlet weak var lblUname:UILabel!
    @IBOutlet weak var lblMessage:UILabel!
    
    //MARK:- Default Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
