//
//  AudioChatCell.swift
//  CineStool
//
//  Created by Youngbrainz Mac Air on 30/05/22.
//  Copyright © 2022 Youngbrainz. All rights reserved.
//

import UIKit

class AudioChatReceiverCell: UITableViewCell,ReusableView,NibLoadableView {

    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet var LblTime: UILabel!
    @IBOutlet weak var lblAudioTime: UILabel!
    @IBOutlet weak var btnPlayPause: UIButton!
    
    @IBOutlet weak var viewForReply: UIView!
    @IBOutlet weak var lblReplyMsg: UILabel!
    @IBOutlet weak var imgMicrophone: UIImageView!
    @IBOutlet weak var lblReplyName: UILabel!
    @IBOutlet weak var imgReply: UIImageView!
    @IBOutlet weak var viewForMain: UIView!
    @IBOutlet weak var btnReply:UIButton!
    var audioUrl = ""
    var onClickBtnPlay:(()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data:ModelChat) {
        LblTime.text = data.datetime//getDateFromTimeStamp(timeStamp: Double(data.timeStamp) ??  0)
        lblSenderName.text = data.senderName
        audioUrl = data.audioUrl
        lblReplyName.text = ""
        lblReplyMsg.text = ""
        viewForReply.isHidden = data.replyName.isEmpty
        if !viewForReply.isHidden {
            lblReplyName.text = data.replySenderId == appDel.loggedInUserData?._id ? "You":data.replyName
            if data.replyDuration.isEmpty {
                lblReplyMsg.text = data.reply
                imgMicrophone.isHidden = true
            } else {
                lblReplyMsg.text = data.replyDuration
                imgMicrophone.isHidden = false
            }
        }
    }
    @IBAction func onClickBtnPlayPause(_ sender: UIButton) {

        onClickBtnPlay!()
    }
}
