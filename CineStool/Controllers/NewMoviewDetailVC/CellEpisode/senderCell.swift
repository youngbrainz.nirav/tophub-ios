//
//  senderCell.swift
//  CineStool
//
//  Created by Youngbrainz Mac Air on 16/05/22.
//  Copyright © 2022 Youngbrainz. All rights reserved.
//

import UIKit

class senderCell: UITableViewCell,ReusableView,NibLoadableView {
    
    @IBOutlet weak var viewForChat: UIView!
    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet var LblTime: UILabel!
    @IBOutlet var LblMsg: UILabel!
    
    @IBOutlet weak var viewForReply: UIView!
    @IBOutlet weak var lblReplyMsg: UILabel!
    @IBOutlet weak var imgMicrophone: UIImageView!
    @IBOutlet weak var lblReplyName: UILabel!
    @IBOutlet weak var imgReply: UIImageView!
    @IBOutlet weak var viewForMain: UIView!
    @IBOutlet weak var btnReply:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
       // self.txtMsg.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data:ModelChat) {
        LblMsg.text = data.message
        LblTime.text = data.datetime//getDateFromTimeStamp(timeStamp: Double(data.timeStamp) ??  0)
        lblSenderName.text = "You"//data.senderName
       
        lblReplyMsg.text = ""
        lblReplyName.text = ""
        viewForReply.isHidden = data.replyName.isEmpty
        lblSenderName.isHidden = !data.replyName.isEmpty
        if !viewForReply.isHidden {
            lblReplyName.text = data.replySenderId == appDel.loggedInUserData?._id ? "You":data.replyName
            if data.replyDuration.isEmpty {
                lblReplyMsg.text = data.reply
                imgMicrophone.isHidden = true
            } else {
                lblReplyMsg.text = data.replyDuration
                imgMicrophone.isHidden = false
            }
        }
        
//        LblTime.translatesAutoresizingMaskIntoConstraints = false
//        LblTime.centerXAnchor.constraint(equalTo: viewForChat.leftAnchor).isActive = true
//        LblTime.topAnchor.constraint(equalTo: viewForChat.bottomAnchor, constant: 10).isActive = true
    }
    
}
func getDateFromTimeStamp(timeStamp : Double) -> String {
    
    let date = NSDate(timeIntervalSince1970: timeStamp/1000)
    
    let dayTimePeriodFormatter = DateFormatter()
    dayTimePeriodFormatter.dateFormat = "dd/MM/yyyy, hh:mm a"
    // UnComment below to get only time
    //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
    
    
    let isCheckToday = Calendar.current.isDateInToday(date as Date)
    let dateString = dayTimePeriodFormatter.string(from: date as Date)
    return dateString.lowercased()
//    if isCheckToday == true
//    {
//        dayTimePeriodFormatter.dateFormat = "hh:mm a"
//
//        var dateString = dayTimePeriodFormatter.string(from: date as Date)
//
//        dateString = "\(dateString)"
//
//        return dateString
//    }
//    else
//    {
//        let dateString = dayTimePeriodFormatter.string(from: date as Date)
//        return dateString
//    }
    
    
}
struct ModelChat {
    
    var senderId:String = ""
    var senderName:String = ""
    var timeStamp:String = ""
    var message:String = ""
    var datetime:String = ""
    var audioUrl:String = ""
    var duration:String = ""
    var messageId:String = ""
    var reply:String = ""
    var replyName:String = ""
    var replyId:String = ""
    var replyDuration:String = ""
    var replySenderId:String = ""
    init(_ senderId:String, _ senderName:String, _ timeStamp:String, _ message:String, _ senderDateTime:String, _ audioUrl: String, _ duration: String, _ messageId:String, _ reply:String, _ replyName:String, _ replyId:String, _ replyDuration:String, _ replySenderId:String) {
        
        self.senderId = senderId
        self.senderName = senderName
        self.timeStamp = timeStamp
        self.message = message
        self.datetime = senderDateTime
        self.audioUrl = audioUrl
        self.duration = duration
        self.messageId = messageId
        self.reply = reply
        self.replyName = replyName
        self.replyId = replyId
        self.replyDuration = replyDuration
        self.replySenderId = replySenderId
    }
    
    func toAnyObject() -> Any {
        return [
            
            "senderId": senderId,
            "senderName": senderName,
            "timeStamp": timeStamp,
            "message": message,
            "datetime": datetime,
            "audioUrl":audioUrl,
            "duration":duration,
            "messageId":messageId,
            "reply":reply,
            "replyName":replyName,
            "replyId":replyId,
            "replyDuration":replyDuration,
            "replySenderId":replySenderId
        ]
    }
}
