//
//  AudioChatSenderCell.swift
//  CineStool
//
//  Created by Youngbrainz Mac Air on 30/05/22.
//  Copyright © 2022 Youngbrainz. All rights reserved.
//

import UIKit
import AVKit

class AudioChatSenderCell: UITableViewCell,ReusableView,NibLoadableView {

    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet var LblTime: UILabel!
    @IBOutlet weak var lblAudioTime: UILabel!
    @IBOutlet weak var btnPlayPause: UIButton!
    
    @IBOutlet weak var viewForReply: UIView!
    @IBOutlet weak var lblReplyMsg: UILabel!
    @IBOutlet weak var imgMicrophone: UIImageView!
    @IBOutlet weak var lblReplyName: UILabel!
    @IBOutlet weak var imgReply: UIImageView!
    @IBOutlet weak var viewForMain: UIView!
    @IBOutlet weak var btnReply:UIButton!
    var audioUrl = ""
    var audioPlayer:AVPlayer?
    var playerItem:AVPlayerItem?
    var onClickBtnPlay:(()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    deinit {
        print("deinit cell")
       // audioPlayer?.removeTimeObserver(self)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
//        if !(audioPlayer?.isPlaying ?? true) && audioPlayer?.timeControlStatus != .paused {
//        playerItem = nil
//        }
    }
    func setData(data:ModelChat) {
        LblTime.text = data.datetime//getDateFromTimeStamp(timeStamp: Double(data.timeStamp) ??  0)
        lblSenderName.text = "You"//data.senderName
        audioUrl = data.audioUrl
        lblReplyMsg.text = ""
        lblReplyName.text = ""
        viewForReply.isHidden = data.replyName.isEmpty
        lblSenderName.isHidden = !data.replyName.isEmpty
        if !viewForReply.isHidden {
            lblReplyName.text = data.replySenderId == appDel.loggedInUserData?._id ? "You":data.replyName
            if data.replyDuration.isEmpty {
                lblReplyMsg.text = data.reply
                imgMicrophone.isHidden = true
            } else {
                lblReplyMsg.text = data.replyDuration
                imgMicrophone.isHidden = false
            }
        }
        
    }
    @objc func finishedPlaying( _ myNotification:NSNotification) {
        btnPlayPause.isSelected = false
        //reset player when finish
        let maxDuration = Int(self.playerItem?.duration.seconds ?? 0)
        let strMax = formatSecondsToString(maxDuration)
        self.lblAudioTime.text = "00:00/"+strMax
        let targetTime:CMTime = CMTimeMake(value: 0, timescale: 1)
        audioPlayer?.seek(to: targetTime)
    }
    @IBAction func onClickBtnPlayPause(_ sender: UIButton) {
//        sender.isSelected.toggle()
//        
//        if sender.isSelected {
//            setupAudioPlayer()
//        
//            audioPlayer?.play()
//        } else {
//            audioPlayer?.pause()
//        }
        onClickBtnPlay!()
    }
//    func setupAudioPlayer(){
//        guard let url = URL.init(string: audioUrl) else {return}
//        if getVideoUrl() == url && audioPlayer != nil {return}
//            playerItem = AVPlayerItem.init(url: url)
//            audioPlayer = AVPlayer.init(playerItem: playerItem)
//
//            audioPlayer?.addPeriodicTimeObserver(forInterval: CMTime.init(seconds: 1, preferredTimescale: 1), queue: DispatchQueue.main, using: { time in
//                if self.audioPlayer?.currentItem?.status == .readyToPlay {
//                    let maxDuration = Int(self.playerItem?.duration.seconds ?? 0)
//                    let currentDuration = Int(self.playerItem?.currentTime().seconds ?? 0)
//                    let strMax = formatSecondsToString(maxDuration)
//                    let strCurrent = formatSecondsToString(currentDuration)
//                    self.lblAudioTime.text = strCurrent+"/"+strMax
////
//                }
//
//            })
//            NotificationCenter.default.addObserver(self, selector: #selector(self.finishedPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
//    }
//    func getVideoUrl() -> URL? {
//        let asset = self.audioPlayer?.currentItem?.asset
//        if asset == nil {
//            return nil
//        }
//        if let urlAsset = asset as? AVURLAsset {
//            return urlAsset.url
//        }
//        return nil
//    }
    
}

