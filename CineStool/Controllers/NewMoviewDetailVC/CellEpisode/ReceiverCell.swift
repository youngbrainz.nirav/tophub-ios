//
//  ReceiverCell.swift
//  CineStool
//
//  Created by Youngbrainz Mac Air on 16/05/22.
//  Copyright © 2022 Youngbrainz. All rights reserved.
//

import UIKit

class ReceiverCell: UITableViewCell,ReusableView,NibLoadableView {
    
    @IBOutlet weak var imgReply: UIImageView!
    @IBOutlet weak var viewForChat: UIView!
    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet weak var viewForReply: UIView!
    @IBOutlet weak var lblReplyMsg: UILabel!
    @IBOutlet weak var imgMicrophone: UIImageView!
    @IBOutlet weak var lblReplyName: UILabel!
    @IBOutlet var LblTime: UILabel!
    @IBOutlet var LblMsg: UILabel!
    @IBOutlet weak var viewForMain: UIView!
    @IBOutlet weak var btnReply:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
      //  self.txtMsg.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data:ModelChat) {
        LblMsg.text = data.message
        LblTime.text = data.datetime//getDateFromTimeStamp(timeStamp: Double(data.timeStamp) ??  0)
        lblSenderName.text = data.senderName
        lblReplyMsg.text = ""
        lblReplyName.text = ""
        viewForReply.isHidden = data.replyName.isEmpty
        if !viewForReply.isHidden {
            lblReplyName.text = data.replySenderId == appDel.loggedInUserData?._id ? "You":data.replyName
            if data.replyDuration.isEmpty {
                lblReplyMsg.text = data.reply
                imgMicrophone.isHidden = true
            } else {
                lblReplyMsg.text = data.replyDuration
                imgMicrophone.isHidden = false
            }
        }
        
//        LblTime.translatesAutoresizingMaskIntoConstraints = false
//        LblTime.centerXAnchor.constraint(equalTo: viewForChat.rightAnchor).isActive = true
//        LblTime.topAnchor.constraint(equalTo: viewForChat.bottomAnchor, constant: 10).isActive = true
    }
}
