//
//  ParticipateCell.swift
//  CineStool
//
//  Created by Youngbrainz Mac Air on 23/05/22.
//  Copyright © 2022 Youngbrainz. All rights reserved.
//

import UIKit

class ParticipateCell: UITableViewCell,ReusableView,NibLoadableView {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
   
    override func layoutSubviews() {
        super.layoutSubviews()
        imgUser.cornerRadius = imgUser.frame.height/2
    }
    
}
