//
//  CellCollEpisode.swift
//  CineStool
//
//  Created by Dharam YB on 05/08/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit

class CellCollEpisode: UICollectionViewCell, ReusableView, NibLoadableView  {

    @IBOutlet weak var imgUserProfile:UIImageView!
    @IBOutlet weak var lblEventName:UILabel!
    @IBOutlet weak var lblLiveTag:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
