//
//  NewMoviewDetailVC.swift
//  CineStool
//
//  Created by Dharam YB on 30/04/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import Cosmos
import AVKit
import AVFoundation
import GoogleCast
import BMPlayer
import FirebaseDynamicLinks
import FirebaseDatabase
import FirebaseStorage
import IQKeyboardManagerSwift
import PusherSwift
import Alamofire
func delay(_ seconds: Double, completion:@escaping ()->()) {
  let popTime = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds )) / Double(NSEC_PER_SEC)
  
  DispatchQueue.main.asyncAfter(deadline: popTime) {
    completion()
  }
}

enum Params: String {
    case bundleID = "com.video.CineStool"
    case minimumAppVersion = "3.7"
    case appStoreID = "1475084627"
    case packageName = "com.app.cinestool"
    case minimumVersion = "1026"
    case fallbackURL = "https://cinestool.page.link/PZXe"
}


class NewMoviewDetailVC: UIViewController,reviewDelegate, UIGestureRecognizerDelegate {

    //MARK:- Variables & Outlets
    @IBOutlet weak var mainScrollView: UIScrollView!
    var intValue:Int = 5
    @IBOutlet weak var imgBlurMovie: UIImageView!
    var PlayerState:Bool = false
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var btnDownload: UIButton!
    
    @IBOutlet weak var btnWatchTrailer: UIButton!
    @IBOutlet weak var btnWatchMovie: UIButton!
    @IBOutlet weak var btnCenterWatchMovie: UIButton!
    @IBOutlet weak var cnst_download: NSLayoutConstraint!
    @IBOutlet weak var lblRemainingData: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnDescription: UIButton!
    @IBOutlet weak var btnCast: UIButton!
    @IBOutlet weak var progressDownload: UIProgressView!
    
    @IBOutlet weak var lblMovieName: UILabel!
    var totalVideoDuration:CGFloat = 0.0
    var CurrentVideoDuration:CGFloat = 0.0
    var CurrentVideoDurationTemp:CGFloat = 0.0
    @IBOutlet weak var imgDownload: UIImageView!
    @IBOutlet weak var lblDownload: UILabel!

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var tblCast: UITableView!
    @IBOutlet weak var heightlblDesc: NSLayoutConstraint!
    @IBOutlet weak var heighttblCast: NSLayoutConstraint!
    
    @IBOutlet weak var lblMyList: UILabel!
    @IBOutlet weak var imgMyList: UIImageView!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblMyRate: UILabel!
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var imgRate: UIImageView!
    @IBOutlet weak var lblShare: UILabel!
    @IBOutlet weak var imgShare: UIImageView!
    
    @IBOutlet weak var collRelatedMovie: UICollectionView!
    @IBOutlet weak var heightcollRelatedMovie: NSLayoutConstraint!
    
    @IBOutlet weak var btnGiveRate: UIButton!
    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var widthRating: NSLayoutConstraint!
    
    @IBOutlet weak var viewBlack: UIView!
    
    @IBOutlet weak var viewrate: UIView!
    @IBOutlet weak var Heightviewrate: NSLayoutConstraint!
    
    @IBOutlet weak var viewLandscape: UIView!
    //Player Setup
    @IBOutlet weak var btnTopBack: UIButton!
    @IBOutlet weak var btnGoogleCast: UIButton!
    @IBOutlet weak var heightBannerView: NSLayoutConstraint!
    @IBOutlet weak var viewPlayer: UIView!
    @IBOutlet weak var heightPlayerView: NSLayoutConstraint!
    var myPlayer: BMPlayer!
    @IBOutlet weak var viewGoogleCast: UIView!
    @IBOutlet weak var btnGoogleCast2: UIButton!
    @IBOutlet weak var viewGoogleCast2: UIView!
    @IBOutlet weak var scrollHeightLandscape: NSLayoutConstraint!
    private var castButton: GCKUICastButton!
    private var mediaInformation: GCKMediaInformation?
    private var sessionManager: GCKSessionManager!
    
    @IBOutlet weak var vwDownload: UIView!
    @IBOutlet weak var viewPaymentDone: UIView!
    
    @IBOutlet weak var lblEposides: UILabel!
    @IBOutlet weak var lblMoreLike: UILabel!
    @IBOutlet weak var lblCenterMoreLike: UILabel!
    
    @IBOutlet weak var btnEpisode: UIButton!
    @IBOutlet weak var btnMoreLike: UIButton!
    @IBOutlet weak var btnCenterMoreLike: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var heightForChatView: NSLayoutConstraint!
    //
    @IBOutlet weak var txtChat: UITextView!
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var txtChatLandScape: UITextView!
    
    @IBOutlet weak var chatViewLandscape: UIView!
    @IBOutlet weak var tblChatLandScape: UITableView!
    
    @IBOutlet weak var btnChatLandscape: UIButton!
    @IBOutlet var scrollHeight: NSLayoutConstraint!
    var strVideoURL = String()
    
    
    @IBOutlet weak var heightTxtChatLandscape: NSLayoutConstraint!
    
    @IBOutlet weak var heightForTxtChat: NSLayoutConstraint!
    @IBOutlet weak var stkGoogleCast: UIStackView!
    var strImageURL = String()
    @IBOutlet weak var bottomForChatView: NSLayoutConstraint!
    @IBOutlet weak var tblParticipate: UITableView!
    @IBOutlet weak var viewParticipate: UIView!
    @IBOutlet weak var tblParticipateLandscape: UITableView!
    
    @IBOutlet weak var btnShareWatchParty: UIButton!
    @IBOutlet weak var btnCloseChat: UIButton!
    
    @IBOutlet weak var viewSend: UIView!
    @IBOutlet weak var btnWatchParty: UIButton!
    @IBOutlet weak var btnParticipants: UIButton!
    @IBOutlet weak var viewVoiceChat: UIView!
    
    @IBOutlet weak var btnStopRecording: UIButton!
    
    
    @IBOutlet weak var btnCancelRecording: UIButton!
    
    @IBOutlet weak var viewWatchParty: UIView!
    
    @IBOutlet weak var btnParticipantsIcon: UIButton!
    
    @IBOutlet weak var btnStartWatchParty: UIButton!
    @IBOutlet weak var btnChatIcon: UIButton!
    @IBOutlet weak var btnEndWatchParty: UIButton!
    @IBOutlet weak var lblTimeAudio: UILabel!
    
    @IBOutlet weak var viewVoiceChatLandscape: UIView!
    
    @IBOutlet weak var lblTimeAudioLandscape: UILabel!
    
    @IBOutlet weak var btnStopLandscape: UIButton!
    
    @IBOutlet weak var viewCastDescription: UIView!
    
    @IBOutlet weak var heightForWatchParty: NSLayoutConstraint!
    
    @IBOutlet weak var lblSepWatchParty: UILabel!
    
    @IBOutlet weak var lblSepDescription: UILabel!
    @IBOutlet weak var lblSepCast: UILabel!
    @IBOutlet weak var lblWatchPartyDescription: UILabel!
    
    @IBOutlet weak var viewForReply: UIView!
    @IBOutlet weak var lblSenderName: UILabel!
    
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var imgMicrophone: UIImageView!
    
    
    @IBOutlet weak var viewForReplyLandscape: UIView!
    @IBOutlet weak var lblSenderNameLandscape: UILabel!
    
    @IBOutlet weak var lblMsgLandscape: UILabel!
    @IBOutlet weak var imgMicrophoneLandscape: UIImageView!
    
    @IBOutlet weak var btnRecordPlayPause: UIButton!
    @IBOutlet weak var btnRecordPlayPauseLandscape: UIButton!
    
    @IBOutlet weak var stkForWatchParty: UIStackView!
    var audioUrl = ""
    var audioPlayer:AVPlayer?
    var playerItem:AVPlayerItem?
    var strName = String()
    var strDesc = String()
    var isTrailer = false
    var isNextEpisodeTapped = false
    var isCastTapped = false
    var isEpisodeTapped = false
    var movieID = ""
    var partyCode = ""
    var movieDetail: Movies!
    var relatedMovies : [related_movies] = []
    var EpisodeMovies : [related_movies] = []
    var movieDescription = ""
    var givenRating = ""
    var isFirstTime = true
    var groupId = "TheTasteOfAWoman_+917016453913"
    var longLink: URL?
    var shortLink: URL?
    let domain: String = "https://tophub.page.link"
    //"https://cinestool.page.link"
    
    var movie_banner = ""
    var isRatingTapped = false
    
    var isFromDownloads = false
    var arrChats:[ModelChat] = []
    var arrParticipate:[movie_party_list] = []
    var objReviewView:ReviewPopup?
    var pusher:Pusher!
    var channel:PusherChannel!
    var audioRecorder:AVAudioRecorder?
    var timerForAudio:Timer?
    var counterTime = 0
    var selectedIndex = -1
    var currentAudioDuration = "00:00"
    var isWatchParty = false
    var channelName = ""
    var eventName = ""
    var eventParticipateName = ""
    var pusher_event_id = ""
    var host_user_id = ""
    var isFullscreen = false
    var selectedReply = -1
    var isReplyHide = false
    @IBOutlet weak var viewParticipateLandscape: UIView!
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.preparePlayer(iscall: false)
        setupPlayerManager()
        NotificationCenter.default.addObserver(self,selector: #selector(applicationDidEnterBackground),
                                                    name: UIApplication.didEnterBackgroundNotification,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(applicationWillEnterForeground),
                                                  name: UIApplication.willEnterForegroundNotification,object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getFullScreenStatus), name: NSNotification.Name(rawValue: "setFullScreen"), object: nil)
        //
        sessionManager = GCKCastContext.sharedInstance().sessionManager
        sessionManager.add(self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadSuccesfull(_:)), name: Notification.Name("download_completed"), object: nil)

        
        NotificationCenter.default.addObserver(self,
        selector: #selector(castDeviceDidChange(notification:)),
        name: NSNotification.Name.gckCastStateDidChange,
        object: GCKCastContext.sharedInstance())
        
        if GCKCastContext.sharedInstance().castState != GCKCastState.noDevicesAvailable {
                        btnGoogleCast.isHidden = true
                        btnGoogleCast2.isHidden = true
        //                GCKCastContext.sharedInstance().presentCastInstructionsViewControllerOnce(with: castButton)
                        if GCKCastContext.sharedInstance().castState == GCKCastState.connected{
                            //myPlayer.pause()
                            //playVideoRemotely()
                        }
                    }else{
                        //btnGoogleCast.isHidden = false
                    }
        self.lblEposides.isHidden = false
        self.lblMoreLike.isHidden = true
        
    }
    override func didReceiveMemoryWarning() {
        print("Got Memory warning")
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        heightForChatView.constant = (tblChat.superview?.superview?.frame.height ?? (UIScreen.main.bounds.height)) - heightBannerView.constant - 10
       
      //  heightForWatchParty.constant = (viewWatchParty.superview?.frame.height ?? (UIScreen.main.bounds.height)) - viewCastDescription.frame.maxY
        if GCKCastContext.sharedInstance().castState != GCKCastState.noDevicesAvailable {
            btnGoogleCast.isHidden = true
            btnGoogleCast2.isHidden = true
            GCKCastContext.sharedInstance().presentCastInstructionsViewControllerOnce(with: castButton)
        }
        let screenSize = UIScreen.main.bounds
//        if screenSize.height >= 812{
//            lblWatchPartyDescription.font = UIFont(name: "Poppins-SemiBold", size: 15.0)
//        } else {
//            lblWatchPartyDescription.font = UIFont(name: "Poppins-SemiBold", size: 13.0)
//        }
    }
    
    @IBAction func btnNoCastDevice(sender _: AnyObject) {
        alertOk(title: "", message: "There is no any casting device available please turn on the casting device to connect.")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        appDel.myOrientation = .portrait
        
        if self.channel != nil {
            self.channel.unbindAll()
        }
        audioPlayer?.pause()
       
       stopRecording()
        pusher.disconnect()
        if !groupId.isEmpty {
            appDel.ref.child(self.groupId).child("Messages").removeAllObservers()
        }
        myPlayer.playerLayer?.resetPlayer()
        myPlayer.avPlayer?.replaceCurrentItem(with: nil)
        
        myPlayer.playerLayer?.removeFromSuperview()
        myPlayer.prepareToDealloc()

        myPlayer.removeFromSuperview()
        print("allowAutoPlay: false==>")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.beginBackgroundTask {}

        if heightPlayerView.constant == 0{
            appDel.myOrientation = .portrait
            print("is it Portrait==>")
        }else{
          //  appDel.myOrientation = .all
            print("is it All==>")
        }
        appDel.isFullScreen = false
//        DispatchQueue.main.async {
//            self.imgBlurMovie.sd_setImage(with: URL(string: self.movie_banner), placeholderImage:nil)
//        }
        if isFromDownloads{
            DispatchQueue.main.async {
                self.DisplayData()
            }
        }else{
            if Reachability.isConnectedToNetwork(){
                apiCallGetMovieDetails(callSilently: false)
            }else{
                let objTab = NoInternetVC(nibName: "NoInternetVC", bundle: nil)
                objTab.modalPresentationStyle = .fullScreen
                objTab.clickRetry = {
                    self.apiCallGetMovieDetails(callSilently: false)
                }
                objTab.ClickOffline = {
                    let objTab = DownloadedVC(nibName: "DownloadedVC", bundle: nil)
                   findtopViewController()?.self.navigationController!.pushViewController(objTab, animated: true)
                }
                findtopViewController()?.self.navigationController?.present(objTab, animated: true, completion: nil)
            }
        }
        if !Reachability.isConnectedToNetwork(){
            self.btnWatchTrailer.isEnabled = false
            self.btnWatchTrailer.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0.2274509804, alpha: 0.5)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.imgDownload.image = UIImage(named: "ic_downloaded_cmpltd")
            }
        }else{
            self.btnWatchTrailer.isEnabled = true
            self.btnWatchTrailer.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0.2274509804, alpha: 1)
        }
        
    }
    
    deinit {
      myPlayer.prepareToDealloc()
      print("VideoPlayViewController Deinit")
    }
    func getMessages(){
//        appDel.ref.child("").observe(.childAdded) { snapshot in
//
//                if let data = snapshot.value as? [String:Any] {
//                let id = data["senderId"] as? String ?? ""
//                let message = data["message"] as? String ?? ""
////                let name = data["senderName"] as? String ?? ""
////                let timeStamp = data["timeStamp"] as? String ?? ""
//                    if !message.isEmpty {
////
//                        if !self.isFirstTime {
//                            if id == self.recipient_id {
//                                self.wsReadMsg()
//                            }
//                        }
//                    }
//                }
//        }
     
        appDel.ref.child(self.groupId).child("Messages").observe(.value) { snapshot in
            let snap = snapshot.children
            self.arrChats.removeAll()
            for child in snap {
                let snapshot = child as? DataSnapshot
                if let data = snapshot?.value as? [String:Any] {
                let id = data["senderId"] as? String ?? ""
                let message = data["message"] as? String ?? ""
                let name = data["senderName"] as? String ?? ""
                let timeStamp = data["timeStamp"] as? String ?? ""
                let dateTime = data["datetime"] as? String ?? ""
                let audioUrl = data["audioUrl"] as? String ?? ""
                let duration = data["duration"] as? String ?? ""
                let messageId = data["messageId"] as? String ?? ""
                let reply = data["reply"] as? String ?? ""
                    let replyName = data["replyName"] as? String ?? ""
                    let replyId = data["replyId"] as? String ?? ""
                    let replyDuration = data["replyDuration"] as? String ?? ""
                    let replySenderId = data["replySenderId"] as? String ?? ""
                    let chat = ModelChat.init(id, name, timeStamp, message, dateTime, audioUrl, duration,messageId,reply, replyName, replyId, replyDuration, replySenderId)
                    if !message.isEmpty || !duration.isEmpty || !replyId.isEmpty {
                        self.arrChats.append(chat)
                    }
              //  print(data)
                }
            }
            if !self.chatView.isHidden {
            self.tblChat.reloadData()
                
                    if self.arrChats.count > 0 {
                        self.tblChat.scrollToRow(at: IndexPath.init(row: self.arrChats.count-1, section: 0), at: .bottom, animated: !self.isFirstTime)

                        self.isFirstTime = false
                        
                   
                }
            
            } else if !self.chatViewLandscape.isHidden {
                DispatchQueue.main.async {
                    self.tblChatLandScape.reloadData()
                    
                        if self.arrChats.count > 0 {
                            self.tblChatLandScape.scrollToRow(at: IndexPath.init(row: self.arrChats.count-1, section: 0), at: .bottom, animated: !self.isFirstTime)
                            self.isFirstTime = false
                            
                    }
                    
                    
                }
               
            }
            
        }
    }
    func OpenReviewPopup(){
        self.objReviewView = Bundle.main.loadNibNamed("ReviewPopup", owner: self, options: nil)?[0] as? ReviewPopup
        self.objReviewView?.Delegate = self
        self.view.addSubview(self.objReviewView!)
        self.objReviewView?.DisplayView()
    }
    @objc func downloadSuccesfull(_ notification:Notification){
        self.DisplayData(isFromProcess: true)
        
    }
    func getBackfromReview()
    {
        myPlayer.pause(allowAutoPlay: false)
        myPlayer.pause()
        print("allowAutoPlay: false==>")
        if self.heightPlayerView.constant == 0{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.heightPlayerView.constant = 0
            self.heightBannerView.constant = 230
            DispatchQueue.main.async {
                self.mainScrollView.setContentOffset(.zero, animated: true)
                self.view.layoutIfNeeded()
                self.view.updateConstraintsIfNeeded()
            }
        }
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                
                UIView.animate(withDuration: 0.3, animations: {
                    print(keyboardSize.height , "keyboard show")
                    let screenSize = UIScreen.main.bounds
                    if screenSize.height >= 812{
                        self.scrollHeight.constant = keyboardSize.height - 34
                        self.scrollHeightLandscape.constant = keyboardSize.height
                        
                            self.heightForChatView.constant += (self.viewForReply.frame.height/2 + 25)
                        if !self.viewForReplyLandscape.isHidden {
                            self.isReplyHide = true
                            self.viewForReplyLandscape.isHidden = true
                        }
                        
                    }else{
                        self.scrollHeight.constant = keyboardSize.height
                        self.heightForChatView.constant += (self.viewForReply.frame.height)
                        if !self.viewForReplyLandscape.isHidden {
                            self.isReplyHide = true
                            self.viewForReplyLandscape.isHidden = true
                        }
                        self.scrollHeightLandscape.constant = keyboardSize.height - 34
                    }
                    self.chatView.layoutIfNeeded()
                })
            }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
       
            self.scrollHeight.constant = 0
        if isReplyHide {
            isReplyHide = false
            self.viewForReplyLandscape.isHidden = false
        }
        self.scrollHeightLandscape.constant = 0
        self.heightForChatView.constant = (tblChat.superview?.superview?.frame.height ?? (UIScreen.main.bounds.height)) - heightBannerView.constant - 10
            UIView.animate(withDuration: 0.2, animations: {
            })
        
    }
    
    func sendMessage(tag:Int,audioUrl:String="",duration:String="",reply:String="",replyName:String="",replyId:String="",replyDuration:String="",replySenderId:String="") {
        let date = Int((NSDate().timeIntervalSince1970) * 1000)

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd/MM/YYYY, hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: Date())
        let uid = appDel.ref.childByAutoId().key ?? "";
        let msg = (tag == 0 ? txtChat.text! : txtChatLandScape.text!)
        let sessionItem = ModelChat(appDel.loggedInUserData?._id ?? "", appDel.loggedInUserData?.user_name ?? "", "\(date)", msg, dateString, audioUrl,duration,uid,reply,replyName,replyId,replyDuration, replySenderId)
        
        
        
        
        
        
        
        let sessionItemRef = appDel.ref.child(self.groupId).child("Messages").child(uid)

        
        sessionItemRef.setValue(sessionItem.toAnyObject())
        viewVoiceChat.isHidden = true
        viewVoiceChatLandscape.isHidden = true
        viewForReply.isHidden = true
        viewForReplyLandscape.isHidden = true
        self.view.endEditing(true)
    }
    func startTimerForAudio(){
        timerForAudio?.invalidate()
        counterTime = 0
        timerForAudio = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(setTimer), userInfo: nil, repeats: true)
    }
    @objc func setTimer(){
        counterTime += 1
        lblTimeAudio.text = formatSecondsToString(counterTime)
        lblTimeAudioLandscape.text = formatSecondsToString(counterTime)
        if counterTime >= 120 {
            alertOk(title: "", message: "you can record audio upto 2 min.")
            btnStopRecordingAction(btnStopRecording)
        }
    }
    func stopTimer(){
        timerForAudio?.invalidate()
        timerForAudio = nil
    }
    func setupPusher(){
        self.pusher.connect()
        self.pusher.connection.delegate = self
        self.channel =  self.pusher.subscribe(channelName: channelName)
        
        self.channel.bind(eventName: "pusher:subscription_succeeded") { event in
            if let data = event.data {
                print("success pusher:", data)
                self.channel.bind(eventName: self.eventParticipateName) { event in
                    if let json = event.data?.convertToDictionary() {
                        if let data = Movies(JSON: json) {
                        print("arrParticipate", data.movie_party_list.count)
                            self.arrParticipate = data.movie_party_list
                        self.tblParticipate.reloadData()
                        self.tblParticipateLandscape.reloadData()
                        }
                    }
                }
                self.channel.bind(eventName: self.eventName) { event in
                    print("Socket Callback: ", event.data)
                    
                    self.handlePusherCallback(data:event.data ?? "")
                }
                 
             
                
            }
        }
       
    }
    func setupPusherForHost(){
        if self.pusher.connection.connectionState == .connected {return}
        self.pusher.connect()
        self.pusher.connection.delegate = self
        self.channel =  self.pusher.subscribe(channelName: channelName)
        
        self.channel.bind(eventName: "pusher:subscription_succeeded") { event in
            if let data = event.data {
                print("success pusher:", data)
//
                self.channel.bind(eventName: self.eventParticipateName) { event in
                    if let json = event.data?.convertToDictionary() {
                        if let data = Movies(JSON: json) {
                        print("arrParticipate", data.movie_party_list.count)
                        self.arrParticipate = data.movie_party_list ?? []
                        self.tblParticipate.reloadData()
                        self.tblParticipateLandscape.reloadData()
                        }
                    }
                }
               
            }
        }
      
    }
    func setupAudioPlayer(audioUrl:String, cell:AudioChatSenderCell?,cell2:AudioChatReceiverCell?){
        
        guard let url = URL.init(string: audioUrl) else {return}
        
            if getVideoUrl() == url && audioPlayer != nil {return}
        let session = AVAudioSession.sharedInstance()

                do{
                    try session.setCategory(.playback,options: [.defaultToSpeaker])
                    try session.setActive(true)
                } catch {
                    print ("\(#file) - \(#function) error: \(error.localizedDescription)")
                }
        self.currentAudioDuration = "00:00"
        self.playerItem = AVPlayerItem.init(url: url)
        self.audioPlayer = AVPlayer.init(playerItem: self.playerItem)
            
        self.audioPlayer?.addPeriodicTimeObserver(forInterval: CMTime.init(seconds: 1, preferredTimescale: 1), queue: DispatchQueue.main, using: { time in
                if self.audioPlayer?.currentItem?.status == .readyToPlay {
                    let maxDuration = Int(self.playerItem?.duration.seconds ?? 0)
                    let currentDuration = Int(self.playerItem?.currentTime().seconds ?? 0)
                    let strMax = formatSecondsToString(maxDuration)
                    self.currentAudioDuration = formatSecondsToString(currentDuration)
                    let indexpath = IndexPath.init(row: self.selectedIndex, section: 0)
                    if let cell = self.tblChat.cellForRow(at: indexpath) as? AudioChatSenderCell {
                        cell.lblAudioTime.text = self.currentAudioDuration+"/"+strMax
                    }
                    if let cell = self.tblChat.cellForRow(at: indexpath) as? AudioChatReceiverCell {
                        cell.lblAudioTime.text = self.currentAudioDuration+"/"+strMax
                    }
                    if let cell = self.tblChatLandScape.cellForRow(at: indexpath) as? AudioChatSenderCell {
                        cell.lblAudioTime.text = self.currentAudioDuration+"/"+strMax
                    }
                    if let cell = self.tblChatLandScape.cellForRow(at: indexpath) as? AudioChatReceiverCell {
                        cell.lblAudioTime.text = self.currentAudioDuration+"/"+strMax
                    }
//                    if cell != nil {
//                        cell!.lblAudioTime.text = self.currentAudioDuration+"/"+strMax
//                    }
//                    if cell2 != nil {
//                        cell2?.lblAudioTime.text = self.currentAudioDuration+"/"+strMax
//                    }
                   // self.lblAudioTime.text = strCurrent+"/"+strMax
//
                }
                
            })
            NotificationCenter.default.addObserver(self, selector: #selector(self.finishedPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
    @objc func finishedPlaying( _ myNotification:NSNotification) {
        if getVideoUrl() == getAudioFileURL() {
            let targetTime:CMTime = CMTimeMake(value: 0, timescale: 1)
            audioPlayer?.seek(to: targetTime)
            btnRecordPlayPause.isSelected = false
            btnRecordPlayPauseLandscape.isSelected = false
          //  lblTimeAudio.text = "00:00"
           // lblTimeAudioLandscape.text = "00:00"
            return
        }
        //btnPlayPause.isSelected = false
        //reset player when finish
        let maxDuration = Int(self.playerItem?.duration.seconds ?? 0)
        let strMax = formatSecondsToString(maxDuration)
        let indexpath = IndexPath.init(row: selectedIndex, section: 0)
        if let cell = tblChat.cellForRow(at: indexpath) as? AudioChatSenderCell {
        cell.lblAudioTime.text = "00:00/"+strMax
        cell.btnPlayPause.isSelected = false
        }
        if let cell = tblChat.cellForRow(at: indexpath) as? AudioChatReceiverCell {
        cell.lblAudioTime.text = "00:00/"+strMax
        cell.btnPlayPause.isSelected = false
        }
        if let cell = tblChatLandScape.cellForRow(at: indexpath) as? AudioChatSenderCell {
        cell.lblAudioTime.text = "00:00/"+strMax
        cell.btnPlayPause.isSelected = false
        }
        if let cell = tblChatLandScape.cellForRow(at: indexpath) as? AudioChatReceiverCell {
        cell.lblAudioTime.text = "00:00/"+strMax
        cell.btnPlayPause.isSelected = false
        }
        self.currentAudioDuration = "00:00"
        selectedIndex = -1
      //  self.lblAudioTime.text = "00:00/"+strMax
        let targetTime:CMTime = CMTimeMake(value: 0, timescale: 1)
        audioPlayer?.seek(to: targetTime)
    }
    func getVideoUrl() -> URL? {
        let asset = self.audioPlayer?.currentItem?.asset
        if asset == nil {
            return nil
        }
        if let urlAsset = asset as? AVURLAsset {
            return urlAsset.url
        }
        return nil
    }
    
    func handlePusherCallback(data:String) {
        if let jsonData = data.convertToDictionary() {
            myPlayer.disableControlView()
            let controlData = ControlSocketObject.init(dict: jsonData)
            if controlData.isStart {
                if heightPlayerView.constant < 230 {
                btnWatchMovieTapped(btnWatchMovie)
                }
            }
            if controlData.currentDuration > 0 {
                
                if ((controlData.currentDuration/1000)-Int(self.CurrentVideoDuration)) > 10 || ((controlData.currentDuration/1000)-Int(self.CurrentVideoDuration)) < -10 {
                myPlayer.seek(TimeInterval(controlData.currentDuration/1000)) {
                    
                }
                }
            }
            if controlData.isPlay {
                myPlayer.play()
            } else {
                myPlayer.pause()
            }
            if controlData.isEnd {
                myPlayer.pause()
                if self.isFullscreen {
                    appDel.myOrientation = .portrait
                    UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
                    UIApplication.shared.setStatusBarHidden(false, with: .fade)
                    UIApplication.shared.statusBarOrientation = .portrait
                }
                myPlayer.pause(allowAutoPlay: false)
                myPlayer.avPlayer?.replaceCurrentItem(with: nil)
                self.heightPlayerView.constant = 0
                self.heightBannerView.constant = 230
                DispatchQueue.main.async {
                    self.mainScrollView.setContentOffset(.zero, animated: true)
                    self.view.layoutIfNeeded()
                    self.view.updateConstraintsIfNeeded()
                }
                alertOk(title: "", message: "Watch party has been ended", buttonTitle: "Ok") { result in
                    self.btnEndWatchParty.isSelected = false
                    self.btnBackTapped(self.btnTopBack)
                    self.pusher.disconnect()
                    if self.channel != nil {
                        self.channel.unbindAll()
                    }
                }

            }
        }
    }
    //MARK:- Button Tapped Events
    @IBAction func btnPaymentDoneTapped(_ sender: UIButton) {
        appDel.isWatchMovie = false
        self.viewPaymentDone.isHidden = true
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
//            myPlayer.pause(allowAutoPlay: false)
//                    myPlayer.pause()
//                    print("allowAutoPlay: false==>")
//                    if self.heightPlayerView.constant == 0{
//                        self.navigationController?.popViewController(animated: true)
//                    }else{
//                        self.heightPlayerView.constant = 0
//                        self.heightBannerView.constant = 230
//                        DispatchQueue.main.async {
//                            self.mainScrollView.setContentOffset(.zero, animated: true)
//                            self.view.layoutIfNeeded()
//                            self.view.updateConstraintsIfNeeded()
//                        }
//                }
        
        if isWatchParty {
            self.btnEndWatchPartyAction(self)
            return
        }
        if btnEndWatchParty.isSelected {
            self.btnEndWatchPartyAction(self)
            return
        }
        
        if self.CurrentVideoDuration > (self.totalVideoDuration / 2.0){
            self.OpenReviewPopup()
        }else{
          //  myPlayer.pause(allowAutoPlay: false)
            myPlayer.playerLayer?.resetPlayer()
            myPlayer.playerLayer?.removeObersers()
            myPlayer.playerLayer?.timer?.invalidate()
         //   myPlayer.pause()
            myPlayer.playerLayer?.player = nil
            myPlayer.playerLayer?.removeFromSuperview()
            
            myPlayer.removeFromSuperview()
            preparePlayer(iscall: false)
            if self.heightPlayerView.constant == 0{
                self.navigationController?.popViewController(animated: true)
            }else{
                self.heightPlayerView.constant = 0
                self.heightBannerView.constant = 230
                DispatchQueue.main.async {
                    self.mainScrollView.setContentOffset(.zero, animated: true)
                    self.view.layoutIfNeeded()
                    self.view.updateConstraintsIfNeeded()
                }
            }
        }
        
    }
    
    @IBAction func btnWatchTrailerTapped(_ sender: UIButton) {
        
      //  appDel.myOrientation = .all
        print("is it All==>")
        self.myPlayer.playerLayer?.resetPlayer()
        self.preparePlayer(iscall: true)
        self.heightPlayerView.constant = 230
        self.heightBannerView.constant = 0
        self.isTrailer = true
        self.setupPlayerResource(strUrl: movieDetail.movie_trailer)
        myPlayer.play()
        DispatchQueue.main.async {
            self.mainScrollView.setContentOffset(.zero, animated: true)
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        }
    }
    
    @IBAction func btnWatchPartyAction(_ sender: Any) {
        setupDescCast2(tag: 3)
    }
    func PlayVideoOnClick(){
       // appDel.myOrientation = .all
        print("is it All==>")
        if isWatchParty {
            btnEndWatchParty.isHidden = false
            btnStartWatchParty.isHidden = true
                    let dataEvent = ControlSocketObject.init(isStart: true, currentDuration: 0, isPlay: true, isEnd: false)
                    self.channel.trigger(eventName: self.eventName, data: dataEvent.toDictionary())
            
          
        }
        self.myPlayer.playerLayer?.resetPlayer()
        self.preparePlayer(iscall: true)
        self.heightPlayerView.constant = 230
        self.heightBannerView.constant = 0
        self.isTrailer = false
        let arr = Common.GetDownloadedVideofromLibrary(strUrl: self.movieDetail.final_video)
        if arr != ""{
            self.setupPlayerResource_local(strUrl: arr)
        }else{
            self.apiCallAddMyRecent()
            self.setupPlayerResource(strUrl: movieDetail.final_video)
        }
        DispatchQueue.main.async {
            self.mainScrollView.setContentOffset(.zero, animated: true)
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        }
        

    }
    @IBAction func btnWatchMovieTapped(_ sender: UIButton) {
        if sender == btnWatchMovie {
            isWatchParty = false
        }
        if appDel.isAppLive == appStatus.Live{
            if ((movieDetail.isPayPerView == "1")){
                if movieDetail.is_payment_done == "1" || movieDetail.is_host_subscribed == "1" {
                    self.PlayVideoOnClick()
                }else{
                    let objTab = PaymentPopupVC(nibName: "PaymentPopupVC", bundle: nil)
                    objTab.isFrom = .advertisement
                    objTab.PopupFile = .PaidNow
                    objTab.movieID = movieID
                    objTab.price = self.movieDetail.payPerViewPrice
                    objTab.modalPresentationStyle = .overFullScreen
                    objTab.blockPayClick = {
                        let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                        objTab.isFromSubscribe = .PayPerView
                        objTab.isfromLogin = .MoviePlaying
                        objTab.PayPerViewPrice = self.movieDetail.payPerViewPrice
                        objTab.movieSelectedMovieID = self.movieDetail._id
                        self.navigationController!.pushViewController(objTab, animated: true)
                    }
                    self.present(objTab, animated: true, completion: nil)
                }
            }else{
                if appDel.is_Subscribed || movieDetail.is_host_subscribed == "1"  {
                    self.PlayVideoOnClick()
            }else{
                    let viewController = SubscriptionListVC(nibName: "SubscriptionListVC", bundle: nil)
                    viewController.isfrom = .MoviePlaying
                    self.navigationController!.pushViewController(viewController, animated: true)
            }
          }
        }else{
           // appDel.myOrientation = .all
            print("is it All==>")
            self.preparePlayer(iscall: true)
            self.heightPlayerView.constant = 230
            self.heightBannerView.constant = 0
            self.isTrailer = false
            self.setupPlayerResource(strUrl: movieDetail.final_video)
            DispatchQueue.main.async {
                self.mainScrollView.setContentOffset(.zero, animated: true)
                self.view.layoutIfNeeded()
                self.view.updateConstraintsIfNeeded()
            }
            self.apiCallAddMyRecent()
        }
    }
    
    @IBAction func btnDescriptionTapped(_ sender: UIButton) {
        setupDescCast2(tag: 1)
    }
    
    @IBAction func btnCastTapped(_ sender: UIButton) {
        setupDescCast2(tag: 2)
    }
    
    @IBAction func btnMyListTapped(_ sender: UIButton) {
        self.apiCallAddMyList()
    }
    
    @IBAction func btnRateTapped(_ sender: UIButton) {
        if self.movieDetail.my_rating == "0.0" || self.movieDetail.my_rating == "0" || self.movieDetail.my_rating == "" {
            self.btnGiveRate.isHidden = false
            self.ratingBar.isHidden = false
            self.Heightviewrate.constant = 100
            self.view.updateConstraintsIfNeeded()
            
            let y = ((self.mainScrollView.contentSize.height - self.mainScrollView.bounds.size.height) + 100) - self.heightcollRelatedMovie.constant
            let bottomOffset:CGPoint = CGPoint(x: 0, y: y)
            self.mainScrollView.setContentOffset(bottomOffset, animated: true)
        } else {
        }
    }
    
    @IBAction func btnEposodesTapped(_ sender: UIButton) {
        self.lblEposides.isHidden = false
        self.lblMoreLike.isHidden = true
        self.setupRelatedMoiewView(tag: 1)

        print("Episodes")
    }
    
    @IBAction func btnMoreLikeTapped(_ sender: UIButton) {
        self.lblEposides.isHidden = true
        self.lblMoreLike.isHidden = false
        self.setupRelatedMoiewView(tag: 2)
        print("More Likre")
    }
    
    @IBAction func btnShareTapped(_ sender: UIButton) {
        //https://cinestool.com/api
        if sender.tag == 0 {
            getSharedLink(partyCode: "")
        } else {
            if !appDel.is_Subscribed {
                self.btnWatchMovieTapped(btnStartWatchParty)
                return
            }
       apiCallGetPartyCode()
        }
        //self.presentShareActivity(longDynamicLink)
    }
    
    @IBAction func btnCloseReplyAction(_ sender: Any) {
        viewForReply.isHidden = true
        viewForReplyLandscape.isHidden = true
    }
    
    @IBAction func btnRecordPlayPauseAction(_ sender: UIButton) {
        if self.myPlayer.isPlaying {return}
        if getVideoUrl() != getAudioFileURL() {
            playerItem = AVPlayerItem.init(url: getAudioFileURL())
            audioPlayer = AVPlayer.init(playerItem: playerItem)
            audioPlayer?.addPeriodicTimeObserver(forInterval: CMTime.init(seconds: 1, preferredTimescale: 1), queue: DispatchQueue.main, using: { time in
                if self.audioPlayer?.currentItem?.status == .readyToPlay {
                  //  let maxDuration = Int(self.playerItem?.duration.seconds ?? 0)
                    let currentDuration = Int(self.playerItem?.currentTime().seconds ?? 0)
                   // let strMax = formatSecondsToString(maxDuration)
                    self.lblTimeAudio.text = formatSecondsToString(currentDuration)
                    self.lblTimeAudioLandscape.text = formatSecondsToString(currentDuration)
                   // self.lblAudioTime.text = strCurrent+"/"+strMax
//
                }
                
            })
            NotificationCenter.default.addObserver(self, selector: #selector(self.finishedPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        }
        if sender.isSelected {
            audioPlayer?.pause()
        } else {
            audioPlayer?.play()
        }
        sender.isSelected.toggle()
    }
    
    @IBAction func btnChatAction(_ sender: UIButton) {
        if arrParticipate.count > 1 {
        }
        else {
           alertOk(title: "", message: "Chat will start when participants join the Watch Party.")
            return
       }
        viewParticipate.isHidden = true
        viewVoiceChat.isHidden = true
        viewVoiceChatLandscape.isHidden = true
        viewForReply.isHidden = true
        viewForReplyLandscape.isHidden = true
        btnStopRecording.isSelected = false
        selectedIndex = -1
      
        if sender.tag == 0 {
            
            if self.arrChats.isEmpty {
                getMessages()
            }
            else {
                tblChat.reloadData()
                self.tblChat.scrollToRow(at: IndexPath.init(row: self.arrChats.count-1, section: 0), at: .bottom, animated: true)
            }
            hideChat(isHide: false)
            btnChat.backgroundColor = .themeColor
            btnChat.setTitleColor(.white, for: .normal)
            btnParticipants.backgroundColor = .clear
            btnParticipants.setTitleColor(.themeColor, for: .normal)
            mainScrollView.setContentOffset(CGPoint.init(x: mainScrollView.contentOffset.x, y: 0), animated: false)
            chatView.isHidden = false
            
        } else if sender.tag == 1 {
            if self.arrChats.isEmpty {
                getMessages()
            } else {
                tblChatLandScape.reloadData()
                self.tblChatLandScape.scrollToRow(at: IndexPath.init(row: self.arrChats.count-1, section: 0), at: .bottom, animated: true)
            }
            
            myPlayer.snp.remakeConstraints { (make) in
                
                make.left.equalTo(self.viewLandscape.snp.left)
                make.width.equalTo(self.viewLandscape.frame.width - chatViewLandscape.frame.width - 25)
                make.top.equalTo(self.viewLandscape.snp.top)
                make.height.equalTo(self.viewLandscape.snp.height)
                make.bottom.equalTo(self.viewLandscape.snp.bottom)

            }
            chatViewLandscape.isHidden = false
        } else {
            if self.arrChats.isEmpty {
                getMessages()
            }else {
                self.tblChat.scrollToRow(at: IndexPath.init(row: self.arrChats.count-1, section: 0), at: .bottom, animated: true)
            }
            hideChat(isHide: false)
            btnChat.backgroundColor = .themeColor
            btnChat.setTitleColor(.white, for: .normal)
            btnParticipants.backgroundColor = .clear
            btnParticipants.setTitleColor(.themeColor, for: .normal)
            
        }
        
        
       
        mainScrollView.isScrollEnabled = false
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
       
        if txtChat.text.isEmpty && txtChatLandScape.text.isEmpty {
            //alertOk(title: "", message: "Please type a message.")
            return
        }
      //  self.CallLastReadMsg(strMsg: txtMsg.text!)
        if viewForReply.isHidden && !isReplyHide && viewForReplyLandscape.isHidden {
            sendMessage(tag: sender.tag)
        } else {
            let replyObject = arrChats[selectedReply]
            
            
            sendMessage(tag: sender.tag, audioUrl: "", duration: "", reply: replyObject.message, replyName: replyObject.senderName, replyId: replyObject.messageId,replyDuration: replyObject.duration,replySenderId: replyObject.senderId)
            
        }
        
        txtChat.text = ""
        txtChatLandScape.text = ""
    }
    @IBAction func btnCloseChat(_ sender: UIButton) {
        self.view.endEditing(true)
        txtChat.text = ""
        txtChatLandScape.text = ""
        stopRecording()
        heightForTxtChat.constant = 33
        heightTxtChatLandscape.constant = 33
        mainScrollView.isScrollEnabled = true
        audioPlayer?.pause()
        if sender.tag == 0 {
        chatView.isHidden = true
        
        } else {
            chatViewLandscape.isHidden = true
            myPlayer.snp.remakeConstraints { (make) in
                
                make.left.equalTo(self.viewLandscape.snp.left)
                make.width.equalTo(self.viewLandscape.snp.width)
                make.top.equalTo(self.viewLandscape.snp.top)
                make.height.equalTo(self.viewLandscape.snp.height)
                make.bottom.equalTo(self.viewLandscape.snp.bottom)

            }
        }
    }
    
    func getSharedLink(partyCode:String) {
        
        let shareLink:String = partyCode.isEmpty ? "https://tophubonline.com/shareableMovie/?movie_id=\(self.movieID)" : "https://tophubonline.com/shareableMovie/?movie_id=\(self.movieID)?PartyCode=\(partyCode)"
        guard let newSharelink = URL(string: shareLink) else { return }
        let components = DynamicLinkComponents.init(link: newSharelink, domainURIPrefix: self.domain)
        
        let iOSParams = DynamicLinkIOSParameters(bundleID: Params.bundleID.rawValue)
        iOSParams.minimumAppVersion = Params.minimumAppVersion.rawValue
        iOSParams.appStoreID = Params.appStoreID.rawValue
        iOSParams.customScheme = Params.bundleID.rawValue
        components?.iOSParameters = iOSParams
        
        let androidParams = DynamicLinkAndroidParameters(packageName: Params.packageName.rawValue)
        androidParams.minimumVersion = Int(Params.minimumVersion.rawValue)!
        components?.androidParameters = androidParams
        
        let options = DynamicLinkComponentsOptions()
        //options.pathLength = .short
        options.pathLength = .unguessable
        components?.options = options

        guard let longDynamicLink = components?.url else { return }
        print("The long URL is: \(longDynamicLink)")
        
//        self.longLink = components?.url
//        print("Dharam",longLink as Any)
        
        components?.shorten { (shortURL, warnings, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            let shortLink = shortURL
            print(shortLink!)
            self.presentShareActivity(shortLink!)
        }
    }
    private func presentShareActivity(_ url: URL?) {
        let promotText = "\(movieDetail.name)"
        let activityController = UIActivityViewController(activityItems: [promotText, url], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        activityController.popoverPresentationController?.sourceRect = self.view.frame
        self.present(activityController, animated: true, completion: nil)
    }
    
    @IBAction func btnGiveRatingAction(_ sender: UIButton) {
        self.btnGiveRate.isHidden = true
        self.ratingBar.isHidden = true
        self.Heightviewrate.constant = 0
        self.view.updateConstraintsIfNeeded()
        if self.givenRating != "" {
            apiCallGiveRating()
        }
    }
    
    @IBAction func btnCloseParticipateAction(_ sender: UIButton) {
        if sender.tag == 0 {
            viewParticipate.isHidden = true
        } else {
            viewParticipateLandscape.isHidden = true
        }
    }
    @IBAction func btnParticipateAction(_ sender: UIButton) {
//        if self.movieDetail.movie_party_list.count > 1 {
//        }
//        else {
//           alertOk(title: "", message: "There are no participants joined for watch party.")
//            return
//       }
        if sender.tag == 0 {
            viewParticipate.isHidden = false
            tblParticipate.reloadData()
            btnParticipants.backgroundColor = .themeColor
            btnParticipants.setTitleColor(.white, for: .normal)
            btnChat.backgroundColor = .clear
            btnChat.setTitleColor(.themeColor, for: .normal)
        }
        else if sender.tag == 1 {
            viewParticipateLandscape.isHidden = false
            tblParticipateLandscape.reloadData()
        } else {
            mainScrollView.setContentOffset(CGPoint.init(x: mainScrollView.contentOffset.x, y: 0), animated: false)
            chatView.isHidden = false
            viewParticipate.isHidden = false
            tblParticipate.reloadData()
            btnParticipants.backgroundColor = .themeColor
            btnParticipants.setTitleColor(.white, for: .normal)
            btnChat.backgroundColor = .clear
            btnChat.setTitleColor(.themeColor, for: .normal)
        }
       
    }
    
    @IBAction func btnStopRecordingAction(_ sender: UIButton) {
        if sender.isSelected  {
            handleAudioSendWith()
            
        } else {
            sender.isSelected = true
            btnRecordPlayPause.isHidden = false
            btnRecordPlayPauseLandscape.isHidden = false
            stopRecording()
        }
        
        
    }
    
    @IBAction func btnCancelRecording(_ sender: Any) {
        stopRecording()
        viewVoiceChat.isHidden = true
        viewVoiceChatLandscape.isHidden = true
        
        if getVideoUrl() == getAudioFileURL() {
            audioPlayer?.pause()
            audioPlayer?.replaceCurrentItem(with: nil)
        }
    }
    
    @IBAction func btnStartRecording(_ sender: Any) {
        
        self.view.endEditing(true)
        btnRecordPlayPause.isHidden = true
        btnRecordPlayPauseLandscape.isHidden = true
        btnRecordPlayPause.isSelected = false
        btnRecordPlayPauseLandscape.isSelected = false
        txtChat.text = ""
        txtChatLandScape.text = ""
        lblTimeAudio.text = "00:00"
        lblTimeAudioLandscape.text = "00:00"
        btnStopRecording.isSelected = false
        btnStopLandscape.isSelected = false
        heightForTxtChat.constant = 38
        heightTxtChatLandscape.constant = 38
        startRecording()
        
    }
    
    @IBAction func btnStartWatchPartyAction(_ sender: Any) {
        if !appDel.is_Subscribed {
            self.btnWatchMovieTapped(btnStartWatchParty)
            return
        }
        
        
        if self.host_user_id.isEmpty || self.arrParticipate.count < 2 {

           // alertOk(title: "", message: "Please share at least one watch party link to your friends.")
            alertOk(title: "", message: "You can start watch party when participants join the Watch Party.")
             return
        }
        disableViewsForParticipants()
        isWatchParty = true
        self.btnWatchMovieTapped(btnStartWatchParty)
    }
    
    @IBAction func btnEndWatchPartyAction(_ sender: Any) {
        if btnEndWatchParty.isSelected {
            alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to leave watch party?") { result in
                if result {
                    self.apiCallChangePartyStatus(status: "1")
                }
            }
            
        } else {
            alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to end watch party?") { result in
                if result {
                    let dataEvent = ControlSocketObject.init(isStart: false, currentDuration: 0, isPlay: true, isEnd: true)
                    self.channel.trigger(eventName: self.eventName, data: dataEvent.toDictionary())
                    self.apiCallChangePartyStatus(status: "2")
                }
            }
            
        }
       
    }
    
    open override var shouldAutorotate: Bool {
        get {
            return false
        }
    }
    
   
}

extension NewMoviewDetailVC:UITableViewDelegate,UITableViewDataSource{
    //MARK:- Tableview Delegate & Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblChat || tableView == tblChatLandScape {
            return  arrChats.count
        }
        if tableView == tblParticipate || tableView == tblParticipateLandscape {
            return arrParticipate.count
        }
        if self.movieDetail != nil {
            if self.movieDetail.cast_data.count != 0 {
                return self.movieDetail.cast_data.count
            } else {
                return 1
            }
        } else {
            return 0
        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
   
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if let panGestureRecognizer = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = panGestureRecognizer.translation(in: panGestureRecognizer.view?.superview)
            
            if abs(translation.x) > abs(translation.y)  {

                    return true
                }

                return false
            }
        return false
    }
    @objc func handleReply(sender:UIButton) {
        showReply(tag: sender.tag)
    }
    @objc func handleSwipe(sender:UIPanGestureRecognizer) {
        print("gesture func")
        guard let tag = sender.view?.tag, let _ = sender.view else {return}
    
        let translation = sender.translation(in: sender.view)
        if let cell = sender.view as? ReceiverCell {
            if sender.state == .began {
                
            }
            if sender.state == .changed && translation.x > 20 {
                if translation.x < cell.frame.width/5 {
                  //  cell.imgReply.isHidden = false
                    cell.viewForMain.transform = CGAffineTransform.init(translationX: translation.x, y: 0)
                } else {
                    UIView.animate(withDuration: 0.3) {
                        cell.viewForMain.transform = .identity
                        
                    } completion: { success in
                     //   cell.imgReply.isHidden = true
                        self.showReply(tag: tag)
                    }

                }
            }
            if sender.state == .ended {
                if cell.viewForMain.transform != .identity {
                    UIView.animate(withDuration: 0.3) {
                        cell.viewForMain.transform = .identity
                        
                    } completion: { success in
                      //  cell.imgReply.isHidden = true
                    }
                }
            }
        }
        if let cell = sender.view as? senderCell {
            if sender.state == .began {
                
            }
            if sender.state == .changed && translation.x > 10 {
                if translation.x < cell.frame.width/5 {
                   // cell.imgReply.isHidden = false
                    cell.viewForMain.transform = CGAffineTransform.init(translationX: translation.x, y: 0)
                } else {
                    UIView.animate(withDuration: 0.3) {
                        cell.viewForMain.transform = .identity
                        
                    } completion: { success in
                       // cell.imgReply.isHidden = true
                        self.showReply(tag: tag)
                    }

                }
            }
            if sender.state == .ended {
                if cell.viewForMain.transform != .identity {
                    UIView.animate(withDuration: 0.3) {
                        cell.viewForMain.transform = .identity
                        
                    } completion: { success in
                      //  cell.imgReply.isHidden = true
                    }
                }
            }
        }
        if let cell = sender.view as? AudioChatReceiverCell {
            if sender.state == .began {
                
            }
            if sender.state == .changed && translation.x > 10 {
                if translation.x < cell.frame.width/5 {
                  //  cell.imgReply.isHidden = false
                    cell.viewForMain.transform = CGAffineTransform.init(translationX: translation.x, y: 0)
                } else {
                    UIView.animate(withDuration: 0.3) {
                        cell.viewForMain.transform = .identity
                        
                    } completion: { success in
                     //   cell.imgReply.isHidden = true
                        self.showReply(tag: tag)
                    }

                }
            }
            if sender.state == .ended {
                if cell.viewForMain.transform != .identity {
                    UIView.animate(withDuration: 0.3) {
                        cell.viewForMain.transform = .identity
                        
                    } completion: { success in
                       // cell.imgReply.isHidden = true
                    }
                }
            }
        }
        if let cell = sender.view as? AudioChatSenderCell {
            if sender.state == .began {
                
            }
            if sender.state == .changed && translation.x > 10 {
                if translation.x < cell.frame.width/5 {
                  //  cell.imgReply.isHidden = false
                    cell.viewForMain.transform = CGAffineTransform.init(translationX: translation.x, y: 0)
                } else {
                    UIView.animate(withDuration: 0.3) {
                        cell.viewForMain.transform = .identity
                        
                    } completion: { success in
                      //  cell.imgReply.isHidden = true
                        self.showReply(tag: tag)
                    }

                }
            }
            if sender.state == .ended {
                if cell.viewForMain.transform != .identity {
                    UIView.animate(withDuration: 0.3) {
                        cell.viewForMain.transform = .identity
                        
                    } completion: { success in
                      //  cell.imgReply.isHidden = true
                    }
                }
            }
        }
//            if sender.state == .began {
//
//            }
//            if sender.state == .changed && translation.x > 0 {
//                if translation.x < UIScreen.main.bounds.width/5 {
//                    sender.view!.transform = CGAffineTransform.init(translationX: translation.x, y: 0)
//                } else {
//                    UIView.animate(withDuration: 0.3) {
//                        sender.view!.transform = .identity
//
//                    } completion: { success in
//                        self.showReply(tag: tag)
//                    }
//
//                }
//            }
//            if sender.state == .ended {
//                if sender.view!.transform != .identity {
//                    UIView.animate(withDuration: 0.3) {
//                        sender.view!.transform = .identity
//
//                    } completion: { success in
//
//                    }
//                }
//            }
        
        
      
    }
    func showReply(tag:Int) {
        if arrChats[tag].duration.isEmpty {
            lblMsg.text = arrChats[tag].message
            imgMicrophone.isHidden = true
            
            lblMsgLandscape.text = arrChats[tag].message
            imgMicrophoneLandscape.isHidden = true
        } else {
            lblMsg.text = arrChats[tag].duration
            imgMicrophone.isHidden = false
            
            lblMsgLandscape.text = arrChats[tag].duration
            imgMicrophoneLandscape.isHidden = false
        }
        
        lblSenderName.text = arrChats[tag].senderId == appDel.loggedInUserData?._id ? "You":arrChats[tag].senderName
        lblSenderNameLandscape.text = arrChats[tag].senderId == appDel.loggedInUserData?._id ? "You":arrChats[tag].senderName
        selectedReply = tag
        viewForReply.isHidden = false
        viewForReplyLandscape.isHidden = false
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblChat || tableView == tblChatLandScape {
            if arrChats[indexPath.row].audioUrl.isEmpty {
            if arrChats[indexPath.row].senderId != (appDel.loggedInUserData?._id ?? "") {
                let cell:ReceiverCell = tableView.dequeueReusableCellTb(for: indexPath)
                cell.setData(data: arrChats[indexPath.row])
                cell.tag = indexPath.row
                cell.btnReply.tag = indexPath.row
                cell.imgReply.isHidden = true
                cell.btnReply.addTarget(self, action: #selector(handleReply(sender:)), for: .touchUpInside)
                let swipe = UIPanGestureRecognizer.init(target: self, action: #selector(handleSwipe(sender:)))
                swipe.delegate = self
                cell.addGestureRecognizer(swipe)
                cell.layoutIfNeeded()
                return cell
            }
          
            else {
                let cell:senderCell = tableView.dequeueReusableCellTb(for: indexPath)
                cell.setData(data: arrChats[indexPath.row])
                cell.imgReply.isHidden = true
                cell.tag = indexPath.row
                cell.btnReply.tag = indexPath.row
                cell.btnReply.addTarget(self, action: #selector(handleReply(sender:)), for: .touchUpInside)
                let swipe = UIPanGestureRecognizer.init(target: self, action: #selector(handleSwipe(sender:)))
                swipe.delegate = self
                cell.addGestureRecognizer(swipe)
                cell.layoutIfNeeded()
                return cell
            
            }
            }else {
                if arrChats[indexPath.row].senderId != (appDel.loggedInUserData?._id ?? "") {
                    let cell:AudioChatReceiverCell = tableView.dequeueReusableCellTb(for: indexPath)
                    cell.setData(data: arrChats[indexPath.row])
                    cell.imgReply.isHidden = true
                    cell.tag = indexPath.row
                    cell.btnReply.tag = indexPath.row
                    cell.btnReply.addTarget(self, action: #selector(handleReply(sender:)), for: .touchUpInside)
                    let swipe = UIPanGestureRecognizer.init(target: self, action: #selector(handleSwipe(sender:)))
                    swipe.delegate = self
                    cell.addGestureRecognizer(swipe)
                    if indexPath.row == selectedIndex {
                        cell.btnPlayPause.isSelected = true
                        cell.lblAudioTime.text = self.currentAudioDuration+"/"+arrChats[indexPath.row].duration
                    } else {
                        cell.btnPlayPause.isSelected = false
                        cell.lblAudioTime.text = "00:00/"+arrChats[indexPath.row].duration
                    }
                    cell.onClickBtnPlay = {
                        if self.myPlayer.isPlaying {return}
                        cell.btnPlayPause.isSelected.toggle()
                        
                        self.selectedIndex = indexPath.row
                        if cell.btnPlayPause.isSelected {
                            tableView.reloadData()
                            self.setupAudioPlayer(audioUrl: self.arrChats[indexPath.row].audioUrl, cell: nil, cell2: cell)
                            self.audioPlayer?.play()
                        } else {
                            self.audioPlayer?.pause()
                        }
                        
                    }
                    
                    return cell
                }
              
                else {
                    let cell:AudioChatSenderCell = tableView.dequeueReusableCellTb(for: indexPath)
                    cell.setData(data: arrChats[indexPath.row])
                    cell.imgReply.isHidden = true
                    //cell.btnPlayPause.tag = indexPath.row
                   // cell.btnPlayPause.addTarget(self, action: #selector(handlePlayPause(sender:)), for: .touchUpInside)
                    cell.tag = indexPath.row
                    cell.btnReply.tag = indexPath.row
                    cell.btnReply.addTarget(self, action: #selector(handleReply(sender:)), for: .touchUpInside)
                    let swipe = UIPanGestureRecognizer.init(target: self, action: #selector(handleSwipe(sender:)))
                    swipe.delegate = self
                    cell.addGestureRecognizer(swipe)
                    if indexPath.row == selectedIndex {
                        cell.btnPlayPause.isSelected = true
                        cell.lblAudioTime.text = self.currentAudioDuration+"/"+arrChats[indexPath.row].duration
                    } else {
                        cell.btnPlayPause.isSelected = false
                        cell.lblAudioTime.text = "00:00/"+arrChats[indexPath.row].duration
                    }
                    cell.onClickBtnPlay = {
                        if self.myPlayer.isPlaying {return}
                        cell.btnPlayPause.isSelected.toggle()
                        
                        self.selectedIndex = indexPath.row
                        if cell.btnPlayPause.isSelected {
                            tableView.reloadData()
                            self.setupAudioPlayer(audioUrl: self.arrChats[indexPath.row].audioUrl, cell: cell, cell2: nil)
                            self.audioPlayer?.play()
                        } else {
                            self.audioPlayer?.pause()
                        }
                        
                    }
                    return cell
                }
                
            }
        }
        else if tableView == tblParticipate || tableView == tblParticipateLandscape {
            let cell:ParticipateCell = tableView.dequeueReusableCellTb(for: indexPath)
            //cell.imgUser.backgroundColor = .white
            cell.lblUserName.textColor = .white
            cell.lblUserName.text = arrParticipate[indexPath.row].user_name
            cell.imgUser.sd_setImage(with: URL(string: arrParticipate[indexPath.row].user_profile), placeholderImage: nil)
            cell.imgUser.contentMode = .scaleAspectFill
            return cell
        }
        let cell: MovieCastTblCell = tableView.dequeueReusableCellTb(for: indexPath)
        if self.movieDetail.cast_data.count != 0 {
            cell.lblcastType.text = self.movieDetail.cast_data[indexPath.row].cast_type + " : "
            cell.lblcastName.text = self.movieDetail.cast_data[indexPath.row].cast_name
            cell.lblNoInfo.text = ""
        } else {
            cell.stackView.isHidden = true
            cell.lblNoInfo.text = "No Information"
        }

        if self.isCastTapped{
            heighttblCast.constant = self.tblCast.contentSize.height
        }else{
            heighttblCast.constant = 0
        }
        return cell
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblChat || tableView == tblChatLandScape || tableView == tblParticipate || tableView == tblParticipateLandscape {
            return UITableView.automaticDimension
        }
        return 55
    }
   
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        
    }
//    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        if tableView == tblChat || tableView == tblChatLandScape {
//            let action = UIContextualAction.init(style: .normal, title: "") {action,view,success in
//                print("reply", action)
//                success(true)
//            }
//            action.image = UIImage.init(named: "share")
//            action.backgroundColor = .black
//            let config = UISwipeActionsConfiguration.init(actions: [action])
//            config.performsFirstActionWithFullSwipe = true
//            return config
//        }
//       return nil
//    }
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
    
//
}

extension NewMoviewDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isEpisodeTapped{
            return self.relatedMovies.count
        }else{
            return self.EpisodeMovies.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        if isEpisodeTapped{
//            let cell: HomeUnderTblCollectionCell = collectionView.dequeueReusableCell(for: indexPath)
//            let url = URL(string: relatedMovies[indexPath.row].poster_image)
//            cell.imgMoviePoster.sd_setImage(with: url, placeholderImage: nil)
//            cell.lblName.text = relatedMovies[indexPath.row].name
//
//            let heightShrt:CGFloat = self.collRelatedMovie.collectionViewLayout.collectionViewContentSize.height
//            self.heightcollRelatedMovie.constant = heightShrt
//            self.view.layoutIfNeeded()
//            self.view.updateFocusIfNeeded()
//            DispatchQueue.main.async {
//                self.collRelatedMovie.collectionViewLayout.invalidateLayout()
//            }
//            return cell
//        }else{
//            let cell: CellCollEpisode = collectionView.dequeueReusableCell(for: indexPath)
//            if indexPath.item == 0 || indexPath.item == 2{
//                cell.lblEventName.text = "By default, the FCM SDK generates a registration token for the client app instance on initial startup of your app. Similar to the APNs device token, this token allows you to target notification messages to this particular instance of the app."
//            }else{
//                cell.lblEventName.text = "Episod description"
//            }
//            cell.lblEventName.sizeToFit()
//            let heightShrt:CGFloat = self.collRelatedMovie.collectionViewLayout.collectionViewContentSize.height
//            self.heightcollRelatedMovie.constant = heightShrt
//            self.view.layoutIfNeeded()
//            self.view.updateFocusIfNeeded()
//            DispatchQueue.main.async {
//                self.collRelatedMovie.collectionViewLayout.invalidateLayout()
//            }
//            return cell
//        }
        let cell: HomeUnderTblCollectionCell = collectionView.dequeueReusableCell(for: indexPath)
        if isEpisodeTapped{
            let url = URL(string: relatedMovies[indexPath.row].poster_image)
            cell.imgMoviePoster.sd_setImage(with: url, placeholderImage: nil)
            cell.lblName.text = relatedMovies[indexPath.row].name
        }else{
            let url = URL(string: EpisodeMovies[indexPath.row].poster_image)
            cell.imgMoviePoster.sd_setImage(with: url, placeholderImage: nil)
            cell.lblName.text = EpisodeMovies[indexPath.row].name
        }
        let heightShrt:CGFloat = self.collRelatedMovie.collectionViewLayout.collectionViewContentSize.height
        self.heightcollRelatedMovie.constant = heightShrt
        self.view.layoutIfNeeded()
        self.view.updateFocusIfNeeded()
        DispatchQueue.main.async {
            self.collRelatedMovie.collectionViewLayout.invalidateLayout()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if btnEndWatchParty.isSelected {
            alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to leave watch party?") { result in
                if result {
                    self.btnEndWatchPartyAction(self)
                }
            }
            return
        }
        if isWatchParty {
            alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to end watch party?") { result in
                if result {
                    self.btnEndWatchPartyAction(self)
                }
            }
            return
        }
        if isEpisodeTapped{
            self.movieID = self.relatedMovies[indexPath.row].movie_id
            self.isRatingTapped = false
            self.apiCallGetMovieDetails(callSilently: false)
        }else{
            self.movieID = self.EpisodeMovies[indexPath.row].movie_id
            self.isNextEpisodeTapped = true
            self.isRatingTapped = false
            self.apiCallGetMovieDetails(callSilently: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //if isEpisodeTapped{return 15}else{return 0}
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if isEpisodeTapped{
//            print("==3==")
//            let width = (collRelatedMovie.frame.size.width - 20) / 3
//            let height = width + 70//collectionHomeUnderTbl.frame.size.height
//            return CGSize(width: width, height: height)
//        }else{
//            print("==1==")
//            return CGSize(width: collRelatedMovie.frame.size.width, height: collRelatedMovie.frame.size.width - 140)
//        }
        let width = (collRelatedMovie.frame.size.width - 20) / 3
        let height = width + 70//collectionHomeUnderTbl.frame.size.height
        return CGSize(width: width, height: height)
    }
}

extension NewMoviewDetailVC{
    //MARK:- Setup view events
    func setUp(){
        let options = PusherClientOptions(
            authMethod: AuthMethod.endpoint(authEndpoint: "https://tophubonline.com/pusher/auth")/*AuthMethod.inline(secret: "4f446dd13ce8ea9c6612")*/, host: .cluster("mt1")
        )
        pusher = Pusher.init(withAppKey: "a71af9896fa4c9827449", options: options)
        let mylistIcon = #imageLiteral(resourceName: "Plus1").withRenderingMode(.alwaysTemplate)
        self.imgMyList.image = mylistIcon
        self.imgMyList.tintColor = #colorLiteral(red: 1, green: 0, blue: 0.2274509804, alpha: 1)
        self.imgMyList.tintColorDidChange()
        
        let rateIcon = #imageLiteral(resourceName: "Like1").withRenderingMode(.alwaysTemplate)
        self.imgRate.image = rateIcon
        self.imgRate.tintColor = #colorLiteral(red: 1, green: 0, blue: 0.2274509804, alpha: 1)
        self.imgRate.tintColorDidChange()
        
        let shareIcon = #imageLiteral(resourceName: "Send1").withRenderingMode(.alwaysTemplate)
        self.imgShare.image = shareIcon
        self.imgShare.tintColor = #colorLiteral(red: 1, green: 0, blue: 0.2274509804, alpha: 1)
        self.imgShare.tintColorDidChange()
        
        self.tblCast.register(MovieCastTblCell.self)
        self.tblChat.register(ReceiverCell.self)
        self.tblChat.register(senderCell.self)
        self.tblChatLandScape.register(ReceiverCell.self)
        self.tblChatLandScape.register(senderCell.self)
        
        self.tblChat.register(AudioChatSenderCell.self)
        self.tblChat.register(AudioChatReceiverCell.self)
        self.tblChatLandScape.register(AudioChatSenderCell.self)
        self.tblChatLandScape.register(AudioChatReceiverCell.self)
        
        
        self.tblParticipate.register(ParticipateCell.self)
        self.tblParticipateLandscape.register(ParticipateCell.self)
        self.collRelatedMovie.register(HomeUnderTblCollectionCell.self)
//        self.collRelatedMovie.register(CellCollEpisode.self)
        setupDescCast2(tag: 3)
        //self.setupRelatedMoiewView(tag: 1)
        widthRating.constant = UIScreen.main.bounds.width - 50
        if Double((widthRating.constant - 40) / 10) > 50 {
            ratingBar.settings.starSize = 50.0
            widthRating.constant = 54 * 10

        } else {
            ratingBar.settings.starSize = Double((widthRating.constant - 40) / 10)
        }
        ratingBar.settings.starMargin = 4.0
        ratingBar.didFinishTouchingCosmos = { rating in
            print(rating)
            self.givenRating = "\(rating)"
            //self.lblMyRate.text = "\(Int(rating))"
        }
        btnChatLandscape.setImage(UIImage.init(named: "comment")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit)?.sd_tintedImage(with: .white), for: .normal)
        txtChatLandScape.delegate = self
        txtChat.delegate = self
        btnWatchTrailer.titleLabel?.numberOfLines = 2
        btnWatchTrailer.titleLabel?.textAlignment = .center
        btnWatchMovie.titleLabel?.numberOfLines = 2
        btnWatchMovie.titleLabel?.textAlignment = .center
        btnWatchParty.titleLabel?.numberOfLines = 2
        btnWatchParty.titleLabel?.textAlignment = .center
        hideChat()
        btnStopRecording.setTitle("Stop", for: .normal)
        btnStopRecording.setTitle("Confirm", for: .selected)
        btnStopLandscape.setTitle("Stop", for: .normal)
        btnStopLandscape.setTitle("Confirm", for: .selected)
        btnEndWatchParty.setTitle("End Watch Party", for: .normal)
        btnEndWatchParty.setTitle("Leave Quietly", for: .selected)
        
        btnParticipantsIcon.setImage(UIImage.init(named: "ic_group_chat")?.sd_tintedImage(with: .themeColor), for: .normal)
        btnChatIcon.setImage(UIImage.init(named: "ic_chat")?.sd_tintedImage(with: .themeColor), for: .normal)
        btnShareWatchParty.setImage(UIImage.init(named: "share")?.sd_tintedImage(with: .themeColor), for: .normal)
        txtChat.textColor = .white
        txtChatLandScape.textColor = .white
    }
    func disableViewsForParticipants(disable:Bool=true) {
        btnWatchMovie.isUserInteractionEnabled = !disable
        btnWatchTrailer.isUserInteractionEnabled = !disable
        self.btnWatchTrailer.backgroundColor = disable ?  .gray:UIColor.themeColor
        self.btnWatchMovie.backgroundColor = disable ? .gray:UIColor.themeColor
        
    }
    func hideChat(isHide:Bool=true) {
        tblChat.isHidden = isHide
        viewSend.isHidden = isHide
    }
    func setupRelatedMoiewView(tag:Int){
        self.heightcollRelatedMovie.constant = 10
        if tag == 1{
            isEpisodeTapped = false
            self.EpisodeMovies = self.movieDetail.episode_array
            self.collRelatedMovie.isHidden = false
            collRelatedMovie.reloadData()
        }else{
            isEpisodeTapped = true
            self.relatedMovies = self.movieDetail.related_movies
            self.collRelatedMovie.isHidden = false
            collRelatedMovie.reloadData()
        }
//        self.view.layoutIfNeeded()
//        self.view.updateConstraintsIfNeeded()
    }
    
    func setupDescCast2(tag:Int){
        lblSepCast.isHidden = true
        lblSepDescription.isHidden = true
        lblSepWatchParty.isHidden = true
        viewWatchParty.isHidden = true
        mainScrollView.isScrollEnabled = true
        stkForWatchParty.isHidden = true
        
        if tag == 1{
            lblSepDescription.isHidden = false
            self.tblCast.isHidden = true
            self.heighttblCast.constant = 0
            self.btnCast.setTitleColor(.gray, for: .normal)
            self.btnWatchParty.setTitleColor(.gray, for: .normal)
            self.btnDescription.setTitleColor(.white, for: .normal)
            lblDesc.text = self.movieDescription
            isCastTapped = false
            btnWatchParty.isSelected = false
            tblCast.reloadData()
            heightcollRelatedMovie.constant = 10
            collRelatedMovie.reloadData()
        }else if tag == 2 {
            lblSepCast.isHidden = false
            self.tblCast.isHidden = false
            self.btnCast.setTitleColor(.white, for: .normal)
            self.btnDescription.setTitleColor(.gray, for: .normal)
            self.btnWatchParty.setTitleColor(.gray, for: .normal)
            lblDesc.text = ""
            isCastTapped = true
            btnWatchParty.isSelected = false
            self.tblCast.reloadData()
            heightcollRelatedMovie.constant = 10
            collRelatedMovie.reloadData()
        }
        else {
            lblSepWatchParty.isHidden = false
            lblDesc.text = ""
            self.heighttblCast.constant = 0
            self.btnWatchParty.setTitleColor(.white, for: .normal)
            self.btnWatchParty.setTitleColor(.white, for: .selected)
            self.btnWatchParty.backgroundColor = .clear
            self.btnDescription.setTitleColor(.gray, for: .normal)
            self.btnCast.setTitleColor(.gray, for: .normal)
            viewWatchParty.isHidden = false
            stkForWatchParty.isHidden = false
          //  mainScrollView.isScrollEnabled = false
            btnWatchParty.isSelected = true
         //   mainScrollView.setContentOffset(CGPoint.init(x: mainScrollView.contentOffset.x, y: 0), animated: false)
            heightcollRelatedMovie.constant = 0
        }
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
    }
    
    func displayLanguageAlert()  {
            let alert = UIAlertController (title: "Voice Over Language", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

            let fullString = NSMutableAttributedString (string: "Currently You\'re Watching this movie in it default language. To change it, click on the icon ")
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = #imageLiteral(resourceName: "lang")
            let image1String = NSAttributedString(attachment: image1Attachment)
            fullString.append(image1String)
            fullString.append(NSAttributedString(string: " at the Bottom-Right corner on this screen. If the movie has voice over language available, you will see it and make a selection of your choice. Thank you."))

            alert.setValue(fullString, forKey: "attributedMessage")

            self.presentedViewController!.present(alert, animated: true, completion: nil)
    }
}

extension NewMoviewDetailVC{
    func apiCallChangePartyStatus(status:String){
        //status 1: leave, 2: end
        let params = ["movie_id":self.movieID,"host_user_id":self.host_user_id,"party_status":status,"pusher_event_id":self.pusher_event_id,"groupId":self.groupId] as [String : Any]
        WebService.Request.patch(url: changeMoviePartyStatus, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.pusher.disconnect()
                    self.isWatchParty = false
                    self.myPlayer.pause()
                    self.btnEndWatchParty.isSelected = false
                    self.heightPlayerView.constant = 0
                    self.btnEndWatchParty.isHidden = true
                    self.btnStartWatchParty.isHidden = false
                    self.btnBackTapped(self.btnTopBack)
                } else {
                    self.alertOk(title: "", message: response!["msg"] as? String ?? "")
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    func apiCallGetPartyCode(){
        let params = ["movie_id":self.movieID] as [String : Any]
        WebService.Request.patch(url: savePartyInfo, type: .post, parameter: params) { (response, error) in
            if error == nil {
                print(response)
                if response!["status"] as? Int == 1 {
                    let code = response?["PartyCode"] as? String ?? ""
                    self.channelName = response?["channelName"] as? String ?? ""
                    self.eventName = response?["eventName"] as? String ?? ""
                    self.eventParticipateName = response?["eventParticipateName"] as? String ?? ""
                    self.pusher_event_id = response?["pusher_event_id"] as? String ?? ""
                    self.host_user_id = response?["movie_host_id"] as? String ?? ""
                    self.groupId = response?["groupId"] as? String ?? ""
                    
                    if let data = Movies(JSON: response!) {
                    self.arrParticipate = data.movie_party_list ?? []
                    self.tblParticipate.reloadData()
                    self.tblParticipateLandscape.reloadData()
                    }
                    self.setupPusherForHost()
                    self.getSharedLink(partyCode: code)
                    
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }


    
    func apiCallNotificationSubscribe(_ pId:String,isSubscribe:String) {
        let params = ["producer_id":pId,"is_subscribe":isSubscribe] as [String : Any]
        WebService.Request.patch(url: makeSubscribeChannel, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallGetMovieDetails(callSilently:Bool) {
        let params = self.partyCode.isEmpty ? (["movie_id":self.movieID,"mobile_uuid":appDel.getDeviceUUID()] as [String : Any] ):(["movie_id":self.movieID,"mobile_uuid":appDel.getDeviceUUID(),"PartyCode":self.partyCode] as [String : Any])
        WebService.Request.patch(url: getMovieDetail, type: .post, parameter: params, callSilently:callSilently) { [self] (response, error) in
            print(response)
            if error == nil {
                if response!["status"] as? Int == 1 {
                    WebService.Loader.show()
                    if let data = response!["data"] as? [String : Any] {
                        self.viewBlack.isHidden = true
                        self.movieDetail = Movies(JSON: data)
                        print(self.movieDetail.final_video)
                        if let _ = URL.init(string: self.movieDetail.final_video) {
                            // this code is for reduce player loading
                            let asset = AVAsset.init(url: URL.init(string: self.movieDetail.final_video)!)
                            asset.loadValuesAsynchronously(forKeys: ["playable","duration"]) {
                                var error: NSError? = nil
                                let status = asset.statusOfValue(forKey: "playable", error: &error)
                                        switch status {
                                        case .loaded:
                                            print("loaded")
                                        case .failed:
                                            print(".failed")
                                        case .cancelled:
                                            print(".cancelled")
                                        default:
                                            print("default")
                            }
                            }
                        }
                       
                        let videoIndex = DELEGATE.downloadManager.downloadingArray.firstIndex{$0.fileURL == self.movieDetail.final_video }
                      ///  let arr1 = Common.GetDownloadedVideofromLibrary(strUrl: self.movieDetail.final_video)
                        self.imgDownload.image = UIImage(named: "ic_download")
                        self.arrParticipate = self.movieDetail.movie_party_list
                        self.groupId = self.movieDetail.groupId
                        self.host_user_id = self.movieDetail.host_user_id
                        self.eventName = self.movieDetail.event_name
                        self.channelName = self.movieDetail.channel_name
                        self.eventParticipateName = self.movieDetail.event_participate_name
                        self.pusher_event_id = self.movieDetail.pusher_event_id
                        if self.movieDetail.host_user_id.isEmpty {
                            self.btnStartWatchParty.isHidden = false
                            self.btnEndWatchParty.isSelected = false
                            self.btnShareWatchParty.isHidden = false
                            self.btnEndWatchParty.isHidden = true
                            self.disableViewsForParticipants(disable: false)
                            self.pusher.disconnect()
                        } else {
                            self.btnStartWatchParty.isHidden = self.movieDetail.host_user_id != appDel.loggedInUserData?._id
                            self.btnShareWatchParty.isHidden = self.movieDetail.host_user_id != appDel.loggedInUserData?._id
                            self.btnEndWatchParty.isSelected = self.movieDetail.host_user_id != appDel.loggedInUserData?._id
                            if self.btnEndWatchParty.isSelected {
                                
                                btnEndWatchParty.setTitle("Leave Quietly", for: .normal)
                            } else {
                                btnEndWatchParty.setTitle("End Watch Party", for: .normal)
                            }
                            if self.movieDetail.host_user_id != appDel.loggedInUserData?._id {
                                self.disableViewsForParticipants()
                                self.btnEndWatchParty.isHidden = false
                                
                                self.setupPusher()
                            } else {
                                self.setupPusherForHost()
                            }
                        }
                        
                        
                       
                        
//                        if videoIndex != nil{
//                            self.CheckVideoStatus(videoIndex: videoIndex!)
//                        }else{
                            let arr = Common.GetDownloadedVideofromLibrary(strUrl: self.movieDetail.final_video)
                            if arr != ""{
                                
                                self.lblDownload.text = "Downloaded"
                                self.cnst_download.constant = 0
                            }else{
                                if videoIndex != nil{
                                    self.CheckVideoStatus(videoIndex: videoIndex!)
                                }
                            }
                        if self.movieDetail.isAvailabelToDownload == "1"{
                            self.vwDownload.isHidden = false
                        }else{
                            self.vwDownload.isHidden = true
                        }
                        
                        DispatchQueue.main.async {
                            self.imgBlurMovie.sd_setImage(with: URL(string: self.movieDetail.movie_banner),placeholderImage:nil)
                        }
                        self.lblMovieName.text = self.movieDetail.name
                        self.lblDesc.text = self.movieDetail.description
                        
                        //"\(self.movieDetail.month_name), " + self.movieDetail.year
                        if self.movieDetail.date_of_upload != ""{
                            let date = self.movieDetail.date_of_upload
                            let format1 = DateFormatter()
                            format1.dateFormat = "mm/dd/yyyy"
                            let uploadDate = format1.date(from: date)
                            
                            let format2 = DateFormatter()
                            format2.dateFormat = "mm/yyyy"
                            let uploadStre = format2.string(from: uploadDate!)
                                
                            let prodName = self.movieDetail.production_name
                            let ageLimit = "PG 18+"
                            self.lblDuration.text = "\(uploadStre)  |  \(prodName)  |  \(ageLimit)"
                        }else{
                            let prodName = self.movieDetail.production_name
                            let ageLimit = "PG 18+"
                            self.lblDuration.text = "\(prodName)  |  \(ageLimit)"
                        }
                        
                        if self.movieDetail.my_rating == "0.0" || self.movieDetail.my_rating == "0" || self.movieDetail.my_rating == "" {
                            self.btnRate.isUserInteractionEnabled = true
                        } else {
                            self.btnRate.isUserInteractionEnabled = false
                        }
                        
                        let intRating = (self.movieDetail.my_rating as NSString).integerValue
                        if intRating == 0 {
                            self.lblMyRate.text = ""
//                            self.imgMyRateStar.image = #imageLiteral(resourceName: "starBlank")
                        } else {
                           self.lblMyRate.text = "\(intRating)"
//                            self.imgMyRateStar.image = #imageLiteral(resourceName: "starFill")
                        }
                        
                        if self.movieDetail.movie_trailer == "" {
                            self.btnWatchTrailer.isHidden = true
                            self.btnWatchMovie.isHidden = false
                            self.btnCenterWatchMovie.isHidden = true
                        } else {
                            self.btnWatchTrailer.isHidden = false
                            self.btnWatchMovie.isHidden = false
                            self.btnCenterWatchMovie.isHidden = true
                        }
                        
                        if self.movieDetail.is_subscribe == "" || self.movieDetail.is_subscribe == "0"{
//                            self.btnNotification.setImage(#imageLiteral(resourceName: "notificationBell"), for: .normal)
//                            self.btnSubscribe.setTitle("SUBSCRIBE", for: .normal)
                        } else {
//                            self.btnNotification.setImage(#imageLiteral(resourceName: "notificationAll"), for: .normal)
//                            self.btnSubscribe.setTitle("SUBSCRIBED", for: .normal)
                        }
                        
                        let strSubscribed = self.movieDetail.is_plan_subscribed
                        if strSubscribed == "1"{
                            appDel.is_Subscribed = true
                            if appDel.isWatchMovie{
                                self.viewPaymentDone.isHidden = false
                                //self.alertOk(title: "TopHub", message: "Thank you for the payment, click on the Watch Movie button for movie to start playing.")
                            }
                        }else{
                            appDel.is_Subscribed = false
                        }
                        UserDefaults.standard.set(appDel.is_Subscribed, forKey: "is_Subscribed")
                        
                        self.movieDescription = self.movieDetail.description
                        
                        
                        self.relatedMovies = self.movieDetail.related_movies
                        self.EpisodeMovies = self.movieDetail.episode_array
                        //self.heightcollRelatedMovie.constant = 10
                        //self.collRelatedMovie.reloadData()
                        
                        if self.isRatingTapped{
                            
                        }else{
                            self.myPlayer.removeFromSuperview()
                            self.myPlayer.pause()
                            self.heightPlayerView.constant = 0
                            self.heightBannerView.constant = 230
                        }
                        self.Heightviewrate.constant = 0
                        let bottomOffset:CGPoint = CGPoint(x: 0, y: -self.mainScrollView.contentInset.top)
                        
                        DispatchQueue.main.async {
                            self.mainScrollView.setContentOffset(bottomOffset, animated: true)
                            self.view.layoutIfNeeded()
                            self.view.updateConstraintsIfNeeded()
                        }
                        
                        if self.movieDetail.is_series == "1"{
                            print("========YESSSSSSSS=========")
                            self.btnEpisode.isHidden = false
                            self.btnMoreLike.isHidden = false
                            self.btnCenterMoreLike.isHidden = true
                            
                            self.lblEposides.isHidden = false
                            self.lblMoreLike.isHidden = true
                            self.lblCenterMoreLike.isHidden = true
                            self.setupRelatedMoiewView(tag: 1)
                            
                            
                            if !self.isEpisodeTapped && self.isNextEpisodeTapped{
                                self.isNextEpisodeTapped = false
                              //  appDel.myOrientation = .all
                                print("is it All==>")
                                self.preparePlayer(iscall: true)
                                self.heightPlayerView.constant = 230
                                self.heightBannerView.constant = 0
                                self.isTrailer = false
                                self.setupPlayerResource(strUrl: self.movieDetail.final_video)
                                DispatchQueue.main.async {
                                    self.mainScrollView.setContentOffset(.zero, animated: true)
                                    self.view.layoutIfNeeded()
                                    self.view.updateConstraintsIfNeeded()
                                }
                            }else{
//                                self.myPlayer.removeFromSuperview()
//                                self.myPlayer.pause()
//                                self.heightPlayerView.constant = 0
//                                self.heightBannerView.constant = 230
                            }
                            
                        }else{
                            print("========NOOOOOOOOO=========")
                            self.btnEpisode.isHidden = true
                            self.btnMoreLike.isHidden = true
                            self.btnCenterMoreLike.isHidden = false
                            
                            self.lblEposides.isHidden = true
                            self.lblMoreLike.isHidden = true
                            self.lblCenterMoreLike.isHidden = false
                            self.setupRelatedMoiewView(tag: 2)
                        }
                        self.setupDescCast2(tag: 3)
                        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                            WebService.Loader.hide()
                        }
                        
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
        
    func apiCallGiveRating() {
        let params = ["movie_id":self.movieID,
                      "rating": self.givenRating,"feedback":""]
        
        WebService.Request.patch(url: giveMovieRating, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.isRatingTapped = true
                    self.apiCallGetMovieDetails(callSilently: false)
                    self.alertOk(title: "", message: response!["msg"] as! String)
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    func DisplayData(isFromProcess:Bool = false){
        let videoIndex = DELEGATE.downloadManager.downloadingArray.firstIndex{$0.fileURL == self.movieDetail.final_video }
      //  let arr = Common.GetDownloadedVideofromLibrary(strUrl: self.movieDetail.final_video)
        self.imgDownload.image = UIImage(named: "ic_download")

      //  if videoIndex != nil{
       // }else{
            let arr = Common.GetDownloadedVideofromLibrary(strUrl: self.movieDetail.final_video)
            if arr != ""{
                self.imgDownload.image = UIImage(named: "ic_download")
                if isFromProcess{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self.lblRemainingData.text = ""
                        self.lblDownload.text = "Downloaded"
                        self.lblStatus.text = "Download complete successfully check download screen."
                    }
                }else{
                    self.lblDownload.text = "Downloaded"
                    self.cnst_download.constant = 0
                }
            }else{
                if videoIndex != nil{
                    self.CheckVideoStatus(videoIndex: videoIndex!)
                }
            }
        
        
        DispatchQueue.main.async {
            self.imgBlurMovie.sd_setImage(with: URL(string: self.movieDetail.movie_banner),placeholderImage:nil)
        }
        self.lblMovieName.text = self.movieDetail.name
        self.lblDesc.text = self.movieDetail.description
        self.viewBlack.isHidden = true

        if self.movieDetail.isAvailabelToDownload == "1"{
            self.vwDownload.isHidden = false
        }else{
            self.vwDownload.isHidden = true
        }

        //"\(self.movieDetail.month_name), " + self.movieDetail.year
        if self.movieDetail.date_of_upload != ""{
            let date = self.movieDetail.date_of_upload
            let format1 = DateFormatter()
            format1.dateFormat = "mm/dd/yyyy"
            let uploadDate = format1.date(from: date)
            
            let format2 = DateFormatter()
            format2.dateFormat = "mm/yyyy"
            let uploadStre = format2.string(from: uploadDate!)
            
            let prodName = self.movieDetail.production_name
            let ageLimit = "PG 18+"
            self.lblDuration.text = "\(uploadStre)  |  \(prodName)  |  \(ageLimit)"
        }else{
            let prodName = self.movieDetail.production_name
            let ageLimit = "PG 18+"
            self.lblDuration.text = "\(prodName)  |  \(ageLimit)"
        }
        
        if self.movieDetail.my_rating == "0.0" || self.movieDetail.my_rating == "0" || self.movieDetail.my_rating == "" {
            self.btnRate.isUserInteractionEnabled = true
        } else {
            self.btnRate.isUserInteractionEnabled = false
        }
        
        let intRating = (self.movieDetail.my_rating as NSString).integerValue
        if intRating == 0 {
            self.lblMyRate.text = ""
            //                            self.imgMyRateStar.image = #imageLiteral(resourceName: "starBlank")
        } else {
            self.lblMyRate.text = "\(intRating)"
            //                            self.imgMyRateStar.image = #imageLiteral(resourceName: "starFill")
        }
        
        if self.movieDetail.movie_trailer == "" {
            self.btnWatchTrailer.isHidden = true
            self.btnWatchMovie.isHidden = false
            self.btnCenterWatchMovie.isHidden = true
        } else {
            self.btnWatchTrailer.isHidden = false
            self.btnWatchMovie.isHidden = false
            self.btnCenterWatchMovie.isHidden = true
        }
        
        if self.movieDetail.is_subscribe == "" || self.movieDetail.is_subscribe == "0"{
            //                            self.btnNotification.setImage(#imageLiteral(resourceName: "notificationBell"), for: .normal)
            //                            self.btnSubscribe.setTitle("SUBSCRIBE", for: .normal)
        } else {
            //                            self.btnNotification.setImage(#imageLiteral(resourceName: "notificationAll"), for: .normal)
            //                            self.btnSubscribe.setTitle("SUBSCRIBED", for: .normal)
        }
        
        let strSubscribed = self.movieDetail.is_plan_subscribed
        if strSubscribed == "1"{
            appDel.is_Subscribed = true
            if appDel.isWatchMovie{
                self.viewPaymentDone.isHidden = false
                //self.alertOk(title: "TopHub", message: "Thank you for the payment, click on the Watch Movie button for movie to start playing.")
            }
        }else{
            appDel.is_Subscribed = false
        }
        UserDefaults.standard.set(appDel.is_Subscribed, forKey: "is_Subscribed")
        
        self.movieDescription = self.movieDetail.description
        
        self.setupDescCast2(tag: 1)
        self.relatedMovies = self.movieDetail.related_movies
        self.EpisodeMovies = self.movieDetail.episode_array
        //self.heightcollRelatedMovie.constant = 10
        //self.collRelatedMovie.reloadData()
        
        if self.isRatingTapped{
            
        }else{
            self.myPlayer.removeFromSuperview()
            self.myPlayer.pause()
            self.heightPlayerView.constant = 0
            self.heightBannerView.constant = 230
        }
        self.Heightviewrate.constant = 0
        let bottomOffset:CGPoint = CGPoint(x: 0, y: -self.mainScrollView.contentInset.top)
        
        DispatchQueue.main.async {
            self.mainScrollView.setContentOffset(bottomOffset, animated: true)
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        }
        
        if self.movieDetail.is_series == "1"{
            print("========YESSSSSSSS=========")
            self.btnEpisode.isHidden = false
            self.btnMoreLike.isHidden = false
            self.btnCenterMoreLike.isHidden = true
            
            self.lblEposides.isHidden = false
            self.lblMoreLike.isHidden = true
            self.lblCenterMoreLike.isHidden = true
            self.setupRelatedMoiewView(tag: 1)
            
            
            if !self.isEpisodeTapped && self.isNextEpisodeTapped{
                self.isNextEpisodeTapped = false
               // appDel.myOrientation = .all
                print("is it All==>")
                self.preparePlayer(iscall: true)
                self.heightPlayerView.constant = 230
                self.heightBannerView.constant = 0
                self.isTrailer = false
                self.setupPlayerResource(strUrl: self.movieDetail.final_video)
                DispatchQueue.main.async {
                    self.mainScrollView.setContentOffset(.zero, animated: true)
                    self.view.layoutIfNeeded()
                    self.view.updateConstraintsIfNeeded()
                }
            }else{
                //                                self.myPlayer.removeFromSuperview()
                //                                self.myPlayer.pause()
                //                                self.heightPlayerView.constant = 0
                //                                self.heightBannerView.constant = 230
            }
            
        }else{
            print("========NOOOOOOOOO=========")
            self.btnEpisode.isHidden = true
            self.btnMoreLike.isHidden = true
            self.btnCenterMoreLike.isHidden = false
            
            self.lblEposides.isHidden = true
            self.lblMoreLike.isHidden = true
            self.lblCenterMoreLike.isHidden = false
            self.setupRelatedMoiewView(tag: 2)
        }
    }
    func apiCallAddMyList() {
        let params = ["movie_id":self.movieID]
        WebService.Request.patch(url: addToMyCollection, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    func apiCallAddMyRecent() {
        let params = ["movie_id":self.movieID]
        WebService.Request.patch(url: addMovieToRecent, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                  //  self.alertOk(title: "", message: response!["msg"] as! String)
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
}

extension NewMoviewDetailVC{
    @objc func applicationWillEnterForeground() {
      
    }
    
    @objc func applicationDidEnterBackground() {
        myPlayer.pause(allowAutoPlay: false)
        myPlayer.pause()
        print("allowAutoPlay: false==>")
    }
    
    @objc func getFullScreenStatus(_ notification: Notification) {
    
    }
}

extension NewMoviewDetailVC{
    func preparePlayer(iscall:Bool) {
        
        let controller: BMPlayerControlView? = nil
        myPlayer = BMPlayer(customControlView: controller)
       
        if !self.viewPlayer.subviews.contains(myPlayer) {
        self.viewPlayer.addSubview(myPlayer)
        }
      
        if iscall{
            myPlayer.snp.makeConstraints { (make) in
                make.top.equalTo(self.viewPlayer.snp.top)
                make.left.equalTo(self.viewPlayer.snp.left)
                make.right.equalTo(self.viewPlayer.snp.right)
                make.bottom.equalTo(self.viewPlayer.snp.bottom)
                make.width.equalTo(self.viewPlayer.snp.width)
                make.height.equalTo(self.viewPlayer.snp.height)
                //make.height.equalTo(self.viewPlayer.snp.width).multipliedBy(7.0/14.0).priority(300)
            }
        }else{
        }
              
        myPlayer.delegate = self
        myPlayer.backBlock = { [unowned self] (isFullScreen) in
            if isFullScreen {
                return
            } else {
                let _ = self.navigationController?.popViewController(animated: true)
            }
        }
        castButton = GCKUICastButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        if !self.viewGoogleCast.subviews.contains(castButton) {
        self.viewGoogleCast.addSubview(castButton)
            self.viewPlayer.bringSubviewToFront(stkGoogleCast)
        } else {
            
            
            self.viewPlayer.bringSubviewToFront(stkGoogleCast)
        }
       
    }
    func setupPlayerResource_local(strUrl:String) {
        if self.PlayerState == false{
        
        var fileURL  : URL!
        do {
            let contentOfDir: [String] = try FileManager.default.contentsOfDirectory(atPath: MZUtility.baseFilePath + "/My Downloads" as String)
            print(contentOfDir)
            if contentOfDir.count == 0{
                do {
                    let contentOfDir: [String] = try FileManager.default.contentsOfDirectory(atPath: MZUtility.baseFilePath + "/Default folder" as String)
                    
                    print(contentOfDir)
                    let videoUrl = contentOfDir.filter{$0 == strUrl}
                    if videoUrl.count > 0{
                        fileURL = URL(fileURLWithPath: (MZUtility.baseFilePath + "/Default folder" as NSString).appendingPathComponent(videoUrl[0] ))
                    }else{
                        
                    }
                    
                } catch let error as NSError {
                    print("Error while getting directory content \(error)")
                }
            }
            let videoUrl = contentOfDir.filter{$0 == strUrl}
            if videoUrl.count > 0{
                fileURL = URL(fileURLWithPath: (MZUtility.baseFilePath + "/My Downloads" as NSString).appendingPathComponent(videoUrl[0] ))
            }else{
            }
            
        } catch let error as NSError {
            print("Error while getting directory content \(error)")
            do {
                let contentOfDir: [String] = try FileManager.default.contentsOfDirectory(atPath: MZUtility.baseFilePath + "/Default folder" as String)
                
                print(contentOfDir)
                let videoUrl = contentOfDir.filter{$0 == strUrl}
                if videoUrl.count > 0{
                    fileURL = URL(fileURLWithPath: (MZUtility.baseFilePath + "/Default folder" as NSString).appendingPathComponent(videoUrl[0] ))
                }else{
                    
                }
                
            } catch let error as NSError {
                print("Error while getting directory content \(error)")
            }
        }

        let url = URL(string:strUrl)
        let fileName : NSString = strUrl as NSString

        let asset = BMPlayerResource(url: fileURL)
        myPlayer.setVideo(resource: asset)
        myPlayer.videoGravity = .resizeAspect
        
        if !myPlayer.isPlaying{
            myPlayer.play()
        }
        if GCKCastContext.sharedInstance().castState == GCKCastState.connected{
                myPlayer.pause()
                playVideoRemotely()
        }
    }
    }
    func setupPlayerResource(strUrl:String) {
      //  if self.PlayerState == false{
        
            let url = URL(string:strUrl)
        
            let asset = BMPlayerResource(url: url!)
            //BMPlayerResource(url: url!, name: "", cover: nil, subtitle: nil)
            //BMPlayerResource(url: url!)
        DispatchQueue.main.async {
            
            self.myPlayer.setVideo(resource: asset)
            self.myPlayer.videoGravity = .resizeAspect
            //self.myPlayer.play()
        }
            
            
            if GCKCastContext.sharedInstance().castState == GCKCastState.connected{
                    myPlayer.pause()
                    playVideoRemotely()
            }
      //  }
    }
    
    func setupPlayerManager() {
        BMPlayerConf.allowLog = false
        BMPlayerConf.shouldAutoPlay = true
        BMPlayerConf.tintColor = UIColor.white
        BMPlayerConf.topBarShowInCase = .always
    }
}

extension NewMoviewDetailVC: BMPlayerDelegate {
  // Call when player orinet changed
  func bmPlayer(player: BMPlayer, playerOrientChanged isFullscreen: Bool) {
    //  heightForWatchParty.constant = 0
      viewWatchParty.isHidden = true
      chatView.isHidden = true
      stkForWatchParty.isHidden = !btnWatchParty.isSelected
      chatViewLandscape.isHidden = true
      mainScrollView.isScrollEnabled = true
      txtChat.text = ""
      self.isFullscreen = isFullscreen
      txtChatLandScape.text = ""
    if self.heightPlayerView.constant == 0{
        appDel.myOrientation = .portrait
        print("is it Portrait==>")
    }else{
        if isFullscreen {
            appDel.myOrientation = .landscape
            UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
            UIApplication.shared.setStatusBarHidden(false, with: .fade)
            UIApplication.shared.statusBarOrientation = .landscapeRight
            stkForWatchParty.isHidden = true
            
            print("is it fullscreen==>")
            self.viewGoogleCast2.isHidden = false
          //  myPlayer.removeFromSuperview()
           // self.viewBlack.isHidden = false
            self.btnTopBack.isHidden = true
            //self.viewBlack.addSubview(myPlayer)
            castButton = GCKUICastButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        //    self.viewGoogleCast2.addSubview(castButton)
          //  self.viewBlack.bringSubviewToFront(viewGoogleCast2)
            self.heightPlayerView.constant = self.viewLandscape.frame.height
            btnChatLandscape.isHidden = self.arrParticipate.count < 2//false
            player.snp.remakeConstraints { (make) in
                
                make.left.equalTo(self.viewLandscape.snp.left)
                make.width.equalTo(self.viewLandscape.snp.width)
                make.top.equalTo(self.viewLandscape.snp.top)
                make.height.equalTo(self.viewLandscape.snp.height)
                make.bottom.equalTo(self.viewLandscape.snp.bottom)

            }
            myPlayer.videoGravity = .resizeAspect
        }else{
            appDel.myOrientation = .portrait
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            UIApplication.shared.setStatusBarHidden(false, with: .fade)
            UIApplication.shared.statusBarOrientation = .portrait
            btnChatLandscape.isHidden = true
            
         //   heightForWatchParty.constant = (tblChat.superview?.frame.height ?? (UIScreen.main.bounds.height)) - viewCastDescription.frame.maxY
            viewWatchParty.isHidden = !btnWatchParty.isSelected
          //  mainScrollView.isScrollEnabled = !btnWatchParty.isSelected
            
            print("is it normal screen==>")
            self.viewBlack.isHidden = true
            self.btnTopBack.isHidden = false
         //   myPlayer.removeFromSuperview()
            //self.viewPlayer.addSubview(myPlayer)
            self.heightPlayerView.constant = 230
            player.snp.remakeConstraints { (make) in
                make.top.equalTo(self.viewPlayer.snp.top)
                make.left.equalTo(self.viewPlayer.snp.left)
                make.right.equalTo(self.viewPlayer.snp.right)
                make.bottom.equalTo(self.viewPlayer.snp.bottom)
                make.width.equalTo(self.viewPlayer.snp.width)
                make.height.equalTo(self.viewPlayer.snp.height)
            }
            self.viewGoogleCast2.isHidden = true
            castButton = GCKUICastButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            self.viewGoogleCast.addSubview(castButton)
            self.viewPlayer.bringSubviewToFront(stkGoogleCast)
        }
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
    }
  }
  
  // Call back when playing state changed, use to detect is playing or not
  func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
    print("| BMPlayerDelegate | playerIsPlaying | playing - \(playing)")
    self.PlayerState = playing
      if isWatchParty {
          let dataEvent = ControlSocketObject.init(isStart: true, currentDuration: Int(self.CurrentVideoDuration*1000), isPlay: playing, isEnd: false)
      self.channel.trigger(eventName: self.eventName, data: dataEvent.toDictionary())
      }
  }
  
  // Call back when playing state changed, use to detect specefic state like buffering, bufferfinished
  func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
    print("| BMPlayerDelegate | playerStateDidChange | state - \(state)")
      if state == .error {
          let arr = Common.GetDownloadedVideofromLibrary(strUrl: self.movieDetail.final_video)
          if arr != ""{
              self.setupPlayerResource_local(strUrl: arr)
          }else{
              
              self.setupPlayerResource(strUrl: movieDetail.final_video)
          }
      }
      if state == .playedToTheEnd && Int((player.avPlayer?.currentTime().seconds)!) >= Int(self.totalVideoDuration) {
        appDel.myOrientation = .portrait
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        UIApplication.shared.setStatusBarHidden(false, with: .fade)
        UIApplication.shared.statusBarOrientation = .portrait
        print("is it Portrait==>")
        self.viewGoogleCast2.isHidden = true
        myPlayer.removeFromSuperview()
        self.heightPlayerView.constant = 0
        self.heightBannerView.constant = 230
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
        self.OpenReviewPopup()
//        if !isEpisodeTapped{
//            appDel.myOrientation = .all
//            print("is it All==>")
//            self.preparePlayer(iscall: true)
//            self.heightPlayerView.constant = 230
//            self.heightBannerView.constant = 0
//            self.isTrailer = true
//            self.setupPlayerResource(strUrl:"https://cinestool.s3.amazonaws.com/trailers%2FRDNT4qI3vk7Ac8UVf8SKdkwIdbWLoavjeiPAyLon4h87lC2x6CZMdQ7QBMsFcD9WA12MNUp41D4zLgLd06OhzZSQ3bvsPFm0zH4G7gOdgE32fjuEL5WvlWtQbaCTQfrmtrailer972.mp4")// movieDetail.movie_trailer)
//            //https://cinestool.s3.amazonaws.com/trailers%2FZPn9c3KObNWoA4twfS0ppwIiAhmyW5G4ci1UmZxx5KBm5VfpkamcKfGugVNd6eX0JgcDItw5VlIOYwfqGc429L3C8qOXgLPSBVS6dPhngFvAbY0bBExii1iGM8fiMcjFtrailer899.mp4
//            DispatchQueue.main.async {
//                self.mainScrollView.setContentOffset(.zero, animated: true)
//                self.view.layoutIfNeeded()
//                self.view.updateConstraintsIfNeeded()
//            }
//        }else{
//            appDel.myOrientation = .portrait
//            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
//            UIApplication.shared.setStatusBarHidden(false, with: .fade)
//            UIApplication.shared.statusBarOrientation = .portrait
//            print("is it Portrait==>")
//            self.viewGoogleCast2.isHidden = true
//            myPlayer.removeFromSuperview()
//            self.heightPlayerView.constant = 0
//            self.heightBannerView.constant = 230
//            self.view.layoutIfNeeded()
//            self.view.updateConstraintsIfNeeded()
//        }
    }
  }
  
  // Call back when play time change
  func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {
      if CGFloat(NSInteger(currentTime)) == self.CurrentVideoDuration {return}
    self.CurrentVideoDuration = CGFloat(NSInteger(currentTime))
      print("CurrentTime: ",self.CurrentVideoDuration)
    //  player.disableControlView()
      if isWatchParty && (abs(CurrentVideoDuration - CurrentVideoDurationTemp) >= 10)  {
          CurrentVideoDurationTemp = CurrentVideoDuration
          let dataEvent = ControlSocketObject.init(isStart: true, currentDuration: Int(self.CurrentVideoDuration*1000), isPlay: true, isEnd: false)
      self.channel.trigger(eventName: self.eventName, data: dataEvent.toDictionary())
      }
    //        print("| BMPlayerDelegate | playTimeDidChange | \(currentTime) of \(totalTime)")
  }
  
  // Call back when the video loaded duration changed
  func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
    //        print("| BMPlayerDelegate | loadedTimeDidChange | \(loadedDuration) of \(totalDuration)")
      if !totalDuration.isNaN {
    self.totalVideoDuration = CGFloat(NSInteger(totalDuration))
      }
  }
}

extension NewMoviewDetailVC: GCKSessionManagerListener, GCKRemoteMediaClientListener, GCKRequestDelegate{
        
    @objc func castDeviceDidChange(notification _: Notification) {
         if GCKCastContext.sharedInstance().castState != GCKCastState.noDevicesAvailable {
          // Display the instructions for how to use Google Cast on the first app use.
            btnGoogleCast.isHidden = true
            btnGoogleCast2.isHidden = true
              GCKCastContext.sharedInstance().presentCastInstructionsViewControllerOnce(with: castButton)
              if GCKCastContext.sharedInstance().castState == GCKCastState.connected{
                    myPlayer.pause()
                    playVideoRemotely()
              }
         }
    }

    // MARK: Cast Actions

    func playVideoRemotely() {
//        self.navigationController?.popViewController(animated: true)
//        dismiss(animated: true) {
            GCKCastContext.sharedInstance().presentDefaultExpandedMediaControls()
//        }
        
        
        if self.isTrailer == true{
            strVideoURL = movieDetail.movie_trailer
            strImageURL = movieDetail.movie_image
            strName = movieDetail.name
            strDesc = movieDetail.description
        }else{
            strVideoURL = movieDetail.final_video
            strImageURL = movieDetail.movie_image
            strName = movieDetail.name
            strDesc = movieDetail.description
        }
        

        // Define media metadata.
        let metadata = GCKMediaMetadata()
        metadata.setString(strName, forKey: kGCKMetadataKeyTitle)
        metadata.setString(strDesc,forKey: kGCKMetadataKeySubtitle)
        metadata.addImage(GCKImage(url: URL(string: strImageURL)!,width: 480,height: 360))
        
        let mediaInfoBuilder = GCKMediaInformationBuilder(contentURL: URL(string: strVideoURL)!)
        mediaInfoBuilder.streamType = GCKMediaStreamType.buffered
        mediaInfoBuilder.mediaTracks = []
        mediaInfoBuilder.contentType = "video/mp4"
        mediaInfoBuilder.metadata = metadata
        mediaInformation = mediaInfoBuilder.build()

        let mediaLoadRequestDataBuilder = GCKMediaLoadRequestDataBuilder()
        mediaLoadRequestDataBuilder.mediaInformation = mediaInformation

        // Send a load request to the remote media client.
        if let request = sessionManager.currentSession?.remoteMediaClient?.loadMedia(with: mediaLoadRequestDataBuilder.build()) {
            request.delegate = self
        }
    }

    // MARK: GCKSessionManagerListener

    func sessionManager(_: GCKSessionManager,
                        didStart session: GCKSession) {
        print("sessionManager didStartSession: \(session)")
//        self.navigationController?.presentedViewController!.dismiss(animated: true, completion: nil)
        
        session.remoteMediaClient?.add(self)
    }

    func sessionManager(_: GCKSessionManager,
                        didResumeSession session: GCKSession) {
      print("sessionManager didResumeSession: \(session)")

      // Add GCKRemoteMediaClientListener.
      session.remoteMediaClient?.add(self)
    }

    func sessionManager(_: GCKSessionManager,
                        didEnd session: GCKSession,
                        withError error: Error?) {
      print("sessionManager didEndSession: \(session)")
      // Remove GCKRemoteMediaClientListener.
      session.remoteMediaClient?.remove(self)
      if let error = error {
        showError(error)
      }
    }

    func sessionManager(_: GCKSessionManager,
                        didFailToStart session: GCKSession,
                        withError error: Error) {
      print("sessionManager didFailToStartSessionWithError: \(session) error: \(error)")

      // Remove GCKRemoteMediaClientListener.
        session.remoteMediaClient?.remove(self)
        if !myPlayer.isPlaying{
            myPlayer.play()
        }
    }

    // MARK: GCKRemoteMediaClientListener

    func remoteMediaClient(_: GCKRemoteMediaClient,
                           didUpdate mediaStatus: GCKMediaStatus?) {
      if let mediaStatus = mediaStatus {
        mediaInformation = mediaStatus.mediaInformation
      }
    }

    // MARK: - GCKRequestDelegate
    func requestDidComplete(_ request: GCKRequest) {
      print("request \(Int(request.requestID)) completed")
    }

    func request(_ request: GCKRequest,
                 didFailWithError error: GCKError) {
      print("request \(Int(request.requestID)) didFailWithError \(error)")
    }

    func request(_ request: GCKRequest,
                 didAbortWith abortReason: GCKRequestAbortReason) {
      print("request \(Int(request.requestID)) didAbortWith reason \(abortReason)")
    }

    // MARK: Misc

    func showError(_ error: Error) {
      let alertController = UIAlertController(title: "Error",
                                              message: error.localizedDescription,
                                              preferredStyle: UIAlertController.Style.alert)
      let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
      alertController.addAction(action)

      present(alertController, animated: true, completion: nil)
    }
}
//http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4
/*"msg": successfully., "status": 1, "data":
"msg": successfully., "status": 1, "data":
    "_id" = 5f24a98a07193078603a2c14;
    "_id" = 5f296783e265dc57bc2249c2;
    "cast_data" = ;
    "cast_data" = ;
    category = "5db6533b54375858656327c5,5d39598810dffb5e9538a572,5d38c7540b575b12712331f3,5d38c6480b575b12712331f2";
    category = 5d38c66cdb982e0137062882;
    categoryNames = "Premier,New | Trending,Love | Romance,Exclusive Releases";
    categoryNames = "Series | Episodes";
    "converted_video" = "";
    "converted_video" = "";
    "created_at" = "2020-07-31 23:30:18";
    "created_at" = "2020-08-04 13:49:55";
    "created_date" = "2020-07-31 16:30:18";
    "created_date" = "2020-08-05 08:37:07";
    "currency_actual_symbol" = "$";
    "currency_actual_symbol" = "$";
    "currency_code" = INR;
    "currency_code" = INR;
    "currency_symbol" = "\U20b9";
    "currency_symbol" = "\U20b9";
    date = "2020-07-31";
    date = "2020-08-05";
    "date_of_upload" = "07/31/2020";
    "date_of_upload" = "09/04/2020";
    description = "A young Lady was infected with sexually transmitted Disease (STD) and it affected her whole life. However since life entails hope, she got her self together and found love again upon all the pain she encountered.";
    description = test;
    "director_name" = "Tissy Nnachi";
    "director_name" = "Vincent Daniel";
    "director_number" = 4784423863;
    "director_number" = 719;
    "episode_array" = ;
    "episode_array" = ;
    "final_video" = "https://cinestool.s3.amazonaws.com/movie_051928341be67dcba03f0e04104d90471596235864051928341be67dcba03f0e04104d9047%2FmVKvvK6efpazw2emDyJ9a9kUyVBZrdiK6Pth9t1nMWoMhy13OcwSchPNvHGIBRraB9iYaCtfeFes8ip89XJJzzGrLnXq40AQ8qaCv3Vg26xK1DAMpauE4LjYJiuac1Luvideo741.mp4";
    "final_video" = "https://cinestool.s3.amazonaws.com/movie_051928341be67dcba03f0e04104d90471596548822051928341be67dcba03f0e04104d9047/VOE9g0H9f3D9JULN5fwLMxkFazWb0fdoc7zhUDzYfADBacdKui7p8ZRJvoTuwo6oG7K8QDjLXQq4cr04DaRLpyJk69CbvxY5MxES9Vo8xE272V9RtsI5i5or6w3roUJivideo121.mp4";
    identity = "67.166.165.44";
    identity = "157.32.115.240";
    "is_advertisement_payment_done" = 0;
    "is_advertisement_payment_done" = 0;
    "is_advertisement_show" = 0;
    "is_advertisement_show" = 0;
    "is_approved" = 1;
    "is_approved" = 1;
    "is_banner_type" = 0;
    "is_banner_type" = 0;
    "is_done" = 1;
    "is_done" = 1;
    "is_first_episode" = 0;
    "is_first_episode" = 1;
    "is_movie_payment_done" = 1;
    "is_movie_payment_done" = 1;
    "is_mpd_done" = 0;
    "is_mpd_done" = 0;
    "is_notification_sent" = 0;
    "is_notification_sent" = 0;
    "is_paid_movie" = No;
    "is_paid_movie" = No;
    "is_payment_done" = 1;
    "is_payment_done" = 1;
    "is_plan_subscribed" = 0;
    "is_plan_subscribed" = 0;
    "is_premier_movie" = No;
    "is_premier_movie" = No;
    "is_series" = 0;
    "is_series" = 1;
    "is_subscribe" = 0;
    "is_subscribe" = 0;
    "is_valid_for_automatic_approve" = 1;
    "is_valid_for_automatic_approve" = 1;
    "is_watched" = 0;
    "is_watched" = 0;
    "modified_date" = "2020-07-31 16:30:32";
    "modified_date" = "2020-08-05 08:46:15";
    month = 07;
    month = 08;
    "month_name" = July;
    "month_name" = August;
    "movie_actual_price" = "1.50";
    "movie_actual_price" = "1.50";
    "movie_banner" = "https://cinestool.s3.amazonaws.com/moviebanners/xtC5TPuVXRFy26dJw0S9YS1PcrVa6DEtvaRDO57tPrsS83VKXz7p1diNaWbJRLkZlt1Eq0FlMhdzlBzVVDjNkqjGSXXP1d76p2bPtntIYruBo1IXRmBXSOmE1UonRcYrmoviebanner248After%20Hurt%20Movie%20Banner.jpg";
    "movie_banner" = "https://cinestool.s3.amazonaws.com/moviebanners/BUEorw66IOHnygSsUguQB0zke5JaLkDYESENH4To1KCd4Oi6Y7hXPJyxb89aWQslcltdeQCGP9vPibGNBeJXsJ1UEORlv9cMRmUrbydmAofuciTlJ1M0ePLMqmwacmv8moviebanner5494HLqqZUiRAX1bHLxUXz9lTTeHgBqjv3CKq14XET58Esf8qCPe8LpaorgKYKkt4s62RfZsWBvBqOUFbsOgDkfN4G8h2KORSMzRtuftFQisIt5mI5VmaOljJSm93gVxeqo.jpg";
    "movie_id" = 5f24a98a07193078603a2c14;
    "movie_id" = 5f296783e265dc57bc2249c2;
    "movie_image" = "https://cinestool.s3.amazonaws.com/moviebanners/rIIB8Ei2O4sI9VaaYYruGSYP8LCNBfhvgKQFOkHG142hOa8nVk0lpUmYuTF9fJDSc8x6R9B18ukUzSxGIHJBcFRjhH3rx760ewm6NxLXGXHxHpvuehzH84ZB8CjnMWCvmovieposter71After%20Hurt%20Movie%20Poster.jpg";
    "movie_image" = "https://cinestool.s3.amazonaws.com/moviebanners/LJhLYjo4gPQW9vCxwM6xrqvU7oTp7J3FwbX5SzkRfEc2Bhc56e0ZdfVETVTzzYR1n42NkMLO4e51cHQuam4BGZQKWRFXIBcK6VhVNjUWAPQBlzFEGs4ZewCj1HSnWWtdmovieposter2162ACrgUxnL04idM6ounVbf2mcwcey2gKFfeHSC1KiOkWV4mq86m41bTe5mstOWiZbrt7OO5lnZWOnSSdYbS5wcNN5n2jvXSsY4k6jXBiF2gKO6hqi9gcUyIoeK8TzZ39X.jpg";
    "movie_link" = "https://cinestool.s3.amazonaws.com/movie_051928341be67dcba03f0e04104d90471596235864051928341be67dcba03f0e04104d9047%2FmVKvvK6efpazw2emDyJ9a9kUyVBZrdiK6Pth9t1nMWoMhy13OcwSchPNvHGIBRraB9iYaCtfeFes8ip89XJJzzGrLnXq40AQ8qaCv3Vg26xK1DAMpauE4LjYJiuac1Luvideo741.mp4";
    "movie_link" = "https://cinestool.s3.amazonaws.com/movie_051928341be67dcba03f0e04104d90471596548822051928341be67dcba03f0e04104d9047/VOE9g0H9f3D9JULN5fwLMxkFazWb0fdoc7zhUDzYfADBacdKui7p8ZRJvoTuwo6oG7K8QDjLXQq4cr04DaRLpyJk69CbvxY5MxES9Vo8xE272V9RtsI5i5or6w3roUJivideo121.mp4";
    "movie_price" = "112.49";
    "movie_price" = "112.50";
    "movie_producer_name" = "Chinyere Okoroafor";
        "movie_producer_name" = "Anthony Vaughn";
    "movie_trailer" = "https://cinestool.s3.amazonaws.com/trailers%2FZPn9c3KObNWoA4twfS0ppwIiAhmyW5G4ci1UmZxx5KBm5VfpkamcKfGugVNd6eX0JgcDItw5VlIOYwfqGc429L3C8qOXgLPSBVS6dPhngFvAbY0bBExii1iGM8fiMcjFtrailer899.mp4";
    "movie_trailer" = "";
    "mpd_video" = "";
    "mpd_video" = "";
    "my_rating" = 0;
    "my_rating" = 0;
    name = "After HURT";
    name = "MIB Series S1:E1";
    "next_episode_movie_banner" = "";
    "next_episode_movie_banner" = "https://cinestool.s3.amazonaws.com/moviebanners/BUEorw66IOHnygSsUguQB0zke5JaLkDYESENH4To1KCd4Oi6Y7hXPJyxb89aWQslcltdeQCGP9vPibGNBeJXsJ1UEORlv9cMRmUrbydmAofuciTlJ1M0ePLMqmwacmv8moviebanner5494HLqqZUiRAX1bHLxUXz9lTTeHgBqjv3CKq14XET58Esf8qCPe8LpaorgKYKkt4s62RfZsWBvBqOUFbsOgDkfN4G8h2KORSMzRtuftFQisIt5mI5VmaOljJSm93gVxeqo.jpg";
    "next_episode_movie_id" = "";
    "next_episode_movie_id" = 5f2ac3dfbbff5f70d620e723;
    "next_episode_movie_image" = "";
    "next_episode_movie_image" = "https://cinestool.s3.amazonaws.com/moviebanners/LJhLYjo4gPQW9vCxwM6xrqvU7oTp7J3FwbX5SzkRfEc2Bhc56e0ZdfVETVTzzYR1n42NkMLO4e51cHQuam4BGZQKWRFXIBcK6VhVNjUWAPQBlzFEGs4ZewCj1HSnWWtdmovieposter2162ACrgUxnL04idM6ounVbf2mcwcey2gKFfeHSC1KiOkWV4mq86m41bTe5mstOWiZbrt7OO5lnZWOnSSdYbS5wcNN5n2jvXSsY4k6jXBiF2gKO6hqi9gcUyIoeK8TzZ39X.jpg";
    "next_episode_movie_url" = "";
    "next_episode_movie_url" = "https://cinestool.s3.amazonaws.com/movie_051928341be67dcba03f0e04104d90471596638072051928341be67dcba03f0e04104d9047/kwNd4CEu8gwPgkkkN1kt2NChIMq8al7KG4WJ4gzJMEugvAE3r4OyZ6EqlhzGq8h3DojWcvb4aqinz4X8Z9MAk0ObQzTsjn3Y1bLXM62kGW5J8wMvLqASiEfjwsaMWKpTvideo314.mp4";
    "next_episode_name" = "";
    "next_episode_name" = "MIB Series S1:E4";
    "next_episode_poster_image" = "";
    "next_episode_poster_image" = "https://cinestool.s3.amazonaws.com/moviebanners/LJhLYjo4gPQW9vCxwM6xrqvU7oTp7J3FwbX5SzkRfEc2Bhc56e0ZdfVETVTzzYR1n42NkMLO4e51cHQuam4BGZQKWRFXIBcK6VhVNjUWAPQBlzFEGs4ZewCj1HSnWWtdmovieposter2162ACrgUxnL04idM6ounVbf2mcwcey2gKFfeHSC1KiOkWV4mq86m41bTe5mstOWiZbrt7OO5lnZWOnSSdYbS5wcNN5n2jvXSsY4k6jXBiF2gKO6hqi9gcUyIoeK8TzZ39X.jpg";
    "original_video" = "https://cinestool.s3.amazonaws.com/movie_051928341be67dcba03f0e04104d90471596235864051928341be67dcba03f0e04104d9047%2FmVKvvK6efpazw2emDyJ9a9kUyVBZrdiK6Pth9t1nMWoMhy13OcwSchPNvHGIBRraB9iYaCtfeFes8ip89XJJzzGrLnXq40AQ8qaCv3Vg26xK1DAMpauE4LjYJiuac1Luvideo741.mp4";
    "original_video" = "https://cinestool.s3.amazonaws.com/movie_051928341be67dcba03f0e04104d90471596548822051928341be67dcba03f0e04104d9047/VOE9g0H9f3D9JULN5fwLMxkFazWb0fdoc7zhUDzYfADBacdKui7p8ZRJvoTuwo6oG7K8QDjLXQq4cr04DaRLpyJk69CbvxY5MxES9Vo8xE272V9RtsI5i5or6w3roUJivideo121.mp4";
    "payment_info" = "Payment Done";
    "payment_info" = "Payment Done";
    "poster_image" = "https://cinestool.s3.amazonaws.com/moviebanners/rIIB8Ei2O4sI9VaaYYruGSYP8LCNBfhvgKQFOkHG142hOa8nVk0lpUmYuTF9fJDSc8x6R9B18ukUzSxGIHJBcFRjhH3rx760ewm6NxLXGXHxHpvuehzH84ZB8CjnMWCvmovieposter71After%20Hurt%20Movie%20Poster.jpg";
    "poster_image" = "https://cinestool.s3.amazonaws.com/moviebanners/LJhLYjo4gPQW9vCxwM6xrqvU7oTp7J3FwbX5SzkRfEc2Bhc56e0ZdfVETVTzzYR1n42NkMLO4e51cHQuam4BGZQKWRFXIBcK6VhVNjUWAPQBlzFEGs4ZewCj1HSnWWtdmovieposter2162ACrgUxnL04idM6ounVbf2mcwcey2gKFfeHSC1KiOkWV4mq86m41bTe5mstOWiZbrt7OO5lnZWOnSSdYbS5wcNN5n2jvXSsY4k6jXBiF2gKO6hqi9gcUyIoeK8TzZ39X.jpg";
    "producer_id" = 5d36b0404412b527275a17f2;
    "producer_id" = 5d35b31f6d7d7257bb56b717;
    "producer_name" = "Eric Pope";
    "producer_name" = Admin;
    "producer_number" = 4784423863;
    "producer_number" = "";
    "production_name" = "Dorch Production";
    "production_name" = "Gray Bond";
    "related_movies" =  ;
    "related_movies" =  ;
    "remain_time" = "2020-08-03 16:30:18";
    "remain_time" = "2020-08-08 08:37:07";
    "series_id" = "";
    "series_id" = 5f294fb67593ad0ea928d0b2;
    "total_rating" = 8;
    "total_rating" = 0;
    "total_watching" = 1;
    "total_watching" = 0;
    uniqid = "movie_id_1596235864";
    uniqid = "movie_id_1596641800";
    "updated_at" = "2020-08-06 07:43:12";
    "updated_at" = "2020-08-05 15:46:15";
    "user_id" = 5d36b0404412b527275a17f2;
    "user_id" = 5d35b31f6d7d7257bb56b717;
    "user_name" = "Eric Pope";
    "user_name" = Admin;
    "user_type" = 10;
    "user_type" = 10;
    year = 2020;
    year = 2020;*/
// MARK:- Offline download methods && MZDownloadmanager Delegates

extension NewMoviewDetailVC{
        
    func CallDownload(){
        UIApplication.shared.beginBackgroundTask {}
            let arr = Common.GetDownloadedVideofromLibrary(strUrl: self.movieDetail.final_video)
            if arr == ""{
                let downloadModel = DELEGATE.downloadManager.downloadingArray.filter{$0.fileURL == self.movieDetail.final_video
                }
                self.cnst_download.constant = 55
                if downloadModel.count == 0{
                    self.AddVideoToOffline()
                    
                    let myDownloadPath = MZUtility.baseFilePath + "/My Downloads"
                    if !FileManager.default.fileExists(atPath: myDownloadPath) {
                        try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
                    }
                    self.imgDownload.image = UIImage(named: "ic_pauseDownload")

                    self.lblStatus.text = "Downloading..."
                    self.lblDownload.text = "Downloading"
                    let fileURL  : NSString = self.movieDetail.final_video as NSString
                    var fileName : NSString = fileURL.lastPathComponent as NSString
                    fileName = MZUtility.getUniqueFileNameWithPath(myDownloadPath.appending(fileName as String) as NSString)
                    DELEGATE.downloadManager.addDownloadTask(fileName as String, fileURL: fileURL as String, destinationPath: myDownloadPath)
                    
                }else{
                    var downloadModel = MZDownloadModel()
                    let objectModel = DELEGATE.downloadManager.downloadingArray.filter{$0.fileURL == self.movieDetail.final_video}
                    if objectModel.count > 0{
                        downloadModel = objectModel[0]
                    }
                    let videoIndex = DELEGATE.downloadManager.downloadingArray.firstIndex{$0.fileURL == self.movieDetail.final_video}
                    if videoIndex != nil{
                        if downloadModel.status == TaskStatus.downloading.description() {
                            DELEGATE.downloadManager.pauseDownloadTaskAtIndex(videoIndex!)
                        }else if downloadModel.status == TaskStatus.failed.description() {
                            DELEGATE.downloadManager.retryDownloadTaskAtIndex(videoIndex!)
                        }else if downloadModel.status == TaskStatus.paused.description() {
                            DELEGATE.downloadManager.resumeDownloadTaskAtIndex(videoIndex!)
                        }
                        if videoIndex != nil{
                            self.CheckVideoStatus(videoIndex: videoIndex!)
                        }
                    }
                }
            }
        
        
    }
    //MARK:- Button Clicks
    
    @IBAction func btnDownload_Click(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork(){
            if ((movieDetail.isPayPerView == "1")){
                if movieDetail.is_payment_done == "1"{
                    if movieDetail.isAvailabelToDownload == "1"{
                        self.CallDownload()
                    }else{
                        alertOk(title: "Tophub", message: "Opps, Currently this movie is not available for download.", buttonTitle: "OK") { (tr) in
                        }
                    }
                }else{
                    
                    let objTab = PaymentPopupVC(nibName: "PaymentPopupVC", bundle: nil)
                    objTab.isFrom = .advertisement
                    objTab.PopupFile = .PaidNow
                    objTab.movieID = movieID
                    objTab.price = self.movieDetail.payPerViewPrice
                    objTab.modalPresentationStyle = .overFullScreen
                    objTab.blockPayClick = {
                        let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                        objTab.isFromSubscribe = .PayPerView
                        objTab.isfromLogin = .MoviePlaying
                        objTab.PayPerViewPrice = self.movieDetail.payPerViewPrice
                        objTab.movieSelectedMovieID = self.movieDetail._id
                        self.navigationController!.pushViewController(objTab, animated: true)
                    }
                    self.present(objTab, animated: true, completion: nil)

                    
                }
            }else{
            if appDel.is_Subscribed {
                if movieDetail.isAvailabelToDownload == "1"{
                    self.CallDownload()
                }else{
                    alertOk(title: "Tophub", message: "Opps, Currently this movie is not available for download.", buttonTitle: "OK") { (tr) in
                    }
                }
            }else{
                let viewController = SubscriptionListVC(nibName: "SubscriptionListVC", bundle: nil)
                viewController.isfrom = .MoviePlaying
                self.navigationController!.pushViewController(viewController, animated: true)
            }
          }
        }
    }
    //MARK:- Other Functions
    func CheckVideoStatus(videoIndex:Int){
        var downloadModel = MZDownloadModel()
        let objectModel = DELEGATE.downloadManager.downloadingArray.filter{$0.fileURL == self.movieDetail.final_video}
        let videoIndex = DELEGATE.downloadManager.downloadingArray.firstIndex{$0.fileURL == self.movieDetail.final_video}

        if objectModel.count > 0{
            downloadModel = objectModel[0]
        }
        self.displayDownloadedData(downloadModel: downloadModel)
        if DELEGATE.downloadManager.downloadingArray.count > 0{
                if downloadModel.status == TaskStatus.downloading.description() {
                    self.imgDownload.image = UIImage(named: "ic_pauseDownload")
                    DELEGATE.downloadManager.resumeDownloadTaskAtIndex(videoIndex!)
                    self.lblStatus.text = "Downloading..."
                    self.lblDownload.text = "Downloading"
                }else if downloadModel.status == TaskStatus.failed.description() {
                    self.imgDownload.image = UIImage(named: "ic_download")
                    self.lblDownload.text = "Download"
                    self.lblStatus.text = "Failed..."
                }else if downloadModel.status == TaskStatus.paused.description() {
                    self.imgDownload.image = UIImage(named: "ic_download")
                    self.lblStatus.text = "Paused..."
                    self.lblDownload.text = "Paused"

                }
                self.cnst_download.constant = 55
          }
     }
    func displayDownloadedData(downloadModel:MZDownloadModel){
            print("updating download...")
            
            var remainingTime: String = ""
        self.progressDownload.progress = downloadModel.progress
            var fileSize = ""
            if let _ = downloadModel.file?.size {
                fileSize = String(format: "%.2f %@", (downloadModel.file?.size)!, (downloadModel.file?.unit)!)
            }

            var speed = ""
            if let _ = downloadModel.speed?.speed {
                speed = String(format: "%.2f %@/sec", (downloadModel.speed?.speed)!, (downloadModel.speed?.unit)!)
            }
            
            var downloadedFileSize = ""
            if let _ = downloadModel.downloadedFile?.size {
                downloadedFileSize = String(format: "%.2f %@", (downloadModel.downloadedFile?.size)!, (downloadModel.downloadedFile?.unit)!)
            }
            
            let detailLabelText = NSMutableString()
            detailLabelText.appending("\(fileSize) / \(downloadedFileSize) | \(downloadModel.progress * 100.0)%")
            let str = String(format:"%.2f",downloadModel.progress * 100.0)

            if downloadModel.progress * 100.0 > 0.0{
                DispatchQueue.main.async {
                    self.lblRemainingData.text = "\(fileSize) / \(downloadedFileSize) | \(str)%"
                }
            }
    }
    func AddVideoToOffline(){
        let currentmovie = DELEGATE.downloadManager.downloadingArray.filter{$0.fileURL == self.movieDetail.final_video}
        if currentmovie.count == 0{
            let JSONString = self.movieDetail.toJSONString(prettyPrint: true)
            Common.DictDownloadedData(ArrData: JSONString ?? "")
            
            let today = Date()
            print(today)
            let modifiedDate = Calendar.current.date(byAdding: .day, value: intValue, to: today)!
            print(modifiedDate)

            let dict_movieToDelete:[String:Any] = ["movie_id":self.movieDetail._id ,"last_date":modifiedDate]
            var movieLast = Common.FetchMovieLastDate()
            if movieLast.count > 0{
                movieLast.append(dict_movieToDelete)
                Common.SetLastDateOfMovie(ArrData: movieLast)
            }
        }
    }
}


//MARK:-  New changes of application 20 April

extension NewMoviewDetailVC: UITextViewDelegate{

    func textViewDidChange(_ textView: UITextView) {
        heightForTxtChat.constant = 70
        if txtChatLandScape.isFirstResponder
        {
            
            if textView.contentSize.height > 43
            {
                
            }

            if heightTxtChatLandscape.constant < 50
            {
                textView.isScrollEnabled = false
              //  if textView.contentSize.height > 33 {
                    textView.sizeToFit()
                 //   print("heigt: ",textView.contentSize.height)
                heightTxtChatLandscape.constant = textView.contentSize.height
                  
              //  }
            } else {
                textView.isScrollEnabled = true
            }
        }
        if txtChat.isFirstResponder
        {
            
            if textView.contentSize.height > 50 {
                textView.isScrollEnabled = true
                heightForTxtChat.constant = 61
            } else {
                textView.sizeToFit()
                textView.isScrollEnabled = false
                heightForTxtChat.constant = textView.contentSize.height
            }

//            if heightForTxtChat.constant < 50 || textView.contentSize.height < 50
//            {
//                textView.isScrollEnabled = false
//                  //  heightForTxtChat.constant = textView.contentSize.height
//
//            } else {
//                textView.isScrollEnabled = true
//            }
        }
    }
    
}

extension NewMoviewDetailVC:PusherDelegate {
    func subscribedToChannel(name: String) {
     
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
          //  self.channel.trigger(eventName: self.eventName, data: "Nirav")
        }
       
        
    }
    func debugLog(message: String) {
        print("debugLog: ", message)
    }

}

extension NewMoviewDetailVC:AVAudioRecorderDelegate {
    //Mark: Audio Recording and voice chat
    func startRecording() {
        
       let recordingSession = AVAudioSession.sharedInstance()

        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default,options: [.defaultToSpeaker])
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        viewVoiceChat.isHidden = false
                        viewVoiceChatLandscape.isHidden = false
                        let settings = [
                            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                            AVSampleRateKey: 12000,
                            AVNumberOfChannelsKey: 1,
                            AVEncoderAudioQualityKey: AVAudioQuality.low.rawValue
                        ]

                        
                        do {
                            let audioFileUrl = getAudioFileURL()
                            print("audioFileUrl: ",audioFileUrl)
                            audioRecorder = try AVAudioRecorder(url: audioFileUrl, settings: settings)
                            audioRecorder?.delegate = self
                            audioRecorder?.record()
                            startTimerForAudio()
                        } catch {
                           // finishRecording(success: false)
                        }
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
       
    }
    func stopRecording(){
        audioRecorder?.stop()
        stopTimer()
        
    }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    func getAudioFileURL() -> URL {
        return getDocumentsDirectory().appendingPathComponent("Recording.m4a")
    }
    func getDownloadAudioFileURL() -> URL {
        return getDocumentsDirectory().appendingPathComponent("Download.m4a")
    }
    func handleAudioSendWith() {
        var isForReply = true
        if self.viewForReply.isHidden && !self.isReplyHide && self.viewForReplyLandscape.isHidden {
            isForReply = false
        }
        let fileUrl = getAudioFileURL()
        let item = AVPlayerItem.init(url: fileUrl)
        let duration = Int(item.asset.duration.seconds)
        let strDuration = formatSecondsToString(duration)
        do {
        let data = try Data.init(contentsOf: fileUrl)
        let metadata = StorageMetadata()
        metadata.contentType = "audio/m4a"
        let fileName = String(Int(Date().timeIntervalSince1970*1000)) + ".m4a"
            WebService.Loader.show()
            appDel.storageRef.child(self.groupId).child((appDel.loggedInUserData?._id ?? "id")+"#"+(fileName)).putData(data, metadata: metadata) { (metadata, error) in
            if error != nil {
                print(error ?? "error")
            }
                self.viewVoiceChat.isHidden = true
                self.viewVoiceChatLandscape.isHidden = true
                self.viewForReply.isHidden = true
                self.viewForReplyLandscape.isHidden = true
                appDel.storageRef.child(self.groupId).child((appDel.loggedInUserData?._id ?? "id")+"#"+(fileName)).downloadURL { url, error in
                    if self.getVideoUrl() == self.getAudioFileURL() {
                        self.audioPlayer?.pause()
                        self.audioPlayer?.replaceCurrentItem(with: nil)
                    }
                    print("download url: ",url?.absoluteString)
                    WebService.Loader.hide()
                  
                    if let url = url {
                        print("duration: ",strDuration)
                        if !isForReply {
                            self.sendMessage(tag: 1, audioUrl: url.absoluteString,duration:strDuration)
                        } else {
                            let replyObject = self.arrChats[self.selectedReply]
                            self.sendMessage(tag: 1, audioUrl: url.absoluteString, duration: strDuration, reply: replyObject.message, replyName: replyObject.senderName, replyId: replyObject.messageId,replyDuration: replyObject.duration,replySenderId: replyObject.senderId)
                        }
                        
                    } else {
                        print("audio url error")
                    }
                }
           
        }
        }
        catch {
            
        }
    }
    func downloadFile(url:URL,tempUrl:URL, block: @escaping ()->Void) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                   
                    return (tempUrl, [.removePreviousFile])
                }
      
        
            Alamofire.download(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
                
            }
            .validate { request, response, temporaryURL, destinationURL in
                // Custom evaluation closure now includes file URLs (allows you to parse out error messages if necessary)
                return .success
            }
            .responseJSON { response in
                debugPrint(response)
                print(response.temporaryURL?.absoluteString)
                print(response.destinationURL?.absoluteString)
                block()
            }
        
        
    }
}
public func formatSecondsToString(_ sec: Int) -> String {
   
  
    let seconds = sec % 60
    let minutes = (sec / 60) % 60
    let hour = (sec / 3600) % 3600
    if hour > 0 {
        return String(format: "%02d:%02d:%02d", hour, minutes, seconds)
    }
    return String(format: "%02d:%02d", minutes, seconds)
}
class ControlSocketObject {
    var isStart:Bool = true
    var currentDuration:Int = 0
    var isPlay:Bool = true
    var isEnd:Bool = false
    
    init(isStart:Bool,currentDuration:Int,isPlay:Bool,isEnd:Bool) {
        self.isStart = isStart
        self.currentDuration = currentDuration
        self.isPlay = isPlay
        self.isEnd = isEnd
    }
    init(dict:[String:Any]) {
        self.isStart = dict["isStart"] as? Bool ?? true
        self.currentDuration = dict["currentDuration"] as? Int ?? 0
        self.isPlay = dict["isPlay"] as? Bool ?? true
        self.isEnd = dict["isEnd"] as? Bool ?? false
    }
    func toDictionary()->[String:Any] {
        var dict:[String:Any] = [:]
        dict["isStart"] = self.isStart
        dict["currentDuration"] = self.currentDuration
        dict["isPlay"] = self.isPlay
        dict["isEnd"] = self.isEnd
        return dict
    }
}

