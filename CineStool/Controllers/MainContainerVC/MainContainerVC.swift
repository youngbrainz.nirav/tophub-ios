//
//  MainContainerVC.swift
//  CineStool
//
//  Created by YB on 23/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import SwiftyJSON
import MobileRTC

class ViewTab: UIView {
    //New Tab Button
    @IBOutlet weak var btnNewHome: UIButton!
    @IBOutlet weak var btnNewMyList: UIButton!
    @IBOutlet weak var btnNewSearch: UIButton!
    @IBOutlet weak var btnNewNotification: UIButton!
    @IBOutlet weak var btnNewProfile: UIButton!
    
    @IBOutlet weak var lblNewHome: UILabel!
    @IBOutlet weak var lblNewMyList: UILabel!
    @IBOutlet weak var lblNewSearch: UILabel!
    @IBOutlet weak var lblNewNotification: UILabel!
    @IBOutlet weak var lblNewProfile: UILabel!
    
    @IBOutlet weak var imgNewHome: UIImageView!
    @IBOutlet weak var imgNewMyList: UIImageView!
    @IBOutlet weak var imgNewSearch: UIImageView!
    @IBOutlet weak var imgNewNotification: UIImageView!
    @IBOutlet weak var imgNewProfile: UIImageView!

    var blockTab: (()->())!
    
    @IBAction func btnTabAction(_ sender: UIButton) {
        blockTab()
    }
}

class MainContainerVC: UIViewController,MobileRTCAuthDelegate {
    var isLiveShowAvailable:Bool = false
    var ObjLiveEventData:modelLiveShows = modelLiveShows(objData: JSON(["":""]))

    var meeting_no = ""
    var meeting_pwd = ""

    @IBOutlet var mainContainer: UIView!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblBadge: UILabel!
    
    @IBOutlet weak var viewHeader: UIView!
    
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var heightViewHeader: NSLayoutConstraint!

    @IBOutlet weak var btnTopSideMenu: UIButton!
    var arrTopMenu = ["HOST EVENT","MY LIST","DOWNLOADS"]
    
    @IBOutlet weak var viewBottomTab: UIView!
    @IBOutlet weak var heightBottomView: NSLayoutConstraint!
    
    @IBOutlet weak var topMainContainer: NSLayoutConstraint!
    
    @IBOutlet weak var viewTabNewHome: ViewTab!
    @IBOutlet weak var viewTabNewMyList: ViewTab!
    @IBOutlet weak var viewTabNewSearch: ViewTab!
    @IBOutlet weak var viewTabNewNotification: ViewTab!
    @IBOutlet weak var viewTabNewProfile: ViewTab!
    
    
    @IBOutlet weak var topBarView: UIView!
    @IBOutlet weak var lblHostLiveEvent: UILabel!
    @IBOutlet weak var lblLiveShows: UILabel!
    @IBOutlet weak var lblMyList: UILabel!
    
    var isfor = "Home"
    var notificationVC: NotificationVC!
    var mycollectionVC: MyCollectionVC!
    var createprofileVC: CreateProfileVC!
    var searchVC: MovieListingVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork(){
            self.startAnimatingLoader(message: "")
          //  onloadOperation()
            self.GetRunningApi()
            NotificationCenter.default.addObserver(self, selector: #selector(getBadge), name: .badge, object: nil)
        }
        onloadOperation()
    }
    
    @objc func getBadge(_ notification: Notification) {
        if let data = notification.userInfo as? [String: Any] {
            let count = data["badge"] as! Int
            lblBadge.text = "\(count)"
            if self.isfor != "Notifications" {
                if appDel.badgeCount != 0 {
                    lblBadge.isHidden = false
                } else {
                    lblBadge.isHidden = true
                }
            } else {
                lblBadge.isHidden = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if ((findtopViewController() as? LandscapeAVPlayerController) != nil) {
            return
        }
        
        self.lblHostLiveEvent.textColor = self.isLiveShowAvailable ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)//self.arrTopMenu[0]
        if self.isLiveShowAvailable{
            self.btnJoin.layer.cornerRadius = 19.0
            self.btnJoin.layer.borderColor = #colorLiteral(red: 0.8039215686, green: 0, blue: 0, alpha: 1)
            self.btnJoin.layer.borderWidth = 1.2

        }else{
            self.btnJoin.layer.borderColor = #colorLiteral(red: 0.8039215686, green: 0, blue: 0, alpha: 0)
        }
        
        self.lblHostLiveEvent.text = self.isLiveShowAvailable ? "JOIN EVENT" : "HOST EVENT"//self.arrTopMenu[0]
        self.lblLiveShows.text = self.arrTopMenu[1]
        self.lblMyList.text = self.arrTopMenu[2]
        
        self.viewBottomTab.isHidden = false
        self.heightBottomView.constant = 55
        viewHeader.isHidden = false
        heightViewHeader.constant = 50
        
        if isfor == "Home"{
            self.setupHomeTab()
        }else if isfor == "My Collection"{
            self.setupMyListTab()
        }else if isfor == "Search"{
            self.setupSearchTab()
        }else if isfor == "Notifications"{
            self.setupNotificationTab()
        }else if isfor == "My Profile"{
            self.setupProfileTab()
        }
       // withRenderingMode(.alwaysTemplate)
        let img = #imageLiteral(resourceName: "logo")
        self.btnTopSideMenu.setImage(img, for: .normal)
        self.btnTopSideMenu.tintColor = .white
    }
    
    func onloadOperation() {
        if appDel.isFirstTime {
            self.setupHomeTab()
            appDel.isFirstTime = false
        }
        
        viewTabNewHome.blockTab = {
            self.setupHomeTab()
        }
        
        viewTabNewMyList.blockTab = {
            self.setupMyListTab()
        }
        
        viewTabNewSearch.blockTab = {
            self.setupSearchTab()
        }
        
        viewTabNewNotification.blockTab = {
            self.setupNotificationTab()
        }
        
        viewTabNewProfile.blockTab = {
            self.setupProfileTab()
        }
    }
    
    func setupHomeTab(){
        viewHeader.isHidden = false
        heightViewHeader.constant = 50
        
        self.isfor = "Home"
        self.lblHeader.isHidden = true
        self.topBarView.isHidden = false
        setContiner(VC: "HomeVC", nibClass: HomeVC.self, parent: MainContainerVC.self, container: self.mainContainer) { vc in
            if let homevc = vc as? HomeVC {
                homevc.reloadSidemenu = {
                    if self.drawerController?.getViewController(for: .left) is SideMenuVC {
                        let sideVC = self.drawerController?.getViewController(for: .left) as! SideMenuVC
                        
                        sideVC.reloadData()
                    }
                }
            }
        }
        self.setupBottomImage(tag: 1)
    }
    
    func setupMyListTab(){
        viewHeader.isHidden = false
        heightViewHeader.constant = 50
        
        self.isfor = "My Collection"
        self.lblHeader.isHidden = false
        self.lblHeader.text = "My Collection"
        self.topBarView.isHidden = true
        setContiner(VC: "MyCollectionVC", nibClass: MyCollectionVC.self, parent: MainContainerVC.self, container: self.mainContainer, newController: { (controller) in
            if let new = controller as? MyCollectionVC {
                self.mycollectionVC = new
                new.listType = "1"
            }
        })
        self.setupBottomImage(tag: 2)
    }
    
    func setupSearchTab(){
        viewHeader.isHidden = true
        heightViewHeader.constant = 0
        topMainContainer.constant = 0
        
        self.isfor = "Search"
        self.lblHeader.isHidden = false
        self.lblHeader.text = "Search"
        self.topBarView.isHidden = true
        setContiner(VC: "MovieListingVC", nibClass: MovieListingVC.self, parent: MainContainerVC.self, container: self.mainContainer, newController: { (controller) in
            if let new = controller as? MovieListingVC {
                self.searchVC = new
                new.isFrom = .home
                new.blockBack = {
                    guard self.drawerController?.drawerSide != .left else {
                        self.drawerController?.closeSide()
                        return
                    }
                    self.drawerController?.openSide(.left)
                }
            }
        })
        self.setupBottomImage(tag: 3)
    }
    
    func setupNotificationTab(){
        viewHeader.isHidden = false
        heightViewHeader.constant = 50
        
        self.lblBadge.isHidden = true
        self.lblBadge.text = "0"
        self.isfor = "Notifications"
        self.lblHeader.isHidden = false
        self.lblHeader.text = "Notifications"
        self.topBarView.isHidden = true
        setContiner(VC: "NotificationVC", nibClass: NotificationVC.self, parent: MainContainerVC.self, container: self.mainContainer, newController: { (controller) in
            if let new = controller as? NotificationVC {
                self.notificationVC = new
            }
        })
        self.setupBottomImage(tag: 4)
    }
    
    func setupProfileTab(){
        viewHeader.isHidden = false
        heightViewHeader.constant = 50
        
        self.isfor = "My Profile"
        self.lblHeader.isHidden = true
        self.topBarView.isHidden = true
        setContiner(VC: "CreateProfileVC", nibClass: CreateProfileVC.self, parent: MainContainerVC.self, container: self.mainContainer, newController: { (controller) in
            if let new = controller as? CreateProfileVC {
                self.createprofileVC = new
                new.blockDidDismiss = {
                    
                }
            }
        })
        self.setupBottomImage(tag: 5)
    }
    
    func setupBottomImage(tag:Int){
        
        self.viewTabNewHome.lblNewHome.textColor = .gray
        self.viewTabNewHome.imgNewHome.image = #imageLiteral(resourceName: "Home1").withRenderingMode(.alwaysTemplate)
        self.viewTabNewHome.imgNewHome.tintColor = .gray
        
        self.viewTabNewMyList.lblNewMyList.textColor = .gray
        self.viewTabNewMyList.imgNewMyList.image = #imageLiteral(resourceName: "List1").withRenderingMode(.alwaysTemplate)
        self.viewTabNewMyList.imgNewMyList.tintColor = .gray
        
        self.viewTabNewSearch.lblNewSearch.textColor = .gray
        self.viewTabNewSearch.imgNewSearch.image = #imageLiteral(resourceName: "Search1").withRenderingMode(.alwaysTemplate)
        self.viewTabNewSearch.imgNewSearch.tintColor = .gray
        
        
        self.viewTabNewNotification.lblNewNotification.textColor = .gray
        self.viewTabNewNotification.imgNewNotification.image = #imageLiteral(resourceName: "Notify1").withRenderingMode(.alwaysTemplate)
        self.viewTabNewNotification.imgNewNotification.tintColor = .gray
        
        self.viewTabNewProfile.lblNewProfile.textColor = .gray
        self.viewTabNewProfile.imgNewProfile.image = #imageLiteral(resourceName: "Profile1").withRenderingMode(.alwaysTemplate)
        self.viewTabNewProfile.imgNewProfile.tintColor = .gray
        
        if tag == 1{
            self.viewTabNewHome.imgNewHome.tintColor = .white
            self.viewTabNewHome.lblNewHome.textColor = .white
        }else if tag == 2{
            self.viewTabNewMyList.lblNewMyList.textColor = .white
            self.viewTabNewMyList.imgNewMyList.tintColor = .white
        }else if tag == 3{
            self.viewTabNewSearch.lblNewSearch.textColor = .white
            self.viewTabNewSearch.imgNewSearch.tintColor = .white
        }else if tag == 4{
            self.viewTabNewNotification.lblNewNotification.textColor = .white
            self.viewTabNewNotification.imgNewNotification.tintColor = .white
        }else if tag == 5{
            self.viewTabNewProfile.lblNewProfile.textColor = .white
            self.viewTabNewProfile.imgNewProfile.tintColor = .white
        }
    }
        
    @IBAction func btnSideMenuAction(_ sender: UIButton) {
        guard drawerController?.drawerSide != .left else {
            drawerController?.closeSide()
            return
        }
        drawerController?.openSide(.left)
    }
    
    //
    @IBAction func btnHostLiveEventTapped(_ sender: UIButton) {
//        if appDel.isHomePaymentResounse{
//        }else{
//            alertOk(title: "", message: "Please wait a moment and try again")
//        }
        
        let startDate = self.ObjLiveEventData.start_time.toDate(withFormat: "dd-MM-yyyy hh:mm:ss") ?? Date()
        let endDate = self.ObjLiveEventData.end_time.toDate(withFormat: "dd-MM-yyyy hh:mm:ss") ?? Date()
        let currentDate = Date().toString(withFormat: "dd-MM-yyyy hh:mm:ss").toDate(withFormat: "dd-MM-yyyy hh:mm:ss") ?? Date()
        

        if self.isLiveShowAvailable{
          //  if  (currentDate > startDate) && (currentDate < endDate){
                if self.ObjLiveEventData.meeting_type == "1"{
                    self.meeting_no = self.ObjLiveEventData.meeting_number
                    self.meeting_pwd = self.ObjLiveEventData.password
                    self.initilizeZoom()
                }else{
                    if self.ObjLiveEventData.isPaid == "1"{
                        self.meeting_no = self.ObjLiveEventData.meeting_number
                        self.meeting_pwd = self.ObjLiveEventData.password
                        self.initilizeZoom()
                    }else{
                        let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                        objTab.isFromSubscribe = .GoLive
                        objTab.liveShowData = ObjLiveEventData
                        self.navigationController!.pushViewController(objTab, animated: true)
                    }
                }
            ///}
        }else{
           let objTab = GoLiveReqVC(nibName: "GoLiveReqVC", bundle: nil)
            self.navigationController!.pushViewController(objTab, animated: true)
//            let objTab = GoLiveVC(nibName: "GoLiveVC", bundle: nil)
//            objTab.isFrom = .audience
//            objTab.arrEventData = self.arrEventData[indexPath.row]
//            findtopViewController()?.navigationController?.pushViewController(objTab, animated: true)

        }
    }
    
    @IBAction func btnLiveShowsTapped(_ sender: UIButton) {
        let objTab = LiveShowsVC(nibName: "LiveShowsVC", bundle: nil)
//        let objTab = GoLiveVC(nibName: "GoLiveVC", bundle: nil)
//        objTab.isFrom = .broadcast
//        //objTab.arrEventData = self.arrEventData[indexPath.row]
        findtopViewController()?.self.navigationController!.pushViewController(objTab, animated: true)
    }
    
    @IBAction func btnMyListTapped(_ sender: UIButton) {
        
        
        
        let objTab = DownloadedVC(nibName: "DownloadedVC", bundle: nil)
        findtopViewController()?.self.navigationController!.pushViewController(objTab, animated: true)
      //  self.setupMyListTab()
    }
    
    //MARK:- API Calling
    
    func initilizeZoom(){
            let authService = MobileRTC.shared().getAuthService()
            
            authService?.delegate = self
    //        authService?.jwtToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6Ikhva1laN2RlVDhxcUJZNHBWeEtPTHciLCJleHAiOjE2MDc5MjkxNDcsImlhdCI6MTYwNzMyNDM0OH0.Jgo5Uti4-fMNqo-tstEvgYYEh18Xrwr4jfCgPS2lJdk"
            authService?.clientSecret = "k3078OlFvW0RKnE5NyOmGbEAZGysmoc31B8k"
            authService?.clientKey = "YXHy1QZd0XTSV0fY4N5JgW6QQDIGnsF94hhm"
            authService?.sdkAuth()
        }
        
        func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
            if self.meeting_no != "" && self.meeting_pwd != ""{
                if returnValue == .success{
                    print("Successfully authenticated")
                        let paramDict = [
                            kMeetingParam_Username: appDel.loggedInUserData?.user_name == "" ? appDel.loggedInUserData?.name : appDel.loggedInUserData?.user_name,
                            kMeetingParam_MeetingNumber: self.meeting_no,
                            kMeetingParam_MeetingPassword: self.meeting_pwd
                        ]
                        let ms = MobileRTC.shared().getMeetingService()
                    
                        let ret = ms?.joinMeeting(with: paramDict)
                }else{
                    self.meeting_no = ""
                    self.meeting_pwd = ""
                    self.GetRunningApi()
                    self.alertOk(title: "Error", message: "failed authentication")
                }
            }else{
                self.alertOk(title: "Error", message: "failed authentication")
            }
    }
    
    func GetRunningApi(){
        
        if appDel.loginUser_Id != ""{
            let params = ["page":"1","orderBy":"_id","dir":"asc","userID":appDel.loginUser_Id] as [String : Any]
            var ishideLoader = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
                self.GetRunningApi()
            }
            WebService.Request.patch(url: getRunningEvent, type: .post, parameter: params, callSilently : true) { (response, error) in
                //print(response)
                if error == nil {
                    if response!["status"] as? Int == 1 {
                        if let data = response!["data"] as? [String: Any] {
                            self.ObjLiveEventData = modelLiveShows(objData: JSON(data))
                            self.isLiveShowAvailable = true
                            self.lblHostLiveEvent.text = "JOIN EVENT"
                            
                        }else{
                            self.isLiveShowAvailable = false
                            self.lblHostLiveEvent.text = "HOST EVENT"
                        }
                        self.lblHostLiveEvent.textColor = self.isLiveShowAvailable ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)//self.arrTopMenu[0]
                        if self.isLiveShowAvailable{
                            self.btnJoin.layer.cornerRadius = 19.0
                            self.btnJoin.layer.borderColor = #colorLiteral(red: 0.8039215686, green: 0, blue: 0, alpha: 1)
                            self.btnJoin.layer.borderWidth = 1.2
                        }else{
                            self.btnJoin.layer.borderColor = #colorLiteral(red: 0.8039215686, green: 0, blue: 0, alpha: 0)
                        }
                    } else {
                        self.alertOk(title: "", message: response!["msg"] as! String)
                    }
                } else {
                    self.isLiveShowAvailable = false
                    self.btnJoin.layer.borderColor = #colorLiteral(red: 0.8039215686, green: 0, blue: 0, alpha: 0)
                    self.lblHostLiveEvent.text = "HOST EVENT"
                    //                self.alertOk(title: "", message: "Something went wrong please try again!")
                }
                WebService.Loader.hide()
            }
        }
    }
}
