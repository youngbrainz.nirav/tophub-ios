//
//  TopMenuCollCell.swift
//  CineStool
//
//  Created by Dharam YB on 24/04/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit

class TopMenuCollCell: UICollectionViewCell, ReusableView, NibLoadableView {

    //MARK:- Variables & Outlets
    @IBOutlet weak var lblMenuTitle: UILabel!
    
    //MARK:- Default Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
