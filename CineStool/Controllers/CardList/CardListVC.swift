//
//  CardListVC.swift
//  CineStool
//
//  Created by Dharam YB on 18/02/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import AVFoundation
import ACFloatingTextfield_Swift
import IQKeyboardManagerSwift
import KWDrawerController
import SwiftyJSON
import MobileRTC

enum forSubscribe {
    case No
    case Yes
    case GoLive
    case PayPerView
}

class CardListVC: UIViewController,UITextFieldDelegate,MobileRTCAuthDelegate {

    @IBOutlet var tblCardList: UITableView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnPayNow: UIButton!
    @IBOutlet var heighBtnPayNow: NSLayoutConstraint!
    @IBOutlet var imgAddCard: UIImageView!

    @IBOutlet weak var viewMobileMoneyInfo: UIView!
    @IBOutlet weak var lblPopUpPrice: UILabel!
    
    @IBOutlet weak var viewInsertCVV: UIView!
    @IBOutlet weak var lblCVVTitle: UILabel!
    @IBOutlet weak var lblCVVDesc: UILabel!
    @IBOutlet weak var btnAddNewCard: UIButton!
    @IBOutlet weak var txtCVV: ACFloatingTextfield!
    @IBOutlet weak var btnAdd: UIButton!

    var isFromSubscribe:forSubscribe = .No
    var index = 0
    var liveShowData:modelLiveShows = modelLiveShows(objData: ["":""])
    var selectedSubscription = SubscriptionModel()
    var cardListing = [cardListingModel]()
    var paymentType = "2"
    var isfromLogin:isComingFrom = .none
    var arrEventData: EventData?
    var GoLivePrice = ""
    var PayPerViewPrice = ""
    var movieSelectedMovieID:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnAddNewCard.isHidden = true
        self.imgAddCard.isHidden = true
        viewInsertCVV.isHidden = true
        self.btnAdd.isHidden = true

        self.txtCVV.isSecureTextEntry = true
        self.txtCVV.keyboardType = .numberPad
        self.tblCardList.register(CellCardLIst.self)
        tblCardList.tableFooterView = UIView()
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
        let codeInfo = countryInfo["country"] as! String
        let currencyCodedetail = Locale.currencies[codeInfo]
        let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")
        if isFromSubscribe == .Yes{
            let price = Float(self.selectedSubscription?.price ?? "0.0")! * Float(self.selectedSubscription?.plan_number ?? "0.0")!
            lblPopUpPrice.text = "\(currencyCode ?? "$")\(String(format: "%.2f", price))"
        }else if isFromSubscribe == .GoLive{
            let price = Float(self.liveShowData.amount) ?? 0.0
            lblPopUpPrice.text = "\(currencyCode ?? "$")\(String(format: "%.2f", price))"
        }else if isFromSubscribe == .PayPerView{
            let price = Float(PayPerViewPrice) ?? 0.0
            lblPopUpPrice.text = "\(currencyCode ?? "$")\(String(format: "%.2f", price))"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.isFromSubscribe == .No{
            self.lblTitle.text = "CARD LIST"
            self.btnPayNow.isHidden = true
            self.btnPayNow.setTitle("", for: .normal)
            self.heighBtnPayNow.constant = 0
        }else if isFromSubscribe == .GoLive{
            self.lblTitle.text = "Payment options"
            self.btnPayNow.isHidden = false
            self.heighBtnPayNow.constant = 40
            self.btnPayNow.setTitle("PAY", for: .normal)
        }else{
            self.lblTitle.text = "Payment options"
            self.btnPayNow.isHidden = false
            self.heighBtnPayNow.constant = 40
            self.btnPayNow.setTitle("PAY", for: .normal)
        }
        self.apiCallGetCard()
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnSquareTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.isfromLogin == .MoviePlaying{
            appDel.isAppdelTimerValid = true
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddTapped(_ sender: UIButton) {
        let objTab = AddEditCardVC(nibName: "AddEditCardVC", bundle: nil)
        objTab.isFrom = .add
        self.navigationController!.pushViewController(objTab, animated: true)
    }
    @IBAction func btnAddNewCard(_ sender: UIButton) {
        let objTab = AddEditCardVC(nibName: "AddEditCardVC", bundle: nil)
        objTab.isFrom = .add
        self.navigationController!.pushViewController(objTab, animated: true)
    }
    
    @IBAction func btnPayNowTapped(_ sender: UIButton) {
        if index == 0{
            self.viewMobileMoneyInfo.isHidden = false
        }else{
            if self.isFromSubscribe == .GoLive{
                let strCard = self.cardListing[self.index - 1].card_number.replace_fromStart(str: self.cardListing[self.index - 1].card_number, endIndex: 12, With: "************")
                self.lblCVVDesc.text = "Please insert CVV for this card: \(strCard)"
                self.lblCVVTitle.text = self.lblPopUpPrice.text ?? ""
                self.viewInsertCVV.isHidden = false
            }else if self.isFromSubscribe == .PayPerView{
                let strCard = self.cardListing[self.index - 1].card_number.replace_fromStart(str: self.cardListing[self.index - 1].card_number, endIndex: 12, With: "************")
                self.lblCVVDesc.text = "Please insert CVV for this card: \(strCard)"
                self.lblCVVTitle.text = self.lblPopUpPrice.text ?? ""
                self.viewInsertCVV.isHidden = false
            }else{
                let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
                let codeInfo = countryInfo["country"] as! String
                let currencyCodedetail = Locale.currencies[codeInfo]
                let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")

                let strCard = self.cardListing[self.index - 1].card_number.replace_fromStart(str: self.cardListing[self.index - 1].card_number, endIndex: 12, With: "************")
                if self.isFromSubscribe == .Yes{
                    let price = Float(self.selectedSubscription?.price ?? "0.0")! * Float(self.selectedSubscription?.plan_number ?? "0.0")!
                    self.lblCVVTitle.text = "\(self.selectedSubscription?.plan ?? "") (\(currencyCode ?? "$")\((String(format: "%.2f", price))))"
                }
                self.lblCVVDesc.text = "Please insert CVV for this card: \(strCard)"
                self.viewInsertCVV.isHidden = false
            }
        }
    }
    
    @IBAction func btnHideCVVView(_ sender: UIButton) {
        self.view.endEditing(true)
        txtCVV.text = ""
        viewInsertCVV.isHidden = true
    }
    
    @IBAction func btnCancelCVV(_ sender: UIButton) {
        self.view.endEditing(true)
        txtCVV.text = ""
        viewInsertCVV.isHidden = true
    }

    @IBAction func btnPayCVV(_ sender: UIButton) {
        self.view.endEditing(true)
        viewInsertCVV.isHidden = true
        if txtCVV.text == ""{
            alertOk(title: "", message: "Please insert CVV.")
        }else{
            if self.isFromSubscribe == .GoLive{
                if self.liveShowData._id != ""{
                    self.apiCallMakePaymentForGoLive_shows()
                }else{
                    self.apiCallMakePaymentForGoLive()
                }
                txtCVV.text = ""
            }else if self.isFromSubscribe == .PayPerView{
                self.apiCallMakePayment(amount: self.PayPerViewPrice, PaypalTransId: "", cvv: txtCVV.text ?? "")
            }else{
                apiCallMakePaymentForSubscription(amount: "\(self.selectedSubscription?.actual_amount ?? "")", PaypalTransId: "", cvv: txtCVV.text ?? "")
                txtCVV.text = ""

            }
        }
    }
    
    @IBAction func btnViewMobileMoneyHideAction(_ sender: UIButton) {
        self.viewMobileMoneyInfo.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtCVV {
            let maxLength = 3
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else {
            return true
        }
    }
}

extension CardListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.isFromSubscribe == .No{
            return self.cardListing.count
        }else{
            return self.cardListing.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: CellCardLIst = tableView.dequeueReusableCellTb(for: indexPath)
        cell.btnMakeDefault.tag = indexPath.row
        
        if indexPath.row == self.index{
            cell.btnRadio.setImage(#imageLiteral(resourceName: "ratioTick"), for: .normal)
        }else{
            cell.btnRadio.setImage(#imageLiteral(resourceName: "ratioUnTick"), for: .normal)
        }
        
        if self.isFromSubscribe == .No{
            cell.btnMakeDefault.isHidden = false
            cell.btnRadio.isHidden = true
            cell.lblExpDate.text = cardListing[indexPath.row].expiry_date
            if self.cardListing[indexPath.row].is_default_card == "1"{
                cell.btnMakeDefault.isHidden = true
            }
            
            cell.blockMakeDefault = { tag in
                print(tag)
                self.apiCallMakeDefaultCard(cardId: self.cardListing[tag]._id, indexSelected: tag)
            }
            cell.lblName.isHidden = false
            cell.lblNo.isHidden = false
            cell.lblExpDate.text = cardListing[indexPath.row].expiry_date
            cell.lblName.text = cardListing[indexPath.row].holder_name
            
            cell.lblNo.text = cardListing[indexPath.row].card_number.replace_fromStart(str: cardListing[indexPath.row].card_number, endIndex: 12, With: "************")


        }else{
            cell.btnMakeDefault.isHidden = true
            cell.btnRadio.isHidden = false
            if indexPath.row == 0{
                cell.lblName.isHidden = true
                cell.lblNo.isHidden = true
                cell.lblExpDate.text = "Mobile Money"
            }else{
                cell.lblName.isHidden = false
                cell.lblNo.isHidden = false
                cell.lblExpDate.text = cardListing[indexPath.row - 1].expiry_date
                cell.lblName.text = cardListing[indexPath.row - 1].holder_name
                cell.lblNo.text = cardListing[indexPath.row - 1].card_number.replace_fromStart(str: cardListing[indexPath.row - 1].card_number, endIndex: 12, With: "************")
            }
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.isFromSubscribe == .No{
            return UITableView.automaticDimension
        }else{
            if indexPath.row == 0{
                return 60
            }else{
                return UITableView.automaticDimension
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        if self.isFromSubscribe == .No && self.cardListing.count > 1{
//            return true
//        }else{
//            return false
//        }
        if self.isFromSubscribe == .No{
            if self.cardListing.count > 0{
                return true
            }else{
                return false
            }
        }else{
            return false
            /*if indexPath.row == 0{
                return false
            }else if self.cardListing.count > 0{
                return true
            }else{
                return false
            }*/
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.alertTwoButton(title: "TopHub", message: "Are you sure you want to Delete?") { (result) in
                if result {
                    self.apiCallDeleteCard(cardId: self.cardListing[indexPath.row]._id, indexSelected: indexPath.row)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isFromSubscribe == .No{
            let objTab = AddEditCardVC(nibName: "AddEditCardVC", bundle: nil)
            objTab.isFrom = .edit
            objTab.name = self.cardListing[indexPath.row].holder_name
            objTab.cardNumber = self.cardListing[indexPath.row].card_number
            objTab.date = self.cardListing[indexPath.row].expiry_date
            objTab.cvv = self.cardListing[indexPath.row].cvv
            objTab.card_id = self.cardListing[indexPath.row]._id
            self.navigationController!.pushViewController(objTab, animated: true)
        }else{
            self.index = indexPath.row
            self.tblCardList.reloadData()
        }
    }
    
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
//            print("index path of delete: \(indexPath)")
//            completionHandler(true)
//        }
//
//        let rename = UIContextualAction(style: .normal, title: "Edit") { (action, sourceView, completionHandler) in
//            print("index path of edit: \(indexPath)")
//            completionHandler(true)
//        }
//        let swipeActionConfig = UISwipeActionsConfiguration(actions: [rename, delete])
//        swipeActionConfig.performsFirstActionWithFullSwipe = false
//        return swipeActionConfig
//    }
}

extension CardListVC{
    func apiCallGetCard() {
        WebService.Request.patch(url: getUserCard, type: .post, parameter: [:]) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    print("-->",response!)
                    if let data = response!["data"] as? [[String: Any]] {
                        self.cardListing = data.map{cardListingModel(JSON: $0)!}
                        self.tblCardList.reloadData()
                        if self.cardListing.count == 0{
                            self.imgAddCard.isHidden = false
                            self.btnAddNewCard.isHidden = false
                            self.btnPayNow.isHidden = true
                            self.btnAdd.isHidden = false

                        }else{
                            self.imgAddCard.isHidden = true
                            self.btnAddNewCard.isHidden = true
                            self.btnPayNow.isHidden = false
                            self.btnAdd.isHidden = true

                        }
                    }else{
                        
                    }
                } else {
                    self.imgAddCard.isHidden = false
                    self.btnAddNewCard.isHidden = false
                    self.btnAdd.isHidden = false
                    self.btnPayNow.isHidden = true
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.imgAddCard.isHidden = false
                self.btnAddNewCard.isHidden = false
                self.btnPayNow.isHidden = true
                self.btnAdd.isHidden = false

                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallMakeDefaultCard(cardId: String, indexSelected: NSInteger) {
        let param = ["card_id": cardId]
        WebService.Request.patch(url: makeDefaultUserCard, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    print("-->",response!)
                    let itemFirst = self.cardListing[0]
                    itemFirst.is_default_card = "0"
                    let itemSelected = self.cardListing[indexSelected]
                    itemSelected.is_default_card = "1"
                    self.cardListing[0] = itemSelected
                    self.cardListing[indexSelected] = itemFirst
                    self.tblCardList.reloadData()
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallDeleteCard(cardId: String, indexSelected: NSInteger) {
        let param = ["card_id": cardId]
        WebService.Request.patch(url: deleteUserCard, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    print("-->",response!)
                        self.cardListing.remove(at: indexSelected)
                        self.tblCardList.reloadData()
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    func apiCallMakePayment(amount: String, PaypalTransId: String,cvv:String) {
        
        var param = [String:Any]()
        param = ["payment_type" : paymentType,
                 "amount" : amount,
                 "movie_id": self.movieSelectedMovieID,
                 "payment_for": "4","mobile_uuid":appDel.getDeviceUUID()]
        if paymentType == "1" {
            param["transactions_id"] = PaypalTransId
        } else if paymentType == "2" {
            param["card_number"] = "\(self.cardListing[self.index - 1].card_number)"
            param["cvv"] = "\(cvv)"
            param["expiry_month"] = "\(self.cardListing[self.index - 1].expiry_date)".components(separatedBy: "/").first
            param["expiry_year"] = "\(self.cardListing[self.index - 1].expiry_date)".components(separatedBy: "/").last
        } else {
            
        }
        WebService.Request.patch(url: makePayment, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                        self.alertOk(title: "", message: response!["msg"] as! String, buttonTitle: "OK", completion: { (Bool) in
                        self.navigationController?.popViewController(animated: true)
                    })
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    func apiCallMakePaymentForSubscription(amount: String, PaypalTransId: String, cvv: String) {
        var price = Float()
        if isFromSubscribe == .Yes{
            price = Float(self.selectedSubscription?.actual_amount ?? "0.0")! * Float(self.selectedSubscription?.plan_number ?? "0.0")!
        }

        var param = [String:Any]()
        param = ["payment_type" : paymentType,
                 "amount" : String(format: "%.2f", price),
                 "subscription_id": self.selectedSubscription?._id ?? "",
                 "is_promo_added": appDel.promoCode,
                 "promo_code":appDel.promoCodeText]
        if paymentType == "1" {
            param["transactions_id"] = PaypalTransId
        } else if paymentType == "2" {
            param["card_number"] = "\(self.cardListing[self.index - 1].card_number)"
            param["cvv"] = "\(cvv)"
            param["expiry_month"] = "\(self.cardListing[self.index - 1].expiry_date)".components(separatedBy: "/").first
            param["expiry_year"] = "\(self.cardListing[self.index - 1].expiry_date)".components(separatedBy: "/").last
        } else {
            
        }
        WebService.Request.patch(url: SubscribedUserPayment, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.alertOk(title: "", message: response!["msg"] as! String, buttonTitle: "OK", completion: { (Bool) in
                        let strSubscribed = response!["is_plan_subscribed"] as? String ?? "0"
                        if strSubscribed == "1"{
                            appDel.is_Subscribed = true
                        }else{
                            appDel.is_Subscribed = false
                        }
                        UserDefaults.standard.set(appDel.is_Subscribed, forKey: "is_Subscribed")
                        
                        if self.isfromLogin == .MoviePlaying{
                            appDel.isWatchMovie = true
                            self.navigationController?.popViewController(animated: true)
                            //appDel.displayScreen()
                        }else{
                            let mainViewController = MainContainerVC(nibName: "MainContainerVC", bundle: .main)
                            let menuViewController = SideMenuVC(nibName: "SideMenuVC", bundle: .main)
                            //Set for callback use
                            appDel.sideMenuVC = menuViewController
                            let aDrawer = DrawerController()
                            aDrawer.setViewController(mainViewController, for: .none)
                            aDrawer.setViewController(menuViewController, for: .left)
                            aDrawer.drawerWidth = Float((UIScreen.main.bounds.size.width / 3) * 2) + 30
                            self.navigationController?.pushViewController(aDrawer, animated: true)
                        }
                    })
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallMakePaymentForGoLive_shows() {
        var param = [String:Any]()
        let event_date = self.arrEventData?.event_date ?? ""
        let temp = event_date.components(separatedBy: " ")
        print(temp)
        var eveDate = ""
        if temp.count > 1{
           eveDate = temp[0]
        }else{
           eveDate = self.arrEventData?.event_date ?? ""
        }
        
        param = ["payment_type":self.paymentType,
                 "event_name":self.liveShowData.topic,
                 "event_date":self.liveShowData.start_time,
                 "card_number":"\(self.cardListing[self.index - 1].card_number)",
                 "cvv":"\(self.cardListing[self.index - 1].cvv)",
                "expiry_year":"\(self.cardListing[self.index - 1].expiry_date)".components(separatedBy: "/").last ?? "",
                "expiry_month":"\(self.cardListing[self.index - 1].expiry_date)".components(separatedBy: "/").first ?? "",
                "amount":self.liveShowData.amount,
                 "event_id":self.liveShowData._id]
        
        WebService.Request.patch(url: makeGoLivePayment, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
//                    let strSubscribed = response!["is_plan_subscribed"] as? String ?? "0"
//                    if strSubscribed == "1"{
//                        appDel.is_Subscribed = true
//                    }else{
//                        appDel.is_Subscribed = false
//                    }
//                    UserDefaults.standard.set(appDel.is_Subscribed, forKey: "is_Subscribed")
                    self.initilizeZoom()
                    
//                    let objTab = HomeVC(nibName: "HomeVC", bundle: nil)
////                    objTab.isFrom = .audience
////                    objTab.arrEventData = self.arrEventData
//                    findtopViewController()?.self.navigationController!.pushViewController(objTab, animated: true)
                    //self.alertOk(title: "", message: response!["msg"] as! String, buttonTitle: "OK", completion: { (Bool) in
                    //})
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallMakePaymentForGoLive() {
        var param = [String:Any]()
        let event_date = self.arrEventData?.event_date ?? ""
        let temp = event_date.components(separatedBy: " ")
        print(temp)
        var eveDate = ""
        if temp.count > 1{
           eveDate = temp[0]
        }else{
           eveDate = self.arrEventData?.event_date ?? ""
        }
        
        param = ["payment_type":self.paymentType,
                 "event_name":self.arrEventData?.event_name ?? "",
                 "event_date":eveDate,
                 "card_number":"\(self.cardListing[self.index - 1].card_number)",
                 "cvv":"\(self.cardListing[self.index - 1].cvv)",
                "expiry_year":"\(self.cardListing[self.index - 1].expiry_date)".components(separatedBy: "/").last ?? "",
                "expiry_month":"\(self.cardListing[self.index - 1].expiry_date)".components(separatedBy: "/").first ?? "",
                 "amount":self.GoLivePrice,
                 "event_id":self.arrEventData?._id ?? ""]
        
        WebService.Request.patch(url: makeGoLivePayment, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    let strSubscribed = response!["is_plan_subscribed"] as? String ?? "0"
                    if strSubscribed == "1"{
                        appDel.is_Subscribed = true
                    }else{
                        appDel.is_Subscribed = false
                    }
                    UserDefaults.standard.set(appDel.is_Subscribed, forKey: "is_Subscribed")
                    let objTab = GoLiveVC(nibName: "GoLiveVC", bundle: nil)
                    objTab.isFrom = .audience
                    objTab.arrEventData = self.arrEventData
                    findtopViewController()?.self.navigationController!.pushViewController(objTab, animated: true)
                    //self.alertOk(title: "", message: response!["msg"] as! String, buttonTitle: "OK", completion: { (Bool) in
                    //})
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallFetchGoLivePayment() {
        WebService.Request.patch(url: fetchPayemntData, type: .post, parameter: [:], callSilently : true) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String : Any] {
                        //"actual_currency_code" = "$";
                        //"go_live_view_actual_charge" = "1.50";
                        //"go_live_view_charge" = "114.05";
                        //"user_currency_code" = INR;
                        
                        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
                        let codeInfo = countryInfo["country"] as! String
                        let currencyCodedetail = Locale.currencies[codeInfo]
                        let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")
                        self.lblPopUpPrice.text = "\(currencyCode ?? "") \(data["go_live_view_charge"] as? String ?? "")"
                        self.GoLivePrice = data["go_live_view_charge"] as? String ?? ""
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    //MARK: - Zoom calling
    func initilizeZoom(){
            let authService = MobileRTC.shared().getAuthService()
            authService?.delegate = self
    //        authService?.jwtToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6Ikhva1laN2RlVDhxcUJZNHBWeEtPTHciLCJleHAiOjE2MDc5MjkxNDcsImlhdCI6MTYwNzMyNDM0OH0.Jgo5Uti4-fMNqo-tstEvgYYEh18Xrwr4jfCgPS2lJdk"
            authService?.clientSecret = "k3078OlFvW0RKnE5NyOmGbEAZGysmoc31B8k"
            authService?.clientKey = "YXHy1QZd0XTSV0fY4N5JgW6QQDIGnsF94hhm"

            authService?.sdkAuth()
        }
        
        func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
            if returnValue == .success{
                print("Successfully authenticated")
                
             //   DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    let paramDict = [
                        kMeetingParam_Username: appDel.loggedInUserData?.user_name == "" ? appDel.loggedInUserData?.name : appDel.loggedInUserData?.user_name,
                        kMeetingParam_MeetingNumber: self.liveShowData.meeting_number,
                        kMeetingParam_MeetingPassword: self.liveShowData.password
                    ]
                    
    //                let user = MobileRTCMeetingStartParam4WithoutLoginUser()
    //                user.userType = .unknown
    //                user.zak = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6Ikhva1laN2RlVDhxcUJZNHBWeEtPTHciLCJleHAiOjE2MDc5MjkxNDcsImlhdCI6MTYwNzMyNDM0OH0.Jgo5Uti4-fMNqo-tstEvgYYEh18Xrwr4jfCgPS2lJdk"
    //                user.userName = "test"
                    let ms = MobileRTC.shared().getMeetingService()
                    
                    let ret = ms?.joinMeeting(with: paramDict)
                    
           //     }
            }else{
                self.alertOk(title: "Error", message: "failed authentication")
                print("failed authentication")
            }
        }
}

extension String
{
    func group(by groupSize:Int=3, separator:String="-") -> String{
         if self.count <= groupSize   { return self }
         let splitSize  = min(max(1,self.count-1) , groupSize)
         let splitIndex = index(startIndex, offsetBy:splitSize)
         return substring(to:splitIndex)
              + separator
              + substring(from:splitIndex).group(by:groupSize, separator:separator)
      }
    func replace_fromStart(str:String , endIndex:Int , With:String) -> String {
        var strReplaced = str ;
        let start = str.startIndex;
        let end = str.index(str.startIndex, offsetBy: endIndex);
        strReplaced = str.replacingCharacters(in: start..<end, with: With) ;
        return strReplaced;
    }
    
    
}

//["msg": Your Go Live Request has been submitted successfully. Will remain on the Go live whiles waiting for admin to approve., "is_plan_subscribed": 1, "status": 1]
/*
 //OLD
 SUBSCRIPTION PAYMENT WITH MOBILE MONEY
 
 1. Send amount to Mobile Money Number 0546 872 809 (Name on Account Eric Ackaah)

 2. Send us SMS or WhatsApp on 0546 872 809 to Confirm your payment Transaction Number

 3. Send Us SMS or WhatsApp on 0546 872 809 to confirm your Cinestool Name, Registered Number and Name of Movie you want to watch

 If the above steps are completed, you'll be granted access automatically to watch with your Mobile Money on Cinestool
 
 //NEW
  TOPHUB MOBILE MONEY PAYMENT PROCESS

                      {Auto fill Amount from API here}

 Step 1: Send the full amount listed above to our mobile money number +233 546 872 809.

 Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.

 When these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money
 */
