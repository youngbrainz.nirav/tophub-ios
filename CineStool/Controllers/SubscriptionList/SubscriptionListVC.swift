//
//  SubscriptionListVC.swift
//  CineStool
//
//  Created by Dharam YB on 17/02/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

enum isComingFrom {
    case none
    case Login
    case MoviePlaying
}


class SubscriptionListVC: UIViewController {

    @IBOutlet var tblSubscription: UITableView!
    @IBOutlet var btnSubscribe: UIButton!
    @IBOutlet var btnBack: UIButton!
    var index = Int()
    var subscriptionList = [SubscriptionModel]()
    var isfrom:isComingFrom = .none
    
    @IBOutlet var btnCancelSubscription: UIButton!
    
    @IBOutlet var viewSubDetail: UIView!
    @IBOutlet var lblCurrSub: UILabel!
    @IBOutlet var heightviewSubDetail: NSLayoutConstraint!
    
    @IBOutlet var heightTableView: NSLayoutConstraint!
    @IBOutlet var heightPromoView: NSLayoutConstraint!
    @IBOutlet var viewPromo: UIView!
    
    @IBOutlet var txtPromoCode: UITextField!
    
    var subscriptionStatus = String()
    var selectedSubscriptionID = String()
    
    var timerForMobileMoneyDone = Timer()
    var isPaymentDone = false
    var isTableReload = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tblSubscription.register(UINib(nibName: "CellSubscription", bundle: .main), forCellReuseIdentifier: "CellSubscription")
        tblSubscription.register(UINib(nibName: "CelllNewSubscription", bundle: .main), forCellReuseIdentifier: "CelllNewSubscription")
        self.tblSubscription.estimatedRowHeight = 50
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
        self.tblSubscription.reloadData()
        
        tblSubscription.tableFooterView = UIView()
        self.index = 0
        self.apiCallGetSubscriptionList()
        if self.isfrom == .Login{
            self.btnBack.isHidden = true
        }else if self.isfrom == .MoviePlaying{
            self.btnBack.isHidden = false
        }else{
            self.btnBack.isHidden = false
        }
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        self.viewPromo.isHidden = true
        self.setTimer()
    }
    
    @objc func APIPaymentCheck() {
        print("Payment Check for Mobile Money...")
        if self.isPaymentDone{
            appDel.isAppdelTimerValid = false
            self.timerForMobileMoneyDone.invalidate()
        }else{
            self.apiCallGetCategoryList()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.isfrom == .MoviePlaying{
            if appDel.isAppdelTimerValid{
                self.setTimer()
            }
        }
        if appDel.is_Subscribed{
            self.isPaymentDone = true
            self.navigationController?.popViewController(animated: true)
        }else{
            self.isPaymentDone = false
        }
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnSquareTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.isPaymentDone = true
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubscribeTapped(_ sender: UIButton) {
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
        let codeInfo = countryInfo["country"] as! String
        let currencyCodedetail = Locale.currencies[codeInfo]
        let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")
        //"By subscribing, you agree to Cinestool monthly automatic billing and subscription terms on this platform."
        alertOk(title: "", message: "By subscribing you agree to our terms and conditions.", buttonTitle: "Ok") { (result) in
            if result{   
                self.isPaymentDone = true
                let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                objTab.isFromSubscribe = .Yes
                objTab.isfromLogin = self.isfrom
                objTab.selectedSubscription = self.subscriptionList[self.index]
                self.navigationController!.pushViewController(objTab, animated: true)
            }
        }
    }
    
    @IBAction func btnCancelSubscribeTapped(_ sender: UIButton) {
        print("Cancel")
        
        var strMessage = ""
        if subscriptionStatus == "2"{
            strMessage = "Are you sure you want to unsubscribe current subscription."
        }else{
            strMessage = "Are you sure you want to auto subscribe your current subscription."
        }
        alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: strMessage) { (status) in
            if status{
                self.apiCallUnSubscription(subscription_status: self.subscriptionStatus)
            }
        }
    }
}

extension SubscriptionListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.subscriptionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell: CelllNewSubscription = tableView.dequeueReusableCellTb(for: indexPath)
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
        let codeInfo = countryInfo["country"] as! String
        let currencyCodedetail = Locale.currencies[codeInfo]
        let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")
        
        cell.lblTitle.text = "\(self.subscriptionList[indexPath.row].plan) (\(currencyCode ?? "$")\(self.subscriptionList[indexPath.row].price)/month)"
        if self.subscriptionList[indexPath.row].is_selected_plan == "1"{
            cell.btnCheck.setImage(#imageLiteral(resourceName: "ratioTick"), for: .normal)
            self.index = indexPath.row
        }else{
            cell.btnCheck.setImage(#imageLiteral(resourceName: "ratioUnTick"), for: .normal)
        }
        
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
        self.heightTableView.constant = self.tblSubscription.contentSize.height
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<self.subscriptionList.count{
            self.subscriptionList[i].is_selected_plan = "0"
        }
        self.subscriptionList[indexPath.row].is_selected_plan = "1"
        self.index = indexPath.row
        self.tblSubscription.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 120//UITableView.automaticDimension
    }
    
    func apiCallGetSubscriptionList() {
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
        let codeInfo = countryInfo["country"] as! String
        let currencyCodedetail = Locale.currencies[codeInfo]
        let param = ["currency_code": "\(currencyCodedetail?.code ?? "USD")"]
        WebService.Request.patch(url: getSubscriptionList, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    print("-->",response!)
                    if let data = response!["data"] as? [[String: Any]] {
                        self.subscriptionList = data.map{ SubscriptionModel(JSON: $0)!}
                        self.tblSubscription.reloadData()
                        if self.subscriptionList.count == 0{
                            self.btnSubscribe.isHidden = true
                        }else{
                            self.btnSubscribe.isHidden = false
                        }
                        self.selectedSubscriptionID = response!["subscription_id"] as? String ?? ""
                        if self.isTableReload{
                            self.heightviewSubDetail.constant -= self.viewSubDetail.frame.size.height + 20//0
                        }else{
                            
                        }
                        self.lblCurrSub.text = response!["subscription_msg"] as? String ?? ""
                        self.subscriptionStatus = response!["subscription_status"] as? String ?? "2"
                        self.btnCancelSubscription.isHidden = false
                        if self.subscriptionStatus == "1"{
                            self.btnCancelSubscription.setTitle("Auto Subscription", for: .normal)
                        }else if self.subscriptionStatus == "2"{
                            self.btnCancelSubscription.setTitle("UnSubscribe", for: .normal)
                        }else{
                            self.btnCancelSubscription.isHidden = true
                        }
//                        if response!["is_auto_subscribed"] as? String == "1"{
//                        }else{
//                            self.viewSubDetail.isHidden = true
//                            self.heightviewSubDetail.constant -= self.viewSubDetail.frame.size.height
//                            self.subscriptionStatus = response!["is_auto_subscribed"] as? String ?? "2"
//                        }
                    }else{
                        
                    }
                } else {
                    self.navigationController?.popViewController(animated: true)
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
                self.viewPromo.isHidden = false
            } else {
                self.navigationController?.popViewController(animated: true)
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallUnSubscription(subscription_status:String) {
        //subscription_status : 1 : subscription, 2 : unsubscription
        let param = ["subscription_status":subscriptionStatus, "subscription_id":selectedSubscriptionID]
        WebService.Request.patch(url: changeSubscribedPlan, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    print("-->",response!)
                    self.heightviewSubDetail.constant -= self.viewSubDetail.frame.size.height + 20//0
//                     self.selectedSubscriptionID = response!["subscription_id"] as? String ?? ""
                    self.lblCurrSub.text = response!["subscription_msg"] as? String
                    self.subscriptionStatus = response!["subscription_status"] as? String ?? "2"
                    self.btnCancelSubscription.isHidden = false
                    if self.subscriptionStatus == "1"{
                        self.btnCancelSubscription.setTitle("Auto Subscription", for: .normal)
                    }else if self.subscriptionStatus == "2"{
                        self.btnCancelSubscription.setTitle("UnSubscribe", for: .normal)
                    }else{
                        self.btnCancelSubscription.isHidden = true
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallCheckPromoCode(code:String) {
        let param = ["promocode":code]
        WebService.Request.patch(url: checkPromoCode, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    appDel.promoCode = "1"
                    appDel.promoCodeText = code
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                    appDel.promoCode = "0"
                    appDel.promoCodeText = ""
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
}

extension SubscriptionListVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textField.text == "" || textField.text?.isEmpty == true{
            
        }else{
            //api
            self.apiCallCheckPromoCode(code: self.txtPromoCode.text ?? "")
        }
        return true
    }
}

extension SubscriptionListVC{
    func setTimer(){
        self.timerForMobileMoneyDone = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(APIPaymentCheck), userInfo: nil, repeats: true)
    }
    
    func apiCallGetCategoryList() {
        let params = ["mobile_uid":appDel.getDeviceUUID(),"password":appDel.Loginpassword] as [String : Any]
        WebService.Request.patch(url: getMovieCategoryV2, type: .post, parameter: params, callSilently : true) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                } else {
                }
                appDel.userPaymentDetail = ["is_go_live_event_approval":response!["is_go_live_event_approval"] as? String ?? "",
                                    "is_plan_subscribed":response!["is_plan_subscribed"] as? String ?? "",
                                    "go_live_view_actual_charge":response!["go_live_view_actual_charge"] as? String ?? "",
                                    "is_event_requested":response!["is_event_requested"] as? String ?? "",
                                    "go_live_view_charge":response!["go_live_view_charge"] as? String ?? "",
                                    "actual_currency_code":response!["actual_currency_code"] as? String ?? "",
                                    "user_currency_code":response!["user_currency_code"] as? String ?? "",
                                    "is_go_live_payment_done":response!["is_go_live_payment_done"] as? String ?? "",
                                    "is_password_added":response!["is_password_added"] as? String ?? "",
                                    "is_start_event":response!["is_start_event"] as? String ?? ""]

                let strSubscribed = appDel.userPaymentDetail["is_plan_subscribed"] as? String
                if strSubscribed == "1"{
                    appDel.is_Subscribed = true
                    self.isPaymentDone = true
                    appDel.isWatchMovie = true
                    self.navigationController?.popViewController(animated: true)
                }else{
                    appDel.is_Subscribed = false
                    self.isTableReload = false
                    //self.apiCallGetSubscriptionList()
                }
                UserDefaults.standard.set(appDel.is_Subscribed, forKey: "is_Subscribed")
            }else{
                
            }
            WebService.Loader.hide()
        }
    }
}
//Enter your Promo Code in the field below and get one (1) additional month free subscription
/*
 ["msg": successfully., "subscription_msg": Your current subscription expires on 13 September, 2020 14:02 and autosubscribed on same time., "subscription_status": 2, "status": 1, "subscription_id": 5e4e9b675437580f7e0c91b2]
 
 i'm also working on this  flow
 1 user login
 2 redirect to home
 3 check movie details screen
 4 if user subscribed watch movie
 5 otherwise redirect to subscription screen
 6 in subscription screen they pay with mobile money and card payment
 7 after card payment succeed they come back to movie details screen
 8 if user pay with mobile money app refreshing and go back to movie detail screen

 */
