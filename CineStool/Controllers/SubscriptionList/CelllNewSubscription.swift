//
//  CelllNewSubscription.swift
//  CineStool
//
//  Created by Dharam YB on 02/05/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit

class CelllNewSubscription: UITableViewCell, ReusableView, NibLoadableView {

    //MARK:- Variables & Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    var blockCheckChanged : ((Int)->())?
    
    //MARK:- Default Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
