//
//  NoInternetVC.swift
//  CineStool
//
//  Created by hiren  mistry on 24/05/21.
//  Copyright © 2021 Youngbrainz. All rights reserved.
//

import UIKit

class NoInternetVC: UIViewController {

    var clickRetry: (()->())!
    var ClickOffline: (()->())!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func btnDownloadsClick(_ sender: Any) {
        self.dismiss(animated: true) {
            self.ClickOffline()
        }
    }
    
    @IBAction func btnRetryClick(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            self.dismiss(animated: false, completion: nil)
            clickRetry()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
