//
//  ChangePasswordVC.swift
//  CineStool
//
//  Created by Dharam YB on 04/04/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ChangePasswordVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var btnShowPass: UIButton!
    @IBOutlet weak var btnShowConfPass: UIButton!
    
    let InsertPassword = "Insert Password"
    let InsertConfirmPassword = "Insert Confirm Password"
    let PasswordNotMatched = "Password & Confirm Password Not Matched"
    var regEmail = String()
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        
        let img = #imageLiteral(resourceName: "eye").withRenderingMode(.alwaysTemplate)
        self.btnShowPass.setImage(img, for: .normal)
        self.btnShowConfPass.setImage(img, for: .normal)
        
        self.btnShowPass.tintColor = .white
        self.btnShowConfPass.tintColor = .white
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        if txtPassword.text == "" || txtPassword.text?.isEmpty == true{
            alertController(message: InsertPassword, controller: self)
        }else if txtConfirmPassword.text == "" || txtConfirmPassword.text?.isEmpty == true{
            alertController(message: InsertConfirmPassword, controller: self)
        }else if txtPassword.text != txtConfirmPassword.text {
            alertController(message: PasswordNotMatched, controller: self)
        }else{
            self.ChangePassAPI(email: self.regEmail, password: self.txtPassword.text ?? "")
        }
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShowPassTapped(_ sender: UIButton) {
        if self.txtPassword.isSecureTextEntry {
           self.txtPassword.isSecureTextEntry = false
        }else{
            self.txtPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnShowConfPassTapped(_ sender: UIButton) {
        if self.txtConfirmPassword.isSecureTextEntry {
           self.txtConfirmPassword.isSecureTextEntry = false
        }else{
            self.txtConfirmPassword.isSecureTextEntry = true
        }
    }
}

extension ChangePasswordVC{
    func ChangePassAPI(email:String,password:String){
        let params = ["email_id":email,"password":password] as [String : Any]
        WebService.Request.patch(url: changePassword, type: .post, parameter: params, callSilently: false, header: nil) { (response, error) in
            if error == nil {
                print(response!)
                if response!["status"] as? Bool == true
                {
                    self.alertOk(title: "", message: response!["msg"] as? String ?? "", buttonTitle: "Ok") { (status) in
                        if status{
                            self.view.endEditing(true)
                            self.txtPassword.text = ""
                            self.txtConfirmPassword.text = ""
                            resetDefaults(fromForgotNumber: true)
                            appDel.displayScreen()
                        }
                    }
                } else{
                    self.alertOk(title: "", message: response!["msg"] as? String ?? "Something Went Wrong..")
                }
            }else{
                self.alertOk(title: "", message:"Something Went Wrong..")
            }
        }
    }
}
