//
//  SideMenuVC.swift
//  CineStool
//
//  Created by YB on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import KWDrawerController
import SDWebImage

class SideMenuVC: UIViewController {
    
    @IBOutlet weak var tblExpandable: UITableView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    var tableData = [[String: Any]]()
    var cellTitle: String?
    var blockBtnExpandAction : ((String)->())!
    var blockSubMenuAction : ((String)->())!
    var arrExpandedIndex : [Int] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        onloadOperation()
    }
    
    func onloadOperation() {
        tblExpandable.register(ExpandableTblCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupProfile()
    }
    func reloadData(){
        tblExpandable.reloadData()
    }
    func setupProfile(){
        self.lblName.text = appDel.loggedInUserData?.user_name ?? ""
        let url = URL(string: appDel.loggedInUserData?.profile_image ?? "")
        self.imgProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeHolder"))
    }

    func apiCallLogout() {
        WebService.Request.patch(url: logOut, type: .post, parameter: [:]) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    resetDefaults()
                    appDel.loginUser_Id = ""
                    appDel.userSessionToken = ""
                    appDel.loggedInUserData = User()
                    let objTab = MobileVerifyVC(nibName: "MobileVerifyVC", bundle: .main)
                    self.drawerController?.setViewController(objTab, for: .none)
                    appDel.displayScreen(isOpenlaunch: false)
                    let nc = NotificationCenter.default
                    nc.post(name: Notification.Name("UserLoggedIn"), object: nil)
                } else {}
            } else {
                print("ERROR: \(String(describing: error))")
            }
        }
    }
}

extension SideMenuVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appDel.movieCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ExpandableTblCell = tableView.dequeueReusableCellTb(for: indexPath)
        
        let data = appDel.movieCategories[indexPath.row]
        cell.lblheaderTitle.text = data.name
        cell.btnExpandCell.setTitle(data.name, for: .normal)
        cell.arrSubMenu = data.subCategories
        cell.viewcontroller = self
        
        if data.subCategories.count > 0 {
            let img = #imageLiteral(resourceName: "RightArrow").withRenderingMode(.alwaysTemplate)
            cell.imgDropDown.image = img
            cell.imgDropDown.tintColor = .white
            cell.imgDropDown.tintColorDidChange()
            cell.imgDropDown.isHidden = false
        } else {
            cell.imgDropDown.isHidden = true
        }
        cell.blockBtnExpandAction = { strTitle in
            if data.subCategories.count == 0 {
                if strTitle == "History"{
                    let objTab = MyCollectionVC(nibName: "MyCollectionVC", bundle: .main)
                    objTab.listType = "2"
                    self.drawerController?.setViewController(objTab, for: .none)
                    self.drawerController?.closeSide()
                    
                }else if strTitle == "Downloads"{
                    let objTab = DownloadedVC(nibName: "DownloadedVC", bundle: .main)
                    self.drawerController?.setViewController(objTab, for: .none)
                    self.drawerController?.closeSide()
                }else if strTitle == "My List"{
                    let objTab = MainContainerVC(nibName: "MainContainerVC", bundle: .main)
                    objTab.isfor = "My Collection"
                    self.drawerController?.setViewController(objTab, for: .none)
                    self.drawerController?.closeSide()
                } else if strTitle != "Log Out" && strTitle != "Settings"{
                    let objTab = MainContainerVC(nibName: "MainContainerVC", bundle: .main)
                    objTab.isfor = strTitle
                    self.drawerController?.setViewController(objTab, for: .none)
                    self.drawerController?.closeSide()
                }else if strTitle == "Settings" {
                    let objTab = SettingsVC(nibName: "SettingsVC", bundle: .main)
                    self.drawerController?.setViewController(objTab, for: .none)
                    self.drawerController?.closeSide()
                }else if strTitle == "Log Out" {
                    self.alertTwoButton(title: "TopHub", message: "Are you sure you want to logout?") { (result) in
                        if result {
                            self.apiCallLogout()
                        } else {
                        }
                    }
                }
            } else {
                defer {
                    tableView.beginUpdates()
                    tableView.endUpdates()
                    if data.subCategories.count > 0 {
                        cell.imgDropDown.isHidden = false
                    } else {
                        cell.imgDropDown.isHidden = true
                    }
                }
                guard let aIndex = self.arrExpandedIndex.firstIndex(of: indexPath.row) else {
                    cell.imgDropDown.image = #imageLiteral(resourceName: "DownArrow").withRenderingMode(.alwaysTemplate)
                    cell.imgDropDown.tintColor = .white
                    cell.imgDropDown.tintColorDidChange()
                    self.arrExpandedIndex.append(indexPath.row)
                    return
                }
                cell.imgDropDown.image = #imageLiteral(resourceName: "RightArrow").withRenderingMode(.alwaysTemplate)
                cell.imgDropDown.tintColor = .white
                cell.imgDropDown.tintColorDidChange()
                self.arrExpandedIndex.remove(at: aIndex)
            }
        }
        
        cell.blockSubMenuAction = { strSubTitle in
            let objTab = MovieListingVC(nibName: "MovieListingVC", bundle: .main)
            objTab.categoryID = strSubTitle._id
            objTab.subCategoryID = strSubTitle._id
            objTab.titleName = strSubTitle.name
            self.navigationController?.pushViewController(objTab, animated: true)
            self.drawerController?.closeSide()
       }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard arrExpandedIndex.contains(indexPath.row) else {
            return 50
        }
        let data = appDel.movieCategories[indexPath.row].subCategories
        if data.count == 0 {
            return 50
        } else {
            return CGFloat((data.count * 40) + 50)
        }
    }
}
