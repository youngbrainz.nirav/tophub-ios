//
//  RecentPlayedCell.swift
//  CineStool
//
//  Created by hiren  mistry on 28/04/21.
//  Copyright © 2021 Youngbrainz. All rights reserved.
//

import UIKit

class RecentPlayedCell: UITableViewCell, ReusableView, NibLoadableView  {
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var collectionRecent:UICollectionView!
    @IBOutlet weak var HeightcollectionHomeUnderTbl: NSLayoutConstraint!
    var arrRecentList: [Movies] = []
    var blockDidTapped: ((Int)->())!
    var indexPath = IndexPath()

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionRecent.delegate = self
        collectionRecent.dataSource = self
        self.HeightcollectionHomeUnderTbl.constant = 20
        self.collectionRecent.register(HomeUnderTblCollectionCell.self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension RecentPlayedCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrRecentList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: HomeUnderTblCollectionCell = collectionView.dequeueReusableCell(for: indexPath)
        let imgURL = arrRecentList[indexPath.item].poster_image
        cell.imgMoviePoster.sd_setImage(with: URL(string: imgURL), placeholderImage:nil)
        cell.lblName.text =  arrRecentList[indexPath.item].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionRecent.frame.size.width - 20) / 3
        let height = width + 70//collectionHomeUnderTbl.frame.size.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        blockDidTapped(indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    
    
    
}
