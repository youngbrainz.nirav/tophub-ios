//
//  HomeVC.swift
//  CineStool
//
//  Created by YB on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import AVKit
import GoogleCast
import SwiftyJSON
import FirebaseDynamicLinks
class HomeVC: UIViewController {
    
    @IBOutlet weak var collectionHome: UICollectionView!
    @IBOutlet weak var TblHome: UITableView!
    @IBOutlet weak var pageControll: UIPageControl!
    
    @IBOutlet weak var collLive: UICollectionView!
    @IBOutlet weak var heightCollLive: NSLayoutConstraint!
    
    @IBOutlet weak var vw_NoInterNet: UIView!

    var arrCategory = [""]
    var timer = Timer()
    var player = AVPlayer()
    var timerForLiveEvent = Timer()
    var isCallTimer = Bool()
    var intRecent:Int = 0
    var dataEventList: LiveEventModel!
    var arrEventData: [EventData] = []
    var arrUserData: UserData?
    var ArrRecentMovie:[Movies] = []
    var reloadSidemenu:(()->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vw_NoInterNet.isHidden = true
        collectionHome.isHidden = true
        TblHome.isHidden = true
        onloadOperation()
        setupDetail()
        apiCallGetCategoryList()
        apiCallGetDashboardData()
        apiCallGetRecentMovie()
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            startTimer()
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.vw_NoInterNet.isHidden = false
            }
        }
    }
    func checkDynamicLink(){
        
        if !movieIdGloble.isEmpty {
            
            
            appDel.redirectToMovieDetail(movieID: movieIdGloble,PartyCode: PartyCodeGloble)
            movieIdGloble = ""
            PartyCodeGloble = ""
        }
        
    }
    func onloadOperation() {
        appDel.isCallAPI = true
        TblHome.register(HomeTblCell.self)
        TblHome.register(RecentPlayedCell.self)
        collectionHome.register(HomeCollectionCell.self)
        collLive.register(CollGoliveCell.self)
        TblHome.rowHeight = UITableView.automaticDimension
        TblHome.estimatedRowHeight = 20
        
        appDel.isEventListAPICall = true
        self.timerForLiveEvent = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(APICallLiveEvent), userInfo: nil, repeats: true)
        self.isCallTimer = false
        self.apiCallEventList()
    }
    override func viewDidAppear(_ animated: Bool) {
        print("did appear called")
    }
    override func viewWillAppear(_ animated: Bool) {
        self.vw_NoInterNet.isHidden = true

        if Reachability.isConnectedToNetwork(){
            apiCallGetDashboardData()
        }else{
            self.vw_NoInterNet.isHidden = false
        }


        
        UIDevice.current.setValue(Int(UIInterfaceOrientationMask.portrait.rawValue), forKey: "orientation")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        appDel.isEventListAPICall = false
    }
    
    @objc func APICallLiveEvent() {
        self.isCallTimer = true
        if appDel.isEventListAPICall{
//            print("API Calling...")
            self.apiCallEventList()
        }else{
//            print("No API Calling...")
            self.timerForLiveEvent.invalidate()
        }
    }
    
    @objc func scrollToNextCell() {
        
        let cellSize = collectionHome.frame.size
        let contentOffset = collectionHome.contentOffset
        
        if collectionHome.contentSize.width <= collectionHome.contentOffset.x + cellSize.width
        {
            let r = CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            collectionHome.scrollRectToVisible(r, animated: false)
            
        } else {
            let r = CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            collectionHome.scrollRectToVisible(r, animated: true)
        }
    }
    
    func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
    }
    
    @IBAction func btnRetryClick(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork(){
            self.collectionHome.isHidden = true
            self.TblHome.isHidden = true
            self.onloadOperation()
            self.setupDetail()
            self.apiCallGetCategoryList()
            self.apiCallGetDashboardData()
            self.apiCallGetRecentMovie()
            self.startTimer()
        }
    }
    @IBAction func btnWatchOfflineClick(_ sender: UIButton) {
        let objTab = DownloadedVC(nibName: "DownloadedVC", bundle: nil)
        findtopViewController()?.self.navigationController!.pushViewController(objTab, animated: true)
    }
    @IBAction func btnSideMenuAction(_ sender: UIButton) {
        guard drawerController?.drawerSide != .left else {
            drawerController?.closeSide()
            return
        }
        
        drawerController?.openSide(.left)
    }
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if appDel.arrDashboardData != nil {
            let movie = appDel.arrDashboardData.categories.filter({$0.data.count != 0})
            return  self.ArrRecentMovie.count > 0 ? movie.count + intRecent : movie.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.ArrRecentMovie.count > 0{
            if indexPath.row == 0{
                let cell : RecentPlayedCell = tableView.dequeueReusableCellTb(for: indexPath)
//                let movie = appDel.arrDashboardData.categories.filter({$0.data.count != 0})
                cell.arrRecentList = self.ArrRecentMovie
                cell.indexPath = indexPath
                DispatchQueue.main.async {
                    cell.collectionRecent.collectionViewLayout.invalidateLayout()
                }
                cell.frame = tableView.bounds
                cell.layoutIfNeeded()
                cell.collectionRecent.reloadData()
                let width = (cell.collectionRecent.frame.size.width - 20) / 3
                let height = width + 70//collectionHomeUnderTbl.frame.size.height
                cell.HeightcollectionHomeUnderTbl.constant = height
                cell.blockDidTapped = { index in
                    appDel.redirectToMovieDetail(movieID: self.ArrRecentMovie[index].movie_id,movie_banner:self.ArrRecentMovie[index].poster_image)
                }
                return cell
            }else{
                let cell: HomeTblCell = tableView.dequeueReusableCellTb(for: indexPath)
                let movie = appDel.arrDashboardData.categories.filter({$0.data.count != 0})
                cell.indexPath = indexPath
                cell.arrDashboardData = movie[indexPath.row - intRecent].data
                DispatchQueue.main.async {
                    cell.collectionHomeUnderTbl.collectionViewLayout.invalidateLayout()
                }
                cell.frame = tableView.bounds
                cell.layoutIfNeeded()
                cell.collectionHomeUnderTbl.reloadData()
                cell.lblCategory.text = movie[indexPath.row - intRecent].name
                cell.HeightcollectionHomeUnderTbl.constant = cell.collectionHomeUnderTbl.collectionViewLayout.collectionViewContentSize.height
                cell.blockDidTapped = { index in
                    appDel.redirectToMovieDetail(movieID: appDel.arrDashboardData.categories[indexPath.row - self.intRecent].data[index]._id,movie_banner:appDel.arrDashboardData.categories[indexPath.row - self.intRecent].data[index].poster_image)
                }
                return cell
                
            }
        }else{
            let cell: HomeTblCell = tableView.dequeueReusableCellTb(for: indexPath)
            let movie = appDel.arrDashboardData.categories.filter({$0.data.count != 0})
            cell.indexPath = indexPath
            cell.arrDashboardData = movie[indexPath.row - intRecent].data
            DispatchQueue.main.async {
                cell.collectionHomeUnderTbl.collectionViewLayout.invalidateLayout()
            }
            cell.frame = tableView.bounds
            cell.layoutIfNeeded()
            cell.collectionHomeUnderTbl.reloadData()
            cell.lblCategory.text = movie[indexPath.row - intRecent].name
            cell.HeightcollectionHomeUnderTbl.constant = cell.collectionHomeUnderTbl.collectionViewLayout.collectionViewContentSize.height
            cell.blockDidTapped = { index in
                appDel.redirectToMovieDetail(movieID: appDel.arrDashboardData.categories[indexPath.row - self.intRecent].data[index]._id,movie_banner:appDel.arrDashboardData.categories[indexPath.row - self.intRecent].data[index].poster_image)
            }
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            return UITableView.automaticDimension
        }else{
            return UITableView.automaticDimension //250
        }
    }
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collLive{
            return self.arrEventData.count
        }else{
            if appDel.arrDashboardData != nil {
                return appDel.arrDashboardData.banner_movies.count
            } else {
                return 0
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collLive{
            let cell: CollGoliveCell = collectionView.dequeueReusableCell(for: indexPath)
            cell.imgUserProfile.alpha = 0.7
            let url = URL(string: self.arrEventData[indexPath.row].profile_image)
            cell.imgUserProfile.sd_setImage(with: url, placeholderImage: nil)
            cell.lblEventName.text = self.arrEventData[indexPath.row].user_name
            return cell
        }else{
            let cell: HomeCollectionCell = collectionView.dequeueReusableCell(for: indexPath)
            let imgURL = appDel.arrDashboardData.banner_movies[indexPath.item].banner_image
            cell.imgPoster.sd_setImage(with: URL(string: imgURL), placeholderImage:nil)
            return cell
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collLive{
            if appDel.isAppLive == appStatus.Live{
                if appDel.userPaymentDetail["is_go_live_payment_done"] as? String == "1"{
                    let objTab = GoLiveVC(nibName: "GoLiveVC", bundle: nil)
                    objTab.isFrom = .audience
                    objTab.arrEventData = self.arrEventData[indexPath.row]
                    findtopViewController()?.self.navigationController!.pushViewController(objTab, animated: true)
                }else{
                    let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                    objTab.isFromSubscribe = .GoLive
                    objTab.arrEventData = self.arrEventData[indexPath.row]
                    findtopViewController()?.self.navigationController!.pushViewController(objTab, animated: true)
                }
            }else{
                let objTab = GoLiveVC(nibName: "GoLiveVC", bundle: nil)
                objTab.isFrom = .audience
                objTab.arrEventData = self.arrEventData[indexPath.row]
                findtopViewController()?.self.navigationController!.pushViewController(objTab, animated: true)
            }
        }else{
            redirectToMoviePlayer(openFrom: .Banner, index: indexPath.row)
        }
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == collLive{
            
        }else{
            pageControll.currentPage = indexPath.item
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collLive{
            return CGSize(width: 90, height: 90)
        }else{
            return collectionHome.frame.size
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

//MARK:- API CALL
extension HomeVC {
    
    
    func apiCallGetCategoryList() {
        /*
        required : [loginuser_id, session_token, user_type(1:Customer)]
        optional : [password,mobile_uid]
        */
        let params = ["mobile_uid":appDel.getDeviceUUID(),"password":appDel.Loginpassword] as [String : Any]
        WebService.Request.patch(url: getMovieCategoryV2, type: .post, parameter: params, callSilently : true) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [[String : Any]] {
                        let catData = ["name": "Home", "get_subcategory": [:]] as [String : Any]
                        var arrcat = [[String:Any]]()
                        arrcat.insert(catData, at: 0)
                        arrcat.append(["name": "Category", "get_subcategory": data])
                        //arrcat.append(["name": "My Collection", "get_subcategory": [:]] as [String : Any])
                        arrcat.append(["name": "My List", "get_subcategory": [:]] as [String : Any])
                       // arrcat.append(["name": "Downloads", "get_subcategory": [:]] as [String : Any])
                        arrcat.append(["name": "History", "get_subcategory": [:]] as [String : Any])
                        arrcat.append(["name": "Settings", "get_subcategory": [:]] as [String : Any])
                        arrcat.append(["name": "My Profile", "get_subcategory": [:]] as [String : Any])
                        arrcat.append(["name": "Log Out", "get_subcategory": [:]] as [String : Any])
                        appDel.movieCategories.removeAll()
                        appDel.movieCategories = arrcat.map{Categories(JSON: $0)!}
                        if self.reloadSidemenu != nil {
                            self.reloadSidemenu!()
                        }
                    }
                } else {
                    //self.alertOk(title: "", message: response!["msg"] as! String)
                }
                //INR,15,0,$,1,1,1133.48,1,0
appDel.userPaymentDetail = ["is_go_live_event_approval":response!["is_go_live_event_approval"] as? String ?? "",
                            "is_plan_subscribed":response!["is_plan_subscribed"] as? String ?? "",
                            "go_live_view_actual_charge":response!["go_live_view_actual_charge"] as? String ?? "",
                            "is_event_requested":response!["is_event_requested"] as? String ?? "",
                            "go_live_view_charge":response!["go_live_view_charge"] as? String ?? "",
                            "actual_currency_code":response!["actual_currency_code"] as? String ?? "",
                            "user_currency_code":response!["user_currency_code"] as? String ?? "",
                            "is_go_live_payment_done":response!["is_go_live_payment_done"] as? String ?? "",
                            "is_password_added":response!["is_password_added"] as? String ?? "",
                            "is_start_event":response!["is_start_event"] as? String ?? ""]
                            
                            appDel.isHomePaymentResounse = true
//                            ["user_currency_code":response!["user_currency_code"] as? String ?? "",
//                            "go_live_view_actual_charge":response!["go_live_view_actual_charge"] as? String ?? "",
//                            "is_go_live_event_approval":response!["is_go_live_event_approval"] as? String ?? "",
//                            "actual_currency_code":response!["actual_currency_code"] as? String ?? "",
//                            "is_go_live_payment_done":response!["is_go_live_payment_done"] as? String ?? "",
//                            "is_password_added":response!["is_password_added"] as? String ?? "",
//                            "go_live_view_charge":response!["go_live_view_charge"] as? String ?? "",
//                            "is_plan_subscribed":response!["is_plan_subscribed"] as? String ?? "",
//                            "is_event_requested":response!["is_event_requested"] as? String ?? ""]
            } else {
                    //self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    
    func setupDetail(){
        if UserDefaults.standard.value(forKey: UserdefaultsConstants.homeData) != nil{
            let data = UserDefaults.standard.value(forKey: UserdefaultsConstants.homeData) as? [String:Any]
            appDel.arrDashboardData = DashboardModel(JSON: data!)!
            if let bannerMovie = data!["banner_movies"] as? [[String: Any]] {
                appDel.bannerMovies = bannerMovie.map{ BannerHome(JSON: $0)! }
            }
            self.pageControll.numberOfPages = appDel.arrDashboardData!.banner_movies.count
            self.collectionHome.reloadData()
            //self.collLive.reloadData()
            self.TblHome.reloadData()
            collectionHome.isHidden = false
            TblHome.isHidden = false
        }
    }
    
    func apiCallGetDashboardData() {
        let params = ["mobile_uuid":appDel.getDeviceUUID(),"start":"0"] as [String : Any]
        var ishideLoader = true
        if appDel.arrDashboardData == nil{
            ishideLoader = false
        }
        WebService.Request.patch(url: getRecentData, type: .post, parameter: params, callSilently : ishideLoader) { [self] (response, error) in
            print(response)
            self.checkDynamicLink()
            self.ArrRecentMovie.removeAll()
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["recentMoviesRecords"] as? [[String: Any]] {
                        for obj in data{
                            self.ArrRecentMovie.append(Movies(JSON: obj)!)
                        }
                        if self.ArrRecentMovie.count > 0{
                            self.intRecent = 1
                        }else{
                            self.intRecent = 0
                        }
                        self.TblHome.reloadData()
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
//                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    func apiCallGetRecentMovie() {
        let params = ["start":"0"] as [String : Any]
        var ishideLoader = true
        if appDel.arrDashboardData == nil{
            ishideLoader = false
        }
        WebService.Request.patch(url: getDashboardData, type: .post, parameter: params, callSilently : ishideLoader) { (response, error) in
            if error == nil {
                print("Dashboard::",response)
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String: Any] {
                        UserDefaults.standard.set(data, forKey: UserdefaultsConstants.homeData)
                        UserDefaults.standard.synchronize()
                        self.setupDetail()
                        self.vw_NoInterNet.isHidden = true
                        appDel.isAppLive = response!["is_live"] as? String ?? ""
                        appDel.badgeCount = Int(response!["badge_count"] as? String ?? "") ?? 0
                        NotificationCenter.default.post(name: .badge, object: nil, userInfo: ["badge": appDel.badgeCount])
                        let strSubscribed = response!["is_plan_subscribed"] as? String ?? "0"
                        if strSubscribed == "1"{
                            appDel.is_Subscribed = true
                        }else{
                            appDel.is_Subscribed = false
                        }
                        UserDefaults.standard.set(appDel.is_Subscribed, forKey: "is_Subscribed")
                    }
                    appDel.subscription_status = response!["subscription_status"] as? String ?? "3"
                    appDel.subscription_id = response!["subscription_id"] as? String ?? "0"
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
//                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    func redirectToMoviePlayer(openFrom: redirectToMoviePlayerFrom, index: NSInteger){
        let storyboard = UIStoryboard(name: "MoviePlayer", bundle: nil)
        let moviePlayer = storyboard.instantiateViewController(withIdentifier: "MoviePlayerVC") as? MoviePlayerVC
        moviePlayer?.modalPresentationStyle = .overFullScreen
        moviePlayer?.banner_movies = appDel.arrDashboardData.banner_movies[index]
        moviePlayer?.openFrom = openFrom
//        let castContainerVC =
//                GCKCastContext.sharedInstance().createCastContainerController(for: moviePlayer!)
//        castContainerVC.miniMediaControlsItemEnabled = true
        findtopViewController()?.navigationController!.pushViewController(moviePlayer!, animated: true)
    }
    
    func apiCallEventList() {
        WebService.Request.patch(url: getGoLiveEventList, type: .post, parameter: [:], callSilently : self.isCallTimer) { (response, error) in
             if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String : Any] {
                        self.dataEventList = LiveEventModel(JSON: data)!
                        
                        if let arrEvent = data["event_data"] as? [[String: Any]] {
                            self.arrEventData = arrEvent.map{ EventData(JSON: $0)! }
                        }
                        
                         if let user_data = data["user_data"] as? [String: Any] {
                            self.arrUserData = UserData(JSON: user_data)
                            appDel.arrUserData = self.arrUserData
                            self.collLive.reloadData()
                         }
                    }
                } else {
                    if !self.isCallTimer {
                        self.alertOk(title: "", message: response!["msg"] as! String)
                    }
                }
            } else {
                if !self.isCallTimer {
                    //self.alertOk(title: "", message: "Something went wrong please try again!")
                }
            }
            if self.arrEventData.count > 0{
                self.heightCollLive.constant = 80
            }else{
                self.heightCollLive.constant = 0
            }
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
            WebService.Loader.hide()
        }
    }
}

extension NSNotification.Name {
    static let badge = NSNotification.Name("badge")
}

/*
 [  "is_go_live_event_approval": 0,
    "is_plan_subscribed": 1,
    "go_live_view_actual_charge": 1.50,
    "is_event_requested": 1,
    "go_live_view_charge": 113.30,
    "actual_currency_code": $,
    "user_currency_code": INR,
    "is_go_live_payment_done": 0]
 
 [  "is_go_live_event_approval": 1,
    "is_plan_subscribed": 0,
    "go_live_view_actual_charge": 1.50,
    "is_event_requested": 1,
    "go_live_view_charge": 113.74,
    "actual_currency_code": $,
    "user_currency_code": INR,
    "is_go_live_payment_done": 0,
    "is_password_added": 1,
    "is_start_event": 1]
 
 [  "is_go_live_event_approval":response!["is_go_live_event_approval"] as? String ?? "",
    "is_plan_subscribed":response!["is_plan_subscribed"] as? String ?? "",
    "go_live_view_actual_charge":response!["go_live_view_actual_charge"] as? String ?? "",
    "is_event_requested":response!["is_event_requested"] as? String ?? ""]
    "go_live_view_charge":response!["go_live_view_charge"] as? String ?? "",
    "actual_currency_code":response!["actual_currency_code"] as? String ?? "",
    "user_currency_code":response!["user_currency_code"] as? String ?? "",
    "is_go_live_payment_done":response!["is_go_live_payment_done"] as? String ?? "",
    "is_password_added":response!["is_password_added"] as? String ?? "",
    "is_start_event":response!["is_start_event"] as? String ?? ""]
 */
