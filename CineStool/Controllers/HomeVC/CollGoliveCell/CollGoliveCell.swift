//
//  CollGoliveCell.swift
//  CineStool
//
//  Created by Dharam YB on 14/05/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit

class CollGoliveCell: UICollectionViewCell, ReusableView, NibLoadableView  {
    //MARK:- Variables & Outlets
    @IBOutlet weak var imgUserProfile:UIImageView!
    @IBOutlet weak var lblEventName:UILabel!
    @IBOutlet weak var lblLiveTag:UILabel!

    //MARK:- Default Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
