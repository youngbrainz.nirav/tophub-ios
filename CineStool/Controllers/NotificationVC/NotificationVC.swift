//
//  NotificationVC.swift
//  CineStool
//
//  Created by YB on 24/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import GoogleCast

class NotificationVC: UIViewController {
    @IBOutlet var tblNotification: UITableView!
    var count = 12
    var arrNotification: [NotificationModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onloadOperation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        apiCallGetNotificationList()
    }
    
    func onloadOperation() {
        appDel.isCallAPI = true
        tblNotification.register(UINib(nibName: "NotificationTblCell", bundle: .main), forCellReuseIdentifier: "NotificationTblCell")
//        tblNotification.register(NotificationTblCell.self, forCellReuseIdentifier: "NotificationTblCell")
    }
}

extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrNotification.count == 0{
            self.tblNotification.isHidden = true
        }else {
            self.tblNotification.isHidden = false
        }
        return self.arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTblCell") as? NotificationTblCell
        let model = arrNotification[indexPath.row]
        cell?.lblTime.text = model.date
        cell?.lblTitle.text = model.message
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.alertTwoButton(title: "TopHub", message: "Are you sure you want to Delete?") { (result) in
                if result { self.apiCallDeleteNotificationList(self.arrNotification[indexPath.row]._id, index: indexPath.row)
                } else {
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrNotification[indexPath.row].type == "10"{
            appDel.eventLiveId = arrNotification[indexPath.row].event_id
            let objTab = GoLiveReqVC(nibName: "GoLiveReqVC", bundle: .main)
            findtopViewController()?.navigationController?.pushViewController(objTab, animated: true)
            /*
             {
                 "_id" = 5ebd69ebdde72e71e11150a2;
                 "created_at" = "2020-05-14 15:55:23";
                 date = "14 May, 2020 15:55";
                 "event_id" = 5ebd5f4313312e2a2e6fb792;
                 "from_id" = "";
                 "is_read" = 1;
                 message = "Your Go Live Request has been approved";
                 "send_user_type" = 10;
                 "to_id" = 5eb40b731ada9f0f703b3872;
                 "to_user_type" = 2;
                 type = 10;
                 "updated_at" = "2020-05-14 16:17:58";
             }
             */
        }else{
            appDel.redirectToMovieDetail(movieID: arrNotification[indexPath.row].movie_id,movie_banner:"")
        }
    }
}

extension NotificationVC{
    func apiCallGetNotificationList() {
        WebService.Request.patch(url: getNotificationList, type: .post, parameter: [:]) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [[String: Any]] {
                        self.arrNotification = data.map { NotificationModel(JSON: $0)! }
                        self.tblNotification.reloadData()
                    }else{
                        self.tblNotification.reloadData()
                    }
                } else {
                    self.tblNotification.reloadData()
                }
            } else {
                self.tblNotification.reloadData()
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallDeleteNotificationList(_ id:String,index:Int) {
        let params = ["notification_id":id] as [String : Any]
        WebService.Request.patch(url: deleteNotification, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.arrNotification.remove(at: index)
                    self.tblNotification.reloadData()
                } else {
                    self.tblNotification.reloadData()
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
                self.tblNotification.reloadData()
            }
        }
    }
}

//4. Are these list of viewers watching live or the list of live event going on in the app..? Help me understand.
//Hello Eric the list is for the list of live event going on in the app
