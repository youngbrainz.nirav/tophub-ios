//
//  NewMobileEnterVC.swift
//  CineStool
//
//  Created by MacBook Air 002 on 02/01/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AccountKit
import FirebaseAuth
import FirebaseMessaging
import FirebaseInstanceID

class EnterNewMobileVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var txtOldMobile: UITextField!
    @IBOutlet weak var lblOldCountryCode: UILabel!
    
    @IBOutlet weak var txtNewMobile: UITextField!
    @IBOutlet weak var lblNewCountryCode: UILabel!
    
    @IBOutlet weak var txtConfirmMobile: UITextField!
    @IBOutlet weak var lblConfirmCountryCode: UILabel!
    
    @IBOutlet weak var imgBackg: UIImageView!
    
    var _accountKit: AccountKit?
    var _pendingLoginViewController: AKFViewController?
    var accountKit = AccountKit(responseType: ResponseType.accessToken)
    
    let InsertOldMobile = "Insert Old Mobile Number"
    let OldMobileNotMatched = "Old Mobile Number and Country Code Not Exist"
    let InsertNewMobile = "Insert New Mobile Number"
    let InsertConfirmMobile = "Insert Confirm Mobile Number"
    let mobileNotMatched = "New Mobile Number & Confirm Mobile Number Not Matched"
    
    var mobileNumber = ""
    var mobileCode = ""
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        if _accountKit == nil {
            _accountKit = AccountKit(responseType: .accessToken)
        }
        setupCurrentCountryDialingCode()
        imgBackg.tintColorDidChange()
    }
    
    func setupCurrentCountryDialingCode() {
        let temp = Common.getCountryList()
        for item in temp{
            if item["country"] as? String == Locale.current.regionCode{
                self.lblOldCountryCode.text = item["code"] as? String ?? "+1"
                self.lblNewCountryCode.text = item["code"] as? String ?? "+1"
                self.lblConfirmCountryCode.text = item["code"] as? String ?? "+1"
            }
        }
     }

    //MARK:- Button Tapped Events
    @IBAction func btnCountryCodeTapped(_ sender: UIButton) {
        let objCountryListVC = CountryListVC(nibName: "CountryListVC", bundle: nil)
        objCountryListVC.selectedCountry = {(countryData) in
            if sender.tag == 1{
                self.lblOldCountryCode.text = countryData["code"] as? String ?? ""
                self.lblNewCountryCode.text = countryData["code"] as? String ?? ""
                self.lblConfirmCountryCode.text = countryData["code"] as? String ?? ""
            }else if sender.tag == 2{
                self.lblNewCountryCode.text = countryData["code"] as? String ?? ""
                self.lblConfirmCountryCode.text = countryData["code"] as? String ?? ""
            }
        }
        self.present(objCountryListVC,animated: true)
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        let oldEnteredMobile = "\(self.lblOldCountryCode.text ?? "")\(self.txtOldMobile.text ?? "")"
        let oldExistMobile = "\(self.mobileCode)\(self.mobileNumber)"
        if txtOldMobile.text == "" {
            alertController(message: InsertOldMobile, controller: self)
        }else if oldEnteredMobile != oldExistMobile {
            alertController(message: OldMobileNotMatched, controller: self)
        }else if txtNewMobile.text == "" {
            alertController(message: InsertNewMobile, controller: self)
        }else if txtConfirmMobile.text == "" {
            alertController(message: InsertConfirmMobile, controller: self)
        }else if txtNewMobile.text != txtConfirmMobile.text {
            alertController(message: mobileNotMatched, controller: self)
        }else{
            //self.mobileSubmit()
            self.ChangeMobileAPI(mobile_no: self.txtNewMobile.text ?? "", country_code: self.lblNewCountryCode.text ?? Locale.current.regionCode ?? "")
            //verifyMobile(phoneNumber: "\(self.lblNewCountryCode.text ?? "\(Locale.current.regionCode ?? "")")\(txtNewMobile.text ?? "")")
        }
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension EnterNewMobileVC{
    func verifyMobile(phoneNumber: String)  {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                self.startAnimatingLoader(message: "")
            }
            else {
                print("Either denied or notDetermined")
            }
        }
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            self.stopAnimatingLoader()
            if let error = error {
                alertController(message: error.localizedDescription, controller: self)
                return
            }
            let objTab = OTPVerifyVC(nibName: "OTPVerifyVC", bundle: nil)
            objTab.verificationID = verificationID!
            objTab.mobileNumber = "\(self.txtNewMobile.text ?? "")"
            objTab.mobileCode = "\(self.lblNewCountryCode.text ?? Locale.current.regionCode ?? "")"
            objTab.isFrom = .changeMobileScreen
            objTab.blockVerifyOtp = { isVerify in
                if isVerify{
                    self.ChangeMobileAPI(mobile_no: self.txtNewMobile.text ?? "", country_code: self.lblNewCountryCode.text ?? Locale.current.regionCode ?? "")
                }
            }
            self.navigationController!.pushViewController(objTab, animated: true)
        }
    }
}

extension EnterNewMobileVC{
    func mobileSubmit() {
        let inputState = NSUUID().uuidString
        let phoneNumber = PhoneNumber (countryCode: "\(self.lblNewCountryCode.text ?? "\(Locale.current.regionCode ?? "")")", phoneNumber: (self.txtNewMobile.text ?? ""))
        let vc: AKFViewController = _accountKit!.viewControllerForPhoneLogin(with: phoneNumber, state: inputState) as AKFViewController
        self.prepareLoginViewController(loginViewController: vc)
        self.present(vc as! UIViewController, animated: true, completion: nil)
    }
    
    func prepareLoginViewController(loginViewController: AKFViewController) {
        loginViewController.delegate = self
        //UIColor(red:2.0/255.0, green:35.0/255.0, blue:61.0/255.0, alpha:1.0)
        let defaultColor = UIColor(red: 2/255, green: 27/255, blue: 52/255, alpha: 1)
        let theme:Theme = Theme.outlineTheme(primaryColor: defaultColor, primaryTextColor: UIColor.white, secondaryTextColor: UIColor(white: 0.4, alpha: 1.0), statusBarStyle: UIStatusBarStyle.default)
        theme.headerTextColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        theme.iconColor = defaultColor
        theme.textColor = UIColor(white: 0.3, alpha: 1.0)
        theme.titleColor = UIColor(red: 0.247, green: 0.247, blue: 0.247, alpha: 1)
        theme.buttonBackgroundColor = defaultColor
        theme.headerTextType = .appName
        theme.statusBarStyle = .lightContent
        loginViewController.setTheme(theme)
    }
    
    func getMobileNumber() {
        accountKit.requestAccount { (account, error) in
            if(error != nil){
                //error while fetching information
            }else{
                print("Account ID  \(account?.accountID ?? "")")
                if let email = account?.emailAddress,email.count > 0{
                    print("Email\(email)")
                }else if let phoneNum = account?.phoneNumber{
                    print("Phone Number\(phoneNum.stringRepresentation())")
                    if appDel.deviceToken == ""{
                        self.gettoken()
                    }else{
                        self.ChangeMobileAPI(mobile_no: phoneNum.phoneNumber, country_code: "+\(phoneNum.countryCode)")
                    }
                }
            }
        }
    }
    
    func gettoken() {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            }
            else if let result = result {
                print("Remote instance ID token: \(result.token)")
                UserDefaults.standard.setValue("\(result.token)", forKey: "token")
                UserDefaults.standard.synchronize()
                appDel.deviceToken = result.token
                self.accountKit.requestAccount { (account, error) in
                    if let phoneNum = account?.phoneNumber{
                        self.ChangeMobileAPI(mobile_no: phoneNum.phoneNumber, country_code: "+\(phoneNum.countryCode)")
                    }
                }
            }
        }
    }
}

//MARK:- Facebook Acount Kit Delegate
extension EnterNewMobileVC: AKFViewControllerDelegate {
    
    func viewController(viewController: (UIViewController & AKFViewController)?, didFailWithError error: NSError!) {
        print("error \(String(describing: error))")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController), didCompleteLoginWith accessToken: AccessToken, state: String) {
        print("did complete login with access token \(accessToken.tokenString) state \(String(describing: state))")
        getMobileNumber()
    }
    
    func ChangeMobileAPI(mobile_no:String,country_code:String)
    {
        let countryInfo = getCountryCodeFromDialcode(dialCode: country_code)
        let codeInfo = countryInfo["country"] as! String
        let currencyCode = Locale.currencies[codeInfo]
        let currencyCodeSymbol = getCurrencySymbol(forCurrencyCode: currencyCode?.code ?? "USD")
                
        let oldExistMobile = "\(self.mobileCode)\(self.mobileNumber)"
        let params = ["mobile_no":oldExistMobile,"contact_number":mobile_no,"country_code":country_code,"country_code_info":codeInfo,"currency_code": currencyCode?.code ?? "USD","currency_symbol": currencyCodeSymbol ?? "$"] as [String : Any]
        WebService.Request.patch(url: changeMobileNo, type: .post, parameter: params, callSilently: false, header: nil) { (response, error) in
            if error == nil {
                print(response!)
                if response!["status"] as? Bool == true
                {
                    let objTab = OTPVerifyVC(nibName: "OTPVerifyVC", bundle: nil)
                    objTab.isFrom = .changeMobileScreen
                    objTab.otp = response!["otp"] as? String ?? ""
                    self.view.endEditing(true)
                    objTab.blockVerifyOtp = { isVerify in
                        if isVerify{
                            self.view.endEditing(true)
                            self.setupCurrentCountryDialingCode()
                            self.txtOldMobile.text = ""
                            self.txtNewMobile.text = ""
                            self.txtConfirmMobile.text = ""
                            resetDefaults(fromForgotNumber: true)
                            appDel.displayScreen()
                        }
                    }
                    self.navigationController!.pushViewController(objTab, animated: true)
                } else{
                    //self.navigationController?.popViewController(animated: true)
                    self.alertOk(title: "", message: response!["msg"] as? String ?? "Something Went Wrong..")
                }
            }else{
                self.alertOk(title: "", message:"Something Went Wrong..")
            }
        }
    }
}
