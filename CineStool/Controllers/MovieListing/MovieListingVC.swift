//
//  MovieListingVC.swift
//  CineStool
//
//  Created by MacBook Air 002 on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class MovieListingVC: UIViewController {

    @IBOutlet weak var collectionMovies: UICollectionView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnClearSearch: UIButton!
    @IBOutlet weak var heightSearchView: NSLayoutConstraint!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblNoMovie: UILabel!

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var leadingSearchView: NSLayoutConstraint!
    
    var arrmovies = [String:Any]()
    var isFrom: screenFrom = .none
    var categoryID = String()
    var subCategoryID = String()
    var titleName = String()
    var movieListing: [Movies] = []
    var tempMovieListing: [Movies] = []
    let refreshControl = UIRefreshControl()
    var page = 1
    var isRefreshing = false
    
    var blockBack: (()->())!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onloadOperation()
    }
    
    func onloadOperation() {
        appDel.isCallAPI = true
        txtSearch.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        self.collectionMovies.register(HomeUnderTblCollectionCell.self)
        IQKeyboardManager.shared.enable = true
        if isFrom != .home {
            self.btnClearSearch.isHidden = true
            self.btnSearch.setImage(nil, for: .normal)
            self.btnSearch.setTitle("Cancel", for: .normal)
            self.heightSearchView.constant = 40
            self.view.layoutIfNeeded()
            self.lblHeaderTitle.isHidden = true
            //withRenderingMode(.alwaysTemplate)
            let img = #imageLiteral(resourceName: "logo")
            self.btnBack.setImage(img, for: .normal)
            self.btnBack.tintColor = .white
            self.btnBack.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        } else {
            self.btnClearSearch.isHidden = true
            let img = #imageLiteral(resourceName: "Search").withRenderingMode(.alwaysTemplate)
            self.btnSearch.setImage(img, for: .normal)
            self.btnSearch.setTitle("", for: .normal)
            self.btnSearch.tintColor = .white
            self.heightSearchView.constant = 0
            self.view.layoutIfNeeded()
            self.lblHeaderTitle.isHidden = false
            self.lblHeaderTitle.text = titleName + " Movies"
        }
        btnSearch.tintColorDidChange()
        apiCallGetMovieList(page: page, searchKeyWord: "")
        if #available(iOS 10.0, *) {
            collectionMovies.refreshControl = refreshControl
        } else {
            collectionMovies.addSubview(refreshControl)
        }
        refreshControl.tintColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    }
    
    @objc private func refreshData(_ sender: Any) {
        self.tempMovieListing.removeAll()
        self.page = 1
        self.collectionMovies.reloadData()
        if isFrom == .home {
            if txtSearch.text! != "" {
                apiCallGetMovieList(page: page, searchKeyWord: txtSearch.text!)
            } else {
                self.refreshControl.endRefreshing()
            }
        } else {
            if txtSearch.text! == "" {
                apiCallGetMovieList(page: page, searchKeyWord: "")
            } else {
                apiCallGetMovieList(page: 1, searchKeyWord: txtSearch.text!)
            }
        }
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
        if sender.currentImage == #imageLiteral(resourceName: "Search") {
            UIView.animate(withDuration: 0.3) {
                self.txtSearch.becomeFirstResponder()
                sender.setImage(nil, for: .normal)
                sender.setTitle("Cancel", for: .normal)
                self.heightSearchView.constant = 40
                self.view.layoutIfNeeded()
                self.lblHeaderTitle.isHidden = true
                if self.txtSearch.text!.count > 0 {
                    self.btnClearSearch.isHidden = false
                } else  {
                    self.btnClearSearch.isHidden = true
                }
            }
        } else {
            UIView.animate(withDuration: 0.3) {
                self.view.endEditing(true)
                self.btnClearSearch.isHidden = true
                let img = #imageLiteral(resourceName: "Search").withRenderingMode(.alwaysTemplate)
                self.btnSearch.setImage(img, for: .normal)
                self.btnSearch.setTitle("", for: .normal)
                self.btnSearch.tintColor = .white
                self.heightSearchView.constant = 0
                self.view.layoutIfNeeded()
                self.lblHeaderTitle.isHidden = false
            }
            self.txtSearch.text = ""
            if self.isRefreshing {
                page -= 1
            }
            tempMovieListing.removeAll()
            apiCallGetMovieList(page: 1, searchKeyWord: "")
        }
    }
    
    @IBAction func btnClearSearchAction(_ sender: UIButton) {
        self.txtSearch.text = ""
        tempMovieListing.removeAll()
        if self.isRefreshing {
            page -= 1
        }
        apiCallGetMovieList(page: 1, searchKeyWord: txtSearch.text!)
        sender.isHidden = true
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        if self.isFrom == .home{
            if blockBack != nil {
              blockBack()
            }
        }else{
            if blockBack != nil {
              blockBack()
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension MovieListingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieListing.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: HomeUnderTblCollectionCell = collectionView.dequeueReusableCell(for: indexPath)
        let url = URL(string: movieListing[indexPath.row].poster_image)
        cell.imgMoviePoster.sd_setImage(with: url, placeholderImage: nil)
        cell.lblName.text = movieListing[indexPath.row].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        appDel.redirectToMovieDetail(movieID: movieListing[indexPath.row].movie_id,movie_banner:movieListing[indexPath.row].movie_banner)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.movieListing.count {
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionMovies.frame.size.width - 20) / 3
        let height = width + 70//collectionHomeUnderTbl.frame.size.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

//MARK:- API CALL
extension MovieListingVC {
    
    func apiCallGetMovieList(page: Int, searchKeyWord: String) {
        let params = ["category_id":categoryID,"page":"\(page)", "limit": "10", "search_by_word": searchKeyWord]

        WebService.Request.patch(url: getMovieList, type: .post, parameter: params, callSilently : true) { (response, error) in
            if error == nil {
                self.refreshControl.endRefreshing()
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [[String : Any]] {
                        self.movieListing = data.map { Movies(JSON: $0)!}
                        self.tempMovieListing.append(contentsOf: self.movieListing)
                        self.movieListing = self.tempMovieListing
                        if self.movieListing.count > 0 {
                            self.collectionMovies.isHidden = false
                            self.collectionMovies.reloadData()
                        } else {
                            self.collectionMovies.isHidden = true
                        }
                    }
                } else {
                    if !self.isRefreshing {
                        self.collectionMovies.isHidden = true
                        self.movieListing.removeAll()
                        self.collectionMovies.reloadData()
                    } else {
                        self.page -= 1
                    }
                }
            } else {
                self.collectionMovies.isHidden = true
            }
        }
    }
}

extension MovieListingVC: UITextFieldDelegate, UIScrollViewDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtSearch.text! != "" {
            self.tempMovieListing.removeAll()
            self.collectionMovies.reloadData()
            apiCallGetMovieList(page: 1, searchKeyWord: txtSearch.text!)
            return true
        } else {
            return false
        }
    }
    
    @objc func textFieldDidChange(textField : UITextField) {
        if txtSearch.text!.count > 0 {
            self.btnClearSearch.isHidden = false
        } else  {
            self.btnClearSearch.isHidden = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var text = textField.text! + string
        if string.count > 0 {
            self.tempMovieListing.removeAll()
            self.collectionMovies.reloadData()
            apiCallGetMovieList(page: 1, searchKeyWord: text)
        } else {
            text.removeLast()
            self.tempMovieListing.removeAll()
            self.collectionMovies.reloadData()
            apiCallGetMovieList(page: 1, searchKeyWord: text)
        }
        return true
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        isRefreshing = true
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height + 40) {
            page += 1
            apiCallGetMovieList(page: page, searchKeyWord: "")
        }
    }
}
