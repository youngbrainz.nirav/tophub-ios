//
//  OTPVerifyVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseMessaging
import FirebaseInstanceID

class OTPVerifyVC: UIViewController {
    @IBOutlet weak var txtOtp: UITextField!
    
    //MARK:- Variables & Outlets
    @IBOutlet weak var txt1: UITextField!
    @IBOutlet weak var txt2: UITextField!
    @IBOutlet weak var txt3: UITextField!
    @IBOutlet weak var txt4: UITextField!
    @IBOutlet weak var txt5: UITextField!
    @IBOutlet weak var txt6: UITextField!
    
    @IBOutlet weak var lblInfo: UILabel!
    
    @IBOutlet weak var lblTimerCount: UILabel!
    @IBOutlet weak var heightTimerCount: NSLayoutConstraint!
    @IBOutlet weak var lblOTPInfo: UILabel!
    @IBOutlet weak var btnSendEmailOTP: UIButton!
    @IBOutlet weak var viewTimer: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var imgBackg: UIImageView!
    
    let InsertEmail = "Please insert Email."
    let InsertValidEmail = "Please insert Valid Email."
    
    var verificationID = ""
    var mobileNumber = ""
    var mobileCode = ""
    var otp = ""
    var isFrom:verifyOTPFrom = .none
    var blockVerifyOtp : ((Bool)->())?
    var regEmail = String()
    var timer = Timer()
    var totalSecond = 30
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        
        txtOtp.attributedPlaceholder = NSAttributedString(string: "* * * * * *",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])

        if #available(iOS 12.0, *) {
            self.txtOtp.textContentType = .oneTimeCode
        } else {
            // Fallback on earlier versions
        }
        if isFrom == .mobileScreen || isFrom == .changeMobileScreen{
            let myString = "If you don't receive verification code on phone after 30 seconds Click Here to get code in a valid Email"
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font:UIFont(name: "Poppins-SemiBold", size: 14.0)!])
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(location:65,length:10))
            self.lblOTPInfo.attributedText = myMutableString
            self.startTimer()
        }
        imgBackg.tintColorDidChange()
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
       
    @objc func updateTime() {
        print("tot->\(totalSecond)")
        self.lblTimerCount.text = "\(totalSecond)"
        if totalSecond != 0 {
            totalSecond -= 1
        } else {
            print("Show Popup")
            endTimer()
        }
    }
       
    func endTimer() {
        timer.invalidate()
        self.view.endEditing(true)
        self.heightTimerCount.constant = 0
        self.lblTimerCount.text = ""
        print("Here popup")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnVerifyTapped(_ sender: UIButton) {
        if txtOtp.text == ""{
            alertController(message: StringText.fillAllFields, controller: self)
        }else{
            if isFrom == .mobileScreen{
                if appDel.deviceToken == ""{
                    gettoken()
                }else{
                    verifyMobile(verificationCode: txtOtp.text!)
                }
                
//                let EnteredOtp = "\(self.txtOtp.text!)"// "\(txt1.text ?? "")\(txt2.text ?? "")\(txt3.text ?? "")\(txt4.text ?? "")\(txt5.text ?? "")\(txt6.text ?? "")"
//                if EnteredOtp == self.otp{
//                    self.blockVerifyOtp!(true)
//                    self.view.endEditing(true)
//                    txt1.text = "";txt2.text = "";txt3.text = ""
//                    txt4.text = "";txt5.text = "";txt6.text = ""
//                }else{
//                    txt1.text = "";txt2.text = "";txt3.text = ""
//                    txt4.text = "";txt5.text = "";txt6.text = ""
//                    alertOk(title: "", message: "Invalid OTP")
//                }
            }else if isFrom == .emailScreen{
                let EnteredOtp = "\(self.txtOtp.text!)" //"\(txt1.text ?? "")\(txt2.text ?? "")\(txt3.text ?? "")\(txt4.text ?? "")\(txt5.text ?? "")\(txt6.text ?? "")"
                if EnteredOtp == self.otp{
                    let objTab = EnterNewMobileVC(nibName: "EnterNewMobileVC", bundle: nil)
                    objTab.mobileCode = self.mobileCode
                    objTab.mobileNumber = self.mobileNumber
                    self.view.endEditing(true)
                    txt1.text = "";txt2.text = "";txt3.text = ""
                    txt4.text = "";txt5.text = "";txt6.text = ""
                    self.navigationController!.pushViewController(objTab, animated: true)
                }else{
                    txt1.text = "";txt2.text = "";txt3.text = ""
                    txt4.text = "";txt5.text = "";txt6.text = ""
                    alertOk(title: "", message: "Invalid OTP")
                }
            }else if isFrom == .changePasswordScreen{
                let EnteredOtp = "\(self.txtOtp.text!)"//"\(txt1.text ?? "")\(txt2.text ?? "")\(txt3.text ?? "")\(txt4.text ?? "")\(txt5.text ?? "")\(txt6.text ?? "")"
                if EnteredOtp == self.otp{
                    let objTab = ChangePasswordVC(nibName: "ChangePasswordVC", bundle: nil)
                    objTab.regEmail = self.regEmail
                    self.view.endEditing(true)
                    txt1.text = "";txt2.text = "";txt3.text = ""
                    txt4.text = "";txt5.text = "";txt6.text = ""
                    self.navigationController!.pushViewController(objTab, animated: true)
                }else{
                    txt1.text = "";txt2.text = "";txt3.text = ""
                    txt4.text = "";txt5.text = "";txt6.text = ""
                    alertOk(title: "", message: "Invalid OTP")
                }
            }else if isFrom == .changeMobileScreen{
//                if appDel.deviceToken == ""{
//                    gettoken()
//                }else{
//                    verifyMobile(verificationCode: "\(txt1.text ?? "")\(txt2.text ?? "")\(txt3.text ?? "")\(txt4.text ?? "")\(txt5.text ?? "")\(txt6.text ?? "")")
//                }
                let EnteredOtp = "\(self.txtOtp.text!)"//"\(txt1.text ?? "")\(txt2.text ?? "")\(txt3.text ?? "")\(txt4.text ?? "")\(txt5.text ?? "")\(txt6.text ?? "")"
                if EnteredOtp == self.otp{
                    self.blockVerifyOtp!(true)
                    self.view.endEditing(true)
                    txt1.text = "";txt2.text = "";txt3.text = ""
                    txt4.text = "";txt5.text = "";txt6.text = ""
                }else{
                    txt1.text = "";txt2.text = "";txt3.text = ""
                    txt4.text = "";txt5.text = "";txt6.text = ""
                    alertOk(title: "", message: "Invalid OTP")
                }
            }
        }
    }
    
    @IBAction func btnSendEmailOTPTapped(_ sender: UIButton) {
        self.viewEmail.isHidden = false
    }
    
    @IBAction func btnCloseEmailViewTapped(_ sender: UIButton) {
        self.viewEmail.isHidden = true
    }
    
    @IBAction func btnSubmitEmailTapped(_ sender: UIButton) {
        if validate(){
            self.sendOTPAPI(email: self.txtEmail.text ?? "")
        }
    }
}

extension OTPVerifyVC : UITextFieldDelegate{
    func validate() -> Bool {
        if txtEmail.text?.count == 0 {
            alertOk(title: "", message: InsertEmail)
            return false
        }else if !(Validate.isValidEmail(testStr: self.txtEmail.text!)) {
            alertOk(title: "", message: InsertValidEmail)
            return false
        }
        return true
    }
    
    func setupUI() {
        
        if self.isFrom == .emailScreen || self.isFrom == .changePasswordScreen {
            self.txt1.becomeFirstResponder()
            self.viewTimer.isHidden = true
        }
        
        self.txt2.isUserInteractionEnabled = false
        self.txt3.isUserInteractionEnabled = false
        self.txt4.isUserInteractionEnabled = false
        self.txt5.isUserInteractionEnabled = false
        self.txt6.isUserInteractionEnabled = false
        
        if self.isFrom == .mobileScreen || self.isFrom == .changeMobileScreen{
            self.lblInfo.text = "Verification code was successfully sent to your phone number or email address. Enter the code in the fields below."
        }else if self.isFrom == .emailScreen{
            self.lblInfo.text = "Verification code was successfully sent to your phone number or email address. Enter the code in the fields below."
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail{
            self.view.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        // Range.length == 1 means,clicking backspace
        if textField == txtEmail{
            return true
        }else{
            if (range.length == 0){
                if textField == txt1 {
                    txt2.isUserInteractionEnabled = true
                    txt2?.becomeFirstResponder()
                }
                if textField == txt2 {
                    txt3.isUserInteractionEnabled = true
                    txt3?.becomeFirstResponder()
                }
                if textField == txt3 {
                    txt4.isUserInteractionEnabled = true
                    txt4?.becomeFirstResponder()
                }
                if textField == txt4 {
                    txt5.isUserInteractionEnabled = true
                    txt5?.becomeFirstResponder()
                }
                if textField == txt5 {
                    txt6.isUserInteractionEnabled = true
                    txt6?.becomeFirstResponder()
                }
                if textField == txt6 {
                    txt6?.resignFirstResponder()
                }
                textField.text? = string
                return false
            }else if (range.length == 1) {
                if textField == txt6 {
                    txt6.isUserInteractionEnabled = false
                    txt5?.becomeFirstResponder()
                }
                if textField == txt5 {
                    txt5.isUserInteractionEnabled = false
                    txt4?.becomeFirstResponder()
                }
                if textField == txt4 {
                    txt4.isUserInteractionEnabled = false
                    txt3?.becomeFirstResponder()
                }
                if textField == txt3 {
                    txt3.isUserInteractionEnabled = false
                    txt2?.becomeFirstResponder()
                }
                if textField == txt2 {
                    txt2.isUserInteractionEnabled = false
                    txt1?.becomeFirstResponder()
                }
                if textField == txt1 {
                    txt1?.resignFirstResponder()
                }
                textField.text? = ""
                return false
            }
            return true
        }
    }
}

//MARK:- Firebase Mobile Auth
extension OTPVerifyVC{
    func verifyMobile(verificationCode: String)  {
        self.startAnimatingLoader(message: "")
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            self.stopAnimatingLoader()
            if let error = error {
                alertController(message: error.localizedDescription, controller: self)
                return
            }
            
            self.blockVerifyOtp!(true)
            //self.apiCallForLoginRegister()
        }
    }
    
    func gettoken(){
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            }
            else if let result = result {
                print("Remote instance ID token: \(result.token)")
                UserDefaults.standard.setValue("\(result.token)", forKey: "token")
                UserDefaults.standard.synchronize()
                appDel.deviceToken = result.token
                self.verifyMobile(verificationCode: "\(self.txt1.text ?? "")\(self.txt2.text ?? "")\(self.txt3.text ?? "")\(self.txt4.text ?? "")\(self.txt5.text ?? "")\(self.txt6.text ?? "")")
            }
        }
    }
}


//MARK:- API CALL
extension OTPVerifyVC{
    func apiCallForLoginRegister() {
        
        let countryInfo = getCountryCodeFromDialcode(dialCode: mobileCode)
        let codeInfo = countryInfo["country"] as! String
        let currencyCode = Locale.currencies[codeInfo]
        let currencyCodeSymbol = getCurrencySymbol(forCurrencyCode: currencyCode?.code ?? "USD")

        let params = ["device_type":"1",
                      "user_type":appDel.userType,
                      "device_token": appDel.deviceToken,
                      "mobile_no": mobileNumber,
                      "country_code": mobileCode,
                      "country_code_info": codeInfo,
                      "currency_code": currencyCode?.code ?? "USD",
                      "currency_symbol": currencyCodeSymbol ?? "$"] as [String : Any]
        
        WebService.Request.patch(url: loginRegister, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String : Any] {
                        let detail = data.nullKeyRemoval()
                        appDel.loggedInUserData = User (JSON: data)
                        UserDefaults.standard.set(detail, forKey: UserdefaultsConstants.userData)
                        UserDefaults.standard.synchronize()

                        appDel.setupProfile()
                        appDel.displayScreen()
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func sendOTPAPI(email:String)
    {
        let params = ["email_id":email] as [String : Any]
        WebService.Request.patch(url: sendEmailOTP, type: .post, parameter: params, callSilently: false, header: nil) { (response, error) in
            if error == nil {
                print(response!)
                if response!["status"] as? Bool == true
                {
                    self.lblInfo.text = "Verification code was successfully sent to your email address. Enter the code in the fields below"
                    self.otp = response!["otp"] as? String ?? ""
                    self.viewEmail.isHidden = true
                    self.view.endEditing(true)
                    self.txtEmail.text = ""
                } else{
                    self.alertOk(title: "", message: response!["msg"] as? String ?? "Something Went Wrong..")
                }
            }else{
                self.alertOk(title: "", message:"Something Went Wrong..")
            }
        }
    }
}
