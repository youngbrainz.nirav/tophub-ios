//
//  DownloadedCell.swift
//  CineStool
//
//  Created by hiren  mistry on 29/09/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit

class DownloadedCell: UITableViewCell {
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblStar: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var btnDeleteMovie: UIButton!

    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var cnst_downloadView: NSLayoutConstraint!
    @IBOutlet weak var downloadProgress: UIProgressView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblRemainingTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func CheckVideoStatus(model:Movies){
       var downloadModel = MZDownloadModel()
       let objectModel = DELEGATE.downloadManager.downloadingArray.filter{$0.fileURL == model.final_video}
        let videoIndex = DELEGATE.downloadManager.downloadingArray.firstIndex{$0.fileURL == model.final_video}
       if objectModel.count > 0{
           downloadModel = objectModel[0]
       }
       self.displayDownloadedData(downloadModel: downloadModel)
       if DELEGATE.downloadManager.downloadingArray.count > 0{
               if downloadModel.status == TaskStatus.downloading.description() {
                   self.btnPlayPause.setImage(UIImage(named: "ic_pauseDownload"), for: .normal)
                   self.lblStatus.text = "Downloading..."
                DELEGATE.downloadManager.resumeDownloadTaskAtIndex(videoIndex!)
               }else if downloadModel.status == TaskStatus.failed.description() {
                   self.btnPlayPause.setImage(UIImage(named: "ic_download"), for: .normal)
                   self.lblStatus.text = "Failed..."
               }else if downloadModel.status == TaskStatus.paused.description() {
                   self.btnPlayPause.setImage(UIImage(named: "ic_download"), for: .normal)
                   self.lblStatus.text = "Paused..."
               }
               self.cnst_downloadView.constant = 0
         }
        
        let arr = Common.GetDownloadedVideofromLibrary(strUrl: model.final_video)
        if arr != ""{
            self.btnPlayPause.setImage(UIImage(named: "ic_downloaded_cmpltd"), for: .normal)
            self.cnst_downloadView.constant = 0
        }
    }
    func displayDownloadedData(downloadModel:MZDownloadModel){
            print("updating download...")
            
            var remainingTime: String = ""
            self.downloadProgress.progress = downloadModel.progress
            var fileSize = ""
            if let _ = downloadModel.file?.size {
                fileSize = String(format: "%.2f %@", (downloadModel.file?.size)!, (downloadModel.file?.unit)!)
            }

            var speed = ""
            if let _ = downloadModel.speed?.speed {
                speed = String(format: "%.2f %@/sec", (downloadModel.speed?.speed)!, (downloadModel.speed?.unit)!)
            }
            
            var downloadedFileSize = ""
            if let _ = downloadModel.downloadedFile?.size {
                downloadedFileSize = String(format: "%.2f %@", (downloadModel.downloadedFile?.size)!, (downloadModel.downloadedFile?.unit)!)
            }
            
            let detailLabelText = NSMutableString()
            detailLabelText.appending("\(fileSize) / \(downloadedFileSize) | \(downloadModel.progress * 100.0)%")
            let str = String(format:"%.2f",downloadModel.progress * 100.0)

            if downloadModel.progress * 100.0 > 0.0{
             //   DispatchQueue.main.async {
                    self.lblRemainingTime.text = "\(fileSize) / \(downloadedFileSize) | \(str)%"
             //   }
            }
    }
    
}
