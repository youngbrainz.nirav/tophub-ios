//
//  DownloadedVC.swift
//  CineStool
//
//  Created by hiren  mistry on 29/09/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import GoogleCast

class DownloadedVC: UIViewController {
    @IBOutlet weak var lblNoData: UILabel!

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tblDownloaded: UITableView!
    var arrDownloads:[Movies] = [Movies]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblDownloaded.register(UINib(nibName: "DownloadedCell", bundle: nil), forCellReuseIdentifier: "DownloadedCell")

        self.tblDownloaded.delegate = self
        self.tblDownloaded.dataSource = self
        
        self.arrDownloads = Common.FetchDownloadedData()
        print(self.arrDownloads.count)
        if self.arrDownloads.count > 0{
            self.lblNoData.isHidden = true
        }else{
            self.lblNoData.isHidden = false
        }
        
        UIApplication.shared.beginBackgroundTask {}

        // Do any additional setup after loading the view.
    }
    @IBAction func btnDeleteClick(_ sender: UIButton) {
        
        let downloadModel = DELEGATE.downloadManager.downloadingArray.filter{$0.fileURL == arrDownloads[sender.tag].final_video}
        if downloadModel.count > 0{
            let index = DELEGATE.downloadManager.downloadingArray.firstIndex{$0.fileURL == arrDownloads[sender.tag].final_video}
            if index != nil{
                DELEGATE.downloadManager.cancelTaskAtIndex(index!)
                self.arrDownloads.remove(at: sender.tag)
                let JSONString = self.arrDownloads.map{$0.toJSONString(prettyPrint: true)}
                Common.RemoveObjectFromUserDefault(ArrData: JSONString as? [String] ?? [""])
            }
        }else{
            self.RemoveFromLocalFiles(str1: self.arrDownloads[sender.tag].final_video)
            self.arrDownloads.remove(at: sender.tag)
            let JSONString = self.arrDownloads.map{$0.toJSONString(prettyPrint: true)}
            Common.RemoveObjectFromUserDefault(ArrData: JSONString as? [String] ?? [""])
        }
        self.tblDownloaded.reloadData()
    }
    func RemoveFromLocalFiles(str1:String){
        var str = str1
        str = str.replacingOccurrences(of: "https://cinestool.s3.amazonaws.com/movie_", with: "")
        do {
            let contentOfDir: [String] = try FileManager.default.contentsOfDirectory(atPath: MZUtility.baseFilePath + "/Default folder" as String)
            let tempArr = contentOfDir.map{$0.replacingOccurrences(of: "My Downloadsmovie_", with: "")}
            
            let arr = tempArr.filter{$0 == str}
            if arr.count > 0{
                let filePathName = MZUtility.baseFilePath + "/Default folder" as String + "/My Downloadsmovie_\(arr[0])"
                try FileManager.default.removeItem(atPath: filePathName)
            }else{
                do {
                    let contentOfDir: [String] = try FileManager.default.contentsOfDirectory(atPath: MZUtility.baseFilePath + "/My Downloads" as String)
                    let tempArr = contentOfDir.map{$0.replacingOccurrences(of: "My Downloadsmovie_", with: "")}
                    
                    let arr = tempArr.filter{$0 == str}
                    if arr.count > 0{
                        let filePathName = MZUtility.baseFilePath + "/My Downloads" as String + "/My Downloadsmovie_\(arr[0])"
                        try FileManager.default.removeItem(atPath: filePathName)
                    }
                    
                    
                } catch let error as NSError {
                    print("Error while getting directory content \(error)")
                }
            }
            
            
        } catch let error as NSError {
            print("Error while getting directory content \(error)")
            do {
                let contentOfDir: [String] = try FileManager.default.contentsOfDirectory(atPath: MZUtility.baseFilePath + "/My Downloads" as String)
                let tempArr = contentOfDir.map{$0.replacingOccurrences(of: "My Downloadsmovie_", with: "")}
                
                let arr = tempArr.filter{$0 == str}
                if arr.count > 0{
                    print(arr)
                    let filePathName = MZUtility.baseFilePath + "/My Downloads" as String + "/My Downloadsmovie_\(arr[0])"
                    try FileManager.default.removeItem(atPath: filePathName)
                }
            } catch let error as NSError {
                print("Error while getting directory content \(error)")
            }
        }
    }

    @IBAction func btnDownloadClick(_ sender: UIButton) {
        UIApplication.shared.beginBackgroundTask {}
            let arr = Common.GetDownloadedVideofromLibrary(strUrl: self.arrDownloads[sender.tag].final_video)
            let cell = self.tblDownloaded.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! DownloadedCell
            if arr == ""{
                var downloadModel = MZDownloadModel()
                let objectModel = DELEGATE.downloadManager.downloadingArray.filter{$0.fileURL == self.arrDownloads[sender.tag].final_video}
                if objectModel.count > 0{
                    downloadModel = objectModel[0]
                }
                let videoIndex = DELEGATE.downloadManager.downloadingArray.firstIndex{$0.fileURL == self.arrDownloads[sender.tag].final_video}
                if videoIndex != nil{
                    if downloadModel.status == TaskStatus.downloading.description() {
                        DELEGATE.downloadManager.pauseDownloadTaskAtIndex(videoIndex!)
                    }else if downloadModel.status == TaskStatus.failed.description() {
                        DELEGATE.downloadManager.retryDownloadTaskAtIndex(videoIndex!)
                    }else if downloadModel.status == TaskStatus.paused.description() {
                        DELEGATE.downloadManager.resumeDownloadTaskAtIndex(videoIndex!)
                    }
                }else{
                    let myDownloadPath = MZUtility.baseFilePath + "/My Downloads"
                    if !FileManager.default.fileExists(atPath: myDownloadPath) {
                        try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
                    }
                    cell.btnPlayPause.setImage(UIImage(named: "ic_pauseDownload"), for: .normal)
                    cell.lblStatus.text = "Downloading..."
                    cell.cnst_downloadView.constant = 0
                    self.tblDownloaded.reloadData()
                    let fileURL  : NSString = self.arrDownloads[sender.tag].final_video as NSString
                    var fileName : NSString = fileURL.lastPathComponent as NSString
                    fileName = MZUtility.getUniqueFileNameWithPath(myDownloadPath.appending(fileName as String) as NSString)
                    DELEGATE.downloadManager.addDownloadTask(fileName as String, fileURL: fileURL as String, destinationPath: myDownloadPath)

                }
                self.refreshCell(downloadModel: downloadModel)
            }
    }

    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        return
        if drawerController == nil{
        }
        guard drawerController?.drawerSide != .left else {
            drawerController?.closeSide()
            return
        }
        drawerController?.openSide(.left)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DownloadedVC:UITableViewDelegate,UITableViewDataSource{
    func refreshCell(downloadModel:MZDownloadModel){
        let index = self.arrDownloads.firstIndex{$0.final_video == downloadModel.fileURL}
        if index != nil{
            self.tblDownloaded.reloadRows(at: [IndexPath(row: index!, section: 0)], with: .none)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDownloads.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DownloadedCell") as! DownloadedCell
        let model = arrDownloads[indexPath.row]
        cell.imgMovie.sd_setImage(with: URL(string: model.poster_image), placeholderImage: nil)
        cell.lblName.text = model.name
        let rate:Float = Float(model.total_rating) ?? 0.0
        let doubleStr1 = String(format: "%.2f", rate).dropLast()
        cell.lblStar.text = "\(doubleStr1)"
        cell.lblDesc.isHidden  = true
        
        cell.btnPlayPause.addTarget(self, action: #selector(self.btnDownloadClick(_:)), for: .touchUpInside)
        cell.btnPlayPause.tag = indexPath.row
        
        cell.btnDeleteMovie.addTarget(self, action: #selector(self.btnDeleteClick(_:)), for: .touchUpInside)
        cell.btnDeleteMovie.tag = indexPath.row

        let downloadModel = DELEGATE.downloadManager.downloadingArray.filter{$0.fileURL == model.final_video}
        if downloadModel.count > 0{
            cell.displayDownloadedData(downloadModel: downloadModel[0])
            cell.CheckVideoStatus(model: model)
        }else{
            let arr = Common.GetDownloadedVideofromLibrary(strUrl: self.arrDownloads[indexPath.row].final_video)
            if arr != ""{
                cell.btnPlayPause.setImage(UIImage(named:"ic_downloaded_cmpltd"), for: .normal)
            }else{
                cell.btnPlayPause.setImage(UIImage(named:"ic_download"), for: .normal)
            }

            cell.cnst_downloadView.constant = 0
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objTab = NewMoviewDetailVC(nibName: "NewMoviewDetailVC", bundle: .main)
        objTab.movieDetail = self.arrDownloads[indexPath.row]
        objTab.isFromDownloads = true
        let castContainerVC =
            GCKCastContext.sharedInstance().createCastContainerController(for: objTab)
        castContainerVC.miniMediaControlsItemEnabled = true
        objTab.movie_banner = self.arrDownloads[indexPath.row].movie_banner
        findtopViewController()?.navigationController!.pushViewController(objTab, animated: true)

//        findtopViewController()?.navigationController?.pushViewController        Common.appDelegate.mainNavController = UINavigationController(rootViewController: self)
//        Common.appDelegate.mainNavController = UINavigationController(rootViewController: self)

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
