//
//  LiveShowsVC.swift
//  CineStool
//
//  Created by Dharam YB on 06/06/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import SwiftyJSON
import MobileRTC

class LiveShowsVC: UIViewController,MobileRTCAuthDelegate {

    @IBOutlet weak var tblLiveShow: UITableView!
    var ArrLiveShowList:[modelLiveShows] = [modelLiveShows]()
    var meeting_no:String = ""
    var meeting_pwd:String = ""

    //MARK:- Variables & Outlets
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblLiveShow.delegate = self
        self.tblLiveShow.dataSource = self
        self.GetListOfLiveShows()
        tblLiveShow.register(UINib(nibName: "LiveShowCell", bundle: nil), forCellReuseIdentifier: "LiveShowCell")
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnBackTapped(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    
    }
    @IBAction func btnJoinEvent(_ sender:UIButton){
        let startDate = self.ArrLiveShowList[sender.tag].start_time.toDate(withFormat: "dd-MM-yyyy hh:mm:ss") ?? Date()
        let endDate = self.ArrLiveShowList[sender.tag].end_time.toDate(withFormat: "dd-MM-yyyy hh:mm:ss") ?? Date()
        let currentDate = Date().toString().toDate(withFormat: "dd-MM-yyyy hh:mm:ss") ?? Date()
        
        if currentDate >  endDate{
            
        }
       // if  currentDate?.compare(startDate) && (currentDate < endDate){
            if self.ArrLiveShowList[sender.tag].meeting_type == "1"{
                self.meeting_no = self.ArrLiveShowList[sender.tag].meeting_number
                self.meeting_pwd = self.ArrLiveShowList[sender.tag].password
                self.initilizeZoom()
            }else{
                if self.ArrLiveShowList[sender.tag].isPaid == "1"{
                    self.meeting_no = self.ArrLiveShowList[sender.tag].meeting_number
                    self.meeting_pwd = self.ArrLiveShowList[sender.tag].password
                    self.initilizeZoom()
                }else{
                    let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                    objTab.isFromSubscribe = .GoLive
                    objTab.liveShowData = self.ArrLiveShowList[sender.tag]
                    self.navigationController!.pushViewController(objTab, animated: true)

                }
            }
      //  }
    }
    
    //MARK:- API Call
    func GetDetailsOfLiveShows(){
        let params = ["page":"1","orderBy":"_id","dir":"asc"] as [String : Any]
            var ishideLoader = false

            WebService.Request.patch(url: getRunningEvent, type: .post, parameter: params, callSilently : ishideLoader) { (response, error) in
                if error == nil {
                    if response!["status"] as? Int == 1 {
                        self.ArrLiveShowList.removeAll()
                        if let data = response!["records"] as? [[String: Any]] {
                            for obj in data{
                                self.ArrLiveShowList.append(modelLiveShows(objData: JSON(obj)))
                            }
                        }
                        self.tblLiveShow.reloadData()
                    } else {
                        self.alertOk(title: "", message: response!["msg"] as! String)
                    }
                } else {
    //                self.alertOk(title: "", message: "Something went wrong please try again!")
                }
                WebService.Loader.hide()
            }
        }
    func GetListOfLiveShows(){
        let params = ["page":"1","orderBy":"_id","dir":"asc","userID":"5f4f6876a975953a0568cf56"] as [String : Any]
            var ishideLoader = false

            WebService.Request.patch(url: GetLiveShowList, type: .post, parameter: params, callSilently : ishideLoader) { (response, error) in
                if error == nil {
                    if response!["status"] as? Int == 1 {
                        self.ArrLiveShowList.removeAll()
                        if let data = response!["records"] as? [[String: Any]] {
                            for obj in data{
                                self.ArrLiveShowList.append(modelLiveShows(objData: JSON(obj)))
                            }
                        }
                        self.tblLiveShow.reloadData()
                    } else {
                        self.alertOk(title: "", message: response!["msg"] as! String)
                    }
                } else {
    //                self.alertOk(title: "", message: "Something went wrong please try again!")
                }
                WebService.Loader.hide()
            }
        }
    //MARK:- Zoom events
    func initilizeZoom(){
            let authService = MobileRTC.shared().getAuthService()
            authService?.delegate = self
    //        authService?.jwtToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6Ikhva1laN2RlVDhxcUJZNHBWeEtPTHciLCJleHAiOjE2MDc5MjkxNDcsImlhdCI6MTYwNzMyNDM0OH0.Jgo5Uti4-fMNqo-tstEvgYYEh18Xrwr4jfCgPS2lJdk"
            authService?.clientSecret = "k3078OlFvW0RKnE5NyOmGbEAZGysmoc31B8k"
            authService?.clientKey = "YXHy1QZd0XTSV0fY4N5JgW6QQDIGnsF94hhm"

            authService?.sdkAuth()
        }
        
        func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
            if returnValue == .success{
                print("Successfully authenticated")
                
             //   DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    let paramDict = [
                        kMeetingParam_Username: appDel.loggedInUserData?.user_name == "" ? appDel.loggedInUserData?.name : appDel.loggedInUserData?.user_name,
                        kMeetingParam_MeetingNumber: "93517478363",
                        kMeetingParam_MeetingPassword: "G6AtUbNFCY"
                    ]
                    
    //                let user = MobileRTCMeetingStartParam4WithoutLoginUser()
    //                user.userType = .unknown
    //                user.zak = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6Ikhva1laN2RlVDhxcUJZNHBWeEtPTHciLCJleHAiOjE2MDc5MjkxNDcsImlhdCI6MTYwNzMyNDM0OH0.Jgo5Uti4-fMNqo-tstEvgYYEh18Xrwr4jfCgPS2lJdk"
    //                user.userName = "test"
                    let ms = MobileRTC.shared().getMeetingService()
                    
                    let ret = ms?.joinMeeting(with: paramDict)
                    
           //     }
            }else{
                print("failed authentication")
            }
        }
}

extension LiveShowsVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ArrLiveShowList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblLiveShow.dequeueReusableCell(withIdentifier: "LiveShowCell", for: indexPath) as! LiveShowCell
        cell.lblTitle.text = self.ArrLiveShowList[indexPath.row].topic
        cell.lblAgenda.text = self.ArrLiveShowList[indexPath.row].agenda
        cell.lblEndtime.text = self.ArrLiveShowList[indexPath.row].end_time.toDate()?.toString()
        cell.lblStartDate.text = self.ArrLiveShowList[indexPath.row].start_time.toDate()?.toString()
        cell.lblEntranceFee.text = self.ArrLiveShowList[indexPath.row].meeting_type == "1" ? "Entrance Fee : Free" : "Entrance Fee : \(self.ArrLiveShowList[indexPath.row].amount)"
        
        cell.btnJoinEvent.tag = indexPath.row
        cell.btnJoinEvent.addTarget(self, action: #selector(self.btnJoinEvent(_:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


//MARK:- Model class

class modelLiveShows: NSObject {
    var _id:String = ""
    var topic:String = ""
    var start_time:String = ""
    var end_time:String = ""
    var duration:String = ""
    var agenda:String = ""
    var is_admin_approved:String = ""
    var customerID:String = ""
    var month:String = ""
    var month_name:String = ""
    var year:String = ""
    var date:String = ""
    var is_event_requested:String = ""
    var eventId:String = ""
    var created_date:String = ""
    var amount:String = ""
    var encrypted_password:String = ""
    var event_status:String = ""
    var h323_password:String = ""
    var host_email:String = ""
    var host_id:String = ""
    var id:String = ""
    var join_url:String = ""
    var meeting_number:String = ""
    var meeting_type:String = ""
    var modified_date:String = ""
    var password:String = ""
    var pstn_password:String = ""
    var status:String = ""
    var timezone:String = ""
    var type:String = ""
    var uuid:String = ""
    var is_event_completed:String = ""
    var isPaid:String = ""
    init(objData:JSON) {
        if let x = objData["topic"].string{
              topic = x
        }
        if let x = objData["_id"].string{
              _id = x
        }
        if let x = objData["_id"].int{
              _id = "\(x)"
        }
        if let x = objData["isPaid"].string{
              isPaid = x
        }
        if let x = objData["isPaid"].int{
              isPaid = "\(x)"
        }
        if let x = objData["start_time"].string{
              start_time = x
        }
        if let x = objData["end_time"].string{
              end_time = x
        }
        if let x = objData["duration"].string{
              duration = x
        }
        if let x = objData["agenda"].string{
              agenda = x
        }
        if let x = objData["is_admin_approved"].string{
              is_admin_approved = x
        }
        if let x = objData["customerID"].string{
              customerID = x
        }
        if let x = objData["month"].string{
              month = x
        }
        if let x = objData["month_name"].string{
              month_name = x
        }
        if let x = objData["year"].string{
              year = x
        }
        if let x = objData["date"].string{
              date = x
        }
        if let x = objData["is_event_requested"].string{
              is_event_requested = x
        }
        if let x = objData["eventId"].string{
              eventId = x
        }
        if let x = objData["created_date"].string{
              created_date = x
        }
        if let x = objData["amount"].string{
              amount = x
        }
        if let x = objData["encrypted_password"].string{
              encrypted_password = x
        }
        if let x = objData["event_status"].string{
              event_status = x
        }
        if let x = objData["h323_password"].string{
              h323_password = x
        }
        if let x = objData["host_email"].string{
              host_email = x
        }
        if let x = objData["host_id"].string{
              host_id = x
        }
        if let x = objData["id"].string{
              id = x
        }
        if let x = objData["join_url"].string{
              join_url = x
        }
        if let x = objData["meeting_number"].string{
              meeting_number = x
        }
        if let x = objData["meeting_type"].string{
              meeting_type = x
        }
        if let x = objData["modified_date"].string{
              modified_date = x
        }
        if let x = objData["password"].string{
              password = x
        }
        if let x = objData["pstn_password"].string{
              pstn_password = x
        }
        if let x = objData["status"].string{
              status = x
        }
        if let x = objData["timezone"].string{
              timezone = x
        }
        if let x = objData["type"].string{
              type = x
        }
        if let x = objData["uuid"].string{
              uuid = x
        }
        if let x = objData["is_event_completed"].string{
              is_event_completed = x
        }
        if let x = objData["topic"].int{
              topic = "\(x)"
        }
        if let x = objData["start_time"].int{
              start_time = "\(x)"
        }
        if let x = objData["end_time"].int{
              end_time = "\(x)"
        }
        if let x = objData["duration"].int{
              duration = "\(x)"
        }
        if let x = objData["agenda"].int{
              agenda = "\(x)"
        }
        if let x = objData["is_admin_approved"].int{
              is_admin_approved = "\(x)"
        }
        if let x = objData["customerID"].int{
              customerID = "\(x)"
        }
        if let x = objData["month"].int{
              month = "\(x)"
        }
        if let x = objData["month_name"].int{
              month_name = "\(x)"
        }
        if let x = objData["year"].int{
              year = "\(x)"
        }
        if let x = objData["date"].int{
              date = "\(x)"
        }
        if let x = objData["is_event_requested"].int{
              is_event_requested = "\(x)"
        }
        if let x = objData["eventId"].int{
              eventId = "\(x)"
        }
        if let x = objData["created_date"].int{
              created_date = "\(x)"
        }
        if let x = objData["amount"].int{
              amount = "\(x)"
        }
        if let x = objData["encrypted_password"].int{
              encrypted_password = "\(x)"
        }
        if let x = objData["event_status"].int{
              event_status = "\(x)"
        }
        if let x = objData["h323_password"].int{
              h323_password = "\(x)"
        }
        if let x = objData["host_email"].int{
              host_email = "\(x)"
        }
        if let x = objData["host_id"].int{
              host_id = "\(x)"
        }
        if let x = objData["id"].int{
              id = "\(x)"
        }
        if let x = objData["join_url"].int{
              join_url = "\(x)"
        }
        if let x = objData["meeting_number"].int{
              meeting_number = "\(x)"
        }
        if let x = objData["meeting_type"].int{
              meeting_type = "\(x)"
        }
        if let x = objData["modified_date"].int{
              modified_date = "\(x)"
        }
        if let x = objData["password"].int{
              password = "\(x)"
        }
        if let x = objData["pstn_password"].int{
              pstn_password = "\(x)"
        }
        if let x = objData["status"].int{
              status = "\(x)"
        }
        if let x = objData["timezone"].int{
              timezone = "\(x)"
        }
        if let x = objData["type"].int{
              type = "\(x)"
        }
        if let x = objData["uuid"].int{
              uuid = "\(x)"
        }
        if let x = objData["is_event_completed"].int{
              is_event_completed = "\(x)"
        }
    }
}
