//
//  GoLiveReqVC.swift
//  CineStool
//
//  Created by Dharam YB on 12/05/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UITextView_Placeholder
import AgoraRtcKit
import SkyFloatingLabelTextField
import SwiftyJSON

class GoLiveReqVC: UIViewController {
    //MARK: -
    @IBOutlet weak var btnRequestToLive: UIButton!
    @IBOutlet weak var dtPicker: UIDatePicker!
    @IBOutlet weak var txtNM: SkyFloatingLabelTextField!
    @IBOutlet weak var vw_datePicker: UIView!
    @IBOutlet weak var txtMob: UITextField!
    @IBOutlet weak var txtStartTime: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEndTime: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAgenda: UITextView!
    @IBOutlet weak var txtTopic: SkyFloatingLabelTextField!
    var strStartTime:String = ""
    var strEndTime:String = ""
    var dtStartDate = Date()
    var isForStartTime:Bool = true
    var objAddedEvent:modelLiveShows = modelLiveShows(objData: JSON(["":""]))
    var isToUpdate:Bool = false
    //MARK:- Variables & Outlets
    
    @IBOutlet weak var mainScrollView:UIScrollView!
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var txtEventName: UITextField!
    @IBOutlet weak var heighttxtEventName: NSLayoutConstraint!
    @IBOutlet weak var txtViewEventDetail: UITextView!
    @IBOutlet weak var lblEventsDate: UILabel!
    
    @IBOutlet weak var viewDatePick: UIView!
    @IBOutlet weak var viewMonthYearPicker: UIDatePicker!
    
    @IBOutlet weak var viewGoLiveApprove: UIView!
    var date = ""
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtMobile.attributedPlaceholder = NSAttributedString(string: "Mobile Number", attributes:    [NSAttributedString.Key.foregroundColor: UIColor.white])
        setupCurrentCountryDialingCode()
        hideView()
        self.viewMonthYearPicker.minimumDate = Date()
        self.viewMonthYearPicker.setValue(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), forKey: "textColor")
        
        self.txtStartTime.delegate = self
        self.txtEndTime.delegate = self
        
        let selectedDate = viewMonthYearPicker.date.toString(fromFormat: dateFormatter.dd_MM_yyyy, toFormat: dateFormatter.dd_MM_yyyy)
        self.date = selectedDate
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
        self.txtViewEventDetail.placeholder = "Event Detail"
        self.txtViewEventDetail.placeholderColor = .white
        self.txtViewEventDetail.tintColorDidChange()
        self.txtViewEventDetail.textContainer.maximumNumberOfLines = 7
        
        self.heighttxtEventName.constant = self.txtViewEventDetail.contentSize.height
        
        if #available(iOS 13.0, *) {
            appDel.window!.overrideUserInterfaceStyle = .dark
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblCountryCode.text = appDel.loggedInUserData?.country_code
        self.txtMob.text = appDel.loggedInUserData?.mobile_no
        dtPicker.setValue(UIColor.white, forKeyPath: "textColor")
        self.apiCallEventDetails()
        
        self.dtPicker.minimumDate = Date()
//        appDel.eventLiveId = appDel.arrUserData?._id ?? ""
//        if appDel.userPaymentDetail["is_event_requested"] as? String == "1"{
//            self.viewGoLiveApprove.isHidden = false
//            if appDel.userPaymentDetail["is_go_live_event_approval"] as? String == "1"{
//                alertOk(title: "", message: "Your Go Live Request \nhas been approved")
//            }else if appDel.userPaymentDetail["is_go_live_event_approval"] as? String == "2"{
//                //alertOk(title: "", message: "Hello \(appDel.loggedInUserData?.name ?? ""), unfortunately your request for Go Live has been declined, you can reach our support team at +233 546 872 809 on watsapp for further details.")
//                alertOk(title: "", message: "Hello \(appDel.loggedInUserData?.name ?? ""), unfortunately your request for Go Live has been declined, you can reach our support team at +233 546 872 809 on watsapp for further details.", buttonTitle: "Ok") { (result) in
//                    if result{
//                        self.navigationController?.popViewController(animated: true)
//                    }
//                }
//            }else{
//                alertOk(title: "", message: "Your Go Live Request is pending review, please wait or contact our suport team at +233 546 872 809 on Watsapp.", buttonTitle: "Ok") { (result) in
//                    if result{
//                        self.navigationController?.popViewController(animated: true)
//                    }
//                }
//            }
//        }else{
//            self.viewGoLiveApprove.isHidden = true
//        }

        
    }

    //MARK:- Button Tapped Events
   
    @IBAction func btnDoneDateClick(_ sender: Any) {
        if self.isForStartTime{
            self.strStartTime = self.dtPicker.date.toString(withFormat: "yyyy-MM-dd hh:mm:ss")
            self.txtStartTime.text = self.dtPicker.date.toString()
            self.dtStartDate = self.dtPicker.date
        }else{
            let diff = self.dtStartDate.timeOfDayInterval(toDate: self.dtPicker.date)
            print(diff)
            
            if diff > 0 {
                self.strEndTime = self.dtPicker.date.toString(withFormat: "yyyy-MM-dd hh:mm:ss")
                self.txtEndTime.text = self.dtPicker.date.toString()
            }else{
                self.alertOk(title: "", message: "End date must be greater than start time.")
            }

        }
        self.vw_datePicker.isHidden = true
    }
    @IBAction func btnCancelDateClick(_ sender: Any) {
        self.vw_datePicker.isHidden = true
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCountryCodeTapped(_ sender: UIButton) {
        var objCountryListVC: CountryListVC?
        objCountryListVC = CountryListVC(nibName: "CountryListVC", bundle: nil)
        objCountryListVC?.selectedCountry = {(countryData) in
           self.lblCountryCode.text = countryData["code"] as? String ?? ""
        }
        self.present(objCountryListVC!,animated: true)
    }
    
    @IBAction func btnDateAction(_ sender: UIButton) {
        self.view.endEditing(true)
        ShowView()
    }
    
    @IBAction func btnHideDatePicker(_ sender: UIButton) {
        self.lblEventsDate.text = self.date
        hideView()
    }
    
    @IBAction func dateChanged(_ sender: UIDatePicker) {
        let selectedDate = sender.date.toString(fromFormat: dateFormatter.dd_MM_yyyy, toFormat: dateFormatter.dd_MM_yyyy)
        self.date = selectedDate
        //self.lblEventsDate.text = selectedDate
    }
    
    @IBAction func btnRequestAction(_ sender: UIButton) {
        if validate() {
            if self.isToUpdate{
                self.apiCallUpdateEditEvent()
            }else{
                self.apiCallAddEditEvent()
            }
        }
    }
    
    @IBAction func btnGoLiveAction(_ sender: UIButton) {
        print("Live....")
        self.viewGoLiveApprove.isHidden = true
        self.mainScrollView.isHidden = false
        self.isToUpdate = true
        self.btnRequestToLive.setTitle("Update", for: .normal)
      //  self.apiCallStartEvent()
    }
    
    @IBAction func btnCancelReqAction(_ sender: UIButton) {
        self.apiCallCancelGoLiveEvent()
        if appDel.eventLiveId == ""{
           // self.navigationController?.popViewController(animated: true)
        }else{
          //  self.apiCallCancelGoLiveEvent()
        }
    }
}

extension GoLiveReqVC:UITextFieldDelegate,UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.heighttxtEventName.constant = self.txtViewEventDetail.contentSize.height
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtStartTime{
            self.view.endEditing(true)
            self.isForStartTime = true
            self.vw_datePicker.isHidden = false
            return false
        }
        if textField == txtEndTime{
            if txtStartTime.text != ""{
                self.view.endEditing(true)
                self.isForStartTime = false
                self.vw_datePicker.isHidden = false
            }else{
                self.alertOk(title: "", message: "Please enter start time.")
            }
            return false
        }
        self.vw_datePicker.isHidden = true

        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.heighttxtEventName.constant = self.txtViewEventDetail.contentSize.height
        let bottomOffset:CGPoint = CGPoint(x: 0, y: (-self.mainScrollView.contentInset.top) + self.heighttxtEventName.constant)
        self.mainScrollView.setContentOffset(bottomOffset, animated: true)
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtMobile{
            IQKeyboardManager.shared.enableAutoToolbar = true
        }else{
            IQKeyboardManager.shared.enableAutoToolbar = false
        }
    }
    
    func hideView() {
        viewDatePick.isHidden = true
    }
    
    func ShowView() {
        viewDatePick.isHidden = false
    }
    
    func setupCurrentCountryDialingCode() {
        if UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileNumber) as? String != nil && UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String != nil{
            
            if  UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String == ""{
                let temp = Common.getCountryList()
                for item in temp{
                    if item["country"] as? String == Locale.current.regionCode{
                        self.lblCountryCode.text = item["code"] as? String ?? "+1"
                    }
                }
            }else{
                self.lblCountryCode.text = UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String ?? Locale.current.regionCode
            }
            self.txtMobile.text = UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileNumber) as? String
        }else{
            let temp = Common.getCountryList()
            for item in temp{
                if item["country"] as? String == Locale.current.regionCode{
                    self.lblCountryCode.text = item["code"] as? String ?? "+1"
                }
            }
        }
        
        if self.lblCountryCode.text == "" || self.lblCountryCode.text == nil{
            let temp = Common.getCountryList()
            for item in temp{
                if item["country"] as? String == Locale.current.regionCode{
                    self.lblCountryCode.text = item["code"] as? String ?? "+1"
                }
            }
        }
    }
    
    //Validate Fields
   func validate() -> Bool
   {
       if txtTopic.text == "" {
           alertOk(title: "", message: "Please Insert Topic") { (result) in}
           return false
       }else if txtAgenda.text == "" {
            alertOk(title: "", message: "Please Insert Agenda") { (result) in}
           return false
       }else if txtStartTime.text == "" {
           alertOk(title: "", message: "Please Insert Start Time") { (result) in}
           return false
       }else if txtEndTime.text == "" {
           alertOk(title: "", message: "Please Insert End Time") { (result) in}
           return false
       }
       self.view.endEditing(true)
       return true
    }
    
    func apiCallAddEditEvent() {
//        let mobile = "\(self.lblCountryCode.text ?? "")\(self.txtMobile.text ?? "")"
        var param = [String:Any]()
        param = ["topic":self.txtTopic.text ?? "","start_time":self.strStartTime,"end_time":self.strEndTime ,"agenda":self.txtAgenda.text ?? "","userID":appDel.loginUser_Id,"updateID":""]
        WebService.Request.patch(url: addeditEventData, type: .post, parameter: param, callSilently : false) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.alertOk(title: "", message: response!["msg"] as! String, buttonTitle: "Ok") { (result) in
                        if result{
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    func apiCallUpdateEditEvent() {
        let mobile = "\(self.lblCountryCode.text ?? "")\(self.txtMobile.text ?? "")"
        var param = [String:Any]()
        param = ["topic":self.txtTopic.text ?? "","start_time":self.strStartTime,"end_time":self.strEndTime,"agenda":self.txtAgenda.text ?? "","updateID":self.objAddedEvent._id,"userID":appDel.loginUser_Id]
        WebService.Request.patch(url: updateEventData, type: .post, parameter: param, callSilently : false) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.alertOk(title: "", message: response!["msg"] as! String, buttonTitle: "Ok") { (result) in
                        if result{
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    func apiCallCancelGoLiveEvent() {
        var param = [String:Any]()
        param = ["updateID":self.objAddedEvent._id]
        WebService.Request.patch(url: cancellEvent, type: .post, parameter: param, callSilently : false) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.alertOk(title: "", message: response!["msg"] as! String, buttonTitle: "Ok") { (result) in
                        if result{
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    
    func apiCallStartEvent() {
        var param = [String:Any]()
        param = ["event_id":appDel.eventLiveId]
        WebService.Request.patch(url: startGoLiveEvent, type: .post, parameter: param, callSilently : true) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    let objTab = GoLiveVC(nibName: "GoLiveVC", bundle: nil)
                    objTab.isFrom = .broadcast
                    self.navigationController!.pushViewController(objTab, animated: true)
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    func apiCallEventDetails() {
        var param = [String:Any]()
        param = ["userID":appDel.loginUser_Id ?? ""]
        WebService.Request.patch(url: getEventDetails, type: .post, parameter: param, callSilently : false) { (response, error) in
            print(response)
            
            self.mainScrollView.isHidden = false
            self.viewGoLiveApprove.isHidden = true
            
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let objData = response!["data"] as? [String:Any]{
                        self.mainScrollView.isHidden = true
                        self.objAddedEvent = modelLiveShows(objData: JSON(objData))
                        self.txtEndTime.text = self.objAddedEvent.end_time.toDate()?.toString()
                        self.txtStartTime.text = self.objAddedEvent.start_time.toDate()?.toString()

                        self.strEndTime = self.objAddedEvent.end_time.toDate()?.toString(withFormat: "yyyy-MM-dd hh:mm:ss") ?? ""
                        self.strStartTime = self.objAddedEvent.start_time.toDate()?.toString(withFormat: "yyyy-MM-dd hh:mm:ss") ?? ""
                        self.txtAgenda.text = self.objAddedEvent.agenda
                        self.txtTopic.text = self.objAddedEvent.topic
                        self.viewGoLiveApprove.isHidden = false
                    }else{
                        self.mainScrollView.isHidden = false
                        self.viewGoLiveApprove.isHidden = true
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
}
