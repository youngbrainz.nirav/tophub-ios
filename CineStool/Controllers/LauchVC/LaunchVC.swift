//
//  LaunchVC.swift
//  CineStool
//
//  Created by hiren  mistry on 01/06/21.
//  Copyright © 2021 Youngbrainz. All rights reserved.
//

import UIKit
import AVKit

class LaunchVC: UIViewController {
    @IBOutlet weak var videoView: UIView!
    var player : AVPlayer!
    var avPlayerLayer : AVPlayerLayer!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        playVideo()
    }
    override func viewDidLayoutSubviews() {
        avPlayerLayer.frame = videoView.layer.bounds
    }

    func playVideo() {
        guard let path = Bundle.main.path(forResource: "TopHub_Splash_video", ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }
        player = AVPlayer(url: URL(fileURLWithPath: path))
        avPlayerLayer = AVPlayerLayer(player: player)
        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoView.layer.addSublayer(avPlayerLayer)
        self.player.play()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
