//
//  MyCollectionVC.swift
//  CineStool
//
//  Created by YB on 11/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class MyCollectionVC: UIViewController {
    
    @IBOutlet weak var tblMyCollection: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var heightNavBar: NSLayoutConstraint! // 50

    var arrMyCollection: [Movies] = []
    var listType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onloadOperation()
    }
    
    func onloadOperation() {
        appDel.isCallAPI = true
        tblMyCollection.register(MyCollectionTblCell.self)
        if listType == "1"{
            heightNavBar.constant = 0
            viewNavBar.isHidden = true
            self.lblNoData.text = "No Movies"
        }else{
            heightNavBar.constant = 50
            viewNavBar.isHidden = false
            lbltitle.text = "History"
            self.lblNoData.text = "No History"
        }
        apiCallGetMyCollection()
    }
    
    func apiCallGetMyCollection() {
        
        //let params = ["page": 1,"limit": "15","mobile_uuid":appDel.getDeviceUUID(),"fetch_list_type": listType] as [String : Any]
        //            required : [loginuser_id, session_token, user_type(1:Customer), page]
        let params = ["page": 1] as [String : Any]
        WebService.Request.patch(url: getMyCollection, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [[String : Any]] {
                        self.arrMyCollection = data.map { Movies(JSON: $0)! }
                        self.tblMyCollection.isHidden = false
                        self.tblMyCollection.reloadData()
                    } else {
                        self.tblMyCollection.isHidden = true
                    }
                } else {
                    self.tblMyCollection.isHidden = true
//                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.tblMyCollection.isHidden = true
//                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }

    @IBAction func btnSideMenuAction(_ sender: UIButton) {
        guard drawerController?.drawerSide != .left else {
            drawerController?.closeSide()
            return
        }
        drawerController?.openSide(.left)
    }
}

extension MyCollectionVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMyCollection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MyCollectionTblCell = tableView.dequeueReusableCellTb(for: indexPath)
        let model = arrMyCollection[indexPath.row]
        cell.imgPoster.sd_setImage(with: URL(string: model.poster_image), placeholderImage: nil)
        cell.lblName.text = model.name
        let rate:Float = Float(model.total_rating) ?? 0.0
        let doubleStr1 = String(format: "%.2f", rate).dropLast()
        cell.lblRating.text = "\(doubleStr1)"
        //cell.lblRemainingTime.text = model.remaining_hours
        
        cell.lblRemainingTime.isHidden = true
        cell.imgRemainingTimer.isHidden = true
//        if listType == "1"{
//            cell.lblRemainingTime.isHidden = false
//            cell.imgRemainingTimer.isHidden = false
//        }else{
//            cell.lblRemainingTime.isHidden = true
//            cell.imgRemainingTimer.isHidden = true
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        appDel.redirectToMovieDetail(movieID: arrMyCollection[indexPath.row].movie_id,movie_banner:arrMyCollection[indexPath.row].movie_banner)
    }
}
