//
//  InstructionVideoVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 04/09/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import AVKit

class InstructionVideoVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var btnSkip: UIButton!
    var player = AVPlayer()
    var video_url = ""
    var isHideSkipButton = false
    
    var blockInstruction: (()->())!
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        btnSkip.isHidden = isHideSkipButton
        if self.video_url != ""{
            let videoURL = URL(string: self.video_url)
            self.player = AVPlayer(url: videoURL!)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = UIScreen.main.bounds//self.view.bounds
            self.view.layer.addSublayer(playerLayer)
            self.view.bringSubviewToFront(self.btnSkip)
            
            player.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(InstructionVideoVC.videoDidEnd(notification:)), name:
                NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
            
            player.play()
        }else{
            
        }
    }
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "timeControlStatus", let change = change, let newValue = change[NSKeyValueChangeKey.newKey] as? Int, let oldValue = change[NSKeyValueChangeKey.oldKey] as? Int {
            let oldStatus = AVPlayer.TimeControlStatus(rawValue: oldValue)
            let newStatus = AVPlayer.TimeControlStatus(rawValue: newValue)
            if newStatus != oldStatus {
                DispatchQueue.main.async {[weak self] in
                    if newStatus == .playing || newStatus == .paused {
                        findtopViewController()?.stopAnimatingLoader()
                    } else { findtopViewController()?.startAnimatingLoader(message: "")
                    }
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.player.pause()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
    }
    
    @objc func videoDidEnd(notification: NSNotification) {
        print("video ended")
        self.player.pause()
        self.dismiss(animated: true, completion: nil)
        self.blockInstruction()
    }
    
    //MARK:- Button tapped events
    @IBAction func btnSkipTapped(_ sender: UIButton) {
        self.player.pause()
        self.dismiss(animated: true, completion: nil)
        self.blockInstruction()
    }
}
