//
//  PaymentHistoryVC.swift
//  CineStool
//
//  Created by Dharam YB on 18/02/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit

class PaymentHistoryVC: UIViewController {

    @IBOutlet var tblHistory: UITableView!
    var paymentHistoryList = [paymentHistory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblHistory.register(UINib(nibName: "CellPaymentHistory", bundle: .main), forCellReuseIdentifier: "CellPaymentHistory")
        tblHistory.tableFooterView = UIView()
        apiCallPaymentHistory()
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnSquareTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
}

extension PaymentHistoryVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return paymentHistoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: CellPaymentHistory = tableView.dequeueReusableCellTb(for: indexPath)
        
        cell.lblDate.text = paymentHistoryList[indexPath.row].created_date
        cell.lblInfo.text = paymentHistoryList[indexPath.row].msg
        
        cell.lblTransId.text = paymentHistoryList[indexPath.row].transcation_id
        cell.lblStartDate.text = paymentHistoryList[indexPath.row].start_date
        cell.lblEndDate.text = paymentHistoryList[indexPath.row].expired_date
        return cell
    }    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func apiCallPaymentHistory() {
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
        let codeInfo = countryInfo["country"] as! String
        let currencyCodedetail = Locale.currencies[codeInfo]
        let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")
        let param = ["currency_code": "\(currencyCodedetail?.code ?? "USD")", "currency_symbol": currencyCode ?? "$"]
        WebService.Request.patch(url: SubscribedUserPaymentList, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    print("-->",response!)
                    if let data = response!["data"] as? [[String: Any]] {
                        self.paymentHistoryList = data.map{paymentHistory(JSON: $0)!}
                        self.tblHistory.reloadData()
                    }else{
                        
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }

}
