//
//  MoviePlayerVC.swift
//  CineStool
//
//  Created by Dharam YB on 31/12/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import VersaPlayer
import GoogleCast

class MoviePlayerVC: UIViewController,VersaPlayerPlaybackDelegate,VersaPlayerGestureRecieverViewDelegate {
    func didPinch(with scale: CGFloat) {
        
    }
    
    func didTap(at point: CGPoint) {
        if isHideControls{
            
        }
    }
    
    func didDoubleTap(at point: CGPoint) {
        
    }
    
    func didPan(with translation: CGPoint, initially at: CGPoint) {
        
    }
    
    @IBOutlet weak var playerView: VersaPlayerView!
    @IBOutlet weak var controls: VersaPlayerControls!
    @IBOutlet weak var viewGoogleCast: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCast: UIButton!
    
    var movieDetail: Movies!
    var banner_movies: BannerHome!
    var strVideoURL = String()
    var strImageURL = String()
    var strName = String()
    var strDesc = String()
    var isHideControls = false
    
    private var castButton: GCKUICastButton!
    private var mediaInformation: GCKMediaInformation?
    private var sessionManager: GCKSessionManager!
    var openFrom: redirectToMoviePlayerFrom = .Movie
    
    override func viewDidLoad() {
        super.viewDidLoad()

        playerView.layer.backgroundColor = UIColor.black.cgColor
        controls.pipButton?.set(active: false)
        playerView.use(controls: controls)
        
        if openFrom == .Trailer{
            strVideoURL = movieDetail.movie_trailer
            strImageURL = movieDetail.movie_image
            strName = movieDetail.name
            strDesc = movieDetail.description
        }else if openFrom == .Banner{
            strVideoURL = banner_movies.banner_video
            strImageURL = banner_movies.banner_image
            strName = ""
            strDesc = ""
        }else{
            strVideoURL = movieDetail.final_video
            strImageURL = movieDetail.movie_image
            strName = movieDetail.name
            strDesc = movieDetail.description
        }
    
        if let url = URL.init(string: strVideoURL) {
            let item = VersaPlayerItem(url: url)
            playerView.contentMode = .scaleAspectFit
            
            playerView.set(item: item)
            
            sessionManager = GCKCastContext.sharedInstance().sessionManager
            sessionManager.add(self)

            castButton = GCKUICastButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            castButton.tintColor = UIColor.white
            self.viewGoogleCast.addSubview(castButton)
            
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(castDeviceDidChange(notification:)),
                                                   name: NSNotification.Name.gckCastStateDidChange,
                                                   object: GCKCastContext.sharedInstance())

            if GCKCastContext.sharedInstance().castState != GCKCastState.noDevicesAvailable {
                btnCast.isHidden = true
//                GCKCastContext.sharedInstance().presentCastInstructionsViewControllerOnce(with: castButton)
                if GCKCastContext.sharedInstance().castState == GCKCastState.connected{
                    playerView.pause()
                    playVideoRemotely()
                }
            }else{
                btnCast.isHidden = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDel.myOrientation = .all
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if GCKCastContext.sharedInstance().castState != GCKCastState.noDevicesAvailable {
            btnCast.isHidden = true
            GCKCastContext.sharedInstance().presentCastInstructionsViewControllerOnce(with: castButton)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        appDel.myOrientation = .portrait
    }
    
    @IBAction func btnNoCastDevice(sender _: AnyObject) {
        alertOk(title: "", message: "There is no any casting device available please turn on the casting device to connect.")
    }

    @IBAction func btnBack(sender _: AnyObject) {
        playerView.pause()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnPlayerView(_ sender: Any) {
        if isHideControls{
            btnBack.isHidden = false
            viewGoogleCast.isHidden = false
            isHideControls = false
        }else{
            btnBack.isHidden = true
            viewGoogleCast.isHidden = true
            isHideControls = true
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        UIDevice.current.setValue(Int(UIInterfaceOrientationMask.landscape.rawValue), forKey: "orientation")
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        if (self.isMovingFromParent) {
//            UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
//        }
//    }
}

extension MoviePlayerVC: GCKSessionManagerListener, GCKRemoteMediaClientListener, GCKRequestDelegate{
        
    @objc func castDeviceDidChange(notification _: Notification) {
         if GCKCastContext.sharedInstance().castState != GCKCastState.noDevicesAvailable {
          // Display the instructions for how to use Google Cast on the first app use.
            btnCast.isHidden = true
              GCKCastContext.sharedInstance().presentCastInstructionsViewControllerOnce(with: castButton)
              if GCKCastContext.sharedInstance().castState == GCKCastState.connected{
                    playerView.pause()
                    playVideoRemotely()
              }
         }
    }

    // MARK: Cast Actions

    func playVideoRemotely() {
        self.navigationController?.popViewController(animated: true)
//        dismiss(animated: true) {
            GCKCastContext.sharedInstance().presentDefaultExpandedMediaControls()
//        }

      // Define media metadata.
      let metadata = GCKMediaMetadata()
        metadata.setString(strName, forKey: kGCKMetadataKeyTitle)
        metadata.setString(strDesc,
                         forKey: kGCKMetadataKeySubtitle)
        metadata.addImage(GCKImage(url: URL(string: strImageURL)!,
                                 width: 480,
                                 height: 360))
        
        let mediaInfoBuilder = GCKMediaInformationBuilder(contentURL: URL(string: strVideoURL)!)
      mediaInfoBuilder.streamType = GCKMediaStreamType.buffered
        mediaInfoBuilder.mediaTracks = []
      mediaInfoBuilder.contentType = "video/mp4"
      mediaInfoBuilder.metadata = metadata
      mediaInformation = mediaInfoBuilder.build()

      let mediaLoadRequestDataBuilder = GCKMediaLoadRequestDataBuilder()
      mediaLoadRequestDataBuilder.mediaInformation = mediaInformation

      // Send a load request to the remote media client.
      if let request = sessionManager.currentSession?.remoteMediaClient?.loadMedia(with: mediaLoadRequestDataBuilder.build()) {
        request.delegate = self
      }
    }

    // MARK: GCKSessionManagerListener

    func sessionManager(_: GCKSessionManager,
                        didStart session: GCKSession) {
        print("sessionManager didStartSession: \(session)")
//        self.navigationController?.presentedViewController!.dismiss(animated: true, completion: nil)
        
        session.remoteMediaClient?.add(self)
    }

    func sessionManager(_: GCKSessionManager,
                        didResumeSession session: GCKSession) {
      print("sessionManager didResumeSession: \(session)")

      // Add GCKRemoteMediaClientListener.
      session.remoteMediaClient?.add(self)
    }

    func sessionManager(_: GCKSessionManager,
                        didEnd session: GCKSession,
                        withError error: Error?) {
      print("sessionManager didEndSession: \(session)")
      // Remove GCKRemoteMediaClientListener.
      session.remoteMediaClient?.remove(self)
      if let error = error {
        showError(error)
      }
    }

    func sessionManager(_: GCKSessionManager,
                        didFailToStart session: GCKSession,
                        withError error: Error) {
      print("sessionManager didFailToStartSessionWithError: \(session) error: \(error)")

      // Remove GCKRemoteMediaClientListener.
        session.remoteMediaClient?.remove(self)
        playerView.play()
    }

    // MARK: GCKRemoteMediaClientListener

    func remoteMediaClient(_: GCKRemoteMediaClient,
                           didUpdate mediaStatus: GCKMediaStatus?) {
      if let mediaStatus = mediaStatus {
        mediaInformation = mediaStatus.mediaInformation
      }
    }

    // MARK: - GCKRequestDelegate
    func requestDidComplete(_ request: GCKRequest) {
      print("request \(Int(request.requestID)) completed")
    }

    func request(_ request: GCKRequest,
                 didFailWithError error: GCKError) {
      print("request \(Int(request.requestID)) didFailWithError \(error)")
    }

    func request(_ request: GCKRequest,
                 didAbortWith abortReason: GCKRequestAbortReason) {
      print("request \(Int(request.requestID)) didAbortWith reason \(abortReason)")
    }

    // MARK: Misc

    func showError(_ error: Error) {
      let alertController = UIAlertController(title: "Error",
                                              message: error.localizedDescription,
                                              preferredStyle: UIAlertController.Style.alert)
      let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
      alertController.addAction(action)

      present(alertController, animated: true, completion: nil)
    }
}

