//
//  CreateProfileVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SDWebImage
import FirebaseAuth

class CreateProfileVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var btnSquare: UIButton!
    @IBOutlet weak var btnAddPic: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var imgBackg: UIImageView!
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var HeighttxtPassword: NSLayoutConstraint!
    @IBOutlet weak var HeighttxtConfirmPassword: NSLayoutConstraint!
    
    @IBOutlet weak var btnShowPass: UIButton!
    @IBOutlet weak var btnShowConfPass: UIButton!
    @IBOutlet weak var HeightbtnShowPass: NSLayoutConstraint!
    @IBOutlet weak var HeightbtnShowConfPass: NSLayoutConstraint!
    
    let InsertPassword = "Insert Password"
    let InsertConfirmPassword = "Insert Confirm Password"
    let PasswordNotMatched = "Password & Confirm Password Not Matched"
    
    let InsertName = "Please insert Name."
    let InsertEmail = "Please insert Email."
    let InsertValidEmail = "Please insert Valid Email."
    let InsertMobile = "Please insert Mobile."
    let InsertProfile = "Please Add Profile Picture."
    let imagePicker = UIImagePickerController()
    var imgProfilePicture = UIImage()
    var isProfileSelect = false
    var isFrom: screenFrom = .none
    
    var verificationID = ""
    var mobileNumber = ""
    var mobileCode = ""
    var UserData: User?
    var blockDidDismiss: (()->())!
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        onloadOpeartion()
    }
    
    func onloadOpeartion() {
        if isFrom == .mobileVerify {
            btnSave.setTitle("ADD PROFILE", for: .normal)
            btnAddPic.setTitle("ADD PHOTO", for: .normal)
            txtMobile.text = mobileNumber
            btnSquare.isHidden = true
            
            self.HeightbtnShowPass.constant = 40
            self.HeightbtnShowConfPass.constant = 40
            self.btnShowPass.isHidden = false
            self.btnShowConfPass.isHidden = false
            self.HeighttxtPassword.constant = 50
            self.HeighttxtConfirmPassword.constant = 50
            self.txtPassword.isHidden = false
            self.txtConfirmPassword.isHidden = false
            
            let img = #imageLiteral(resourceName: "eye").withRenderingMode(.alwaysTemplate)
            self.btnShowPass.setImage(img, for: .normal)
            self.btnShowConfPass.setImage(img, for: .normal)
            self.btnShowPass.tintColor = .white
            self.btnShowConfPass.tintColor = .white
            
        } else {
            btnSave.setTitle("UPDATE PROFILE", for: .normal)
            btnAddPic.setTitle("UPDATE PHOTO", for: .normal)
            btnSquare.isHidden = true
            
            self.HeightbtnShowPass.constant = 0
            self.HeightbtnShowConfPass.constant = 0
            self.btnShowPass.isHidden = true
            self.btnShowConfPass.isHidden = true
            self.HeighttxtPassword.constant = 0
            self.HeighttxtConfirmPassword.constant = 0
            self.txtPassword.isHidden = true
            self.txtConfirmPassword.isHidden = true
        }
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        imagePicker.delegate = self
        self.imgProfile.contentMode = .scaleAspectFill
        apiCallGetUserProfile()
        
        let img = #imageLiteral(resourceName: "BGProfile").withRenderingMode(.alwaysTemplate)
        self.imgBackg.image = img
        self.imgBackg.tintColor = .black
        self.imgBackg.tintColorDidChange()
    }
    
    //MAEK:- Button Tapped Events
    @IBAction func btnSquareTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        guard drawerController?.drawerSide != .left else {
            drawerController?.closeSide()
            return
        }
        drawerController?.openSide(.left)
    }
    
    @IBAction func btnAddPicTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Profile Picture", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let popover = alert.popoverPresentationController
        popover?.sourceView = view
        popover?.sourceRect = CGRect(x: UIScreen.main.bounds.height / 2, y: UIScreen.main.bounds.width / 2, width: 64, height: 64)
        present(alert, animated: true)
        
        alert.addAction(UIAlertAction(title: "From Camera", style: .default, handler: { action in
            self.dismiss(animated: true, completion: nil)
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = .camera
                self.saveProfileToAppDel()
                findtopViewController()?.present(self.imagePicker, animated: true, completion: nil)
            } else {
                self.noCamera()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "From Gallery", style: .default, handler: { action in
            self.dismiss(animated: true, completion: nil)
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .photoLibrary
            self.saveProfileToAppDel()
            findtopViewController()?.present(self.imagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            self.dismiss(animated: true, completion: nil)
        }))
    }
    
    @IBAction func btnSaveTapped(_ sender: UIButton) {
        if validate() {
            if isFrom == .mobileVerify {
                apiCallForUpdateProfile()
            }else{
                apiCallForUpdateProfile()
            }
        }
    }
    
    func noCamera() {
        alertOk(title: "No Camera", message: "Sorry, this device has no camera")
    }
    
    func saveProfileToAppDel() {
        appDel.profileData["name"] = self.txtName.text!
        appDel.profileData["email"] = self.txtEmail.text!
        appDel.profileData["mobile"] = self.txtMobile.text!
    }
    
    @IBAction func btnShowPassTapped(_ sender: UIButton) {
        if self.txtPassword.isSecureTextEntry {
           self.txtPassword.isSecureTextEntry = false
        }else{
            self.txtPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnShowConfPassTapped(_ sender: UIButton) {
        if self.txtConfirmPassword.isSecureTextEntry {
           self.txtConfirmPassword.isSecureTextEntry = false
        }else{
            self.txtConfirmPassword.isSecureTextEntry = true
        }
    }
}

extension CreateProfileVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    //Validate Fields
    func validate() -> Bool {
        /*
         if self.isProfileSelect == false{
         alertOk(title: "", message: InsertProfile)
         return false
         }else
         */
        
        if isFrom == .mobileVerify{
            if txtName.text?.count == 0 {
                alertOk(title: "", message: InsertName)
                return false
            }else if txtEmail.text?.count == 0 {
                alertOk(title: "", message: InsertEmail)
                return false
            }else if !(Validate.isValidEmail(testStr: self.txtEmail.text!)) {
                alertOk(title: "", message: InsertValidEmail)
                return false
            }else if txtMobile.text?.count == 0{
                alertOk(title: "", message: InsertMobile)
                return false
            }else if txtPassword.text == "" || txtPassword.text?.isEmpty == true{
                alertController(message: InsertPassword, controller: self)
                return false
            }else if txtConfirmPassword.text == "" || txtConfirmPassword.text?.isEmpty == true{
                alertController(message: InsertConfirmPassword, controller: self)
                return false
            }else if txtPassword.text != txtConfirmPassword.text {
                alertController(message: PasswordNotMatched, controller: self)
                return false
            }
        }else{
            if txtName.text?.count == 0 {
                alertOk(title: "", message: InsertName)
                return false
            }else if txtEmail.text?.count == 0 {
                alertOk(title: "", message: InsertEmail)
                return false
            }else if !(Validate.isValidEmail(testStr: self.txtEmail.text!)) {
                alertOk(title: "", message: InsertValidEmail)
                return false
            }else if txtMobile.text?.count == 0{
                alertOk(title: "", message: InsertMobile)
                return false
            }
        }
        return true
    }
    
    //MARK:- UIImagePicker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let possibleImage = info[.editedImage] as? UIImage {
            self.imgProfile.image = possibleImage
            self.imgProfilePicture = possibleImage
            appDel.imgProfilePicture = possibleImage
            appDel.profileData["profileImg"] = possibleImage
            self.isProfileSelect = true
            appDel.isProfileChange = true
            appDel.isCallAPI = false
        }
        else if (info[.originalImage] as? UIImage) != nil {
        }
        else {
            return
        }
        findtopViewController()?.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Image Picker Dismiss")
        if self.blockDidDismiss != nil{
            self.blockDidDismiss()
        }
        findtopViewController()?.dismiss(animated: true)
        appDel.isCallAPI = false
    }
}


//MARK:- API CALL
extension CreateProfileVC {
    func apiCallGetUserProfile() {
        WebService.Request.patch(url: getProfile, type: .post, parameter: [:]) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String : Any] {
                        self.UserData =  User(JSON: data)
                        appDel.loggedInUserData = User (JSON: data)
                        let detail = data.nullKeyRemoval()
                        UserDefaults.standard.set(detail, forKey: UserdefaultsConstants.userData)
                        UserDefaults.standard.synchronize()
                        appDel.setupProfile()
                        self.setupProfile()
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }

    func setupProfile()
    {
        if isFrom != .mobileVerify {
            isProfileSelect = true
        }
        let url = URL(string: self.UserData!.profile_image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        if appDel.isCallAPI {
            self.txtName.text = self.UserData?.user_name
            self.txtEmail.text = self.UserData?.email_id
            self.txtMobile.text = self.UserData?.mobile_no
            self.imgProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeHolder"))
        } else {
            self.txtName.text = appDel.profileData["name"] as? String
            self.txtEmail.text = appDel.profileData["email"] as? String
            self.txtMobile.text = appDel.profileData["mobile"] as? String
            if appDel.imgProfilePicture.pngData() == nil {
                self.imgProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeHolder"))
            } else {
                self.imgProfile.image = appDel.imgProfilePicture
            }
        }
    }

    func apiCallForUpdateProfile() {
        
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "")
        let codeInfo = countryInfo["country"] as! String
        let currencyCode = Locale.currencies[codeInfo]
        let currencyCodeSymbol = getCurrencySymbol(forCurrencyCode: currencyCode?.code ?? "USD")

        let params = ["mobile_no":appDel.loggedInUserData?.phone_number ?? "",
                      "country_code": appDel.loggedInUserData?.country_code ?? "",
                      "user_name": "\(txtName.text ?? "")",
            "email_id": "\(txtEmail.text ?? "")",
            "country_code_info": codeInfo,
            "currency_code": currencyCode?.code ?? "USD",
            "currency_symbol": currencyCodeSymbol ?? "$",
            "password": self.txtPassword.text ?? ""] as [String : Any]
        WebService.Request.uploadSingleFiles(url: updateProfile, images: imgProfile.image!, withName: "profile_image", parameters: params as! [String : String]) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String : Any] {
                        let detail = data.nullKeyRemoval()
                        appDel.loggedInUserData = User (JSON: data)
                        UserDefaults.standard.set(detail, forKey: UserdefaultsConstants.userData)
                        UserDefaults.standard.synchronize()
                        appDel.setupProfile()
                        if self.isFrom == .mobileVerify{
                            Auth.auth().createUser(withEmail: appDel.loggedInUserData?.email_id ?? "", password: appDel.loggedInUserData?.password ?? "") { authResult, error in
                                if error == nil {
                                    WebService.Loader.show()
                                    Auth.auth().signIn(withEmail: appDel.loggedInUserData?.email_id ?? "", password: appDel.loggedInUserData?.password ?? "") {  authResult, error in
                                        
                                        WebService.Loader.hide()
                                        appDel.displayScreen()
                                    }
                                } else {
                                    appDel.displayScreen()
                                }
                            }
                            
                        }else{
                            self.alertOk(title: "", message: response!["msg"] as! String)
                        }
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
}
