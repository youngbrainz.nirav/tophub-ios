//
//  SettingsVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import LocalAuthentication

class SettingsVC: UIViewController {
    
    //MARK:- Variables & Outlets
    @IBOutlet weak var tblSettings: UITableView!
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var collectionTopMenu: UICollectionView!
    @IBOutlet weak var btnTopSideMenu: UIButton!
    var arrTopMenu = ["GO LIVE","LIVE SHOWS","MY LIST","MY DOWNLOADED"]
    
    var arrOption = [""]
    var isOn = false
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        appDel.isCallAPI = true
        collectionTopMenu.isScrollEnabled = false
        collectionTopMenu.register(TopMenuCollCell.self)
        self.tblSettings.register(CellSettings.self)
        self.tblSettings.tableFooterView = UIView()
        self.setSettingOption()
        //withRenderingMode(.alwaysTemplate)
        let img = #imageLiteral(resourceName: "logo")
        self.btnTopSideMenu.setImage(img, for: .normal)
        self.btnTopSideMenu.tintColor = .white
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnSideMenuAction(_ sender: UIButton) {
        guard drawerController?.drawerSide != .left else {
            drawerController?.closeSide()
            return
        }
        drawerController?.openSide(.left)
    }
}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrOption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: CellSettings = tableView.dequeueReusableCellTb(for: indexPath)
        cell.lblTitle.text = self.arrOption[indexPath.item]
        
        
        if appDel.isAppLive == appStatus.Live{
            if appDel.is_Subscribed{
                if appDel.subscription_status == "1"{
                    if indexPath.row == 1 {
                        cell.btnSwitch.isHidden = false
                        if appDel.loggedInUserData?.notification_status == "1"{
                            cell.btnSwitch.setImage(#imageLiteral(resourceName: "On"), for: .normal)
                        }else{
                            cell.btnSwitch.setImage(#imageLiteral(resourceName: "Off"), for: .normal)
                        }
                    }else{
                        cell.btnSwitch.isHidden = true
                    }
                }else if appDel.subscription_status == "2"{
                    if indexPath.row == 2 {
                        cell.btnSwitch.isHidden = false
                        if appDel.loggedInUserData?.notification_status == "1"{
                            cell.btnSwitch.setImage(#imageLiteral(resourceName: "On"), for: .normal)
                        }else{
                            cell.btnSwitch.setImage(#imageLiteral(resourceName: "Off"), for: .normal)
                        }
                    }else{
                        cell.btnSwitch.isHidden = true
                    }
                }else{
                    if indexPath.row == 1 {
                        cell.btnSwitch.isHidden = false
                        if appDel.loggedInUserData?.notification_status == "1"{
                            cell.btnSwitch.setImage(#imageLiteral(resourceName: "On"), for: .normal)
                        }else{
                            cell.btnSwitch.setImage(#imageLiteral(resourceName: "Off"), for: .normal)
                        }
                    }else{
                        cell.btnSwitch.isHidden = true
                    }
                }
            }else{
                if indexPath.row == 2 {
                    cell.btnSwitch.isHidden = false
                    if appDel.loggedInUserData?.notification_status == "1"{
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "On"), for: .normal)
                    }else{
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "Off"), for: .normal)
                    }
                }else{
                    cell.btnSwitch.isHidden = true
                }
            }
        }else{
            if indexPath.row == 0 {
                cell.btnSwitch.isHidden = false
                if appDel.loggedInUserData?.notification_status == "1"{
                    cell.btnSwitch.setImage(#imageLiteral(resourceName: "On"), for: .normal)
                }else{
                    cell.btnSwitch.setImage(#imageLiteral(resourceName: "Off"), for: .normal)
                }
            }else{
                cell.btnSwitch.isHidden = true
            }
        }
        
        
        cell.btnSwitch.tag = indexPath.row
        cell.blockSwitchChanged = { index in
            print(index)
            
            
            
            
            
            if appDel.isAppLive == appStatus.Live{
                if appDel.is_Subscribed{
                    if appDel.subscription_status == "1"{
                        if index == 1 {
                            if cell.btnSwitch.currentImage == #imageLiteral(resourceName: "Off") {
                                cell.btnSwitch.setImage(#imageLiteral(resourceName: "On"), for: .normal)
                                self.apiCallForUpdateProfile(status: statusActiveInactive.Active)
                            } else {
                                cell.btnSwitch.setImage(#imageLiteral(resourceName: "Off"), for: .normal)
                                self.apiCallForUpdateProfile(status: statusActiveInactive.InActive)
                            }
                        }
                    }else if appDel.subscription_status == "2"{
                        if index == 2 {
                            if cell.btnSwitch.currentImage == #imageLiteral(resourceName: "Off") {
                                cell.btnSwitch.setImage(#imageLiteral(resourceName: "On"), for: .normal)
                                self.apiCallForUpdateProfile(status: statusActiveInactive.Active)
                            } else {
                                cell.btnSwitch.setImage(#imageLiteral(resourceName: "Off"), for: .normal)
                                self.apiCallForUpdateProfile(status: statusActiveInactive.InActive)
                            }
                        }
                    }else{
                        if index == 1 {
                            if cell.btnSwitch.currentImage == #imageLiteral(resourceName: "Off") {
                                cell.btnSwitch.setImage(#imageLiteral(resourceName: "On"), for: .normal)
                                self.apiCallForUpdateProfile(status: statusActiveInactive.Active)
                            } else {
                                cell.btnSwitch.setImage(#imageLiteral(resourceName: "Off"), for: .normal)
                                self.apiCallForUpdateProfile(status: statusActiveInactive.InActive)
                            }
                        }
                    }
                }else{
                    if index == 2 {
                        if cell.btnSwitch.currentImage == #imageLiteral(resourceName: "Off") {
                            cell.btnSwitch.setImage(#imageLiteral(resourceName: "On"), for: .normal)
                            self.apiCallForUpdateProfile(status: statusActiveInactive.Active)
                        } else {
                            cell.btnSwitch.setImage(#imageLiteral(resourceName: "Off"), for: .normal)
                            self.apiCallForUpdateProfile(status: statusActiveInactive.InActive)
                        }
                    }
                }
            }else{
                if index == 0 {
                    if cell.btnSwitch.currentImage == #imageLiteral(resourceName: "Off") {
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "On"), for: .normal)
                        self.apiCallForUpdateProfile(status: statusActiveInactive.Active)
                    } else {
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "Off"), for: .normal)
                        self.apiCallForUpdateProfile(status: statusActiveInactive.InActive)
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        
        
        
        
        if appDel.isAppLive == appStatus.Live{
            if appDel.is_Subscribed{
                if appDel.subscription_status == "1"{
                    if indexPath.row == 0{
                        let objTab = PaymentHistoryVC(nibName: "PaymentHistoryVC", bundle: nil)
                        self.navigationController!.pushViewController(objTab, animated: true)
                    }else if indexPath.row == 1{
                        
                    }else if indexPath.row == 2{
                        let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                        objTab.isFromSubscribe = .No
                        self.navigationController!.pushViewController(objTab, animated: true)
                    }else if indexPath.row == 3{
                        let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                        objPrivacyPolicyVC.commonFile = .PrivacyPolicy;
                        self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                    }else if indexPath.row == 4{
                        let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                        objPrivacyPolicyVC.commonFile = .TermsAndCondition; self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                    }else if indexPath.row == 5{
                        let strMessage = "If you cancel your subscription you will not have full access to the app. Do you want to proceed.?"
                        alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: strMessage) { (status) in
                            if status{
                                self.apiCallUnSubscription()
                            }
                        }
                    }
                    else if indexPath.row == 6 {
                        alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "If you deactivate or remove your account, all your records including payment will be removed and you will be required to sign up again if you like to use the app. Do you want to proceed .?") { (status) in
                            if status{
                                self.apiCallDeactivateAccount()
                            }
                        }
                    }
                }else if appDel.subscription_status == "2"{
                    if indexPath.row == 0{
                        let objTab = SubscriptionListVC(nibName: "SubscriptionListVC", bundle: nil)
                        self.navigationController!.pushViewController(objTab, animated: true)
                    }else if indexPath.row == 1{
                        let objTab = PaymentHistoryVC(nibName: "PaymentHistoryVC", bundle: nil)
                        self.navigationController!.pushViewController(objTab, animated: true)
                    }else if indexPath.row == 2{
                        
                    }else if indexPath.row == 3{
                        let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                        objTab.isFromSubscribe = .No
                        self.navigationController!.pushViewController(objTab, animated: true)
                    }else if indexPath.row == 4{
                        let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                        objPrivacyPolicyVC.commonFile = .PrivacyPolicy;
                        self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                    }else if indexPath.row == 5{
                        let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                        objPrivacyPolicyVC.commonFile = .TermsAndCondition; self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                    }
                    else if indexPath.row == 6 {
                        alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "If you deactivate or remove your account, all your records including payment will be removed and you will be required to sign up again if you like to use the app. Do you want to proceed .?") { (status) in
                            if status{
                                self.apiCallDeactivateAccount()
                            }
                        }
                    }
                }else{
                    if indexPath.row == 0{
                        let objTab = PaymentHistoryVC(nibName: "PaymentHistoryVC", bundle: nil)
                        self.navigationController!.pushViewController(objTab, animated: true)
                    }else if indexPath.row == 1{
                        
                    }else if indexPath.row == 2{
                        let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                        objTab.isFromSubscribe = .No
                        self.navigationController!.pushViewController(objTab, animated: true)
                    }else if indexPath.row == 3{
                        let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                        objPrivacyPolicyVC.commonFile = .PrivacyPolicy;
                        self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                    }else if indexPath.row == 4{
                        let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                        objPrivacyPolicyVC.commonFile = .TermsAndCondition; self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                    }
                    else if indexPath.row == 5 {
                        alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "If you deactivate or remove your account, all your records including payment will be removed and you will be required to sign up again if you like to use the app. Do you want to proceed .?") { (status) in
                            if status{
                                self.apiCallDeactivateAccount()
                            }
                        }
                    }
                }
            }else{
                if indexPath.row == 0{
                    let objTab = SubscriptionListVC(nibName: "SubscriptionListVC", bundle: nil)
                    self.navigationController!.pushViewController(objTab, animated: true)
                }else if indexPath.row == 1{
                    let objTab = PaymentHistoryVC(nibName: "PaymentHistoryVC", bundle: nil)
                    self.navigationController!.pushViewController(objTab, animated: true)
                }else if indexPath.row == 2{
                    
                }else if indexPath.row == 3{
                    let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                    objTab.isFromSubscribe = .No
                    self.navigationController!.pushViewController(objTab, animated: true)
                }else if indexPath.row == 4{
                    let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                    objPrivacyPolicyVC.commonFile = .PrivacyPolicy;
                    self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                }else if indexPath.row == 5{
                    let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                    objPrivacyPolicyVC.commonFile = .TermsAndCondition; self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                }
                else if indexPath.row == 6 {
                    alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "If you deactivate or remove your account, all your records including payment will be removed and you will be required to sign up again if you like to use the app. Do you want to proceed .?") { (status) in
                        if status{
                            self.apiCallDeactivateAccount()
                        }
                    }
                }
            }
        }else{
            if indexPath.row == 0{
                
            }else if indexPath.row == 1{
                let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                objPrivacyPolicyVC.commonFile = .PrivacyPolicy;
                self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
            } else if indexPath.row == 2 {
                alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "If you deactivate or remove your account, all your records including payment will be removed and you will be required to sign up again if you like to use the app. Do you want to proceed .?") { (status) in
                    if status{
                        self.apiCallDeactivateAccount()
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 54
    }
}

extension SettingsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrTopMenu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TopMenuCollCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.lblMenuTitle.text =  self.arrTopMenu[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected row",indexPath.row)
        print("Selected item",indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionTopMenu.frame.size.width - 30) / 3
        let height = collectionTopMenu.frame.size.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
}


//MARK:- API CALL
extension SettingsVC{
    
    func apiCallForUpdateProfile(status: String) {
        let params = ["status_type":statusType.notificatioStatus, "status":status] as [String : Any]
        WebService.Request.patch(url: changeUserStatus, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String : Any] {
                        let detail = data.nullKeyRemoval()
                        appDel.loggedInUserData = User (JSON: data)
                        UserDefaults.standard.set(detail, forKey: UserdefaultsConstants.userData)
                        UserDefaults.standard.synchronize()
                        appDel.setupProfile()
                    }
                } else {
                }
            } else {
            }
        }
    }
    
    func apiCallUnSubscription() {
        let param = ["subscription_status":"2"/*appDel.subscription_status*/, "subscription_id":appDel.subscription_id]
        WebService.Request.patch(url: changeSubscribedPlan, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    appDel.subscription_status = response!["subscription_status"] as? String ?? "3"
                   // appDel.is_Subscribed = false
                   // self.setSettingOption()
                    appDel.displayScreen(isOpenlaunch: false)
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    func apiCallDeactivateAccount(){
        let param = ["mobile_no":appDel.loggedInUserData!.mobile_no]
        WebService.Request.patch(url: DeactivateAccount, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    resetDefaults()
                    appDel.loginUser_Id = ""
                    appDel.userSessionToken = ""
                    appDel.loggedInUserData = User()
                    let objTab = MobileVerifyVC(nibName: "MobileVerifyVC", bundle: .main)
                    self.drawerController?.setViewController(objTab, for: .none)
                    appDel.displayScreen(isOpenlaunch: false)
                    let nc = NotificationCenter.default
                    nc.post(name: Notification.Name("UserLoggedIn"), object: nil)
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    func setSettingOption(){
        if appDel.isAppLive == appStatus.Live{
            if appDel.is_Subscribed{
                if appDel.subscription_status == "1"{
                    self.arrOption = ["Payment History", "Push Notification","Payment Method", "Privacy Policy","Terms & Conditions","Remove Subscription", "Deactivate Account"]
                }else if appDel.subscription_status == "2"{
                    self.arrOption = ["Subscription Plan", "Payment History", "Push Notification","Payment Method", "Privacy Policy","Terms & Conditions", "Deactivate Account"]
                }else {
                    self.arrOption = ["Payment History", "Push Notification","Payment Method", "Privacy Policy","Terms & Conditions", "Deactivate Account"]
                }
            }else{
                self.arrOption = ["Subscription Plan", "Payment History", "Push Notification","Payment Method", "Privacy Policy","Terms & Conditions", "Deactivate Account"]
            }
        }else{
            self.arrOption = ["Push Notification","Privacy Policy", "Deactivate Account"]
        }
        self.tblSettings.reloadData()
    }
    
    func chekTouchId() -> Bool {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Use Passcode"
        var authError: NSError?
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            if #available(iOS 11.0, *) {
                switch (localAuthenticationContext.biometryType){
                case .none:
                    return false
                case .touchID:
                    return true
                case .faceID:
                    return true
                default: break
                }
            } else {
                // Fallback on earlier versions
                return false
            }
        } else {
            return false
        }
        return false
    }
}


