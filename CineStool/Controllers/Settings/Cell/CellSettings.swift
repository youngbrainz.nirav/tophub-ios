//
//  CellSettings.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class CellSettings: UITableViewCell, ReusableView, NibLoadableView {

    //MARK:- Variables & Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSwitch: UIButton!
    
    var blockSwitchChanged : ((Int)->())?
    
    //MARK:- Default Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnSwitchTapped(_ sender: UIButton)
    {
        blockSwitchChanged?(sender.tag)
    }
}
