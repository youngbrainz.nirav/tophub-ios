//
//  ReviewPopup.swift
//  CineStool
//
//  Created by hiren  mistry on 02/10/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import Cosmos

//MARK:- textview
extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = !self.text.isEmpty
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height

            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.white
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = !self.text.isEmpty
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
    
}

protocol reviewDelegate {
    func getBackfromReview()
}
class ReviewPopup: UIView {
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var vw_review: UIView!

    @IBOutlet weak var txtReview: UITextView!
    @IBOutlet weak var strRating: CosmosView!
    @IBOutlet weak var btnCancel: UIButton!
    var givenRating:String = "1"        

    
    
    
    
   // Common.appDelegate.mainNavController = UINavigationController(rootViewController: self)

    
    
    
    var Delegate:reviewDelegate?
    var movieID:String = ""
    
    override func awakeFromNib() {
        self.btnCancel.layer.cornerRadius = 5.0
        self.btnSubmit.layer.cornerRadius = 5.0
        let screenSize: CGRect = UIScreen.main.bounds
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.vw_review.layer.cornerRadius = 5.0
        self.isHidden = true
        strRating.didFinishTouchingCosmos = { rating in
            print(rating)
            self.givenRating = "\(rating)"
            
            //self.lblMyRate.text = "\(Int(rating))"
        }
        self.txtReview.layer.cornerRadius = 5.0
        self.txtReview.layer.borderWidth = 1.0
        self.txtReview.layer.borderColor = UIColor.white.cgColor
        self.txtReview.placeholder = "Enter your comments here..."
        self.txtReview.textColor = .white
    }
        /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
     
    */
    
    func DisplayView(){
        self.isHidden = false
        self.alpha = 0.0

         UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseOut], animations: {
             self.alpha = 1.0
             self.layoutIfNeeded()
         }, completion: nil)
    }
    func DismissView(){
        self.alpha = 1.0
         UIView.animate(withDuration: 0.2, delay: 0, options:  [.curveEaseOut], animations: {
            
            self.alpha = 0.0
            self.layoutIfNeeded()
            
         }) { (Bool) in
             self.removeFromSuperview()
         }
        Delegate?.getBackfromReview()
    }

    @IBAction func btnCancelClick(_ sender: Any) {
        DismissView()
    }
    @IBAction func btnSubmitClick(_ sender: Any) {
            self.apiCallGiveRating()
    }
    
    func apiCallGiveRating() {
        let params = ["movie_id":"self.movieID",
                      "rating": self.givenRating,"feedback":self.txtReview.text!]
        
        WebService.Request.patch(url: giveMovieRating, type: .post, parameter: params) { (response, error) in
            if error == nil {
                //self.Delegate?.getBackfromReview()
                self.DismissView()
                if response!["status"] as? Int == 1 {
                } else {
                }
            } else {
            }
        }
    }
    

}
