//
//  MobileVerifyVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//"https://cinestool.s3.amazonaws.com/trailers%2FAneZgS5Vs8nloTajwR7vzXGzSFH71PKgmR0ZxgI4or3Ua6sbUjnhHXpAHdMCc924S1tzPDN3rRtYpyC87gCi6A1UkwhcsW0ZRgmaKS5ROrLwRcCplcD77h2bsrGpSTf4bannerVideo151.mp4"

import UIKit
import IQKeyboardManagerSwift
import LocalAuthentication
import FirebaseAuth

class MobileVerifyVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var lblCountryCode: UILabel!
    
    @IBOutlet weak var viewCreatePassword: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfPassword: UITextField!
    @IBOutlet weak var heightConfPass: NSLayoutConstraint!
    @IBOutlet weak var heightShowConfPass: NSLayoutConstraint!
    @IBOutlet weak var btnCloseViewCreatePass: UIButton!
    @IBOutlet weak var btnShowConfPass: UIButton!
    
    @IBOutlet weak var txtViewInfo: UITextView!
    @IBOutlet weak var btnCreatePassword: UIButton!
    
     @IBOutlet weak var imgBackg: UIImageView!
    
    let InsertPassword = "Insert Password"
    let InsertConfirmPassword = "Insert Confirm Password"
    let PasswordNotMatched = "Password & Confirm Password Not Matched"
    
    var isCheckPassword = false
    
    var mobile_money_video = ""
    var signup_video = ""
    var str_instruction = ""
    var signup_video_status = "0"
    var mobile_money_video_status = "0"
    var mob_status_text = "0"
    var detail:[String:Any] = [:]
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        setupCurrentCountryDialingCode()
        imgBackg.tintColorDidChange()
    }
    
    func setupCurrentCountryDialingCode() {
        if UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileNumber) as? String != nil && UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String != nil{
            
            if  UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String == ""{
                let temp = Common.getCountryList()
                for item in temp{
                    if item["country"] as? String == Locale.current.regionCode{
                        self.lblCountryCode.text = item["code"] as? String ?? "+1"
                    }
                }
            }else{
                self.lblCountryCode.text = UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String ?? Locale.current.regionCode
            }
            self.txtMobile.text = UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileNumber) as? String
        }else{
            let temp = Common.getCountryList()
            for item in temp{
                if item["country"] as? String == Locale.current.regionCode{
                    self.lblCountryCode.text = item["code"] as? String ?? "+1"
                }
            }
        }
        
        if self.lblCountryCode.text == "" || self.lblCountryCode.text == nil{
            let temp = Common.getCountryList()
            for item in temp{
                if item["country"] as? String == Locale.current.regionCode{
                    self.lblCountryCode.text = item["code"] as? String ?? "+1"
                }
            }
        }
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnCountryCodeTapped(_ sender: UIButton) {
        var objCountryListVC: CountryListVC?
        objCountryListVC = CountryListVC(nibName: "CountryListVC", bundle: nil)
        objCountryListVC?.selectedCountry = {(countryData) in
           self.lblCountryCode.text = countryData["code"] as? String ?? ""
        }
        self.present(objCountryListVC!,animated: true)
    }
    
    @IBAction func btnLetsGoTapped(_ sender: UIButton) {
        if txtMobile.text == "" {
            alertController(message: "Insert Mobile Number", controller: self)
        }else{
            if (self.txtMobile.text == UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileNumber) as? String) && (self.lblCountryCode.text == UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String) &&    (UserDefaults.standard.bool(forKey: "isTouchID") == true) {
                apiCallForLoginRegister(UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileNumber) as? String ?? "", code: UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String ?? "")
            }else{
                self.apiCallForLoginRegister(self.txtMobile.text ?? "", code: self.lblCountryCode.text ?? Locale.current.regionCode ?? "")
                //self.APICallgenerateOTP(self.txtMobile.text ?? "", code: self.lblCountryCode.text ?? Locale.current.regionCode ?? "")
            }
        }
    }
    
    @IBAction func btnForgotTapped(_ sender: UIButton) {
        let objEmailVC = EmailVerifyVC(nibName: "EmailVerifyVC", bundle: nil)
        objEmailVC.isfrom = .fromForgotMobile
        self.navigationController?.pushViewController(objEmailVC, animated: true)
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: UIButton) {
        let objEmailVC = EmailVerifyVC(nibName: "EmailVerifyVC", bundle: nil)
        objEmailVC.isfrom = .fromForgotPassword
        self.navigationController?.pushViewController(objEmailVC, animated: true)
    }
    
    @IBAction func btnHideCreatePassViewTapped(_ sender: UIButton) {
        self.viewCreatePassword.isHidden = true
        self.view.endEditing(true)
        self.heightConfPass.constant = 50
        self.heightShowConfPass.constant = 40
        
        self.txtPassword.text = ""
        self.txtConfPassword.text = ""
    }
    
    @IBAction func btnShowPassTapped(_ sender: UIButton) {
        if self.txtPassword.isSecureTextEntry {
           self.txtPassword.isSecureTextEntry = false
        }else{
            self.txtPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnShowConfPassTapped(_ sender: UIButton) {
        if self.txtConfPassword.isSecureTextEntry {
           self.txtConfPassword.isSecureTextEntry = false
        }else{
            self.txtConfPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnCreatePassTapped(_ sender: UIButton) {
        if appDel.loggedInUserData?.is_password_added == "1"{
            if txtPassword.text == "" || txtPassword.text?.isEmpty == true{
                alertController(message: InsertPassword, controller: self)
            }else{
                if appDel.loggedInUserData?.password != ""{
                    if txtPassword.text != appDel.loggedInUserData?.password{
                        alertController(message: "Password does not match, Please try again later!", controller: self)
                    }else{
                        UserDefaults.standard.set(self.detail, forKey: UserdefaultsConstants.userData)
                        self.isCheckPassword = true
                        self.apiCallInstruction()
                    }
                }else{
                    UserDefaults.standard.set(self.detail, forKey: UserdefaultsConstants.userData)
                    self.isCheckPassword = false
                    self.apiCallInstruction()
                }
            }
        }else{
            if txtPassword.text == "" || txtPassword.text?.isEmpty == true{
                alertController(message: InsertPassword, controller: self)
            }else if txtConfPassword.text == "" || txtConfPassword.text?.isEmpty == true{
                alertController(message: InsertConfirmPassword, controller: self)
            }else if txtPassword.text != txtConfPassword.text {
                alertController(message: PasswordNotMatched, controller: self)
            }else{
                self.isCheckPassword = false
                self.apiCallInstruction()
            }
        }
    }
}

extension MobileVerifyVC : UITextFieldDelegate {
    //MARK:- TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtMobile
        {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
        }else {
            return true
        }
    }
}

extension MobileVerifyVC{
    func apiCallForLoginRegister(_ mobile:String,code:String) {
        let countryInfo = getCountryCodeFromDialcode(dialCode: code)
        let codeInfo = countryInfo["country"] as! String
        let currencyCode = Locale.currencies[codeInfo]
        let currencyCodeSymbol = getCurrencySymbol(forCurrencyCode: currencyCode?.code ?? "USD")
        
        let params = ["device_type":"1",
                      "user_type":appDel.userType,
                      "device_token": "dfsdssdcasdfgfkevso8e48838cfneikek",//,appDel.deviceToken,
                      "mobile_no": mobile,
                      "country_code": code,
                      "country_code_info": codeInfo,
                      "currency_code": currencyCode?.code ?? "USD",
                      "currency_symbol": currencyCodeSymbol ?? "$",
                      "mobile_uid":appDel.getDeviceUUID()] as [String : Any]
        //loginRegister
        WebService.Request.patch(url: loginRegisterV2, type: .post, parameter: params) { (response, error) in
            if error == nil {
                print(response)
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String : Any] {
                        self.detail = data.nullKeyRemoval()
                       // UserDefaults.standard.set(self.detail, forKey: UserdefaultsConstants.userData)
                        appDel.loggedInUserData = User (JSON: data)
                        UserDefaults.standard.set(appDel.loggedInUserData?.phone_number ?? "", forKey: UserdefaultsConstants.mobileNumber)
                        UserDefaults.standard.set(appDel.loggedInUserData?.country_code ?? "", forKey: UserdefaultsConstants.mobileCode)
                        UserDefaults.standard.synchronize()
                        UserDefaults.standard.set(true, forKey: UserdefaultsConstants.isJustLogin)
                        appDel.isJustLoggedIn = true
                        
                        UserDefaults.standard.set(false, forKey: UserdefaultsConstants.isPressedNoToSubscriptionPopup)
                        appDel.isPressedNoToSubscriptionPopup = false
                        UserDefaults.standard.synchronize()
                        
                        let strSubscribed = appDel.loggedInUserData?.is_plan_subscribed
                        if strSubscribed == "1"{
                            appDel.is_Subscribed = true
                        }else{
                            appDel.is_Subscribed = false
                        }
                        UserDefaults.standard.set(appDel.is_Subscribed, forKey: "is_Subscribed")
                        
                        appDel.isAppLive = appDel.loggedInUserData?.is_live ?? ""
                        print(appDel.isAppLive)
                        print(appDel.loggedInUserData?.is_live ?? "")
                        UserDefaults.standard.set(appDel.loggedInUserData?.is_live ?? "", forKey: "is_live")
                        
                        if appDel.loggedInUserData?.new_user == "1" {
                          //  appDel.setupProfile()
//                            let viewController = CreateProfileVC(nibName: "CreateProfileVC", bundle: .main)
//                            viewController.isFrom = .mobileVerify
//                            viewController.mobileNumber = appDel.loggedInUserData?.mobile_no ?? ""
//                            viewController.mobileCode = appDel.loggedInUserData?.country_code ?? ""
//                            self.navigationController?.pushViewController(viewController, animated: true)
                          //  self.APICallgenerateOTP(self.txtMobile.text ?? "", code: self.lblCountryCode.text ?? Locale.current.regionCode ?? "")
                            UserDefaults.standard.set(self.detail, forKey: UserdefaultsConstants.userData)
                            self.verifyMobile(phoneNumber: self.lblCountryCode.text!+self.txtMobile.text!)
                        }else{
                            if appDel.loggedInUserData?.is_same_device == "1"{
                                print("Same Device")
                                if appDel.loggedInUserData?.is_password_added == "1"{
                                    self.viewCreatePassword.isHidden = false
                                    self.heightConfPass.constant = 0
                                    self.heightShowConfPass.constant = 0
                                    self.btnShowConfPass.isHidden = true
                                    self.btnCloseViewCreatePass.isHidden = false
                                    
                                    self.txtViewInfo.text = "Enter your Valid Account Password"
                                    self.txtPassword.placeholder = "Enter your Password to Login"
                                    self.btnCreatePassword.setTitle("Login", for: .normal)
                                }else{
                                    self.viewCreatePassword.isHidden = false
                                    self.heightConfPass.constant = 50
                                    self.heightShowConfPass.constant = 40
                                    self.btnShowConfPass.isHidden = false
                                    self.btnCloseViewCreatePass.isHidden = true
                                    
                                    self.txtViewInfo.text = "Create Password"
                                    self.txtPassword.placeholder = "Enter New Password"
                                    self.btnCreatePassword.setTitle("Login", for: .normal)
                                }
                            }else{
                                print("Not Same Device")
                                if appDel.loggedInUserData?.is_password_added == "1"{
                                    self.viewCreatePassword.isHidden = false
                                    self.heightConfPass.constant = 0
                                    self.heightShowConfPass.constant = 0
                                    self.btnShowConfPass.isHidden = true
                                    self.btnCloseViewCreatePass.isHidden = false
                                    
                                    self.txtViewInfo.text = "Enter your Valid Account Password"
                                    self.txtPassword.placeholder = "Enter your Password to Login"
                                    self.btnCreatePassword.setTitle("Login", for: .normal)
                                }else{
                                    self.viewCreatePassword.isHidden = false
                                    self.heightConfPass.constant = 50
                                    self.heightShowConfPass.constant = 40
                                    self.btnShowConfPass.isHidden = false
                                    self.btnCloseViewCreatePass.isHidden = true
                                    
                                    self.txtViewInfo.text = "Create Password"
                                    self.txtPassword.placeholder = "Enter New Password"
                                    self.btnCreatePassword.setTitle("Login", for: .normal)
                                }
                            }
                        }
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallInstruction() {
        
        appDel.Loginpassword = self.txtPassword.text ?? ""
        self.viewCreatePassword.isHidden = true
        self.view.endEditing(true)
        self.heightConfPass.constant = 50
        self.heightShowConfPass.constant = 40
        
        self.txtPassword.text = ""
        self.txtConfPassword.text = ""
        
        WebService.Request.patch(url: getCustomerInstruction, type: .get, parameter: [:]) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String: Any] {
                        self.mobile_money_video = data["mobile_money_video"] as! String
                        self.signup_video = data["signup_video"] as! String
                        self.str_instruction = data["mob_description"] as! String
                        self.signup_video_status = data["signup_video_status"] as! String
                        self.mobile_money_video_status = data["mobile_money_video_status"] as! String
                        self.mob_status_text = data["mob_status"] as! String
                    }
                }
            }
            WebService.Loader.show()
            Auth.auth().signIn(withEmail: appDel.loggedInUserData?.email_id ?? "", password: appDel.loggedInUserData?.password ?? "") { [weak self] authResult, error in
                WebService.Loader.hide()
              guard let strongSelf = self else { return }
                if let err = error {
                    print(err.localizedDescription)
                    Auth.auth().createUser(withEmail: appDel.loggedInUserData?.email_id ?? "", password: appDel.loggedInUserData?.password ?? "") { authResult, error in
                        if error == nil {
                            WebService.Loader.show()
                            Auth.auth().signIn(withEmail: appDel.loggedInUserData?.email_id ?? "", password: appDel.loggedInUserData?.password ?? "") { authResult, error in
                                WebService.Loader.hide()
                                if strongSelf.isCheckPassword{
                                    strongSelf.registrationInstructionFlow()
                                }else{
                                    appDel.setupProfile()
                                    strongSelf.apiCallGetCategoryList()
                                }
                            }
                        } else {
                            if strongSelf.isCheckPassword{
                                strongSelf.registrationInstructionFlow()
                            }else{
                                appDel.setupProfile()
                                strongSelf.apiCallGetCategoryList()
                            }
                        }
                    }
                } else {
                    if strongSelf.isCheckPassword{
                        strongSelf.registrationInstructionFlow()
                    }else{
                        appDel.setupProfile()
                        strongSelf.apiCallGetCategoryList()
                    }
                }
            }
           
        }
    }
    
    func apiCallGetCategoryList() {
            /*
            required : [loginuser_id, session_token, user_type(1:Customer)]
            optional : [password,mobile_uid]
            */
            let params = ["mobile_uid":appDel.getDeviceUUID(),"password":appDel.Loginpassword] as [String : Any]
            WebService.Request.patch(url: getMovieCategoryV2, type: .post, parameter: params, callSilently : true) { (response, error) in
                print("SideMenu",response)
                //response!["is_go_live_payment_done": 0]
                //response!["is_password_added": 0]
                //response!["is_plan_subscribed": 0]
                //response!["is_go_live_event_approval": 0]
                if error == nil {
                    if response!["status"] as? Int == 1 {
                        if let data = response!["data"] as? [[String : Any]] {
                            let catData = ["name": "Home", "get_subcategory": [:]] as [String : Any]
                            var arrcat = [[String:Any]]()
                            arrcat.insert(catData, at: 0)
                            arrcat.append(["name": "Category", "get_subcategory": data])
                            arrcat.append(["name": "My Collection", "get_subcategory": [:]] as [String : Any])
                         //   arrcat.append(["name": "Downloads", "get_subcategory": [:]] as [String : Any])
                            arrcat.append(["name": "History", "get_subcategory": [:]] as [String : Any])
                            arrcat.append(["name": "Settings", "get_subcategory": [:]] as [String : Any])
                            arrcat.append(["name": "My Profile", "get_subcategory": [:]] as [String : Any])
                            arrcat.append(["name": "Log Out", "get_subcategory": [:]] as [String : Any])
                            appDel.movieCategories.removeAll()
                            appDel.movieCategories = arrcat.map{Categories(JSON: $0)!}
                            print("Sidemenu Count",appDel.movieCategories.count)
                        }
                        self.registrationInstructionFlow()
                    } else {
                        self.alertOk(title: "", message: response!["msg"] as! String)
                    }
                    //INR,15,0,$,1,1,1133.48,1,0
                    //In addedit : is_payment_done : (0, 1), profile_pic
                    
appDel.userPaymentDetail = ["is_go_live_event_approval":response!["is_go_live_event_approval"] as? String ?? "",
                            "is_plan_subscribed":response!["is_plan_subscribed"] as? String ?? "",
                            "go_live_view_actual_charge":response!["go_live_view_actual_charge"] as? String ?? "",
                            "is_event_requested":response!["is_event_requested"] as? String ?? "",
                            "go_live_view_charge":response!["go_live_view_charge"] as? String ?? "",
                            "actual_currency_code":response!["actual_currency_code"] as? String ?? "",
                            "user_currency_code":response!["user_currency_code"] as? String ?? "",
                            "is_go_live_payment_done":response!["is_go_live_payment_done"] as? String ?? "",
                            "is_password_added":response!["is_password_added"] as? String ?? "",
                            "is_start_event":response!["is_start_event"] as? String ?? ""]
                } else {
                    self.alertOk(title: "", message: "Something went wrong please try again!")
                }
                WebService.Loader.hide()
            }
        }
    func verifyMobile(phoneNumber: String)  {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                self.startAnimatingLoader(message: "")
            }
            else {
                print("Either denied or notDetermined")
            }
        }
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            self.stopAnimatingLoader()
            if let error = error {
                alertController(message: (error.localizedDescription.lowercased() == "too_long" || error.localizedDescription.lowercased() == "too_short") ? "Invalid mobile number" : error.localizedDescription, controller: self)
                return
            }
            let objTab = OTPVerifyVC(nibName: "OTPVerifyVC", bundle: nil)
            objTab.verificationID = verificationID!
            objTab.mobileNumber = "\(self.txtMobile.text ?? "")"
            objTab.mobileCode = "\(self.lblCountryCode.text ?? Locale.current.regionCode ?? "")"
            objTab.isFrom = .mobileScreen
            objTab.blockVerifyOtp = { isVerify in
//                if isVerify{
//                    self.ChangeMobileAPI(mobile_no: self.txtNewMobile.text ?? "", country_code: self.lblNewCountryCode.text ?? Locale.current.regionCode ?? "")
//                }
                if isVerify {
                  appDel.setupProfile()
                            let viewController = CreateProfileVC(nibName: "CreateProfileVC", bundle: .main)
                            viewController.isFrom = .mobileVerify
                            viewController.mobileNumber = appDel.loggedInUserData?.mobile_no ?? ""
                            viewController.mobileCode = appDel.loggedInUserData?.country_code ?? ""
                            self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
            self.navigationController!.pushViewController(objTab, animated: true)
        }
    }
    func registrationInstructionFlow()  {
        if signup_video_status == "1"{
            let vc = InstructionVideoVC.init(nibName: "InstructionVideoVC", bundle: .main)
            vc.video_url = self.signup_video
            if appDel.loggedInUserData?.new_user == "1"{
                vc.isHideSkipButton = true
            }else{
                vc.isHideSkipButton = false
            }
            vc.blockInstruction = {
                appDel.setupProfile()
                appDel.displayScreen(isOpenlaunch: false)
            }
            self.present(vc, animated: true, completion: nil)
        }else{
            appDel.setupProfile()
            appDel.displayScreen(isOpenlaunch: false)
        }
    }
    
    //MARK:- Touch ID
    func chekTouchId() -> Bool {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Use Passcode"
        var authError: NSError?
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            if #available(iOS 11.0, *) {
                switch (localAuthenticationContext.biometryType){
                case .none:
                    return false
                case .touchID:
                    return true
                case .faceID:
                    return true
                default: break
                }
            } else {
                // Fallback on earlier versions
                return false
            }
        } else {
            return false
        }
        return false
    }
}

//MARK:- API OTP Generate
extension MobileVerifyVC{
    func APICallgenerateOTP(_ mobile:String,code:String){
        let params = ["mobile_no": mobile,"country_code": code] as [String : Any]
        WebService.Request.patch(url: generateOTP, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    let objTab = OTPVerifyVC(nibName: "OTPVerifyVC", bundle: nil)
                    objTab.isFrom = .mobileScreen
                    objTab.otp = response!["otp"] as? String ?? ""
                    self.view.endEditing(true)
                    objTab.blockVerifyOtp = { isVerify in
                        if isVerify{
                            self.isCheckPassword = true
                            self.apiCallInstruction()
                        }
                    }
                    self.navigationController!.pushViewController(objTab, animated: true)
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
}
