//
//  AppDelegate.swift
//  CineStool
//
//  Created by MacBook Air 002 on 09/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import CoreData
import KWDrawerController
import Firebase
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import Siren
import SwiftKeychainWrapper
import AVKit
import GoogleCast
import MobileCoreServices
import MobileRTC
import FirebaseDatabase
import FirebaseStorage


enum forSubscriptionPopup {
    case none
    case isShowSubscrion
}

extension UIApplication {

    var visibleViewController: UIViewController? {

        guard let rootViewController = keyWindow?.rootViewController else {
            return nil
        }

        return getVisibleViewController(rootViewController)
    }

    private func getVisibleViewController(_ rootViewController: UIViewController) -> UIViewController? {

        if let presentedViewController = rootViewController.presentedViewController {
            return getVisibleViewController(presentedViewController)
        }

        if let navigationController = rootViewController as? UINavigationController {
            return navigationController.visibleViewController
        }

        if let tabBarController = rootViewController as? UITabBarController {
            return tabBarController.selectedViewController
        }

        return rootViewController
    }
}

let DELEGATE = UIApplication.shared.delegate as! AppDelegate
var movieIdGloble = ""
var PartyCodeGloble = ""
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GCKLoggerDelegate
{
    
    var backgroundSessionCompletionHandler : (() -> Void)?
    var ref:DatabaseReference!
    var storageRef:StorageReference!
    var window: UIWindow?
    var navController: UINavigationController?
    var sideMenuVC : SideMenuVC!

    var loginUser_Id = String()
    var userSessionToken = String()
    var deviceToken = String()
    var loggedInUserData: User?
    var userType = "2"
    var movieCategories: [Categories] = []
    var blockPaymentDone: (()->())!
    var deviceUUID = String()
    var isFullScreen = false
    var isFirstTime = true
    var isPressedNoToSubscriptionPopup = false
    var isAppLive = ""
    var is_Subscribed = false
    var badgeCount = 0
    var container = MainContainerVC()
    var isCallAPI = true
    var imgProfilePicture = UIImage()
    var profileData = [String: Any]()
    var isProfileChange = false
    var isJustLoggedIn = false

    let kReceiverAppID = kGCKDefaultMediaReceiverApplicationID
    let kDebugLoggingEnabled = true
    var arrDashboardData: DashboardModel!
    
    var bannerMovies: [BannerHome] = []
    var trandingMovies: [Movies] = []
    var popularMovies: [Movies] = []
    var myHistory: [Movies] = []

    var isFrom:forSubscriptionPopup = .none
    
    var subscription_status:String = "3"
    var subscription_id:String = "0"
    
    var Loginpassword = ""
    var userPaymentDetail = [String:Any]()
    
    var arrUserData: UserData?
    var eventLiveId = ""
    var promoCode = "0"
    var promoCodeText = ""
    
    var isEventListAPICall = true
    var isMessageListAPICall = true
    var isHomePaymentResounse = false
    var myOrientation: UIInterfaceOrientationMask = .portrait
    
    var isAppdelTimerValid = false
    var isWatchMovie = false
    
    lazy var downloadManager: MZDownloadManager = {
        [unowned self] in
        let sessionIdentifer: String = "iphone.MZDownloadManager.backgroundSession"
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var completion = self.backgroundSessionCompletionHandler
        
        let downloadmanager = MZDownloadManager(session: sessionIdentifer, delegate: self, completion: completion)
        return downloadmanager
        }()
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return myOrientation
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.deleteFromCache()
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playback)
        } catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
        
        // Set your receiver application ID.
        let criteria = GCKDiscoveryCriteria(applicationID: kReceiverAppID)
        let options = GCKCastOptions(discoveryCriteria: criteria)
        options.physicalVolumeButtonsWillControlDeviceVolume = true
        GCKCastContext.setSharedInstanceWith(options)
        
        // Theme using GCKUIStyle.
        let castStyle = GCKUIStyle.sharedInstance()
        // Set the property of the desired cast widgets.
        castStyle.castViews.deviceControl.buttonTextColor = .darkGray
        // Refresh all currently visible views with the assigned styles.
        castStyle.apply()
        
//
//        // Enable default expanded controller.
        GCKCastContext.sharedInstance().useDefaultExpandedMediaControls = true
//
//        // Enable logger.
        GCKLogger.sharedInstance().delegate = self
//
//        // Set logger filter.
//        let filter = GCKLoggerFilter()
//        filter.minimumLevel = .error
//        GCKLogger.sharedInstance().filter = filter


        setupProfile()
        Common.DeleteMovieFromLocal()
        if UserDefaults.standard.bool(forKey: UserdefaultsConstants.isJustLogin) == true{
            appDel.isJustLoggedIn = true
        }else{
            appDel.isJustLoggedIn = false
        }
        
        if UserDefaults.standard.bool(forKey: UserdefaultsConstants.isPressedNoToSubscriptionPopup) == true{
            appDel.isPressedNoToSubscriptionPopup = true
        }else{
            appDel.isPressedNoToSubscriptionPopup = false
        }
        
        if UserDefaults.standard.value(forKey: "is_live") != nil{
            if UserDefaults.standard.value(forKey: "is_live") as? String == "" || UserDefaults.standard.value(forKey: "is_live") as? String == "2"{
                appDel.isAppLive = appStatus.Test
            }else{
                appDel.isAppLive = appStatus.Live
            }
        }else{
            appDel.isAppLive = ""
        }
        
        if UserDefaults.standard.value(forKey: "is_Subscribed") != nil{
            if UserDefaults.standard.value(forKey: "is_Subscribed") as? Bool == true{
                appDel.is_Subscribed = true
            }else{
                appDel.is_Subscribed = false
            }
        }else{
            appDel.is_Subscribed = false
        }
        
        displayScreen()

        setupNotification(application: application, launchoption: launchOptions ?? [:])
        if Reachability.isConnectedToNetwork(){
            Siren.shared.launchAppStore()
        }
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        self.sdkInit(self.navController)
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            }
            else if let result = result {
                print("Remote instance ID token: \(result.token)")
                UserDefaults.standard.setValue("\(result.token)", forKey: "token")
                UserDefaults.standard.synchronize()
                self.deviceToken = result.token
            }
        }
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
            guard let dynamicLinks = DynamicLinks.dynamicLinks() as? DynamicLinks else {
                return false
            }
            let handled = dynamicLinks.handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
                print(dynamiclink?.url!.absoluteString as Any)
                let strUrl = dynamiclink?.url?.absoluteURL
                //if dynamiclink?.url?.absoluteString.contains(T##element: Character##Character)
                if let url = strUrl{
                    if url.absoluteString.range(of: "movie_id") != nil {
                        print("Yes, ", url)
                        let compo = strUrl?.absoluteString.components(separatedBy: "?")
                        let movieID = compo?[1].components(separatedBy: "=").last
                        let partyCode = url.absoluteString.contains("PartyCode") ? (compo?.last?.components(separatedBy: "%3D").last) : ""
                        
                        print("movieID:",movieID)
                        print("partyCode:",partyCode)
                        if self.loginUser_Id != "" {
                            if findtopViewController() is LaunchVC {
                                DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                                    self.redirectToMovieDetail(movieID: movieID ?? "",PartyCode: partyCode ?? "")
                                }
                            } else {
                                self.redirectToMovieDetail(movieID: movieID ?? "",PartyCode: partyCode ?? "")
                            }
                           
                        }else{
                            movieIdGloble = movieID ?? ""
                            PartyCodeGloble = partyCode ?? ""
                            self.displayScreen()
                        }
                    }else if url.absoluteString.range(of: "event_id") != nil{
                        print("Yes")
                        let eventID = strUrl?.absoluteString.components(separatedBy: "=").last
                        print(eventID!)
                        appDel.eventLiveId = eventID ?? ""
                        if self.loginUser_Id != "" {
                            if appDel.isAppLive == appStatus.Live{
                                if appDel.userPaymentDetail["is_go_live_payment_done"] as? String == "1"{
                                    let objTab = GoLiveVC(nibName: "GoLiveVC", bundle: nil)
                                    objTab.isFrom = .audience
                                    //objTab.arrEventData = self.arrEventData[indexPath.row]
                                    findtopViewController()?.navigationController?.pushViewController(objTab, animated: true)
                                }else{
                                    let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                                    objTab.isFromSubscribe = .GoLive
                                    //objTab.arrEventData = self.arrEventData[indexPath.row]
                                    findtopViewController()?.navigationController?.pushViewController(objTab, animated: true)
                                }
                            }else{
                                let objTab = GoLiveVC(nibName: "GoLiveVC", bundle: nil)
                                objTab.isFrom = .audience
                                //objTab.arrEventData = self.arrEventData[indexPath.row]
                                findtopViewController()?.navigationController?.pushViewController(objTab, animated: true)
                            }
                        }else{
                            self.displayScreen()
                        }
                    }else{
                        print("No")
                    }
                }
            }
            return handled
        }
    
    func forceLocalizationCustomizationPresentationExample() {
        let siren = Siren.shared
        siren.rulesManager = RulesManager(majorUpdateRules: .critical,
                                          minorUpdateRules: .annoying,
                                          patchUpdateRules: .default,
                                          revisionUpdateRules: Rules(promptFrequency: .weekly, forAlertType: .option))

        siren.wail { results in
            switch results {
            case .success(let updateResults):
                print("AlertAction ", updateResults.alertAction)
                print("Localization ", updateResults.localization)
                print("Model ", updateResults.model)
                print("UpdateType ", updateResults.updateType)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

//    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//        if let rootViewController = self.topViewControllerWithRootViewController(rootViewController: window?.rootViewController) {
//            if (rootViewController.responds(to: Selector("canRotate"))) {
//                // Unlock landscape view orientations for this view controller
//                return .landscape;
//            }
//        }
//        // Only allow portrait (standard behaviour)
//        return .portrait;
//    }
    
    private func topViewControllerWithRootViewController(rootViewController: UIViewController!) -> UIViewController? {
        if (rootViewController == nil) { return nil }
        if (rootViewController.isKind(of: UITabBarController.self)) {
            return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UITabBarController).selectedViewController)
        } else if (rootViewController.isKind(of: UINavigationController.self)) {
            return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UINavigationController).visibleViewController)
        } else if (rootViewController.presentedViewController != nil) {
            return topViewControllerWithRootViewController(rootViewController: rootViewController.presentedViewController)
        }
        return rootViewController
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

    func applicationWillEnterForeground(_ application: UIApplication){
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        if Reachability.isConnectedToNetwork(){
            forceLocalizationCustomizationPresentationExample()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        debugPrint("handleEventsForBackgroundURLSession: \(identifier)")
        print("handleEventsForBackgroundURLSession: \(identifier)")
        backgroundSessionCompletionHandler = completionHandler
        completionHandler()
    }
     func deleteFromCache() {
        var tmpDirectory: [String]? = nil
        do {
            tmpDirectory = try FileManager.default.contentsOfDirectory(atPath: NSTemporaryDirectory())
        } catch {
        }
        let fileManager = FileManager.default
        for file in tmpDirectory ?? [] {
            print("\(URL(fileURLWithPath: NSTemporaryDirectory()))\(file)")
            do {
                try fileManager.removeItem(atPath: NSTemporaryDirectory() + file)
                
            } catch {
                print("Couldn't remove")
            }
        }
        
    }
    


    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CineStool")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: - GCKLoggerDelegate

    func logMessage(_ message: String,
                    at _: GCKLoggerLevel,
                    fromFunction function: String,
                    location: String) {
      if kDebugLoggingEnabled {
        print("\(location): \(function) - \(message)")
      }
    }
}

//MARK:- SetUp Profile
extension AppDelegate{
    func setupProfile()  {
        if let userdata = UserDefaults.standard.value(forKey: UserdefaultsConstants.userData) as? [String:Any]{
            loggedInUserData = User (JSON: userdata)
            loginUser_Id = loggedInUserData?._id ?? ""
            userSessionToken = loggedInUserData?.session_token ?? ""
        }
    }
    
    func displayScreen(isOpenlaunch:Bool = true)  {
        
        window = UIWindow(frame:UIScreen.main.bounds)
        if isOpenlaunch{
            self.isFrom = .isShowSubscrion
            let viewController = LaunchVC(nibName: "LaunchVC", bundle: .main)
            navController = UINavigationController(rootViewController: viewController)
            self.navController?.isNavigationBarHidden = true
            if #available(iOS 13.0, *) {
                window!.overrideUserInterfaceStyle = .light
            }
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + (isOpenlaunch ? 2.0 : 0.001)) {
            if self.loginUser_Id != "" {
                if self.loggedInUserData?.new_user == "1" {
//                    let viewController = CreateProfileVC(nibName: "CreateProfileVC", bundle: .main)
//                    viewController.isFrom = .mobileVerify
//                    viewController.mobileNumber = self.loggedInUserData?.mobile_no ?? ""
//                    viewController.mobileCode = self.loggedInUserData?.country_code ?? ""
//                    self.navController = UINavigationController(rootViewController: viewController)
                    self.isFrom = .isShowSubscrion
                    let viewController = MobileVerifyVC(nibName: "MobileVerifyVC", bundle: .main)
                    self.navController = UINavigationController(rootViewController: viewController)
                }else{
                    let mainViewController = MainContainerVC(nibName: "MainContainerVC", bundle: .main)
                    let menuViewController = SideMenuVC(nibName: "SideMenuVC", bundle: .main)
                    //Set for callback use\
                    let aDrawer = DrawerController()
                    aDrawer.setViewController(mainViewController, for: .none)
                    aDrawer.setViewController(menuViewController, for: .left)
                    aDrawer.drawerWidth = Float((UIScreen.main.bounds.size.width / 3) * 2) + 30
                    self.navController = UINavigationController(rootViewController: aDrawer)
                }
            } else {
                self.isFrom = .isShowSubscrion
                let viewController = MobileVerifyVC(nibName: "MobileVerifyVC", bundle: .main)
                self.navController = UINavigationController(rootViewController: viewController)
            }
            self.navController?.isNavigationBarHidden = true
            if #available(iOS 13.0, *) {
                self.window!.overrideUserInterfaceStyle = .light
            }
            self.window?.rootViewController = self.navController
            self.window?.makeKeyAndVisible()

        }
    }
    
    func getDeviceUUID() -> String {
        if KeychainWrapper.standard.string(forKey: "deviceUUID") != nil{
            deviceUUID = KeychainWrapper.standard.string(forKey: "deviceUUID")!
        }else{
            deviceUUID = UUID.init().uuidString
            deviceUUID = deviceUUID.replacingOccurrences(of: "-", with: "")
            KeychainWrapper.standard.set(deviceUUID, forKey: "deviceUUID")
        }
        return deviceUUID
    }
}

//MARK:- Redirect User to respective Screen
extension AppDelegate {
    func displayScreen(VC: UIViewController.Type, nib: String, sideMenuConfig: Bool) {
        if sideMenuConfig {
            sideMenuConfiguartion(VC: VC, nib: nib)
            return
        }
        window = UIWindow(frame:UIScreen.main.bounds)
        let viewController = VC.init(nibName: nib, bundle: .main)
        navController = UINavigationController(rootViewController: viewController)
        self.navController?.isNavigationBarHidden = true
        if #available(iOS 13.0, *) {
            window!.overrideUserInterfaceStyle = .light
        }
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
    
    //MARK :- SideMenu Configuration
    func sideMenuConfiguartion(VC: UIViewController.Type, nib: String) {
        let mainViewController = VC.init(nibName: nib, bundle: .main)
        let menuViewController = SideMenuVC(nibName: "SideMenuVC", bundle: .main)
        //Set for callback use
        sideMenuVC = menuViewController
        
        let aDrawer = DrawerController()
        aDrawer.setViewController(mainViewController, for: .none)
        aDrawer.setViewController(menuViewController, for: .left)
        
        aDrawer.drawerWidth = Float((UIScreen.main.bounds.size.width / 3) * 2) + 30
//        let castContainerVC =
//                GCKCastContext.sharedInstance().createCastContainerController(for: aDrawer)
//        castContainerVC.miniMediaControlsItemEnabled = true
        navController = UINavigationController(rootViewController: aDrawer)
        self.navController?.isNavigationBarHidden = true
        if #available(iOS 13.0, *) {
            window!.overrideUserInterfaceStyle = .light
        }
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
}

//MARK:- Notification Setup and Delegates
extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate {
    func setupNotification(application: UIApplication, launchoption: [UIApplication.LaunchOptionsKey: Any])  {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions,completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        let remoteNotification: NSDictionary! = launchoption[.remoteNotification] as? NSDictionary
        if (remoteNotification != nil)
        {
            let userInfo = remoteNotification as? [String:Any]

            if let tags = userInfo?["tag"] as? String{
                redirectFromNotification(tag: tags, data: userInfo ?? [:])
            }
        }
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        Messaging.messaging().delegate = self
        FirebaseApp.configure()
        ref = Database.database().reference(withPath: "Groups")
        storageRef = Storage.storage().reference(withPath: "chatImages")
        application.registerForRemoteNotifications()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        self.badgeCount += 1
        NotificationCenter.default.post(name: .badge, object: nil, userInfo: ["badge": self.badgeCount])
        
        let userInfo = notification.request.content.userInfo
        print("User Info",userInfo)
        center.removeAllDeliveredNotifications()
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void){
        let userInfo = response.notification.request.content.userInfo
        if let tags = userInfo["tag"] as? String{
            redirectFromNotification(tag: tags, data: userInfo as! [String : Any])
            self.badgeCount -= 1
            NotificationCenter.default.post(name: .badge, object: nil, userInfo: ["badge": self.badgeCount])
        }
    }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print("Remotemessage",remoteMessage.appData)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("deviceTokenString",deviceTokenString)
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            }
            else if let result = result {
                print("Remote instance ID token: \(result.token)")
                UserDefaults.standard.setValue("\(result.token)", forKey: "token")
                UserDefaults.standard.synchronize()
                self.deviceToken = result.token
            }
        }
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)  {
        print("Firebase registration token: \(fcmToken)")
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        self.deviceToken = fcmToken
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any])  {
        print("my push is: %@", userInfo)
        guard application.applicationState == UIApplication.State.inactive else { return }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("my push is: %@", userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func redirectFromNotification(tag: String, data: [String:Any]){
        if tag == "5" {
            //let objTab = MovieDetailVC(nibName: "MovieDetailVC", bundle: .main)
            let objTab = NewMoviewDetailVC(nibName: "NewMoviewDetailVC", bundle: .main)
            objTab.movieID = data["movie_id"] as? String ?? ""
            let castContainerVC =
                    GCKCastContext.sharedInstance().createCastContainerController(for: objTab)
            castContainerVC.miniMediaControlsItemEnabled = true
            navController?.pushViewController(objTab, animated: true)
        }else if tag == "2" {
        }else if tag == "10"{
            print(data)
            appDel.eventLiveId = data["event_id"] as? String ?? ""
            /*
             if self.loginUser_Id != "" {
                 self.redirectToMovieDetail(movieID: movieID ?? "")
             }else{
                 self.displayScreen()
             }
             
             ["google.c.a.e": 1, "tag": 10, "google.c.sender.id": 74224071724, "gcm.message_id": 1589472416368605, "aps": {
                 alert =     {
                     body = "Your Go Live Request has been approved";
                     title = TopHub;
                 };
                 sound = default;
             }, "event_id": 5ebd5f4313312e2a2e6fb792]
             */
        }
    }
    
    func redirectToMovieDetail(movieID: String,movie_banner:String = "",PartyCode:String="") {
        
        let objTab = NewMoviewDetailVC(nibName: "NewMoviewDetailVC", bundle: .main)
        objTab.movieID = movieID
        objTab.partyCode = PartyCode
        let castContainerVC =
                GCKCastContext.sharedInstance().createCastContainerController(for: objTab)
        castContainerVC.miniMediaControlsItemEnabled = true
        objTab.movie_banner = movie_banner
      
        findtopViewController()?.navigationController?.pushViewController(castContainerVC, animated: true)
        
    }
}
extension AppDelegate:MZDownloadManagerDelegate{
    
        func updateDownloadingScreen(_ downloadModel: MZDownloadModel, index: Int){
            let vcc = findtopViewController()
            
            if vcc?.navigationController != nil{
                
                let viewcontrollers = vcc!.children
                if viewcontrollers.count > 0{
                    for vc in viewcontrollers{
                        if vc is NewMoviewDetailVC{
                            if let viewcontroller = vc as? NewMoviewDetailVC{
                                if viewcontroller.movieDetail != nil
                                {
                                    if viewcontroller.movieDetail.final_video == downloadModel.fileURL{
                                        viewcontroller.displayDownloadedData(downloadModel: downloadModel)
                                    }
                                }
                            }
                            print("find detail viewcontroller")
                        }else if vc is DownloadedVC{
                            if let viewcontroller = vc as? DownloadedVC{
                                viewcontroller.refreshCell(downloadModel: downloadModel)
                            }
                            print("find detail download viewcontroller")
                        }
                    }
                }else{
                    if vcc is DownloadedVC{
                        if let viewcontroller = vcc as? DownloadedVC{
                            viewcontroller.refreshCell(downloadModel: downloadModel)
                        }
                        print("find detail download viewcontroller")
                    }else if  let viewcontroller = vcc as? NewMoviewDetailVC{
                        if viewcontroller.movieDetail != nil
                        {
                            if viewcontroller.movieDetail.final_video == downloadModel.fileURL{
                                viewcontroller.displayDownloadedData(downloadModel: downloadModel)
                            }
                        }
                    }
                }
                
            }
        }
    //MARK:- DELEGATE METHODS
        func downloadRequestStarted(_ downloadModel: MZDownloadModel, index: Int) {
        }
        
        func downloadRequestDidPopulatedInterruptedTasks(_ downloadModels: [MZDownloadModel]) {
            
        }
        
        func downloadRequestDidUpdateProgress(_ downloadModel: MZDownloadModel, index: Int) {
           // self.refreshCellForIndex(downloadModel, index: index)
            self.updateDownloadingScreen(downloadModel, index: index)
            print("GET INDEX:::",index)
            print("GET URL:::",downloadModel.destinationPath)
            self.updateDownloadingScreen(downloadModel, index: index)

        }
        
        func downloadRequestDidPaused(_ downloadModel: MZDownloadModel, index: Int) {
           // self.refreshCellForIndex(downloadModel, index: index)
        }
        
        func downloadRequestDidResumed(_ downloadModel: MZDownloadModel, index: Int) {
           // self.refreshCellForIndex(downloadModel, index: index)
        }
        
        func downloadRequestCanceled(_ downloadModel: MZDownloadModel, index: Int) {
            
        }
        
        func downloadRequestFinished(_ downloadModel: MZDownloadModel, index: Int) {
            
          //  self.safelyDismissAlertController()
//            download_completed
            NotificationCenter.default.post(name: Notification.Name("download_completed"), object: nil)

//            downloadManager.presentNotificationForDownload("Ok", notifBody: "Download did completed")
            
            let indexPath = IndexPath.init(row: index, section: 0)
         //   tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.left)

            let docDirectoryPath : NSString = (MZUtility.baseFilePath as NSString).appendingPathComponent(downloadModel.fileName) as NSString
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: MZUtility.DownloadCompletedNotif as String), object: docDirectoryPath)
            self.updateDownloadingScreen(downloadModel, index: index)
        }
        
        func downloadRequestDidFailedWithError(_ error: NSError, downloadModel: MZDownloadModel, index: Int) {
            
           // self.ShowDownloadDetails()
            self.updateDownloadingScreen(downloadModel, index: index)

            debugPrint("Error while downloading file: \(String(describing: downloadModel.fileName))  Error: \(String(describing: error))")
        }
        
        //Oppotunity to handle destination does not exists error
        //This delegate will be called on the session queue so handle it appropriately
        func downloadRequestDestinationDoestNotExists(_ downloadModel: MZDownloadModel, index: Int, location: URL) {
            let myDownloadPath = MZUtility.baseFilePath + "/Default folder"
            if !FileManager.default.fileExists(atPath: myDownloadPath) {
                try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
            }
            let fileName = MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(downloadModel.fileName as String) as NSString)
            let path =  myDownloadPath + "/" + (fileName as String)
            try! FileManager.default.moveItem(at: location, to: URL(fileURLWithPath: path))
            debugPrint("Default folder path: \(myDownloadPath)")
        }
    
    
    //MARK:- Zoom app
    
    func sdkInit(_ navVC: UINavigationController?) {
        let context = MobileRTCSDKInitContext()
        context.domain = "zoom.us"
        context.enableLog = true
             

     //   context.appGroupId = "group.zoom.us.MobileRTCSampleExtensionReplayKit"
        let initializeSuc = MobileRTC.shared().initialize(context)
        print("initializeSuccessful======>\(NSNumber(value: initializeSuc))")

        print("MobileRTC Version: \(MobileRTC.shared().mobileRTCVersion())")
//        MobileRTC.shared().mobileRTCRootController = navVC
        MobileRTC.shared().setMobileRTCRootController(self.navController)
    }
    
    
}
