//
//  Movies.swift
//  CineStool
//
//  Created by MacBook Air 002 on 15/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import Foundation
import ObjectMapper

class Movies: Mappable {
    var _id: String = ""
    var category: String = ""
    var categoryNames: String = ""
    var converted_video: String = ""
    var created_at: String = ""
    var created_date: String = ""
    var date_of_upload: String = ""
    var director_name: String = ""
    var director_number: String = ""
    //var document_info: String = ""
    var final_video: String = ""
    var identity: String = ""
    var is_advertisement_show: String = ""
    var is_approved: String = ""
    var is_paid_movie: String = ""
    var is_payment_done: String = ""
    var is_movie_payment_done:String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var movie_banner: String = ""
    var movie_id: String = ""
    var movie_image: String = ""
    var movie_view_charge: String = ""
    var movie_price: String = ""
    var isPayPerViewMoviePrice:String = ""
    var payPerViewPrice:String = ""
    var name: String = ""
    var original_video: String = ""
    var payment_info: String = ""
    var producer_name: String = ""
    var producer_number: String = ""
    var production_name: String = ""
    var sell_movie: String = ""
    var total_watching: String = ""
    var uniqid: String = ""
    var updated_at: String = ""
    var user_id: String = ""
    var user_type: String = ""
    var year: String = ""
    var poster_image: String = ""
    //var rating: String = ""
    //var status: String = ""
    var description: String = ""
    //var overview: String = ""
    var my_rating: String = ""
    //var remaining_hours: String = ""
    //var advertisement_price: String = ""
    var currency_code: String = ""
    var currency_symbol: String = ""
    var movie_link: String = ""
    var cast_data: [Cast] = []
    var total_rating: String = ""
    var is_watched: String = ""
  //  var is_movie_payment_done: String = ""
    var is_advertisement_payment_done: String = ""
    var movie_actual_price: String = ""
    var currency_actual_symbol: String = ""
    var movie_trailer: String = ""
    var is_subscribe: String = ""
    var movie_producer_name: String = ""
    var producer_id: String = ""
    var is_plan_subscribed: String = ""
    
    //
    var date: String = ""
    var is_banner_type: String = ""
    var is_done: String = ""
    var is_mpd_done: String = ""
    var is_notification_sent: String = ""
    var is_premier_movie: String = ""
    var is_valid_for_automatic_approve: String = ""
    var mpd_video: String = ""
    var premier_movie_view_charge: String = ""
    var related_movies: [related_movies] = []
    var remain_time: String = ""
    var sell_amount: String = ""
    var user_name: String = ""
    var episode_array: [related_movies] = []
    var is_first_episode: String = ""
    var is_series: String = ""
    var next_episode_movie_banner: String = ""
    var next_episode_movie_id: String = ""
    var next_episode_movie_image: String = ""
    var next_episode_movie_url: String = ""
    var next_episode_name: String = ""
    var next_episode_poster_image: String = ""
    var series_id: String = ""
    var isAvailabelToDownload:String = ""
    var isPayPerView:String = ""
    var movie_party_list:[movie_party_list] = []
    var is_host_subscribed: String = ""
    var groupId:String = ""
    var host_user_id:String = ""
    var event_name:String = ""
    var channel_name:String = ""
    var event_participate_name:String = ""
    var pusher_event_id:String = ""
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id <- map["_id"]
        category    <- map["category"]
        categoryNames   <- map["categoryNames"]
        converted_video <- map["converted_video"]
        created_at  <- map["created_at"]
        created_date    <- map["created_date"]
        date_of_upload  <- map["date_of_upload"]
        director_name   <- map["director_name"]
        director_number <- map["director_number"]
        //document_info   <- map["document_info"]
        final_video <- map["final_video"]
        payPerViewPrice <- map["payPerViewPrice"]
        isPayPerViewMoviePrice <- map["isPayPerViewMoviePrice"]
        identity    <- map["identity"]
        is_advertisement_show <- map["is_advertisement_show"]
        is_approved <- map["is_approved"]
        is_paid_movie   <- map["is_paid_movie"]
        is_payment_done <- map["is_payment_done"]
        is_movie_payment_done <- map["is_movie_payment_done"]
        modified_date   <- map["modified_date"]
        month   <- map["month"]
        month_name  <- map["month_name"]
        movie_banner    <- map["movie_banner"]
        movie_id    <- map["movie_id"]
        movie_image <- map["movie_image"]
        movie_view_charge   <- map["movie_view_charge"]
        name    <- map["name"]
        original_video  <- map["original_video"]
        payment_info    <- map["payment_info"]
        producer_name   <- map["producer_name"]
        producer_number <- map["producer_number"]
        production_name <- map["production_name"]
        sell_movie  <- map["sell_movie"]
        total_watching  <- map["total_watching"]
        uniqid  <- map["uniqid"]
        updated_at  <- map["updated_at"]
        user_id <- map["user_id"]
        user_type   <- map["user_type"]
        year    <- map["year"]
        poster_image   <- map["poster_image"]
        //rating <- map["rating"]
        //status <- map["status"]
        description    <- map["description"]
        //overview   <- map["overview"]
        my_rating  <- map["my_rating"]
        //remaining_hours    <- map["remaining_hours"]
        //advertisement_price    <- map["advertisement_price"]
        currency_code    <- map["currency_code"]
        currency_symbol    <- map["currency_symbol"]
        movie_link    <- map["movie_link"]
        movie_price <- map["movie_price"]
        cast_data <- map["cast_data"]
        is_watched <- map["is_watched"]
        total_rating <- map["total_rating"]
        is_movie_payment_done <- map["is_movie_payment_done"]
        is_advertisement_payment_done <- map["is_advertisement_payment_done"]
        movie_actual_price <- map["movie_actual_price"]
        movie_trailer <- map["movie_trailer"]
        currency_actual_symbol <- map["currency_actual_symbol"]
        is_subscribe <- map["is_subscribe"]
        movie_producer_name <- map["movie_producer_name"]
        producer_id <- map["producer_id"]
        is_plan_subscribed <- map["is_plan_subscribed"]
        
        //
        date <- map["date"]
        is_banner_type <- map["is_banner_type"]
        is_done <- map["is_done"]
        is_mpd_done <- map["is_mpd_done"]
        is_notification_sent <- map["is_notification_sent"]
        is_premier_movie <- map["is_premier_movie"]
        is_valid_for_automatic_approve <- map["is_valid_for_automatic_approve"]
        mpd_video <- map["mpd_video"]
        premier_movie_view_charge <- map["premier_movie_view_charge"]
        related_movies <- map["related_movies"]
        remain_time <- map["remain_time"]
        sell_amount <- map["sell_amount"]
        user_name <- map["user_name"]
        episode_array <- map["episode_array"]
        is_first_episode <- map["is_first_episode"]
        is_series <- map["is_series"]
        next_episode_movie_banner <- map["next_episode_movie_banner"]
        next_episode_movie_id <- map["next_episode_movie_id"]
        next_episode_movie_image <- map["next_episode_movie_image"]
        next_episode_movie_url <- map["next_episode_movie_url"]
        next_episode_name <- map["next_episode_name"]
        next_episode_poster_image <- map["next_episode_poster_image"]
        series_id <- map["series_id"]
        isAvailabelToDownload  <- map["isAvailabelToDownload"]
        isPayPerView <- map["isPayPerView"]
        movie_party_list <- map["movie_party_list"]
        is_host_subscribed <- map["is_host_subscribed"]
        groupId <- map["groupId"]
        host_user_id <- map["host_user_id"]
        event_name <- map["event_name"]
        channel_name <- map["channel_name"]
        event_participate_name <- map["event_participate_name"]
        pusher_event_id <- map["pusher_event_id"]
    }
}


class Cast: Mappable {
    
    var _id: String = ""
    var cast_description: String = ""
    var cast_images: String = ""
    var cast_name: String = ""
    var cast_type: String = ""
    var created_at: String = ""
    var created_date: String = ""
    var date: String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var movie_id: String = ""
    var uniqid: String = ""
    var updated_at: String = ""
    var year: String = ""
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id <- map["_id"]
        cast_description <- map["cast_description"]
        cast_images <- map["cast_images"]
        cast_name <- map["cast_name"]
        cast_type <- map["cast_type"]
        created_at <- map["created_at"]
        created_date <- map["created_date"]
        date <- map["date"]
        modified_date <- map["modified_date"]
        month <- map["month"]
        month_name <- map["month_name"]
        movie_id <- map["movie_id"]
        uniqid <- map["uniqid"]
        updated_at <- map["updated_at"]
        year <- map["year"]
    }
}
//
class related_movies: Mappable {
    var _id:String = ""
    var movie_banner:String = ""
    var movie_id:String = ""
    var movie_image:String = ""
    var name:String = ""
    var poster_image:String = ""
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id <- map["_id"]
        movie_banner    <- map["movie_banner"]
        movie_id   <- map["movie_id"]
        movie_image <- map["movie_image"]
        name  <- map["name"]
        poster_image    <- map["poster_image"]
    }
}

class movie_party_list:Mappable {
    var user_id:String = ""
    var user_name:String = ""
    var user_profile:String = ""
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        user_id <- map["user_id"]
        user_name    <- map["user_name"]
        user_profile   <- map["user_profile"]
       
    }
}
