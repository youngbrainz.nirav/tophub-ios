//
//  LiveEventModel.swift
//  CineStool
//
//  Created by Dharam YB on 21/05/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import Foundation
import ObjectMapper

class LiveEventModel: Mappable {
    var event_data: [EventData] = []
    var user_data: UserData?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        event_data          <- map["event_data"]
        user_data           <- map["user_data"]
    }
}

class UserData: Mappable {
    var _id: String = ""
    var created_at: String = ""
    var customer_id: String = ""
    var event_date: String = ""
    var event_date_only: String = ""
    var event_detail: String = ""
    var event_name: String = ""
    var is_approved: String = ""
    var is_end_event: String = ""
    var is_start_date: String = ""
    var is_start_event: String = ""
    var mobile_no: String = ""
    var profile_image: String = ""
    var profile_pic: String = ""
    var updated_at: String = ""
    var user_name: String = ""
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id                 <- map["_id"]
        created_at          <- map["created_at"]
        customer_id         <- map["customer_id"]
        event_date          <- map["event_date"]
        event_date_only     <- map["event_date_only"]
        event_detail        <- map["event_detail"]
        event_name          <- map["event_name"]
        is_approved         <- map["is_approved"]
        is_end_event        <- map["is_end_event"]
        is_start_date       <- map["is_start_date"]
        is_start_event      <- map["is_start_event"]
        mobile_no           <- map["mobile_no"]
        profile_image       <- map["profile_image"]
        profile_pic         <- map["profile_pic"]
        updated_at          <- map["updated_at"]
        user_name           <- map["user_name"]
    }
}


class EventData: Mappable {
    var _id: String = ""
    var comments_data: [CommentsData] = []
    var created_at: String = ""
    var customer_id: String = ""
    var event_date: String = ""
    var event_date_only: String = ""
    var event_detail: String = ""
    var event_name: String = ""
    var is_approved: String = ""
    var is_end_event: String = ""
    var is_payment_done: String = ""
    var is_start_date: String = ""
    var is_start_event: String = ""
    var mobile_no: String = ""
    var modified_date: String = ""
    var profile_image: String = ""
    var profile_pic: String = ""
    var updated_at: String = ""
    var user_name: String = ""
        
    
    init?() {
    
    }
    
    required init?(map: Map) {
    
    }
    
    // Mappable
    func mapping(map: Map) {
        _id                     <- map["_id"]
        comments_data           <- map["comments_data"]
        created_at              <- map["created_at"]
        customer_id             <- map["customer_id"]
        event_date              <- map["event_date"]
        event_date_only         <- map["event_date_only"]
        event_detail            <- map["event_detail"]
        event_name              <- map["event_name"]
        is_approved             <- map["is_approved"]
        is_end_event            <- map["is_end_event"]
        is_payment_done         <- map["is_payment_done"]
        is_start_date           <- map["is_start_date"]
        is_start_event          <- map["is_start_event"]
        mobile_no               <- map["mobile_no"]
        modified_date           <- map["modified_date"]
        profile_image           <- map["profile_image"]
        profile_pic             <- map["profile_pic"]
        updated_at              <- map["updated_at"]
        user_name               <- map["user_name"]
    }
}


class CommentsData: Mappable {
    var _id: String = ""
    var comment: [EventData] = []
    var created_at: String = ""
    var created_date: String = ""
    var customer_id: String = ""
    var date: String = ""
    var event_id: String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var updated_at: String = ""
    var user_name: String = ""
    var year: String = ""
    
    init?() {
    
    }
    
    required init?(map: Map) {
    
    }
    
    // Mappable
    func mapping(map: Map) {
        _id                    <- map["_id"]
        comment                <- map["comment"]
        created_at             <- map["created_at"]
        created_date           <- map["created_date"]
        customer_id            <- map["customer_id"]
        date                   <- map["date"]
        event_id               <- map["event_id"]
        modified_date          <- map["modified_date"]
        month                  <- map["month"]
        month_name             <- map["month_name"]
        updated_at             <- map["updated_at"]
        user_name              <- map["user_name"]
        year                    <- map["year"]
    }
}
