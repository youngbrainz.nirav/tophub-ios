//
//  MessageModel.swift
//  CineStool
//
//  Created by Dharam YB on 01/06/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import Foundation
import ObjectMapper

class MessageModel: Mappable {
    var _id: String = ""
    var comment: String = ""
    var created_at: String = ""
    var created_date: String = ""
    var customer_id: String = ""
    var date: String = ""
    var event_id: String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var updated_at: String = ""
    var user_name: String = ""
    var year: String = ""
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id             <- map["_id"]
        comment         <- map["comment"]
        created_at      <- map["created_at"]
        created_date    <- map["created_date"]
        customer_id     <- map["customer_id"]
        date            <- map["date"]
        event_id        <- map["event_id"]
        modified_date   <- map["modified_date"]
        month           <- map["month"]
        month_name      <- map["month_name"]
        updated_at      <- map["updated_at"]
        user_name       <- map["user_name"]
        year            <- map["year"]
    }
}
