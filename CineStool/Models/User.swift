//
//  User.swift
//  SMA
//
//  Created by MacBook Air 002 on 27/06/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import ObjectMapper

class User: Mappable {
    var _id: String = ""
    var country_code: String = ""
    var country_code_info: String = ""
    var created_at: String = ""
    var currency_symbol: String = ""
    var currency_code: String = ""
    var email_id: String = ""
    var user_name: String = ""
    var mobile_no: String = ""
    var new_user: String = ""
    var phone_number: String = ""
    var profile_image: String = ""
    var session_token: String = ""
    var user_id: String = ""
    var user_type: String = ""
    var notification_status: String = ""
    var created_date: String = ""
    var device_token: String = ""
    var device_type: String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var updated_at: String = ""
    var year: String = ""
    var date: String = ""
    var device_info: String = ""
    var is_blocked: String = ""
    var is_password_added: String = ""
    var is_same_device: String = ""
    var login_token: String = ""
    var name: String = ""
    var password: String = ""
    var mobile_uid: String = ""
    
    //
    var is_plan_subscribed: String = ""
    var is_live: String = ""
    
    init?() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id                      <- map["_id"]
        country_code             <- map["country_code"]
        country_code_info        <- map["country_code_info"]
        created_at               <- map["created_at"]
        currency_symbol          <- map["currency_symbol"]
        currency_code            <- map["currency_code"]
        email_id                 <- map["email_id"]
        mobile_no                <- map["mobile_no"]
        new_user                 <- map["new_user"]
        phone_number             <- map["phone_number"]
        profile_image            <- map["profile_image"]
        session_token            <- map["session_token"]
        user_id                  <- map["user_id"]
        user_name                <- map["user_name"]
        user_type                <- map["user_type"]
        notification_status      <- map["notification_status"]
        created_date             <- map["created_date"]
        device_token             <- map["device_token"]
        device_type              <- map["device_type"]
        modified_date            <- map["modified_date"]
        month                    <- map["month"]
        month_name               <- map["month_name"]
        updated_at               <- map["updated_at"]
        year                     <- map["year"]
        date                     <- map["date"]
        device_info              <- map["device_info"]
        is_blocked               <- map["is_blocked"]
        is_password_added        <- map["is_password_added"]
        is_same_device           <- map["is_same_device"]
        login_token              <- map["login_token"]
        name                     <- map["name"]
        password                 <- map["password"]
        mobile_uid               <- map["mobile_uid"]
        is_plan_subscribed       <- map["is_plan_subscribed"]
        is_live                  <- map["is_live"]
    }
}


