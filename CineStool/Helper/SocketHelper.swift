//
//  SocketHelper.swift
//  Eber
//
//  Created by Jaydeep Vyas  on 13.03.19.
//  Copyright © 2019 Elluminati. All rights reserved.
//

import Foundation
import SocketIO
import Siren

class SocketHelper : NSObject {
    
    let manager = SocketManager(socketURL: URL(string:baseURL)!, config: [.log(true), .compress, .forceWebsockets(true)])
    var socket:SocketIOClient? = nil
    var blockCompletion: (([String:Any], _ error: Error?)->())!
    let locationEmit:String = "update_location"
    let tripDetailNotify:String = "trip_detail_notify"
    
    static let shared = SocketHelper()
    private override init() {}
    
    func connectSocket() {
        socket = manager.defaultSocket
        socket?.on(clientEvent: .connect) { (data, ack) in
            print("Socket Connection Connected")
        }
        socket?.on(clientEvent: .error) { (data, ack) in
            print("Socket Connection Error")
            self.disConnectSocket()
        }
        
        socket?.on(clientEvent: .pong) { (data, ack) in
            print("Socket Connection Pong \(data) Ack \(ack)")
        }
        socket?.connect()
    }
    
    func disConnectSocket() {
        if self.socket?.status == .connected {
            self.socket?.disconnect()
        }
    }
}
